# electricity_management

#### 介绍
电商管理系统

#### 软件架构
软件架构说明


#### 初始化
1. 下载图片的路径 项目名/download?fileName=
2. 上传图片的路径 D:/upload/images
3. 项目名称和数据库名称 electricity_management
4. 代码多注释，命名规范一点
5. 前端servlet路径固定/front/ 前缀
6. 后台servlet路径固定/back/ 前缀
7. 上传代码要备注好姓名

# 需求分析
### 普通用户
#### 1.个人信息（苏帆）
信息修改：根据用户id进行密码和头像修改；
收货地址管理：根据用户ID增加用户地址，地址增加时选择地址，地址多级联动（省市区），最多五条地址；
收藏夹：根据用户收藏的店铺、商品，获取对应店铺或商品的id，在收藏夹中显示店铺和商品名。
#### 2.商品显示（周文涛）
主页显示：显示全部一级分类选择和全部销量排序的的商品（降序）并分页
一级分类显示：显示全部二级分类和一级下全部商品按照销量排序（降序）并分页
二级分类显示：显示二级分类下全部商品按照销量排序并分页
店铺商品显示：显示店铺全部商品，并可收藏店铺。
商品详情显示：显示商品的图片，名称，价格，介绍，店铺，是否收藏，和添加进购物车。
搜索：按照商品名称或者类别模糊搜索商品，并显示在主页。
评论显示：在商品的详细页面显示用户购买商品后的评论，并可以在评论中评论形成二级评论。
#### 3.登录模块（赵坤）
登录：根据账号和密码登录账户，增加“记住密码”复选框，若用户点击“记住密码”，则下次登陆时页面自动填充密码。
注册：验证用户名是否存在；密码采用md5进行加密；接入短信api；设置密码验证规则；用户名限定为邮箱或者手机号。
#### 4.购物车功能(陈波)
点击购物车（判断有没有登录，有登录就直接跳转到购物车界面，没有就请	求登录，）
购物车列表（购物车编号，用户编号，商品编号，数量，价格）
购物车修改功能（修改商品的数量，改变总价格）
购物车删除功能（根据购物车编号删除商品）
全选复选框（点击全选，选中购物车的所有商品，统计总价）
点击支付（跳转订单界面，）
#### 5.订单功能界面（陈波）
显示订单信息：（订单编号，订单总价格，下单时间，订单状态，用户编号，	付款信息，收货地址，收获人，收获电话）订单编号（时间戳+随机数）
点击提订单（跳转到支付界面）
订单详情功能界面：（支付成功后，跳转到订单详情界面）
显示自己的所有订单（订单详情编号，订单编号，商品编号，商品数量，价	格）
订单删除功能（根据订单详情编号删除订单）

### 商家用户
#### 1.商品管理模块（苏帆）
根据商品id进行增加、删除、修改、查询，用户可点击对应按钮进行类型	选择，也可以根据名称搜索，价格升降序排序；
商家进行商品上架时自动获取时间显示在商品详情中。
#### 2.个人店铺管理（周文涛）
信息查看：店铺名/密码/店铺logo/商家联系方式/商家地址
信息修改：修改店铺名，密码 ，logo，联系方式，地址
#### 3.登录模块（赵坤）
买家登录，根据账号和密码登录，增加“记住密码”功能。
新建店铺，包括新建店铺名，店铺Logo。
#### 4.评论管理：（周文涛）
对商品的评论进行附加评论，查询评论（根据时间，商品模糊查询）
#### 5.销售模块管理 (丁玲) 
销量表： 按月计算销量，x轴是商品名，y轴是月销量。
按日计算销量，x轴是商品名，y轴是日销量。
销售额表：按月计算销量，x轴是商品名，y轴是月销售额。
按日计算销量，x轴是商品名，y轴是销售额。
#### 6.订单管理（陈波）
店铺订单查询：订单编号模糊查询，排序（时间）

### 管理员用户
#### 1.登录（赵坤）
管理员登录，根据账号密码，增加“记住密码”功能。
#### 2.用户管理模块（赵坤）
删除、修改账户信息；全查询搜索用户；根据用户账号，电话查询模糊搜索用户；
接受来自卖家的店铺创建申请，点击“批准”按钮则成功创建店铺，点击“拒绝”按钮则发送“创建失败”信息给卖家；实时监控店铺状态，包括:正常运行，已禁封
#### 3.分类管理（周文涛）
分类的增加删除修改和根据名称模糊查询
#### 4.商品管理（周文涛）
查询：全部查询,根据名称分类模糊查询
下架处理：可以将商品修改为下架状态。
#### 5.店铺管理模块 (丁玲) 
删除功能：点击删除按钮，在页面删除该店铺信息 
查询功能： 根据店铺名称查询,根据联系方式查询
#### 6.订单管理（邓玲琪）
可以订单号，订单状态，日期，查询  有清除搜索选项，有订单详情按钮，可以弹出页面，查看具体信息

# 数据库设计
## 1.用户表：（买家信息表）user表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	user_id	用户编号	int	not null	Primary key
2.	user_account	用户账号	varchar	not null	
3.	user_name	用户名称	varchar	not null	
4.	user_password	用户密码	varchar	not null	
5.	user_picture	用户头像	varchar	null	
6.	user_phone	手机号	varhcar	not null	

## 2.用户表：（卖家信息表）seller表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	seller_id	卖家编号	int	not null	Primary key
2.	seller_account	卖家账号	varchar	not null	
3.	seller_password	卖家密码	varchar	not null	
4.	seller_address	商家地址	varchar	not null	
5.	seller_phone	商家联系方式	varchar	not null	
6.	seller_shop_name	店铺名称	varchar	not null	
7.	seller_logo	店铺logo	varchar	not null	
8.	Seller_status	店铺状态	int	not null	0等待验证/1 正常运行 2/ 已封禁

## 3.分类表：type表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	type_id	分类编号	int	not null	Primary key
2.	type_father_id	父类编号	int	not null	
3.	type_name	分类名称	varchar	not null	


## 4.商品信息表：product表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	product_id	商品编号	int	not null	Primary key
2.	product_name	商品名称	varchar	not null	
3.	seller_id	卖家编号	int	not null	
4.	product_inventory	库存	int	not null	
5.	product_price	价格	float	not null	
6.	product_picture	商品图片	varchar	not null	
7.	type_id	分类编号	int	not null	
8.	product_time	上架时间	datetime	not null	
9.	product_status	商品状态	int	not null	0 未上架/1 已上架
10.	product_introduce	商品介绍	text	not null	

## 5.评论表：comment表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	comment_id	评论编号	int	not null	Primary key
2.	user_id	用户编号	int	not null	
3.	product_id	商品编号	int	not null	
4.	comment_time	评论时间	time	not null	
5.	comment_content	评论内容	varchar	null	
6.	comment_father_id	父类评论编号	int	not null	
7.	comment_score	商品评分	int	not null	

## 6.订单表：order表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	order_id	订单编号	varchar	not null	Primary key
2.	order_total_price	订单总价格	float	not null	
3.	order_time	下单时间	datetime	not null	
4.	order_status	订单状态	varchar	not null	0 已付款 /1 已发货 / 2 已完成
5.	user_id	用户编号	int	not null	
6.	order_pay_method	付款方式	varchar	not null	
7.	order_receive_address	收货地址	varchar	not null	
8.	order_ receive_name	收货人	varchar	not null	
9.	order_receive_phone	收货电话	varchar	not null	

## 7.订单详情表：order_detail表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	order_detail_id	订单详情编号	int	not null	Primary key
2.	order_id	订单编号	varchar	not null	
3.	product_id	商品编号	int	not null	
4.	order_detail_count	商品数量	int	not null	
5.	order_detail_price	价格	float	not null		

## 8.销售表：sales表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	sales_id	销售编号	int	not null	Primary key
2.	product_id	商品编号	int	not null	
3.	sales_count	数量	int	not null	
4.	order_id	订单编号	varchar	not null	
5.	sales_price	销售价格	float	not null	
6.	sales_date	销售时间	datetime	Not null	

## 9.购物车表：cart表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	cart_id	购物车编号	int	not null	Primary key
2.	user_id	用户编号	int	not null	
3.	product_id	商品编号	int	not null	
4.	cart_count	数量	int	not null	
5.	cart_price	价格	float	not null	

## 10.商品收藏表：product_collect表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	product_collect_id	收藏编号	int	not null	Primary key
2.	user_id	用户编号	int	not null	
3.	product_id	商品编号	int	not null	

## 11.店铺收藏表：shops_collect表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	shops_collect_id	店铺收藏编号	int	not null	Primary key
2.	seller_id	卖家编号	int	not null	
3.	user_id	用户编号	int	not null	

## 12.收货地址表address表
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	address_id	店铺地址编号	int	not null	Primary key
2.	user_id	用户编号	int	not null	
3.	address_name	地址	varchar	not null	
4.	address_status	是否默认地址	int	not null	0 普通地址 / 1 默认地址
5.	receive_name	收货人	varchar	not null	
6.	receive_phone	收货电话	varchar	not null
	
## 13.管理员用户表admin
序号	字段名	中文说明	类型	可空	备注(主外键)
1.	admin_id	管理员编号	int	not null	Primary key
2.	admin_account	管理员账号	varchar	not null	
4.	admin_password	管理员密码	varchar	not null

# 计划安排表
## 周文涛：
6-15 完成有关dao和service 
6-16~6-17完成商品显示
6-18商品评论
6-19后台评论管理
6-20卖家信息 分类管理
6-21 商品管理
## 苏帆：
6-15 完成有关dao和service 
6-16~6-17完成用户信息修改
6-18收货地址
6-19收藏夹
6-20-21卖家商品增删改查
6-22-6-24 商品上架分类排序
## 赵坤： 
6-15~6-18完成买家登录及注册
6-19~6-22完成卖家添加店铺及登录
6-22-6-25完成管理员登录及用户管理模块
## 丁玲 ：
16 销量柱状图 17销售额柱状图  18店铺管理的删除 19店铺管理的查找 20验证
## 邓玲琪：
6-15~6-17完成有关service 
6-18-6-20后台订单管理页
6-21-6-23 后台订单详情页
## 陈波：
6-15： 完成有关dao和service 
6-16~6-17：完成购物车信息显示、修改和删除
6-18~6-20：支付功能
6-21~6-22：前台用户订单信息表，显示，查询订单状态
6-22~6-24：前台用户订单详情信息表、显示、删除

