<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>

<html>

<head>

    <title>电商管理系统后台</title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

    

    <!--[if lt IE 9]>

    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>

    <![endif]-->

    <link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />

    <link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / jquery ui -->

    <link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />

    <link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / switch buttons -->

    <link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / xeditable -->

    <link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />

    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / wysihtml5 (wysywig) -->

    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / jquery file upload -->

    <link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / full calendar -->

    <link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / select2 -->

    <link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / mention -->

    <link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / tabdrop (responsive tabs) -->

    <link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / jgrowl notifications -->

    <link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / datatables -->

    <link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / dynatrees (file trees) -->

    <link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / color picker -->

    <link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / datetime picker -->

    <link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / daterange picker) -->

    <link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / flags (country flags) -->

    <link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / slider nav (address book) -->

    <link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / fuelux (wizard) -->

    <link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />

    <!-- / flatty theme -->

    <link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />

    <!-- / demo -->

    <link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class='contrast-red '>

<%@include file="sellerPublicPage.jsp" %>
<div id='wrapper'>

<div id='main-nav-bg'></div>

<nav class='' id='main-nav'>

<div class='navigation'>

<div class='search'>
   <form accept-charset="UTF-8" action="search_results.html" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
       <div class='search-wrapper'>
           <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
           <button class="btn btn-link icon-search" name="button" type="submit"></button>
       </div>
   </form>

</div>

<ul class='nav nav-stacked'>

<li class=''>
   <a href='sellerIndex.jsp'>
       <i class='icon-home'></i>
       <span>首页</span>
   </a>

</li>
<li class=''>
    <a href='sellerSales.jsp'> <i class='icon-dashboard'></i>
        <span>销售管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerManageProduct.jsp'> <i class='icon-magic'></i>
        <span>商品管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerOrderManagement.jsp'><i class='icon-inbox'></i>
        <span>订单管理</span>
    </a>
</li>
<li class='active'>
    <a href='sellerComment.jsp'> <i class='icon-comments'></i>
        <span>评论管理</span>
    </a>
</li>
<li>
    <a class="dropdown-collapse" href="#">
        <i class="icon-cog"></i>
        <span>设置</span>
        <i class="icon-angle-down angle-down"></i>
    </a>
    <ul class="nav nav-stacked">
        <li>
            <a  href="${pageContext.request.contextPath }/back/SellerServlet?op=logout">
                <i class="icon-caret-right"></i>
                <span>退出账号</span>
            </a>
         </li>
    </ul>
</li>
</ul>

</div>


</nav>

<section id='content'>

<div class='container-fluid'>

<div class='row-fluid' id='content-wrapper'>

<div class='span12'>
<div class="row-fluid">

    <div class="span12">

        <div class="page-header">

            <h1 class="pull-left">

                <i class="icon-comments"></i>

                <span>评论</span>

            </h1>


        </div>

    </div>

</div>

<div class='row-fluid'>
<div class='span6'>

<div class='row-fluid recent-activity'>

<div class='span12 box box-nomargin'>

<div class='box-header'>

    <div class='title'>
        <span class="commitTitle">全部评论</span>
    </div>

    <div class='actions'>

        <a href="#" class="btn box-remove btn-mini btn-link"><i class='icon-remove'></i>

        </a>

        <a href="#" class="btn box-collapse btn-mini btn-link"><i></i>

        </a>

    </div>

</div>

<div class='box-content box-no-padding'>

<ul class='nav nav-tabs nav-tabs-simple'>

    <li  class='active'>

        <a href="#comments" class="purple-border" data-toggle="tab" ><i class='icon-comments text-purple'></i>
        	<span class="commitTitle">全部评论</span>
        </a>

    </li>

    <li class='dropdown'>
        <input type="text" data-toggle="dropdown" style="padding:9px" autocomplete="off" id="keyword" placeholder="商品选择">

        <ul class='dropdown-menu' id="showProduct">
        <li><a href="#" class="showProduct" data-productid="all">全部</a></li>
        </ul>

    </li>

</ul>

<div class='tab-content'>


<div class='tab-pane fade in active' id='comments'>

    <ul class='unstyled comments list-hover list-striped' id= "showOneComment">
    </ul>
    <div class='load-more'>
        <span class="btn btn-block" data-loading-text="&lt;i class=&#x27;icon-spinner icon-spin&#x27;&gt;&lt;/i&gt; Loading more..." ><i class='icon-circle-arrow-down'></i>
            	更多评论
        </span>
    </div>

</div>

</div>

</div>

</div>

</div>

</div>
<div class='span6'>

    <div class='chat row-fluid'>
    
    <div class='box box-nomargin span12'>
    
    <div class='box-header orange-background'>
    
        <div class='title'>
    
            <i class='icon-comments-alt'></i>
    
            	<span id = 'showUserName'></span>
    
        </div>
    
        <div class='actions'>
    
            <a href="#" class="btn box-remove btn-mini btn-link"><i class='icon-remove'></i>
    
            </a>
    
            <a href="#" class="btn box-collapse btn-mini btn-link"><i></i>
    
            </a>
    
        </div>
    
    </div>
    
    <div class='box-content box-no-padding'>
    
    <div class='scrollable' data-scrollable-height='422' data-scrollable-start='bottom'>
    
    <ul class='unstyled list-hover list-striped' id = "showTwoComment">
    </ul>
    
    </div>
    
    <form accept-charset="UTF-8" action="#" class="new-message" method="post" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="CFC7d00LWKQsSahRqsfD+e/mHLqbaVIXBvlBGe/KP+I=" /></div>
    
        <input class='span12' id='commentMessage' name='commentContent' placeholder='回复的消息' type='text' />
    
        <button class='btn btn-success'  id = "submitComment">
    
            <i class='icon-mail-reply'></i>
    
        </button>
    
    </form>
    
    </div>
    
    </div>
    
    </div>
    
</div>
</div>
</div>

</div>

</div>

</section>

</div>

<!-- / jquery -->

<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>

<!-- / jquery mobile events (for touch and slide) -->

<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>

<!-- / jquery migrate (for compatibility with new jquery) -->

<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>

<!-- / jquery ui -->

<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>

<!-- / bootstrap -->

<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>

<!-- / sparklines -->

<script src='assets/javascripts/plugins/sparklines/jquery.sparkline.min.js' type='text/javascript'></script>

<!-- / flot charts -->

<script src='assets/javascripts/plugins/flot/flot.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/flot/flot.resize.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/flot/flot.pie.js' type='text/javascript'></script>

<!-- / bootstrap switch -->

<script src='assets/javascripts/plugins/bootstrap_switch/bootstrapSwitch.min.js' type='text/javascript'></script>

<!-- / fullcalendar -->

<script src='assets/javascripts/plugins/fullcalendar/fullcalendar.min.js' type='text/javascript'></script>

<!-- / datatables -->

<script src='assets/javascripts/plugins/datatables/jquery.dataTables.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/datatables/jquery.dataTables.columnFilter.js' type='text/javascript'></script>

<!-- / wysihtml5 -->

<script src='assets/javascripts/plugins/common/wysihtml5.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/common/bootstrap-wysihtml5.js' type='text/javascript'></script>

<!-- / select2 -->

<script src='assets/javascripts/plugins/select2/select2.js' type='text/javascript'></script>

<!-- / color picker -->

<script src='assets/javascripts/plugins/bootstrap_colorpicker/bootstrap-colorpicker.min.js' type='text/javascript'></script>

<!-- / mention -->

<script src='assets/javascripts/plugins/mention/mention.min.js' type='text/javascript'></script>

<!-- / input mask -->

<script src='assets/javascripts/plugins/input_mask/bootstrap-inputmask.min.js' type='text/javascript'></script>

<!-- / fileinput -->

<script src='assets/javascripts/plugins/fileinput/bootstrap-fileinput.js' type='text/javascript'></script>

<!-- / modernizr -->

<script src='assets/javascripts/plugins/modernizr/modernizr.min.js' type='text/javascript'></script>

<!-- / retina -->

<script src='assets/javascripts/plugins/retina/retina.js' type='text/javascript'></script>

<!-- / fileupload -->

<script src='assets/javascripts/plugins/fileupload/tmpl.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/load-image.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/canvas-to-blob.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/jquery.iframe-transport.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/jquery.fileupload.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-fp.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-ui.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-init.js' type='text/javascript'></script>

<!-- / timeago -->

<script src='assets/javascripts/plugins/timeago/jquery.timeago.js' type='text/javascript'></script>

<!-- / slimscroll -->

<script src='assets/javascripts/plugins/slimscroll/jquery.slimscroll.min.js' type='text/javascript'></script>

<!-- / autosize (for textareas) -->

<script src='assets/javascripts/plugins/autosize/jquery.autosize-min.js' type='text/javascript'></script>

<!-- / charCount -->

<script src='assets/javascripts/plugins/charCount/charCount.js' type='text/javascript'></script>

<!-- / validate -->

<script src='assets/javascripts/plugins/validate/jquery.validate.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/validate/additional-methods.js' type='text/javascript'></script>

<!-- / naked password -->

<script src='assets/javascripts/plugins/naked_password/naked_password-0.2.4.min.js' type='text/javascript'></script>

<!-- / nestable -->

<script src='assets/javascripts/plugins/nestable/jquery.nestable.js' type='text/javascript'></script>

<!-- / tabdrop -->

<script src='assets/javascripts/plugins/tabdrop/bootstrap-tabdrop.js' type='text/javascript'></script>

<!-- / jgrowl -->

<script src='assets/javascripts/plugins/jgrowl/jquery.jgrowl.min.js' type='text/javascript'></script>

<!-- / bootbox -->

<script src='assets/javascripts/plugins/bootbox/bootbox.min.js' type='text/javascript'></script>

<!-- / inplace editing -->

<script src='assets/javascripts/plugins/xeditable/bootstrap-editable.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/xeditable/wysihtml5.js' type='text/javascript'></script>

<!-- / ckeditor -->

<script src='assets/javascripts/plugins/ckeditor/ckeditor.js' type='text/javascript'></script>

<!-- / filetrees -->

<script src='assets/javascripts/plugins/dynatree/jquery.dynatree.min.js' type='text/javascript'></script>

<!-- / datetime picker -->

<script src='assets/javascripts/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js' type='text/javascript'></script>

<!-- / daterange picker -->

<script src='assets/javascripts/plugins/bootstrap_daterangepicker/moment.min.js' type='text/javascript'></script>

<script src='assets/javascripts/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.js' type='text/javascript'></script>

<!-- / max length -->

<script src='assets/javascripts/plugins/bootstrap_maxlength/bootstrap-maxlength.min.js' type='text/javascript'></script>

<!-- / dropdown hover -->

<script src='assets/javascripts/plugins/bootstrap_hover_dropdown/twitter-bootstrap-hover-dropdown.min.js' type='text/javascript'></script>

<!-- / slider nav (address book) -->

<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>

<!-- / fuelux -->

<script src='assets/javascripts/plugins/fuelux/wizard.js' type='text/javascript'></script>

<!-- / flatty theme -->

<script src='assets/javascripts/nav.js' type='text/javascript'></script>

<script src='assets/javascripts/tables.js' type='text/javascript'></script>

<script src='assets/javascripts/theme.js' type='text/javascript'></script>

<!-- / demo -->

<script src='assets/javascripts/demo/jquery.mockjax.js' type='text/javascript'></script>

<script src='assets/javascripts/demo/inplace_editing.js' type='text/javascript'></script>

<script src='assets/javascripts/demo/charts.js' type='text/javascript'></script>

<script src='assets/javascripts/demo/demo.js' type='text/javascript'></script>

<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>

</body>
<script type="text/javascript">
	$(document).ready(function(){
		let productIdFlag = null;
		
		// 局部刷新评论内容
		function showComment(data){
			// 遍历评论
			$.each(data,function(index,val){
				let html = "<li>"
				+"<div class='avatar pull-left'>"
				+"<div class='icon-user'></div>"
				+"</div>"
				+"<div class='body'>"
				+"<div class='name'><a href='#' class='text-contrast'>"+val.user.userName+"</a></div>"
				+"<div class='actions'>"
				+"<span href='#' class='btn btn-link  has-tooltip' title='回复'><i class='icon-comment replyUi' data-commentid = '"+val.commentId+"' data-username = '"+val.user.userName+"' data-productid='"+val.product.productId+"'></i>"
				+"</span>"
				+"</div>"
				+"<div class='text'>"+val.commentContent+"</div>"
				+"</div>"
				+"<div class='text-right'>"
				+"<small class='date muted'>"
				+"<span class='timeago  has-tooltip' data-placement='top' title='"+val.commentTime+"'>"+val.commentTime+"</span>"
				+"<i class='icon-time'></i>"
				+"</small>"
				+"</div>"
				+"</li>";
				$("#showOneComment").append(html);
			});
		}
		
		// 显示商品选项
		function showProduct(data){
			if(data.length == 0){
				$("#showProduct").append("<li>没有记录</li>");
			}
			$.each(data,function(index,val){
				let html = '<li><a href="#" class="showProduct" data-productid="'+val.productId+'">'+val.productName+'</a></li>';
				$("#showProduct").append(html);
			});
		}
		// 二级评论显示
		function showTwoComment(data){
			$("#showTwoComment").empty();
			$.each(data,function(index,val){
				let html = '<li class="message">'
							+'<div class="avatar">'
							+'<img alt="Avatar" height="23" src="${pageContext.request.contextPath}/download?fileName='+val.user.userPicture+'" width="23" />'
							+'</div>'
							+'<div class="name-and-time">'
							+'<div class="name pull-left">'
							+'<small>'
							+'<a href="#" class="text-contrast">'+val.user.userName+'</a>'
							+'</small>'
							+'</div>'
							+'<div class="time pull-right">'
							+'<small class="date pull-right muted">'
							+'<span class="timeago has-tooltip" data-placement="top" title="'+val.commentTime+'">'+val.commentTime+'</span>'
							+'<i class="icon-time"></i>'
							+'</small>'
							+'</div>'
							+'</div>'
							+'<div class="body">'
							+val.commentContent
							+'</div>'
							+'</li> ';
				$("#showTwoComment").append(html);
			});
			
			
		}
		
		let sellerId = "${sessionScope.seller.sellerId}";
		pageSize = 5;
		pageNum = 1;
		// 获取全部评论
		function getAllComment(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerComment",
				data:{
					op:"getAll",
					sellerId:sellerId,
					pageSize:pageSize,
					pageNum:pageNum
				},
				dataType:"json",
				success:function(result,status,xhr){
					showComment(result);
				},
				error:function(){
					alert("AJax请求失败");
				}
			});
		}
		// 获取单独商品评论
		function getOneProductComment(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerComment",
				data:{
					op:"getAll",
					productId:productIdFlag,
					pageSize:pageSize,
					pageNum:pageNum
				},
				dataType:"json",
				success:function(result,status,xhr){
					showComment(result);
				},
				error:function(){
					alert("AJax请求失败");
				}
			});
		}
		
		// 显示评论
		getAllComment();
		$(".load-more").click(function(){
			pageNum += 1;
			if(productIdFlag == null){
				getAllComment();
			}else{
				getOneProductComment();
			}
			
		});
		
		
		
		// 获取全部商品
		function getAllProduct(){
			let keyword = $("#keyword").val();
			$("#showProduct").empty();
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerComment",
				data:{
					op:"getProduct",
					sellerId:sellerId,
					keyword:keyword
				},
				dataType:"json",
				success:function(result,status,xhr){
					showProduct(result.data);
				},
				error:function(){
					alert("AJax请求失败");
				}
			});
		}
		getAllProduct();
		$("#keyword").keyup(function(){
			getAllProduct();
		});
		
		// 点击商品时
		$(document).on("click",".showProduct",function(){
			$("#showOneComment").empty();
			$(".commitTitle").text($(this).text()+"评论");
			let productId = $(this).data("productid");
			pageNum = 1;
			if(productId == "all"){
				productIdFlag = null;
				getAllComment();
				return;
			}else{
				productIdFlag = productId;
				getOneProductComment(productId);
				return;
			}
		});
		
		// 聊天框的对象(评论编号)
		let commentObjectId = null;
		// 商品对象
		let productObjectId = null;
		
		// 商家回复
		$(document).on("click",".replyUi",function(){
			let commentId = $(this).data("commentid");
			let userName = $(this).data("username");
			productObjectId = $(this).data("productid");
			
			// 保存评论编号
			commentObjectId = commentId;
			// 获取二级评论
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerComment",
				data:{
					op:"two",
					commentId:commentId
				},
				dataType:"json",
				success:function(result,status,xhr){
					$("#showUserName").text(userName);
					showTwoComment(result);
				},
				error:function(){
					alert("AJax请求失败");
				}
			});
		});
		
		// 保存评论
		$("#submitComment").click(function(){
			// 获取评论内容
			let commentContent = $("#commentMessage").val();
			console.log(productObjectId+"..."); 
			if(commentObjectId == null&&productObjectId==null){
				alert("请选择回复对象");
				return false;
			}
			// 保存
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerComment",
				data:{
					op:"save",
					commentId:commentObjectId,
					commentContent:commentContent,
					productId:productObjectId
				},
				dataType:"json",
				success:function(result,status,xhr){
					$("#showTwoComment").empty();
					$.ajax({
						url:"${pageContext.request.contextPath}/back/sellerComment",
						data:{
							op:"two",
							commentId:commentObjectId
						},
						dataType:"json",
						success:function(result,status,xhr){
							showTwoComment(result);
						},
						error:function(){
							alert("AJax请求失败");
						}
					});
				},
				error:function(){
					alert("AJax请求失败");
				}
			});
			return false;
		});
	});
</script>

</html>
