<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 管理员商品管理界面 wentao -->
<!DOCTYPE html>
<html>

<head>
<title>电商管理系统管理员后台</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport' />
<!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
<link href='assets/stylesheets/bootstrap/bootstrap.css' media='all'
	rel='stylesheet' type='text/css' />
<link href='assets/stylesheets/bootstrap/bootstrap-responsive.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / jquery ui -->
<link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css'
	media='all' rel='stylesheet' type='text/css' />
<link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / switch buttons -->
<link
	href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / xeditable -->
<link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css'
	media='all' rel='stylesheet' type='text/css' />
<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / wysihtml5 (wysywig) -->
<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / jquery file upload -->
<link
	href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / full calendar -->
<link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / select2 -->
<link href='assets/stylesheets/plugins/select2/select2.css' media='all'
	rel='stylesheet' type='text/css' />
<!-- / mention -->
<link href='assets/stylesheets/plugins/mention/mention.css' media='all'
	rel='stylesheet' type='text/css' />
<!-- / tabdrop (responsive tabs) -->
<link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all'
	rel='stylesheet' type='text/css' />
<!-- / jgrowl notifications -->
<link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / datatables -->
<link
	href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / dynatrees (file trees) -->
<link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / color picker -->
<link
	href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / datetime picker -->
<link
	href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / daterange picker) -->
<link
	href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / flags (country flags) -->
<link href='assets/stylesheets/plugins/flags/flags.css' media='all'
	rel='stylesheet' type='text/css' />
<!-- / slider nav (address book) -->
<link href='assets/stylesheets/plugins/slider_nav/slidernav.css'
	media='all' rel='stylesheet' type='text/css' />
<!-- / fuelux (wizard) -->
<link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all'
	rel='stylesheet' type='text/css' />
<!-- / flatty theme -->
<link href='assets/stylesheets/light-theme.css'
	id='color-settings-body-color' media='all' rel='stylesheet'
	type='text/css' />
<!-- / demo -->
<link href='assets/stylesheets/demo.css' media='all' rel='stylesheet'
	type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body class='contrast-red '>
	<%@include file="adminPublic.jsp" %>
	<div id='wrapper'>
		<div id='main-nav-bg'></div>
		<nav class='' id='main-nav'>
			<div class='navigation'>
				<ul class='nav nav-stacked'>
					<li class=''><a href='adminUserManagement.jsp'> <i class='icon-group'></i> <span>用户管理</span>
					</a></li>
					<li class=''><a href='adminSellerManagement.jsp'> <i class='icon-home'></i> <span>店铺管理</span>
					</a></li>
					<li ><a href='adminType.jsp'> <i class='icon-tasks'></i> <span>分类管理</span>
					</a></li>
					<li class='active'><a href='adminProduct.jsp'> <i class='icon-magic'></i>
							<span>商品管理</span>
					</a></li>
					<li class=''><a href='adminOrderManagement.jsp'> <i class='icon-inbox'></i> <span>订单管理</span>
					</a></li>
				</ul>
			</div>
		</nav>
		<section id='content'>
			<div class='container-fluid'>
				<div class='row-fluid' id='content-wrapper'>
					<div class='span12'>
						<div class='row-fluid'>
							<div class='span12'>
								<div class='page-header'>
									<h1 class='pull-left'>
										<i class='icon-magic'></i> <span>全部商品</span>
									</h1>
								</div>
							</div>
						</div>
						<div class="row-fluid">

							<div class="span12 box bordered-box orange-border"
								style="margin-bottom: 0;">

								<div class="box-header orange-background">

									<div class="title">显示所有商家的上架商品</div>
								</div>

								<div class="box-content box-no-padding">

									<div class="responsive-table">

										<div class="scrollable-area">

											<div id="DataTables_Table_0_wrapper"
												class="dataTables_wrapper form-inline" role="grid">
												<div class="row-fluid">
													<div class="span6">
														<div id="DataTables_Table_0_length"
															class="dataTables_length">
															<label><select id="pageSize" size="1"
																name="DataTables_Table_0_length"
																aria-controls="DataTables_Table_0"><option
																		value="5">5</option>
																	<option value="10" selected="selected">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option></select> 每页的记录</label>
														</div>
													</div>
													<div class="span6 text-right">
														<div class="dataTables_filter"
															id="DataTables_Table_0_filter">
															<label>搜索: <input type="text" id="keyword"
																aria-controls="DataTables_Table_0"></label>
														</div>
													</div>
												</div>
												<table
													class="data-table table table-bordered table-striped dataTable"
													style="margin-bottom: 0;" id="table"
													aria-describedby="DataTables_Table_0_info">

													<thead>

														<tr role="row">
															<th class="sorting orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="productName"
																data-ascdesc="desc">商品名称</th>
															<th class="sorting orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="sellerShopName"
																data-ascdesc="desc">商铺名称</th>
															<th class="sorting orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="typeFatherId"
																data-ascdesc="desc">一级分类</th>
															<th class="sorting orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="typeId"
																data-ascdesc="desc">二级分类</th>
															<th class="sorting orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="productPrice"
																data-ascdesc="desc">单价</th>
															<th class="sorting_asc orderBy" role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="productTime"
																data-ascdesc="desc">上架时间</th>
															<th role="columnheader"
																tabindex="0" aria-controls="DataTables_Table_0"
																rowspan="1" colspan="1" data-name="productIntroduce"
																data-ascdesc="desc">商品简介</th>
															<th role="columnheader" tabindex="0"
																aria-controls="DataTables_Table_0" rowspan="1"
																colspan="1">操作</th>
														</tr>

													</thead>

													<tbody role="alert" aria-live="polite" aria-relevant="all">
													</tbody>
												</table>
												<div class="row-fluid">
													<div class="span6">
														<div class="dataTables_info" id="showNumber"></div>
													</div>
													<div class="span6 text-right">
														<div
															class="dataTables_paginate paging_bootstrap pagination pagination-small"
															id="pageNum"></div>
													</div>
												</div>
											</div>

										</div>

									</div>

								</div>

							</div>

						</div>
					</div>
				</div>
		</section>
	</div>
	<div class="modal hide fade" id="modal-example" role="dialog"
		tabindex="-1" aria-hidden="true" style="display: none;">

		<div class="modal-header">

			<button class="close" data-dismiss="modal" type="button">×</button>

			<h3 id="shopName">商品店铺</h3>

		</div>

		<div class="modal-body">
			<div class="row-fluid">
				<div class="span12 box">
					<div class="box-content">
						<div class="accordion accordion-contrast" id="accordion"
							style="margin-bottom: 0;">
							<div class="accordion-group">
								<div class="accordion-heading">
									<span class="accordion-toggle" data-parent="#accordion"
										data-toggle="collapse" href="#collapseOne-accordion"
										id="showProductName"> 商品名称 </span>
								</div>
								<div class="accordion-body collapse in"
									id="collapseOne-accordion">

									<div class="accordion-inner" id="showProductIntroduce">商品简介</div>

								</div>

							</div>

							<div class="accordion-group">

								<div class="accordion-heading">
									<span class="accordion-toggle" id="showProductPrice">
										商品单价</span>
								</div>
							</div>

							<div class="accordion-group">

								<div class="accordion-heading">

									<span class="accordion-toggle" id="showProductTime">
										上架时间 </span>
								</div>
							</div>

						</div>

					</div>

				</div>

				<div class="span12 box" style="margin-bottom: 0">
					<div class="box-content">

						<div class="carousel slide carousel-without-caption"
							id="myCarousel" style="margin-bottom: 0;">

							<ol class="carousel-indicators" id="imgNumber">
							</ol>

							<!-- Carousel items -->

							<div class="carousel-inner" id="img">
							</div>

							<!-- Carousel nav -->

							<a class="carousel-control left" data-slide="prev"
								href="#myCarousel">‹</a> <a class="carousel-control right"
								data-slide="next" href="#myCarousel">›</a>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">关闭</button>
		</div>

	</div>
	<!-- / jquery -->
	<script src='assets/javascripts/jquery/jquery.min.js'
		type='text/javascript'></script>
	<!-- / jquery mobile events (for touch and slide) -->
	<script
		src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js'
		type='text/javascript'></script>
	<!-- / jquery migrate (for compatibility with new jquery) -->
	<script src='assets/javascripts/jquery/jquery-migrate.min.js'
		type='text/javascript'></script>
	<!-- / jquery ui -->
	<script src='assets/javascripts/jquery_ui/jquery-ui.min.js'
		type='text/javascript'></script>
	<!-- / bootstrap -->
	<script src='assets/javascripts/bootstrap/bootstrap.min.js'
		type='text/javascript'></script>
	<script src='assets/javascripts/plugins/flot/excanvas.js'
		type='text/javascript'></script>
	<!-- / slider nav (address book) -->
	<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js'
		type='text/javascript'></script>
	<script src='assets/javascripts/nav.js' type='text/javascript'></script>
	<script src='assets/javascripts/showPage.js' type='text/javascript'></script>
</body>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						// 页码
						let pageNum = 1;
						// 页面显示数量
						let pageSize = parseInt($("#pageSize").val());
						// 搜索条件
						let keyword = $("#keyword").val();
						// 排序条件
						let orderBy = "productTime";
						// 排序顺序
						let ascDesc = "desc";
						// 总页数
						let pages = 1;

						// 查询所有上架商品并显示
						function sendAjax() {
							$
									.ajax({
										url : "${pageContext.request.contextPath}/back/adminProduct",
										data : {
											op : "getAll",
											pageNum : pageNum,
											pageSize : pageSize,
											keyword : keyword,
											orderBy : orderBy,
											ascDesc : ascDesc
										},
										dataType : "json",
										success : function(result, status, xhr) {
											// 清空表格数据
											$("#table tbody").empty();
											$("#pageNum").empty();
											$("#showNumber").empty();

											$.each(result.data,function(index, val) {
												let productIntroduct = val.productIntroduce.length>10?val.productIntroduce.substr(0,10)+"......":val.productIntroduce;
																// 组装数据行
																let html = '<tr id = "'+val.productId+'">'
																		+ '<td>'
																		+ val.productName
																		+ '</td>'
																		+ '<td>'
																		+ val.sellerShopName
																		+ '</td>'
																		+ '<td>'
																		+ val.typeFatherName
																		+ '</td>'
																		+ '<td>'
																		+ val.typeName
																		+ '</td>'
																		+ '<td>'
																		+ val.productPrice
																		+ '</td>'
																		+ '<td>'
																		+ val.productTime
																		+ '</td>'
																		+ '<td>'
																		+ productIntroduct
																		+ '</td>'
																		+ '<td>'
																		+ '<button class="btn btn-success showDetail" data-toggle="modal" href="#modal-example" role="button" data-productname="'+val.productName+'" style="margin-right:5px;">查看详情</span>'
																		+ '<button class="btn btn-danger soldOut" data-productname="'+val.productName+'">下架</span>'
																		+ '</td>'
																		+ '</tr>';
																$(
																		"#table tbody")
																		.append(
																				html);
															});
											// 判断是否有页数
											if (result.pages > 0) {
												pages = result.pages;
												// 组装分页栏
												html = '<ul>'
														+ '<li class="prev '
														+ (1 == result.pageNum ? 'disabled'
																: '')
														+ '">'
														+ '<a href="#">← Previous'
														+ '</a>' + '</li>';
												let arrayPage = page(result.pageNum,result.pages,11);
												for (let i = 0; i < arrayPage.length; i++) {
													if (arrayPage[i] == result.pageNum) {
														html += '<li class="active">'
																+ '<a href="#" class="pageNum">'
																+ arrayPage[i]
																+ '</a>'
																+ '</li>';
													} else {
														html += '<li>'
																+ '<a href="#" class="pageNum">'
																+ arrayPage[i] + '</a>'
																+ '</li>';
													}
												}
												html += '<li class="next '
														+ (result.pages == result.pageNum ? 'disabled'
																: '')
														+ '">'
														+ '<a href="#">Next → </a>'
														+ '</li>' + '</ul>';
												$("#pageNum").append(html);
												$("#showNumber")
														.append(
																"从"
																		+ ((result.pageNum - 1)
																				* result.pageSize + 1)
																		+ "到"
																		+ (result.pageNum* result.pageSize>result.total?result.total:result.pageNum* result.pageSize)
																		+ "  共"
																		+ result.total
																		+ "条数据");
											}
										},
										error : function(xhr, status, error) {
											alert("ajax请求失败");
										}
									});
						}
						sendAjax();

						// 显示数目改变时
						$("#pageSize").change(function() {
							pageNum = 1;
							pageSize = parseInt($("#pageSize").val());
							sendAjax();
						});
						// 搜索条件输入时
						$("#keyword").keyup(function() {
							pageNum = 1;
							keyword = $("#keyword").val();
							sendAjax();
						});
						// 点击排序条件时
						$(".orderBy").click(function() {
							pageNum = 1;
							orderBy = $(this).data("name");
							ascDesc = $(this).data("ascdesc");
							if (ascDesc == "asc") {
								$(this).data("ascdesc", "desc");
							} else {
								$(this).data("ascdesc", "asc");
							}
							sendAjax();
							
							// 改变图标顺序
							$(".orderBy").removeClass("sorting_asc").removeClass("sorting_desc").addClass("sorting");
							if($(this).data("ascdesc") == "asc"){
								$(this).remove("sorting").addClass("sorting_desc");
							}else{
								$(this).remove("sorting").addClass("sorting_asc");
							}
						});

						// 点击下一页时
						$(document).on("click", ".next", function() {
							if (pageNum < pages) {
								pageNum += 1;
								sendAjax();
							}
						});
						// 点击上一页时
						$(document).on("click", ".prev", function() {
							if (pageNum > 1) {
								pageNum -= 1;
								sendAjax();
							}
						});
						// 点击页码时
						$(document).on("click", ".pageNum", function() {
							pageNum = parseInt($(this).text());
							sendAjax();
						});

						// 下架商品
						$(document).on("click",
										".soldOut",
										function() {
											// 获取id
											let productId = $(this).parent()
													.parent().prop("id");
											// 获取商品名称
											let productName = $(this).data(
													"productname");
											// 下架商品
											if (confirm("是否下架<"+productName+">商品？")) {
												$.ajax({
														url : "${pageContext.request.contextPath}/back/adminProduct",
														data : {
															op : "soldOut",
															productId : productId
														},
														success : function() {
															alert("下架成功");
															sendAjax();
														},
														error : function() {
															alert("下架失败，ajax请求失败");
														}
														});
											}
										});
						// 查看详情
						$(document).on("click",".showDetail",function(){
							let productId = $(this).parent().parent().prop("id");
							// ajax请求数据
							$.ajax({
								url:"${pageContext.request.contextPath}/back/adminProduct",
								data:{
									op:"detail",
									productId:productId
								},
								dataType:"json",
								success:function(result,status,xhr){
									// 清空dom
									$("#imgNumber").empty();
									$("#img").empty();
									// 数据显示
									$("#shopName").text(result.product.seller.sellerShopName);
									$("#showProductName").text("商品名称："+result.product.productName);
									$("#showProductIntroduce").text(result.product.productIntroduce);
									$("#showProductPrice").text("商品单价：￥"+result.product.productPrice);
									$("#showProductTime").text("上架时间："+result.product.productTime);
									// 轮播图显示
									// 1.序号
									for(let i = 0 ; i < result.images.length; i++){
										let html = '';
										if(i == 0){
											html += '<li data-slide-to="'+i+'" data-target="#myCarousel" class="active"></li>'
										}else{
											html += '<li data-slide-to="'+i+'" data-target="#myCarousel" class=""></li>'
										}
									}
									$("#imgNumber").append(html);
									// 2. 图片
									$.each(result.images,function(index,img){
										let html = '';
										if(index == 0){
											html += '<div class="item active">'
										}else{
											html += '<div class="item">';
										}
										html += '<img src="${pageContext.request.contextPath}/download?fileName='+img+'" style = "height:250px;">'
												+'</div>';
										$("#img").append(html);
									});
									
								},error:function(){
									alert("ajax请求失败");
								}
							});
							
						});
					});
</script>

</html>
