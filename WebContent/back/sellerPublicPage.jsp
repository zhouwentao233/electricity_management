<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<header>
   <div class='navbar'>
       <div class='navbar-inner'>
           <div class='container-fluid'>
               <a class='brand' href='${pageContext.request.contextPath }/back/sellerSignIn.jsp'>
                   <i class='icon-heart-empty'></i>
                   <span class='hidden-phone sellerShopName'>${sessionScope.seller.sellerShopName }</span>
               </a>
               <a class='toggle-nav btn pull-left' href='#'>
                   <i class='icon-reorder'></i>
               </a>
               <ul class='nav pull-right'>
                   <li class='dropdown dark user-menu'>
                       <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                           <img title='${sessionScope.seller.sellerAccount }' height='23' src='${pageContext.request.contextPath}/download?fileName=${sessionScope.seller.sellerLogo}' width='23' />
                           <span class='user-name hidden-phone'>${sessionScope.seller.sellerAccount }</span>
                           <b class='caret'></b>
                       </a>
                       <ul class='dropdown-menu'>
                          
                           <li>
                               <a href='${pageContext.request.contextPath }/back/SellerServlet?op=logout'>
                                   <i class='icon-signout'></i>
                                  	退出登录
                               </a>
                           </li>
                       </ul>
                   </li>
               </ul>
           </div>
       </div>
   </div>

</header>
