<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %> 
<%@page import="com.zretc.util.DBUtil" %> 
<!-- 商城管理.订单管理 -->
<!-- denglingqi -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>电商管理系统管理员后台</title>
		<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
		<script type='text/javascript'>
			//订单管理首页查询数据函数
			function addTableData(oid,sid,orderstatus,sj,otp){
				$('.firstpage').append("<tr align=\"center\">"+
					"<td>"+oid+"</td>"+
			        "<td>"+sid+"</td>"+
			        "<td><label>"+orderstatus+"</label></td>"+
			        "<td><label>"+sj+"</label></td>"+
			        "<td>"+otp+"</td>"+
					"<!--弹出订单详情页面-->"+
					"<td > <input type=\"button\" class=\"order_details5\" href = \"javascript:void(0)\" value=\"订单详情\" onclick = \"document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'\">"+ 
			    		"<div id=\"light\" class=\"order_details1\">"+
					        "<h4 class=\"order_details4\" align=\"left\">订单管理.订单明细</h4>"+ 
					        "<hr />"+
							"<!--订单详情页面--> "+     
							"<input type=\"button\" href = \"javascript:void(0)\" value=\"关闭\" class=\"order_details3\" onclick = \"document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'\">"+
							"<br />"+
							"<div id=\"\" class=\"\">"+
								"<table border=\"1\" width=\"750\">"+	
									"<tr>"+				
										"<th>商品图片</th>"+
										"<th>商品名称</th>"+
										"<th>商品介绍</th>"+
										"<th>商品数量</th>"+
										"<th>商品总价</th>"+
									"</tr>"+
									"<tbody id=\"detailss\" class=\"detailss\">"+
									"<!-- ====  js动态查询添加  ==== -->"+							
									"</tbody>"+
								"</table>"+
							"</div>"+
						"</div> "+	
						"<div id=\"fade\" class=\"order_details2\"></div>"+ 
					"</td></tr>"							
				)
			};
		</script>
		<script type="text/javascript">	
			$(document).on("click",".querys",function(){
				$('.firstpage').empty(); 
			});
			$(document).on("click",".order_details5",function(result){
				$('.detailss').empty();	
			});
		</script>
	 	<style> 
	        .order_details2{ 
	            display: none; 
	            position: absolute; 
	            top: 0%; 
	            left: 0%; 
	            width: 100%; 
	            height: 100%; 
	            background-color: #F5F5F5; 
	            z-index:1001; 
	            -moz-opacity: 0.8; 
	            opacity:.80; 
	            filter: alpha(opacity=88); 
	        } 
	        .order_details1 { 
	            display: none; 
	            position: absolute; 
	            top: 10%; 
	            left: 16%; 
	            width: 750px; 
	            height: 72%; 
	            padding: 10px;            
	            border: 2px solid orange; 
	            background-color: white; 
	            z-index:1002; 
	            overflow: auto; 
	        } 
	        body{       
	            position: absolute;      
	            left: 0%; 
	            width: 1150px; 
	            height: 85%; 
	            padding: 2px; 
	            border: 0px solid orange; 
	            background-color: white; 
	            z-index:1002; 
	            overflow: auto;          
	        }
		     .order_details3 {
			    float: right;
			    margin:0px;
		    }
		    .order_details4 {
		      	margin:0px;
		    }
		      
		    .order_details5 {
			    margin:3px;
			    padding: 2px;
		     }
		     .close1{
		    	margin:0px;
		    	padding-top: 10px;
		    	padding-left: 10px;
		     }
			 select {
				vertical-align: middle;
			    font-size: 15px;
			    margin: 0;
			 }
	  	</style> 
	</head>
	
<!--	下载图片的路径 项目名/download?fileName=  -->
<!--    上传图片的路径 D:/upload/images    -->
	
	<body>
		<h2 class='close1'>商城管理.订单管理</h2>
	    <br /><hr />
	    		
		<form action="" method="get" enctype="application/x-www-form-urlencoded" name="" id="" enctype="multipart/form-data">	
			<h5>
				<label for="username">&nbsp;&nbsp;&nbsp;&nbsp;按订单号</label> 
				<input type="text" id="order_id" value="" name="" placeholder="请输入订单号" />
				<label for="username">&nbsp;&nbsp;按店铺</label> 
				<input type="text" id="seller_id" value="" name="" placeholder="请输入店铺编号" />
				<label for="username">&nbsp;&nbsp;按订单状态</label>
				<select name="state" id="state">
					<option value="所有状态">所有状态</option>
					<option value="交易已完成">交易已完成</option>
					<option value="交易未完成">交易未完成</option>				
				</select>
				<label for="username">&nbsp;&nbsp;按日期</label> 
				<label for="username">从</label> 
				<input type="date" name="data" id="order_time1" value="" /> <label for="username">到</label>
				<input type="date" name="data" id="order_time2" value="" />
				<input type="button" class="querys" id="querys" value="&nbsp;查&nbsp;询&nbsp;">
				<button type="reset">清除搜索</button>
			</h5>			
			<hr />
			<table border="1" width="1150">
				<thead>
					<tr>				
						<th>订单号</th>
						<th>店铺编号</th>
						<th>订单状态</th>
						<th>交易日期</th>
						<th>总金额</th>
						<th>订单明细</th>
					</tr>
				</thead>
				<tbody id="firstpage" class="firstpage">	
						
					 <% try { %>
					     <%ResultSet rs = null;
					     String sql = "select * from orders A inner join order_detail B on A.order_id=B.order_id inner join product C on B.product_id=C.product_id group by A.order_id limit 0,100";  //查询语句                
					     rs = DBUtil.doQuery(sql);      
					     while (rs.next()) {%>
				      		<!--订单首页显示订单简单数据-->     
				          	<script type="text/javascript">
					         	window.setTimeout(function(){
						        	var oid="<%=rs.getString("order_id") %>";		
						      		var sj="<%=rs.getString("order_time") %>";      
						      		var sid=<%=rs.getString("seller_id") %>;
						      		var otp=<%=rs.getString("order_total_price") %>;
						      		var orderstatus=<%=rs.getString("order_status") %>;
					      		   	
						      		if(orderstatus==1){
						      			orderstatus="未完成.待发货"
						      		}else if(orderstatus==2){
						      			orderstatus="未完成.已发货"
						      		}else{
						      			orderstatus="已完成"
						      		}
					      			addTableData(oid,sid,orderstatus,sj,otp);
								},1);
			         	 	</script> 
			    	    <%}
		            }catch (Exception e) {        }  %> 
	    		</tbody>    		
	    	</table>
	    </form> 		
	           
		<!-- ====  js动态添加订单.商城管理  ==== -->
		<%try {           
			 ResultSet rs = null;
		     String sql = "select * from orders A inner join order_detail B on A.order_id=B.order_id inner join product C on B.product_id=C.product_id group by A.order_id limit 0,100;";  //查询语句                
		     rs = DBUtil.doQuery(sql);  
		     while (rs.next()) {%>
		    	<script type="text/javascript">	
		    	
					$(document).on("click","#querys",function(){		
						var oid="<%=rs.getString("order_id") %>"	
						var sj="<%=rs.getString("order_time") %>"
						var sj1="<%=rs.getDate("order_time") %>"
						var sid="<%=rs.getString("seller_id") %>"
						var otp=<%=rs.getString("order_total_price") %>;
						var orderstatus=<%=rs.getString("order_status") %>;
						var ostate1="所有状态";
						var ostate2="交易已完成";
						var ostate3="交易未完成";
						var orderid=$('#order_id').val();
						var sellerid=$('#seller_id').val();
						var ordertime1=$('#order_time1').val();
						var ordertime2=$('#order_time2').val();      	
						var ostate=$("#state option:selected").text(); 
						//订单号模糊匹配
						var str = oid.search(orderid);
						if(str==-1){
							//2：没有找到匹配字符
							str=2;
						}else{
							//1：找到匹配字符
							str=1;
						}
						//店铺号模糊匹配
						var str0 = sid.search(sellerid);
						if(str0==-1){
							str0=2;
						}else{
							str0=1;
						}
						//发货状态  1：待发货   2：已发货  3:已完成
						if(orderstatus==1){
							orderstatus="未完成.待发货"
						}else if(orderstatus==2){
							orderstatus="未完成.已发货"
						}else{
							orderstatus="已完成"
						}	
						
						if(ostate==ostate1){
							if(orderid=="" && sellerid=="" && ordertime1=="" && ordertime2==""){
								addTableData(oid,sid,orderstatus,sj,otp);		
								// 查询所有
						    }else if(str==1 && sellerid=="" && ordertime1=="" && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	// 按订单查询
						    } else if(orderid=="" && str0==1 && ordertime1=="" && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//	按店铺查询
						    } else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && sj1<=ordertime2){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按时间查询
						    }else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按起始时间查询
						    }else if(str==1 && str0==1 && ordertime1=="" && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && sj1<=ordertime2){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺时间查询
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && sj1<=ordertime2){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺时间查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺起始时间查询	
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && ordertime2==""){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺起始时间查询
						    }	    	    		    			
						}else if(ostate==ostate2 && orderstatus=="已完成"){
							if(orderid=="" && sellerid=="" && ordertime1=="" && ordertime2=="" && orderstatus=="已完成"){
								addTableData(oid,sid,orderstatus,sj,otp);		
								// 查询所有
						    }else if(str==1 && sellerid=="" && ordertime1=="" && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	// 按订单查询
						    } else if(orderid=="" && str0==1 && ordertime1=="" && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//	按店铺查询
						    } else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按时间查询
						    }else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按起始时间查询
						    }else if(str==1 && str0==1 && ordertime1=="" && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺时间查询
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺时间查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺起始时间查询	
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && ordertime2=="" && orderstatus=="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺起始时间查询
						    }	    	    		    			
						}else if(ostate==ostate3 && orderstatus!="已完成"){
							if(orderid=="" && sellerid=="" && ordertime1=="" && ordertime2=="" && orderstatus!="已完成"){
								addTableData(oid,sid,orderstatus,sj,otp);		
								// 查询所有
						    }else if(str==1 && sellerid=="" && ordertime1=="" && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	// 按订单查询
						    } else if(orderid=="" && str0==1 && ordertime1=="" && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//	按店铺查询
						    } else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按时间查询
						    }else if(orderid=="" && sellerid=="" && sj1>=ordertime1 && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按起始时间查询
						    }else if(str==1 && str0==1 && ordertime1=="" && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺时间查询
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && sj1<=ordertime2 && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺时间查询
						    }else if(str==1 && str0==1 && sj1>=ordertime1 && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按订单店铺起始时间查询	
						    }else if(orderid=="" && str0==1 && sj1>=ordertime1 && ordertime2=="" && orderstatus!="已完成"){
						    	addTableData(oid,sid,orderstatus,sj,otp);
						    	//按店铺起始时间查询
						    }	
						}
					});   
		
					
				</script>
			 <%}
	    }catch (Exception e) { } %> 
	    <%try { %>
	
			<%ResultSet rs1 = null;
			 ResultSet rs2 = null;
			 String orderId = null;
			 String sql1 = "select * from orders limit 0,100;";
		     String sql2 = "select * from product A inner join order_detail B on A.product_id=B.product_id inner join orders C on B.order_id=C.order_id where B.order_id=?;";  //查询语句                	     	     	     
		     rs1 = DBUtil.doQuery(sql1);      
			 while (rs1.next()){
				 orderId = rs1.getString("order_id");		
				 rs2 = DBUtil.doQuery(sql2,orderId);		
				 while (rs2.next()){ %>		
					<script type="text/javascript">
						$(document).on("click",".order_details5",function(result){
							var a=$(this).parent().prev().prev().prev().prev().prev().text();
							var root = "${pageContext.request.contextPath}/download?fileName=";	
							var fileName = "<%=rs2.getString("product_picture") %>";
							if("<%=rs1.getString("order_id")%>"==a){
								$('.detailss').append("<tr align=\"center\">"+				
									"<td><img src=\""+root+fileName+"\" width=\"110\" height=\"80\"/></td>"+
									"<td><%=rs2.getString("product_name") %></td>"+
									"<td><%=rs2.getString("product_introduce") %></td>"+
									"<td><%=rs2.getString("order_detail_count") %></td>"+
									"<td><%=rs2.getString("order_detail_price") %></td>"+
									"</tr>"	
								)
							} 
						});   
					</script>
				<%}    
			}	
		}catch (Exception e) {        }  %> 
	</body>
</html>