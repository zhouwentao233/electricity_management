<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>卖家商品管理界面</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
		<!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery ui -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / switch buttons -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / xeditable -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / wysihtml5 (wysywig) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery file upload -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / full calendar -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / select2 -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / mention -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / tabdrop (responsive tabs) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jgrowl notifications -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datatables -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / dynatrees (file trees) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / color picker -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datetime picker -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / daterange picker) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flags (country flags) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / slider nav (address book) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / fuelux (wizard) -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flatty theme -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
		<!-- / demo -->
		<link href='${pageContext.request.contextPath}/back/assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>

	<body class='contrast-red '>
<%@include file="sellerPublicPage.jsp" %>
		<nav class='' id='main-nav'>

<div class='navigation'>

<div class='search'>
   <form accept-charset="UTF-8" action="search_results.html" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
       <div class='search-wrapper'>
           <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
           <button class="btn btn-link icon-search" name="button" type="submit"></button>
       </div>
   </form>

</div>

<ul class='nav nav-stacked'>

<li>
   <a href='sellerIndex.jsp'>
       <i class='icon-home'></i>
       <span>首页</span>
   </a>

</li>
<li class=''>
    <a href='sellerSales.jsp'> <i class='icon-dashboard'></i>
        <span>销售管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerManageProduct.jsp'> <i class='icon-magic'></i>
        <span>商品管理</span>
    </a>
</li>
<li class='active'>
    <a href='sellerOrderManagement.jsp'><i class='icon-inbox'></i>
        <span>订单管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerComment.jsp'> <i class='icon-comments'></i>
        <span>评论管理</span>
    </a>
</li>
<li>
    <a class="dropdown-collapse" href="#">
        <i class="icon-cog"></i>
        <span>设置</span>
        <i class="icon-angle-down angle-down"></i>
    </a>
    <ul class="nav nav-stacked">
        <li>
            <a  href="${pageContext.request.contextPath }/back/SellerServlet?op=logout">
                <i class="icon-caret-right"></i>
                <span>退出账号</span>
            </a>
         </li>
    </ul>
</li>
</ul>

</div>

</nav>
			<section id='content'>
<div class='container-fluid'>
<div class='row-fluid' id='content-wrapper'>
<div class='span12'>
<div id='orders'>
<div class='row-fluid'>
<div class='span4'>
<div class='row-fluid'>
    <div class='span12'>
        <div class='page-header'>
            <h1>
                <i class='icon-inbox'></i>
                <span>订单管理</span>
            </h1>
        </div>
    </div>
</div>
<div class='row-fluid' id='list'>
    <div class='span12 box'>
    	<div class="dataTables_filter" id="DataTables_Table_0_filter">
			<label>查询
			<input type="text" aria-controls="DataTables_Table_0" id="keyword" value="" placeholder="订单编号" >
			<button class="btn btn-info" name="button" style="margin-bottom:5px" id="searchBtn">搜索</button>
			<button class="btn btn-success" id="clearSearchBtn" style="margin-bottom:5px">清除搜索</button></label>
			<label>
				<select size="1" name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="selectPageSize">
					<option value="10" selected="selected">10</option>
					<option value="15">15</option>
					<option value="20">20</option>
					<option value="50">50</option>
				</select>
				<select size="1" name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="orderStatus">
					<option value="0" selected="selected">商品状态</option>
					<option value="1">未发货</option>
					<option value="2">已发货</option>
				</select>
			</label>
		</div>
    	
        <div class='box-content' id="DataTables_OrdersList">        	
            <!--  <div class='pull-left'>
                <p><a href='#detail'><strong>#001</strong>Columbus Lesch</a></p>
                <p><span class='label label-warning'>In process</span></p>
            </div>
            <div class='text-right pull-right'>
                <h4 class='contrast price'>$353.00</h4>
                <p>
                    <i class='icon-time'></i>
                    <span class='has-tooltip' data-title='2013-02-26'>3 months</span>
                </p>
            </div>
            <div class='clearfix'></div>
            <hr class='hr-normal' /> --> 
        </div>
        <div class='pagination pagination-centered' id="DataTable_infoPage"></div>
        <div class='pagination pagination-centered' id="DataTables_pageInfo">
            <!-- <ul>
                <li>
                    <a href='#'>«</a>
                </li>
                <li class='active'>
                    <a href='#'>1</a>
                </li>
                <li>
                    <a href='#'>2</a>
                </li>
                <li>
                    <a href='#'>»</a>
                </li>
            </ul> -->
        </div>
    </div>
</div>
</div>
<div class='span8'>
    <div class='row-fluid'>
        <div class='span12'>
            <div class='page-header'>
                <h1>
                    <span id="orderTop">订单详情</span>
                </h1>
            </div>
        </div>
    </div>
    <div class='row-fluid' id='detail'>
        <div class='span12 box'>
            <div class='box-content'>
                <div class='pull-left'>
                    <a class='btn btn-success fullfill-items' href='#'>商品状态</a>
                    <a class='btn btn-success' href='#' id="updataStatus" >确认发货</a>
                </div>
                <div class='clearfix'></div>
                <hr class='hr-normal' />
                <table class='table table-hover' id='ordersProduct' style='margin-top:50px'>
                    <thead>
                    <tr>
                        <th>全部商品</th>
                        <th>
                            <div class='text-center'>商品数量</div>
                        </th>
                        <th>
                            <div class='text-right'>商品价格</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- <tr>
                        <td class='only-checkbox'>
                            <input type='checkbox' />
                        </td>
                        <td>Apple iPhone 5 16GB</td>
                        <td>
                            <div class='text-center'>1</div>
                        </td>
                        <td>
                            <div class='text-right'>$544.00</div>
                        </td>
                    </tr>        -->    
                    </tbody>
                </table>
                <div class='text-right'>
                    <h3 class='subtotal contrast'>总价格￥<span id='productprice'></span></h3>
                </div>
                <hr class='hr-normal' />
                <div id="orderReceive">
               	 <!--   <div class='row-fluid'>
                    <div class='span4'>
                        <div class='lead'>
                            <i class='icon-envelope contrast'></i>
                            	收货信息
                        </div>
                    </div>
                    <div class='span7 offset1'>
                        <address>
                            <strong>Dallas Denesik</strong>
                            <br />
                            	收货人:
                            <br />
                            	收货地址:
                            <br />
                            <abbr title='Phone'>联系电话:</abbr>
                            	
                        </address>
                    </div>
	                </div>
	                <div class='row-fluid'>
	                    <div class='span4'>
	                        <div class='lead'>
	                            <i class='icon-user contrast'></i>
	                           	 用户信息
	                        </div>
	                    </div>
	                    <div class='span7 offset1'>
	                        <address>
	                            <strong>用户账号</strong>
	                            <br />
	                            <a href='mailto:#'>first.last@example.com</a>
	                        </address>
	                    </div> -->
	                </div>
                </div>             
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
			</div>
			<!-- / jquery -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
			<!-- / jquery mobile events (for touch and slide) -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
			<!-- / jquery migrate (for compatibility with new jquery) -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
			<!-- / jquery ui -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
			<!-- / bootstrap -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
			<!-- / slider nav (address book) -->
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/nav.js' type='text/javascript'></script>
			<script src='${pageContext.request.contextPath}/back/assets/javascripts/showPage.js' type='text/javascript'></script>
	</body>
	<script type="text/javascript">
		let array = [];
		$(document).ready(function() {
			// 页面进入，ajax异步请求服务器响应数据-订单管理列表
			// 调用发送异步请求的方法，参数是页码
			sendAjax(1);
			
			// 发货事件/修改订单状态
			$(document).on("click","#updataStatus",function() {
					var orderSta
				if (window.confirm("是否确定发货")) {
					var orderId = $("#orderTop").text();
					var jsonId = JSON.stringify(array);
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/back/SellerOrdersServlet",
						type:"GET",
						data:{
							op:"updateOrderStatus",
							orderId:orderId,
							orderDetailId:jsonId
						},
						dataType:"json",
						success:function(result,status,xhr) {
							// 调用发送异步请求的方法，参数数页码
							sendAjax(1);
						},
						error:function(xhr,result,error) {
							alert("修改失败");
						}
					});
				}
			});
						
			// 点击订单Id查看订单详情
			$(document).on("click","#orderDetail",function() {
				var orderId = $(this).data("orderid");
				// 头部订单编号
				$("#orderTop").text(orderId);
				var price = $(this).data("price");
				// 总价格
				$("#productprice").text(price);
				// 商品详情
				ordersProductAjax(orderId);		
				// 订单详情
				orderDetailAjax(orderId);
			});
			
			//选择显示状态的下拉框事件
			$(document).on("change","#orderStatus",function() {
				// 调用发送异步请求的方法，参数是页码
				sendAjax(1);
			});
			
			// 选择显示记录条数的下拉框事件
			$(document).on("change","#selectPageSize",function() {
				// 调用发送异步请求的方法，参数是页码
				sendAjax(1);
			});
			
			// 搜索事件
			$(document).on("click","#searchBtn",function() {
				// 调用发送异步请求的方法，参数是页码
				sendAjax(1);
			});
			
			// 清楚搜索事件
			$(document).on("click","#clearSearchBtn",function() {
				$("#keyword").val("");
				// 调用发送异步请求的方法，参数是页码
				sendAjax(1);
			});
			
			// 上一页事件
			$(document).on("click",".prev",function() {
				// 获取当前页
				var currentPageNum = $("#pageInfo").data("pagenum");
				if (currentPageNum == 1) {
					alert("已经是第一页，没有上一页！");
				} else {
					var pageNum = parseInt(currentPageNum) - 1;
					// 调用发送异步请求的方法，参数是页码
					sendAjax(pageNum);
				}
			});
			
			// 下一页事件
			$(document).on("click",".next",function() {
				// 获取当前页
				var currentPageNum = $("#pageInfo").data("pagenum");
				// 获取总页数
				var lastPageNum = $("#pageInfo").data("pagepages");
				if (currentPageNum == lastPageNum) {
					alert("已经是最后一页，没有下一页！");
				} else {
					var pageNum = parseInt(currentPageNum) + 1;
					// 调用发送异步请求的方法，参数是页码
					sendAjax(pageNum);
				}
			});
			
			// 页码点击事件
			$(document).on("click",".current",function() {
				var pageNum = $(this).text();
				// 调用发送异步请求的方法，参数是页码
				sendAjax(pageNum);
			});
		});
		// 发送ajax异步请求，参数：PageNum 页码
		function sendAjax(pageNum) {
			$.ajax({
				async:true,
				url:"${pageContext.request.contextPath}/back/SellerOrdersServlet",
				type:"GET",
				data:{op:"getOrdersListByPage",
					sellerId:"${sessionScope.seller.sellerId}",
					keyword:$("#keyword").val(),
					orderStatus:$("#orderStatus").val(),
					pageNum:pageNum,
					pageSize:$("#selectPageSize").val()},
				dataType:"json",
				success:function(result,status,xhr){
					showData(result);
				},
				error:function(xhr,status,error) {
					alert("异步请求失败！");
				}
			});
		}
		// 渲染订单表格
		function showData(result) {
			// 清空表格原始数据
			$("#DataTables_OrdersList").empty();
			// 清空分页信息简介原始数据
			$("#DataTable_infoPage").empty();
			// 清空分页信息原始数据
			$("#DataTables_pageInfo").empty();
			// 填充数据表格
			$.each(result.data,function(index,order) {
				var temp;
				if (order.orderDetailStatus == 1) {
					temp="未发货"
					$("#DataTables_OrdersList").append("<div class='pull-left'>"+
			                "<p><a href='#'><strong id='orderDetail' data-orderid='"+order.orderId+"' data-price='"+order.orderTotalPrice+"'>"+order.orderId+"</strong><br />"+order.userAccount+"</a></p>"+
		                	"<p><span class='label label-warning  btn-primary'>"+temp+"</span></p>"+
		            	"</div>"+
			            "<div class='text-right pull-right'>"+
			                "<h4 class='contrast price'>￥"+order.orderTotalPrice+"</h4>"+
			                "<p><i class='icon-time'></i><span class='has-tooltip'>"+order.orderTime+"</span></p>"+
			            "</div>"+
			            "<div class='clearfix'></div>"+
			            "<hr class='hr-normal' />");
				} else if (order.orderDetailStatus == 2) {
					temp="已发货"
					$("#DataTables_OrdersList").append("<div class='pull-left'>"+
			                "<p><a href='#detail'><strong id='orderDetail' data-orderid='"+order.orderId+"' data-price='"+order.orderTotalPrice+"'>"+order.orderId+"</strong><br />"+order.userAccount+"</a></p>"+
		                	"<p><span class='label label-warning btn-warning'>"+temp+"</span></p>"+
		            	"</div>"+
			            "<div class='text-right pull-right'>"+
			                "<h4 class='contrast price'>￥"+order.orderTotalPrice+"</h4>"+
			                "<p><i class='icon-time'></i><span class='has-tooltip'>"+order.orderTime+"</span></p>"+
			            "</div>"+
			            "<div class='clearfix'></div>"+
			            "<hr class='hr-normal' />");
				}
			});
			// 填充分页信息
			// 分页信息
			$("#DataTable_infoPage").append("<span id=\"pageInfo\" data-pagenum=\""+result.pageNum+"\" data-pagesize=\""+result.pageSize+"\" data-pagetotal=\""+ result.pageSize+"\" data-pagepages=\""+result.pages+"\">"+
			"每页"+result.pageSize+"条 ,共"+result.total+"条,第"+result.pageNum+"/"+result.pages+"页</span>");
			// 上一页
			let html = "<ul><li class='prev'><a href='#'>«</a></li>";
			// 显示页码
			let arrayPage = page(result.pageNum,result.pages,7);
			for (var i = 0; i < arrayPage.length; i++) {
				// 判断i==当前页，添加样式，disabled不可以用，激活状态
				if (arrayPage[i] == result.pageNum) {
					html += "<li class='current active'><a href='#'>"+arrayPage[i]+"</a></li>";
				} else {
					html += "<li class='current'><a href='#'>"+arrayPage[i]+"</a></li>";
				}
			}
			// 下一页
			html += "<li class='next'><a href='#'>»</a></li></ul>";
			$("#DataTables_pageInfo").append(html);
		}
		// 订单详情
		function orderDetailAjax(orderId) {
			$.ajax({
				async:true,
				url:"${pageContext.request.contextPath}/back/SellerOrdersServlet",
				type:"GET",
				data:{op:"selectOrderDetail",
					orderId:orderId},
				dataType:"json",
				success:function(result,status,xhr) {
					showDateOrderDetail(result);
				},
				error:function(xhr,status,error) {
					alert("连接失败");
				}
			});
		}
		// 渲染订单详情表格
		function showDateOrderDetail(result) {
			
			$.each(result,function(index,orderreceive){
				// 清空表格原始数据
				$("#orderReceive").empty();
				$("#orderReceive").append("<div class='row-fluid'>"+
		                 "<div class='span4'>"+
		                    "<div class='lead'>"+
		                        "<i class='icon-envelope contrast'></i>  收货信息"+
		                    "</div>"+
		                "</div>"+
		               " <div class='span7 offset1'>"+
		                    "<address>"+
		                        "<br />收货人:"+orderreceive.orderReceiveName+""+
		                        "<br />收货地址:"+orderreceive.orderReceiveAddress+""+
		                        "<br /><span>联系电话:"+orderreceive.orderReceivePhone+"</span>"+
		                    "</address>"+
		                "</div>"+
	                "</div>"+
	                "<div class='row-fluid'>"+
	                    "<div class='span4'>"+
	                        "<div class='lead'>"+
	                            "<i class='icon-user contrast'></i>    用户信息"+
	                        "</div>"+
	                    "</div>"+
	                    "<div class='span7 offset1'>"+
	                        "<address><strong>用户账号:"+orderreceive.userAccount+"</strong></address>"+
	                    "</div>"+
	                "</div>");
			});
		}
			
		// 订单商品详情	
		function ordersProductAjax(orderId) {
			$.ajax({
				async:true,
				url:"${pageContext.request.contextPath}/back/SellerOrdersServlet",
				type:"GET",
				data:{op:"ordersProduct",
					orderId:orderId,
					sellerId:"${sessionScope.seller.sellerId}"},
				dataType:"json",
				success:function(result,status,xhr) {
					showDataOrdersProduct(result);
				},
				error:function(xhr,status,error) {
					alert("连接失败");
				}
			});
		}
		// 渲染订单的商品信息
		
		function showDataOrdersProduct(result) {
			array = [];
			// 清空表格原始数据
			$("#ordersProduct tbody").empty();
			$.each(result,function(index,orderproduct) {
				array.push(orderproduct.orderDetailId);
				$("#ordersProduct tbody").append("<tr>"+
                    "<td>"+orderproduct.product.productName+"</td>"+
                    "<td><div class='text-center'>"+orderproduct.orderDetailCount+"</div></td>"+
                    "<td><div class='text-right'>"+orderproduct.orderDetailPrice+"</div></td>"+
                "</tr>");
			});
			console.log(array);
		}
	</script>
</html>