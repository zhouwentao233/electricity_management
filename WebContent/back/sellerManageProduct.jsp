<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 卖家商品管理界面 -->
<!DOCTYPE html>
<html>

	<head>
		<title>卖家商品管理界面</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
		<!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
		<link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery ui -->
		<link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / switch buttons -->
		<link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / xeditable -->
		<link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / wysihtml5 (wysywig) -->
		<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery file upload -->
		<link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / full calendar -->
		<link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / select2 -->
		<link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / mention -->
		<link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / tabdrop (responsive tabs) -->
		<link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jgrowl notifications -->
		<link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datatables -->
		<link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / dynatrees (file trees) -->
		<link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / color picker -->
		<link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datetime picker -->
		<link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / daterange picker) -->
		<link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flags (country flags) -->
		<link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / slider nav (address book) -->
		<link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / fuelux (wizard) -->
		<link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flatty theme -->
		<link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
		<!-- / demo -->
		<link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
	</head>

	<body class='contrast-red '>
		<%@include file="sellerPublicPage.jsp" %>

<nav class='' id='main-nav'>

<div class='navigation'>

<div class='search'>
   <form accept-charset="UTF-8" action="search_results.html" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
       <div class='search-wrapper'>
           <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
           <button class="btn btn-link icon-search" name="button" type="submit"></button>
       </div>
   </form>

</div>

<ul class='nav nav-stacked'>

<li>
   <a href='sellerIndex.jsp'>
       <i class='icon-home'></i>
       <span>首页</span>
   </a>

</li>
<li class=''>
    <a href='sellerSales.jsp'> <i class='icon-dashboard'></i>
        <span>销售管理</span>
    </a>
</li>
<li class='active'>
    <a href='sellerManageProduct.jsp'> <i class='icon-magic'></i>
        <span>商品管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerOrderManagement.jsp'><i class='icon-inbox'></i>
        <span>订单管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerComment.jsp'> <i class='icon-comments'></i>
        <span>评论管理</span>
    </a>
</li>
<li>
    <a class="dropdown-collapse" href="#">
        <i class="icon-cog"></i>
        <span>设置</span>
        <i class="icon-angle-down angle-down"></i>
    </a>
    <ul class="nav nav-stacked">
        <li>
            <a  href="${pageContext.request.contextPath }/back/SellerServlet?op=logout">
                <i class="icon-caret-right"></i>
                <span>退出账号</span>
            </a>
         </li>
    </ul>
</li>
</ul>

</div>

</nav>
			<section id='content'>
				<div class='container-fluid'>
					<div class='row-fluid' id='content-wrapper'>
						<div class='span12'>
							<div class='row-fluid'>
								<div class='span12'>
									<div class='page-header'>
										<h1 class='pull-left'>
										<i class='icon-magic'></i> <span>全部商品</span>
									</h1>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span12 box bordered-box orange-border" style="margin-bottom: 0;">
									<div class="box-header orange-background">
										<div class="title">
												<div class="box-content">
            										<button class="btn " id="all" name="button" style="margin-bottom:5px" type="submit" >全部商品</button>
            										<button class="btn btn-primary" id="nowshow" name="button" style="margin-bottom:5px" type="submit">在售商品</button>
            										<button class="btn btn-info" id="addproduct" name="button" style="margin-bottom:5px" type="submit" data-toggle="modal" data-target="#updateModal">新增商品</button>
            										<!-- <button class="btn btn-success" name="button" style="margin-bottom:5px" type="submit">价格升序</button>
            										<button class="btn btn-warning" name="button" style="margin-bottom:5px" type="submit">价格降序</button> -->
													</div>
            										 </div>
            										 
										<div class="actions">

											<a href="#" class="btn box-remove btn-mini btn-link"><i class="icon-remove"></i> </a>
											<a href="#" class="btn box-collapse btn-mini btn-link"><i></i> </a>

										</div>

									</div>

									<div class="box-content box-no-padding">

										<div class="responsive-table">

											<div class="scrollable-area">

												<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
													<div class="row-fluid">
														<div class="span6">
															<div id="DataTables_Table_0_length" class="dataTables_length">
																<label><select id="pageSize" size="1"
																name="DataTables_Table_0_length"
																aria-controls="DataTables_Table_0"><option
																		value="5" >5</option>
																	<option value="10" selected="selected">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option></select> 每页的记录</label>
															</div>
														</div>
														<div class="span6 text-right">
															<div class="dataTables_filter" id="DataTables_Table_0_filter">
																<label>搜索: <input type="text" id="keyword"
																aria-controls="DataTables_Table_0"></label>
															</div>
														</div>
													</div>
													<table class="data-table table table-bordered table-striped dataTable" style="margin-bottom: 0;" id="table" aria-describedby="DataTables_Table_0_info" >

														<thead>

															<tr role="row">
																<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="productName" data-ascdesc="desc">商品名称</th>
																<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="productInventory" data-ascdesc="desc">库存</th>
																<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="productPrice" data-ascdesc="desc">价格</th>
																<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="productTime" data-ascdesc="desc">上架时间</th>
																<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="productStatus"  data-ascdesc="desc">商品状态</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >操作</th>
															</tr>

														</thead>

														<tbody role="alert" aria-live="polite" aria-relevant="all">
														</tbody>
													</table>
													<div class="row-fluid">
														<div class="span6">
															<div class="dataTables_info" id="showNumber"></div>
														</div>
														<div class="span6 text-right">
															<div class="dataTables_paginate paging_bootstrap pagination pagination-small" id = "pageNum">
															</div>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>

								</div>

							</div>
						</div>
					</div>
			</section>
			
			</div>
			<!-- 改库存 -->
			 <div class="modal hide fade" id="myModal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                                                                              修改库存
                    </h4>
                </div>
                <div class="modal1-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="upInventory" name="upInventory" placeholder="请在这里输入商品库存">
				</div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal">关闭</button>
                <button type="button"  id="input-button" data-dismiss="modal" data-productid="" >
                    		确认修改 <!--成功的按钮-->
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
			<!-- 修改模态框（Modal） -->
			<div class="modal hide fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">
								添加商品
							</h4>
							
						</div>
						<div class="modal-body">
							<form class="form-horizontal" id="subForm" role="form" method="post" enctype="multipart/form-data" >
								<input type="hidden" name="op" value="insertProduct">
								<div class="modal-body">
										<!-- <div class="form-group row" >
												<label for="productId" class="col-sm-2 control-label">商品编号</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="productId"  value=""  name="productId" placeholder="请在这里输入商品Id">
												</div>
										</div> -->
										<div class="form-group row" >
											<label for="productName" class="col-sm-2 control-label">商品名称</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="productName"   value=""  name="productName" placeholder="请在这里输入商品名称" >
											</div>
										</div>
										<input type="hidden" class="form-control" id="sellerId" name="sellerId" value="${sessionScope.seller.sellerId }">
										<div class="form-group row">
					   						 <label for="productInventory" class="col-sm-2 control-label">商品库存</label>
					   						 <div class="col-sm-10">
					   							 <input type="text" class="form-control" id="productInventory" name="productInventory" placeholder="请在这里输入商品库存">
					   						 </div>
					 					</div>
										<div class="form-group row">
											<label for="productPrice" class="col-sm-2 control-label">商品售价</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="productPrice" name="productPrice"  placeholder="请在这里输入商品售价">
											</div>
										</div>
					
										<div class="form-group row">
										<div class="col-sm-10">
												<span >
                						<!--预览图片位置，默认图片-->
                    					<img src="" id="myImg" class="img-circle" width="200px" height="200px" style="position:relative;left:156px;" >
                						</span>
											</div>
											
										</div>
										<div class="form-group row">
											<label for="productPicture" name="productPicture1" class="col-sm-2 control-label">商品图片</label>
											<div class="col-sm-10">
												<input type="file" class="form-control" id="productPicture"  onchange="setImagePriview()" name="productPicture" accept="image/jpg,image/jpeg,image/png">
											</div>
										</div>
										<div class="form-group row">
											<label for="typeFatherId " class="col-sm-2 control-label">父分类</label>
											<div class="col-sm-10">
												<select  id="typeFatherId" name= "typeFatherId" class="from-control">
													<option>请选择分类</option>
												</select>
												
											</div>
											
										</div>
										<div class="form-group row">
											<label for="typeId " class="col-sm-2 control-label">分类</label>
											<div class="col-sm-10">
												<select  id="typeId" name="typeId" class="from-control">
													<option>请选择分类</option>
												</select>
												
											</div>
											
										</div>
										<div class="form-group row">
											<label for="productStatus" class="col-sm-2 control-label">商品状态</label>
											<div class="col-sm-10">
												<select name="productStatus">
													<option value="0">不上架</option>
													<option value="1">上架</option>
												</select>
											</div>
										</div>
										
										<div class="form-group row">
											<label for="productIntroduce " class="col-sm-2 control-label">商品介绍</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="productIntroduce" name="productIntroduce"  placeholder="巴拉巴拉巴拉"  >
											</div>
										</div>
								</div>		
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">关闭
									</button>
									<!-- data-dismiss="modal" 关闭模态框 -->
									<button type="button" class="btn btn-primary" id="insert_button" data-dismiss="modal">
										添加
									</button>
								</div>
							</form>
					</div><!-- /.modal-content -->
			<!-- / jquery -->
			<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
			<!-- / jquery mobile events (for touch and slide) -->
			<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
			<!-- / jquery migrate (for compatibility with new jquery) -->
			<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
			<!-- / jquery ui -->
			<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
			<!-- / bootstrap -->
			<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
			<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
			<!-- / slider nav (address book) -->
			<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
			<script src='assets/javascripts/nav.js' type='text/javascript'></script>
	</body>
	<script type="text/javascript">
		$(document).ready(function(){
			//标杆
			let flag = 1;
			// 页码
			let pageNum = 1;
			// 页面显示数量
			let pageSize = parseInt($("#pageSize").val());
			// 搜索条件
			let keyword = $("#keyword").val();
			
			// 总页数
			let pages = 1;
			// 查询所有并显示
			getLike();
			//查店铺上架的商品
			function sendAjax(){
				$.ajax({
					url:"${pageContext.request.contextPath}/back/SellerProductServlet",
					data:{
						op:"getAll",
						sellerId:"${sessionScope.seller.sellerId}",
						pageNum:pageNum,
						pageSize:pageSize,
						keyword:keyword
					},
					dataType:"json",
					success:function(result,status,xhr){
						
						// 清空表格数据
						$("#table tbody").empty();
						$("#pageNum").empty();
						$("#showNumber").empty();
						
						$.each(result.data,function(index,val){
							// 组装数据行
							let html = '<tr>'
							+'<td>'+val.productName+'</td>'
							+'<td>'+val.productInventory+'</td>'
							+'<td>'+val.productPrice+'</td>'
							+'<td>'+val.productTime+'</td>'
							+'<td>'+(val.productStatus==0?"未上架":"已上架")+'</td>'
							+"<td><button class=\"btn btn-success\" id=\"soldout\" data-productid=\""+""+val.productId+""+"\">下架</td>"
							+'</tr>';
							$("#table tbody").append(html);
						});
						// 判断是否有页数
						if(result.pages > 0 ){
							pages = result.pages;
							// 组装分页栏
							html = '<ul>'
									+'<li class="prev '+(1==result.pageNum?'disabled':'')+'">'
									+'<a href="#">← Previous'
									+'</a>'
									+'</li>'
							for(let i = 1; i <= result.pages; i++){
								if(i == result.pageNum){
									html += '<li class="active">'
									+'<a href="#" class="pageNum">'+i+'</a>'
									+'</li>';
								}else{
									html += '<li>'
									+'<a href="#" class="pageNum">'+i+'</a>'
									+'</li>';
								}
							}
							html += '<li class="next '+(result.pages==result.pageNum?'disabled':'')+'">'
									+'<a href="#">Next → </a>'
									+'</li>'
									+'</ul>';
							$("#pageNum").append(html);
							$("#showNumber").append("从"+((result.pageNum-1)*result.pageSize+1)+"到"+result.pageNum*result.pageSize+"  共"+result.total+"条数据");
						}
					},
					error:function(xhr,status,error){
						alert("ajax请求失败");
					}
				});
			
			}
			//在售商品
			$("#nowshow").click(function(){
				flag=0;
				 sendAjax();
			     
			})
			//全部商品
			function getLike(){
				
				$.ajax({
					url:"${pageContext.request.contextPath}/back/SellerProductServlet",
					data:{
						op:"getLike",
						sellerId:"${sessionScope.seller.sellerId}",
						pageNum:pageNum,
						pageSize:pageSize,
						keyword:keyword
					},
					dataType:"json",
					success:function(result,status,xhr){
						
						// 清空表格数据
						$("#table tbody").empty();
						$("#pageNum").empty();
						$("#showNumber").empty();
						
						$.each(result.data,function(index,val){
							// 组装数据行
							let html = '<tr>'
							+'<td>'+val.productName+'</td>'
							+'<td>'+val.productInventory+'</td>'
							+'<td>'+val.productPrice+'</td>'
							+'<td>'+val.productTime+'</td>'
							+'<td>'+(val.productStatus==0?"未上架":"已上架")+'</td>'
							
							+'<td colspan=\"4\">'+(val.productStatus==0?"":("<button class=\"btn btn-danger\" id=\"soldout\" data-productid=\""+""+val.productId+""+"\">下架 </button>"))
							+"&nbsp;"
							+(val.productStatus==1?"":("<button class=\"btn  btn-success\" id=\"soldon\" data-productid=\""+""+val.productId+""+"\">上架</button> "))
							+"&nbsp;"
							+"<button class=\"btn  btn-info inventory\" id=\"updateInModal\" data-toggle=\"modal\" data-target=\"#myModal_2\" data-productid=\""+""+val.productId+""+"\">修改库存</button> "
							+"&nbsp;"
							+"<button class=\"btn btn-delete\" id=\"btndelete\"  data-productName=\""+""+val.productName+""+"\"data-productid=\""+""+val.productId+""+"\">删除</button></td>"
							+'</tr>';
							$("#table tbody").append(html);
						});
						// 判断是否有页数
						if(result.pages > 0 ){
							pages = result.pages;
							// 组装分页栏
							html = '<ul>'
									+'<li class="prev '+(1==result.pageNum?'disabled':'')+'">'
									+'<a href="#">← Previous'
									+'</a>'
									+'</li>'
							for(let i = 1; i <= result.pages; i++){
								if(i == result.pageNum){
									html += '<li class="active">'
									+'<a href="#" class="pageNum">'+i+'</a>'
									+'</li>';
								}else{
									html += '<li>'
									+'<a href="#" class="pageNum">'+i+'</a>'
									+'</li>';
								}
							}
							html += '<li class="next '+(result.pages==result.pageNum?'disabled':'')+'">'
									+'<a href="#">Next → </a>'
									+'</li>'
									+'</ul>';
							$("#pageNum").append(html);
							$("#showNumber").append("从"+((result.pageNum-1)*result.pageSize+1)+"到"+result.pageNum*result.pageSize+"  共"+result.total+"条数据");
						}
					},
					error:function(xhr,status,error){
						alert("ajax请求失败");
					}
			});
			}
			 //点击删除时
			$(document).on("click",
					"#btndelete",
					function() {
						// 获取id
						let productId = $(this).data(
						"productid");
						let productName = $(this).data(
						"productname");
						
						
						// 下架商品
						if (confirm("是否删除<"+productName+">商品？")) {
							$.ajax({
									url : "${pageContext.request.contextPath}/back/SellerProductServlet",
									data : {
										op : "deleteProduct",
										productId : productId
									},
									success : function() {
										alert("删除成功");
										if(flag==1){
											getLike();
										}else{sendAjax();}
						}
					});
			}
			}); 
			// 显示数目改变时
			$("#pageSize").change(function(){
				
				pageNum = 1;
				pageSize = parseInt($("#pageSize").val());
				if(flag==1){
					getLike();
				}else{sendAjax();}
				
			});
			// 搜索条件输入时
			$("#keyword").blur(function(){
				pageNum = 1;
				keyword = $("#keyword").val();
				if(flag==1){
					getLike();
				}else{sendAjax();}
				
		});
			// 点击下一页时
			$(document).on("click",".next",function(){
				keyword = $("#keyword").val();
				if(pageNum < pages){
					pageNum += 1;
					if(flag==1){
						getLike();
					}else{sendAjax();}
					
				}
			});
			// 点击上一页时
			$(document).on("click",".prev",function(){
				keyword = $("#keyword").val();
				if(pageNum > 1){
					pageNum -= 1;
					if(flag==1){
						getLike();
					}else{sendAjax();}
				}
			});
			// 点击页码时
			$(document).on("click",".pageNum",function(){
				
				pageNum = parseInt($(this).text());
				if(flag==1){
					getLike();
				}else{sendAjax();}
				
			});
			//全部商品
			$("#all").click(function(){
				flag=1;
				getLike();
			})
			//下架
			$(document).on("click",
					"#soldout",
					function() {
						// 获取id
						let productId = $(this).data(
						"productid");
						// 下架商品
						if (confirm("是否下架<"+productId+">商品？")) {
							$.ajax({
									url : "${pageContext.request.contextPath}/back/SellerProductServlet",
									data : {
										op : "soldOut",
										productId : productId
									},
									success : function() {
										alert("下架成功");
										if(flag==1){
											getLike();
										}else{sendAjax();}
						}
					});
			}
			});
			//上架
			$(document).on("click",
					"#soldon",
					function() {
						// 获取id
						let productId = $(this).data(
						"productid");
						// 上架商品
						if (confirm("是否上架<"+productId+">商品？")) {
							$.ajax({
									url : "${pageContext.request.contextPath}/back/SellerProductServlet",
									data : {
										op : "soldon",
										productId : productId
									},
									success : function() {
										alert("上架成功");
										
											getLike();
										
						}
					});
			}
			});
			$(document).on("click",".inventory",function(){
				let productId = $(this).data("productid");
				$("#input-button").data("productid",productId);
				
			})
			
			//更新库存
			$("#input-button").click(function(){
				
				let productId = $(this).data("productid");
				productInventory = $("#myModal_2 #upInventory").val();
				$.ajax({
					async:true,
					url:"${pageContext.request.contextPath}/back/SellerProductServlet",
					type:"POST",
					data:{
						productId:productId,
						  op:"updateInventory",
						  productInventory:productInventory
					},
					dataType:"json",
					success:function(result,status,xhr){
							alert("添加成功")
							getLike();
						},
					
					error:function(xhr,status,error){
						alert("异步请求失败");
					}
			
				})	
					
			})
			
				
			//打开增加模态框：增加商品
			$("#insert_button").click(function(){
				
				var formData = new FormData($("#subForm")[0]);
				$.ajax({
					async:true,
					url:"${pageContext.request.contextPath}/back/SellerProductServlet?op=insertProduct",
					type:"POST",
					data:formData,
					processData:false,
			        contentType:false,
					dataType:"json",
					success:function(result,status,xhr){
							alert("添加成功")
							getLike();
						},
					
					error:function(xhr,status,error){
						alert("异步请求失败");
					}
			
				});	
			})
			
			
			function getObjectURL(file) 
			     {
			       var url = null ;
			       if (window.createObjectURL!=undefined) 
			       { // basic
			         url = window.createObjectURL(file) ;
			       }
			       else if (window.URL!=undefined) 
			       {
			         // mozilla(firefox)
			         url = window.URL.createObjectURL(file) ;
			       } 
			       else if (window.webkitURL!=undefined) {
			         // webkit or chrome
			         url = window.webkitURL.createObjectURL(file) ;
			       }
			       return url ;
			     }
			
			
				// 获取父分类
				$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/back/SellerProductServlet",
						data:{
							op:"getType",
							typeFatherId:0
						},
						dataType:"json",
						success:function(result,status,xhr){
							$.each(result,function(index,val){
								$("#typeFatherId").append('<option value="'+val.typeId+'">'+val.typeName+'</option>');
							});
						},
						error:function(xhr,status,error){
							alert("异步请求失败");
						}
				});
				
				// 修改，点击父分类时
				$(document).on("change","#typeFatherId",function(){
					$("#typeId").empty();
					let typeFatherId = $(this).val();
					console.log(typeFatherId);
					if(typeFatherId == "" || typeFatherId == null || typeFatherId == "请选择分类"){
						$("#typeId").append('<option>请选择分类</option>');
					}else{
						$.ajax({
							async:true,
							url:"${pageContext.request.contextPath}/back/SellerProductServlet",
							data:{
								op:"getType",
								typeFatherId:typeFatherId
							},
							dataType:"json",
							success:function(result,status,xhr){
								$.each(result,function(index,val){
									$("#typeId").append('<option value="'+val.typeId+'">'+val.typeName+'</option>');
								});
							},
							error:function(xhr,status,error){
								alert("异步请求失败");
							}
						});
					}
					
				});
			
			})
			// 获取文件真实名:Servlet 3.0不提供获取上传文件名的方法,通过请求头信息间接获取
    
			function setImagePriview(){
				   // 得到文件列表数组,获取数组中的第一个文件
				   var file = document.getElementById("productPicture").files[0];
				   // 没有选择文件
				   if(file == null) {
				    document.getElementById("myImg").src = "images/2.png";
				    
				   } else {
				    // 构建一个文件渲染对象
				    var reads = new FileReader();
				    // FileReader.readAsDataURL 读取指定Blob或File的内容
				    reads.readAsDataURL(file);
				    // 获取文件数据
				    reads.onload = function() {
				     // 显示图片
				     document.getElementById("myImg").src = this.result;
				    }
				   }

				  }
	</script>

</html>
