<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>

<html>

<head>
   <title>电商管理系统后台</title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
   
   <!--[if lt IE 9]>
   <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
   <![endif]-->
   <link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
   <link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / jquery ui -->
   <link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
   <link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / switch buttons -->
   <link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / xeditable -->
   <link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
   <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / wysihtml5 (wysywig) -->
   <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / jquery file upload -->
   <link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / full calendar -->
   <link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / select2 -->
   <link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / mention -->
   <link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / tabdrop (responsive tabs) -->
   <link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / jgrowl notifications -->
   <link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / datatables -->
   <link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / dynatrees (file trees) -->
   <link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / color picker -->
   <link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / datetime picker -->
   <link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / daterange picker) -->
   <link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / flags (country flags) -->
   <link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / slider nav (address book) -->
   <link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / fuelux (wizard) -->
   <link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
   <!-- / flatty theme -->
   <link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
   <!-- / demo -->
   <link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class='contrast-red '>

<%@include file="sellerPublicPage.jsp" %>

<nav class='' id='main-nav'>

<div class='navigation'>

<div class='search'>
   <form accept-charset="UTF-8" action="search_results.html" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
       <div class='search-wrapper'>
           <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
           <button class="btn btn-link icon-search" name="button" type="submit"></button>
       </div>
   </form>

</div>

<ul class='nav nav-stacked'>

<li class='active'>
   <a href='sellerIndex.jsp'>
       <i class='icon-home'></i>
       <span>首页</span>
   </a>

</li>
<li class=''>
    <a href='sellerSales.jsp'> <i class='icon-dashboard'></i>
        <span>销售管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerManageProduct.jsp'> <i class='icon-magic'></i>
        <span>商品管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerOrderManagement.jsp'><i class='icon-inbox'></i>
        <span>订单管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerComment.jsp'> <i class='icon-comments'></i>
        <span>评论管理</span>
    </a>
</li>
<li>
    <a class="dropdown-collapse" href="#">
        <i class="icon-cog"></i>
        <span>设置</span>
        <i class="icon-angle-down angle-down"></i>
    </a>
    <ul class="nav nav-stacked">
        <li>
            <a  href="${pageContext.request.contextPath }/back/SellerServlet?op=logout">
                <i class="icon-caret-right"></i>
                <span>退出账号</span>
            </a>
         </li>
    </ul>
</li>
</ul>

</div>

</nav>

<section id='content'>

<div class='container-fluid'>

<div class='row-fluid' id='content-wrapper'>

<div class='span12'>

<div class='page-header'>
   <h1 class='pull-left'>
       <i class='icon-home'></i>
       <span id="showSellerName">店铺名称</span>
   </h1>
   <div class='pull-right'>
       <div class='btn-group'>
           <button class="btn btn-white " data-toggle="modal" href="#updatePassword">修改密码</button>
           <button class="btn btn-white sellerDetail" data-toggle="modal" href="#showSellerInfo">店铺信息</button>
       </div>
   </div>

</div>

<div class='row-fluid'>
   <div class='span12 box box-transparent'>
       <div class='row-fluid'>
           <div class='span2 box-quick-link blue-background'>
               <a href='sellerComment.jsp'>
                   <div class='header'>
                       <div class='icon-comments'></div>
                   </div>
                   <div class='content'>评论</div>
               </a>
           </div>
           <div class='span2 box-quick-link green-background sellerDetail' data-toggle="modal" href="#showSellerInfo">
               <a href='#'>
                   <div class='header'>
                       <div class='icon-star'></div>
                   </div>
                   <div class='content'>店铺信息</div>
               </a>
           </div>
           <div class='span2 box-quick-link orange-background'>
               <a href='sellerManageProduct.jsp'>
                   <div class='header'>
                       <div class='icon-magic'></div>
                   </div>
                   <div class='content'>商品管理</div>
               </a>
           </div>
           <div class='span2 box-quick-link purple-background'>
               <a href='sellerSales.jsp'>
                   <div class='header'>
                       <div class='icon-eye-open'></div>
                   </div>
                   <div class='content'>销售管理</div>
               </a>
           </div>
           <div class='span2 box-quick-link red-background'>
               <a href='sellerOrderManagement.jsp'>
                   <div class='header'>
                       <div class='icon-inbox'></div>
                   </div>
                   <div class='content'>订单管理</div>
               </a>
           </div>
           <div class='span2 box-quick-link muted-background' id="reload">
               <a href='#'>
                   <div class='header'>
                       <div class='icon-refresh'></div>
                   </div>
                   <div class='content' >刷新页面</div>
               </a>
           </div>
       </div>
   </div>

</div>

<div class='row-fluid'>
   <div class='span6 box'>
       <div class='box-header'>
           <div class='title'>
               <i class='icon-inbox'></i>
               订单
           </div>
       </div>
       <div class='box-content'>
           <div id='orderChart' style="height:300px;"></div>
       </div>
   </div>
   <div class='span6 box'>
       <div class='box-header'>
           <div class='title'>
               <i class='icon-group'></i>
               用户
           </div>
       </div>
       <div class='box-content'>
           <div id='userChart' style="height:300px;"></div>
       </div>
   </div>

</div>


<hr class='hr-drouble' />

<div class='row-fluid'>
   <div class='span6 box'>
       <div class='box-header'>
           <div class='title'>
               <div class='icon-inbox'></div>
               订单
           </div>
       </div>
       <div class='row-fluid'>
           <div class='span6'>
               <div class='box-content box-statistic'>
                   <h3 class='title text-error' id="newOrderNumber"></h3>
                   <small>新订单</small>
                   <div class='text-error icon-inbox align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-warning' id="finishOrderNumber"></h3>
                   <small>已完成</small>
                   <div class='text-warning icon-check align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-info' id="waitOrderNumber"></h3>
                   <small>等待发货</small>
                   <div class='text-info icon-time align-right'></div>
               </div>
           </div>
           <div class='span6'>
               <div class='box-content box-statistic'>
                   <h3 class='title text-primary' id="shippedOrderNumber"></h3>
                   <small>已发货</small>
                   <div class='text-primary icon-truck align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-success' id="allOrderNumber"></h3>
                   <small>全部订单</small>
                   <div class='text-success icon-flag align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-error' id="collectShopUserNumber"></h3>
                   <small>收藏店铺</small>
                   <div class='text-error icon-heart-empty align-right'></div>
               </div>
           </div>
       </div>
   </div>
   <div class='span3 box'>
       <div class='box-header'>
           <div class='title'>
               <i class='icon-group'></i>
               访问者
           </div>
       </div>
       <div class='row-fluid'>
           <div class='span12'>
               <div class='box-content box-statistic'>
                   <h3 class='title text-error' id="shoppingProductUserNumber"></h3>
                   <small>购买商品用户数</small>
                   <div class='text-error icon-user align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-warning' id="collectProductUserNumber"></h3>
                   <small>收藏商品用户数</small>
                   <div class='text-warning icon-book align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-primary' id="lastOrderDate"></h3>
                   <small>最近下单时间</small>
                   <div class='text-primary icon-time align-right'></div>
               </div>
           </div>
       </div>
   </div>
   <div class='span3 box'>
       <div class='box-header'>
           <div class='title'>
               <i class='icon-comments'></i>
               评论
           </div>
       </div>
       <div class='row-fluid'>
           <div class='span12'>
               <div class='box-content box-statistic'>
                   <h3 class='title text-error' id="allCommentNumber"></h3>
                   <small>评论数</small>
                   <div class='text-error icon-plus align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-success' id="replyCommentNumber"></h3>
                   <small>已回复</small>
                   <div class='text-success icon-ok align-right'></div>
               </div>
               <div class='box-content box-statistic'>
                   <h3 class='title text-info' id="waitReplyCommentNumber"></h3>
                   <small>等待回复</small>
                   <div class='text-info icon-time align-right'></div>
               </div>
           </div>
       </div>
   </div>

</div>


</div>

</div>

</div>

</section>

</div>
<div class="modal hide fade" id="showInventory" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
	    <div class="modal-header">
	        <button class="close" data-dismiss="modal" type="button">×</button>
	        <h3>商品库存</h3>
	    </div>
	    <div class="modal-body">
    		
	    </div>
	    <div class="modal-footer">
	        <button class="btn" data-dismiss="modal">Close</button>
	        <button class="btn btn-primary" data-dismiss="modal" id="subminUpdatePassword">确认修改</button>
	    </div>
</div>
<div class="modal hide fade" id="updatePassword" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
	<form class="form">
	    <div class="modal-header">
	        <button class="close" data-dismiss="modal" type="button">×</button>
	        <h3>修改密码</h3>
	    </div>
	    <div class="modal-body">
	    		<label class="control-label">密码：</label>
	    		<input type="password" class="" id="sellerPassword">
	    </div>
	    <div class="modal-footer">
	        <button class="btn" data-dismiss="modal">Close</button>
	        <button class="btn btn-primary" data-dismiss="modal" id="subminUpdatePassword">确认修改</button>
	    </div>
    </form>
</div>

<div class="modal hide fade" id="showSellerInfo" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">×</button>
        <h3 id="showChange">查看信息</h3><button class="btn btn-primary" id="clickUpdate">修改</button>
    </div>
    <div class="modal-body">
        <form class="form" style="margin-bottom: 0;">
		    <div class="row-fluid">
	           <div class="span12 box">
	               <div class="box-content">
	                   <img  height="200px" width="200px" name="img" id="img" src="#">
	               </div>
	           </div>
	           <div class="span12 box">
	               <span class="btn btn-success fileinput-button">
	              <i class="icon-plus icon-white"></i>
	              <span>更换logo</span>
	              <input data-bfi-disabled=""  disabled multiple="" id="file" type="file" onchange="setImagePriview()">
	              <input type="hidden" name="sellerLogo" >
            </span>
	           </div>
		    </div>
            <div class="control-group">
                <label class="control-label">店铺名称</label>
                <div class="controls">
                    <input disabled class="span5" type="text" name="sellerShopName">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">店铺电话</label>
                <div class="controls">
                    <input disabled class="span5" type="text" name="sellerPhone">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">店铺地址</label>
                <div class="controls">
                    <input disabled class="span5" type="text" name="sellerAddress">
                </div>
            </div>
        </form>
    </div>

    <div class="modal-footer">

        <button class="btn" data-dismiss="modal">取消</button>

        <button class="btn btn-primary" data-dismiss="modal" id = "sellerUpdate">保存记录</button>

    </div>

</div>

<!-- / jquery -->
<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
<!-- / jquery mobile events (for touch and slide) -->
<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
<!-- / jquery migrate (for compatibility with new jquery) -->
<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
<!-- / jquery ui -->
<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
<!-- / bootstrap -->
<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
<!-- / slider nav (address book) -->
<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
<script src='assets/javascripts/nav.js' type='text/javascript'></script>

<!-- 图表echarts导入 -->
<script src='assets/javascripts/echarts.min.js' type="text/javascript"></script>

<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>

</body>
<script type="text/javascript">
	$(document).ready(function(){
		
		// 从session中获取店铺名称
		$("#showSellerName").text("${sessionScope.seller.sellerShopName}");
		// 获取页面数据
		$.ajax({
			url:"${pageContext.request.contextPath}/back/sellerInfo",
			data:{
				op:"data",
				sellerId:"${sessionScope.seller.sellerId}"
			},
			dataType:"json",
			success:function(result,status,xhr){
				$("#allCommentNumber").text(result.allCommentNumber);
				$("#allOrderNumber").text(result.allOrderNumber);
				$("#collectProductUserNumber").text(result.collectProductUserNumber);
				$("#collectShopUserNumber").text(result.collectShopUserNumber);
				$("#finishOrderNumber").text(result.finishOrderNumber);
				$("#lastOrderDate").text(result.lastOrderDate);
				$("#newOrderNumber").text(result.newOrderNumber);
				$("#replyCommentNumber").text(result.replyCommentNumber);
				$("#shippedOrderNumber").text(result.shippedOrderNumber);
				$("#shoppingProductUserNumber").text(result.shoppingProductUserNumber);
				$("#waitOrderNumber").text(result.waitOrderNumber);
				$("#waitReplyCommentNumber").text(result.waitReplyCommentNumber);
				 // 基于准备好的dom，初始化echarts实例
			    var myChart = echarts.init(document.getElementById('orderChart'));
				 // 获取数据
				 // 1. 坐标轴列表
				 // 2. 数据列表
			    // 指定图表的配置项和数据
			    myChart.setOption({
			             title: {
						text: (new Date().getMonth()+1)+"月销售额"
						},
						// 移动y轴
						tooltip: {
						trigger: 'axis'
						},
						// 图例
						legend: {
						data:['销售额(单位：元)']
						},
						// 边框
						grid: {
						left: '0%',
						right: '0%',
						bottom: '0%',
						containLabel: true
						},
						toolbox: {
						feature: {
						  saveAsImage: {}
						}
						},
						// x坐标轴
						xAxis: {
						type: 'category',
						boundaryGap: false,
						data: result.salesDate
						},
						yAxis: {
						type: 'value'
						},
						// 数据
						series: [
						
						{
						  name:'销售额(单位：元)',
						  type:'line',
						  stack: '总量',
						  data:result.salesMoney
						}
					]
			    });
				 // 基于准备好的dom，初始化echarts实例
			    var myChart = echarts.init(document.getElementById('userChart'));
				 // 获取数据
				 // 1. 坐标轴列表
				 // 2. 数据列表
			    // 指定图表的配置项和数据
			    myChart.setOption({
			             title: {
						text: (new Date().getMonth()+1)+"月下单用户数"
						},
						// 移动y轴
						tooltip: {
						trigger: 'axis'
						},
						// 图例
						legend: {
						data:['下单用户数(单位:人)']
						},
						// 边框
						grid: {
						left: '0%',
						right: '0%',
						bottom: '0%',
						containLabel: true
						},
						toolbox: {
						feature: {
						  saveAsImage: {}
						}
						},
						// x坐标轴
						xAxis: {
						type: 'category',
						boundaryGap: false,
						data: result.placeTheOrderDate
						},
						yAxis: {
						type: 'value'
						},
						// 数据
						series: [
						
						{
						  name:'下单用户数(单位:人)',
						  type:'line',
						  stack: '总量',
						  data:result.placeTheOrderNumber
						}
					]
			    });
			},
			error:function(xhr,status,error){
				alert("ajax请求失败");
			}
		});
		
		// 刷新页面
		$("#reload").click(function(){
			location.reload(true);
		});
		
		// 修改密码
		$("#subminUpdatePassword").click(function(){
			// 获取密码
			let sellerPassword = $("#sellerPassword").val();
			// 获取店铺编号
			let sellerId = "${sessionScope.seller.sellerId}";
			sellerId = "${sessionScope.seller.sellerId}";
			// 请求ajax
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerInfo",
				data:{
					op:"updatePassword",
					sellerPassword:sellerPassword,
					sellerId:sellerId
				},
				success:function(){
					alert("修改成功");
				},
				error:function(){
					alert("ajax请求失败");
				}
			});
		});
		
		// 获取店铺信息
		$(".sellerDetail").click(function(){
			// 获取店铺编号
			let sellerId = "${requestScope.seller.sellerId}";
			$.ajax({
				url:"${pageContext.request.contextPath}/back/sellerInfo",
				data:{
					op:"detail",
					sellerId:"${sessionScope.seller.sellerId}"
				},
				dataType:"json",
				success:function(result,status,xhr){
					$("#showSellerInfo img[name='img']").prop("src","${pageContext.request.contextPath}/download?fileName="+result.sellerLogo);
					$("#showSellerInfo input[name='sellerLogo']").val(result.sellerLogo);
					$("#showSellerInfo input[name='sellerShopName']").val(result.sellerShopName);
					$("#showSellerInfo input[name='sellerPhone']").val(result.sellerPhone);
					$("#showSellerInfo input[name='sellerAddress']").val(result.sellerAddress);
				},
				error:function(){
					alert("ajax请求失败");
				}
			});
		});
		
		// 点击修改时
		$("#clickUpdate").click(function(){
			if($(this).text() == '修改'){
				$("#showChange").text("修改信息");
				$(this).text("查看");
				$("#showSellerInfo input[name='sellerPhone'],#showSellerInfo input[name='sellerAddress'],#showSellerInfo input[name='sellerShopName'],#showSellerInfo input[type='file']").attr("disabled",false);
			}else{
				$("#showChange").text("查看信息");
				$(this).text("修改");
				$("#showSellerInfo input[name='sellerPhone'],#showSellerInfo input[name='sellerAddress'],#showSellerInfo input[name='sellerShopName'],#showSellerInfo input[type='file']").attr("disabled",true);
			}

		});
		
		// 店铺更新
		$("#sellerUpdate").click(function(){
			let sellerLogo  = $("#showSellerInfo input[name='sellerLogo']").val();
			let sellerShopName  = $("#showSellerInfo input[name='sellerShopName']").val();
			let sellerPhone = $("#showSellerInfo input[name='sellerPhone']").val();
			let sellerAddress = $("#showSellerInfo input[name='sellerAddress']").val();
			let file = $("#file").get(0).files[0];
			let sellerId = "${sessionScope.seller.sellerId}";
			var fromData = new FormData();
			fromData.append("sellerId",sellerId);
			fromData.append("sellerLogo",sellerLogo);
			fromData.append("sellerShopName",sellerShopName);
			fromData.append("sellerPhone",sellerPhone);
			fromData.append("sellerAddress",sellerAddress);
			fromData.append("file",file);
			// 保存信息
			$.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/back/sellerInfo?op=update",
				data:fromData,
				contentType: false,
				processData: false,
				success:function(){
					$("#showSellerName,.sellerShopName").text(sellerShopName);
				},
				error:function(){
					alert("修改失败");
				}
			});
		});
	});
	
	// 图片预览
	// 选择文件的改变事件,生成图片预览功能
	function setImagePriview() {
		// 得到文件列表数组,获取数组中的第一个文件
		var file = document.getElementById("file").files[0];
		// 没有选择文件
		if(file==null){
			document.getElementById("img").src = "#";
		}else{
			// 构建一个文件渲染对象
			var reads = new FileReader();
			// FileReader.readAsDataURL 读取指定Blob或File的内容
			reads.readAsDataURL(file);
			// 获取文件数据
			reads.onload = function(){
				// 显示图片
				document.getElementById("img").src = this.result;
			}
		}
	}
</script>

</html>
