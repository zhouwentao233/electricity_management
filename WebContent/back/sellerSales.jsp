<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<title>电商管理系统后台</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
	<!--[if lt IE 9]>
   <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
   <![endif]-->
	<link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
	<link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / jquery ui -->
	<link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
	<link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / switch buttons -->
	<link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / xeditable -->
	<link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
	<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / wysihtml5 (wysywig) -->
	<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / jquery file upload -->
	<link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / full calendar -->
	<link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / select2 -->
	<link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / mention -->
	<link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / tabdrop (responsive tabs) -->
	<link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / jgrowl notifications -->
	<link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / datatables -->
	<link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / dynatrees (file trees) -->
	<link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / color picker -->
	<link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / datetime picker -->
	<link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / daterange picker) -->
	<link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / flags (country flags) -->
	<link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / slider nav (address book) -->
	<link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / fuelux (wizard) -->
	<link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
	<!-- / flatty theme -->
	<link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
	<!-- / demo -->
	<link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body class='contrast-red '>
<%@include file="sellerPublicPage.jsp" %>

<nav class='' id='main-nav'>

<div class='navigation'>

<div class='search'>
   <form accept-charset="UTF-8" action="search_results.html" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
       <div class='search-wrapper'>
           <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
           <button class="btn btn-link icon-search" name="button" type="submit"></button>
       </div>
   </form>

</div>

<ul class='nav nav-stacked'>

<li class=''>
   <a href='sellerIndex.jsp'>
       <i class='icon-home'></i>
       <span>首页</span>
   </a>

</li>
<li class='active'>
    <a href='sellerSales.jsp'> <i class='icon-dashboard'></i>
        <span>销售管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerManageProduct.jsp'> <i class='icon-magic'></i>
        <span>商品管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerOrderManagement.jsp'><i class='icon-inbox'></i>
        <span>订单管理</span>
    </a>
</li>
<li class=''>
    <a href='sellerComment.jsp'> <i class='icon-comments'></i>
        <span>评论管理</span>
    </a>
</li>
<li>
    <a class="dropdown-collapse" href="#">
        <i class="icon-cog"></i>
        <span>设置</span>
        <i class="icon-angle-down angle-down"></i>
    </a>
    <ul class="nav nav-stacked">
        <li>
            <a  href="${pageContext.request.contextPath }/back/SellerServlet?op=logout">
                <i class="icon-caret-right"></i>
                <span>退出账号</span>
            </a>
         </li>
    </ul>
</li>
</ul>

</div>

</nav>
			<section id='content'>
				<div class="row-fluid">

    <div class="span6 box" style="margin-bottom: 0">

        <div class="box-header blue-background">

            <div class="title">销售额top10</div>

            

        </div>

        <div class="box-content">

            <div class="tabbable">

                <ul class="nav nav-tabs nav-tabs-centered">

                    <li class="active">

                        <a data-toggle="tab" href="#salesTop10Bar">

                            <i class="icon-indent-left"></i>

                            	条形图

                        </a>

                    </li>

                    <li>

                        <a data-toggle="tab" href="#salesTop10Pie" id="showSalesTopPie">

                            <i class="icon-edit"></i>

                        	    饼状图

                        </a>

                    </li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="salesTop10Bar">

                        <div id="showSalesTop10Bar" style= "height:600px"></div>

                    </div>

                    <div class="tab-pane" id="salesTop10Pie">

                        <div id="showSalesTop10Pie" style= "height:600px"></div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <div class="span6 box" style="margin-bottom: 0">

        <div class="box-header blue-background">

            <div class="title">销量Top10</div>
        </div>

        <div class="box-content">

            <div class="tabbable">

                <ul class="nav nav-tabs nav-tabs-centered">

                    <li class="active">

                        <a data-toggle="tab" href="#numberTop10Pie">

                            <i class="icon-indent-left"></i>

                           	饼状图

                        </a>

                    </li>

                    <li>

                        <a data-toggle="tab" href="#numberTop10Bar" id = "showNumberTopBar">

                            <i class="icon-edit"></i>

                            	条形图

                        </a>

                    </li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="numberTop10Pie">

                        <div id="showNumberTop10Pie" style= "height:600px"></div>


                    </div>

                    <div class="tab-pane" id="numberTop10Bar">

                        <div id="showNumberTop10Bar" style= "height:600px"></div>

                    </div>

                </div>

            </div>

        </div>

    </div>


    </div>
<div class="row-fluid">

    <div class="span12 box">

        <div class="box-header green-background">

            <div class="title">销售统计
            </div>
			<div class="input-append">
				<input id="startTime" class="input-medium daterange" placeholder="开始时间" type="date" >
				</span>
			</div>
			<i class="icon-minus-sign-alt"></i>
			<div class="input-append">
				<input class="input-medium daterange" placeholder="结束时间" type="date" id="endTime">
				</span>
			</div>
			<button class="btn btn-primary" id = "dateSubmit">搜索</button>
        </div>

        <div class="box-content">

            <div class="row-fluid">

                <div class="span12">
                    <div id="showAllSales" style="height:250px;"></div>
                </div>
                
            </div>
            <div class="row-fluid">

                <div class="span12">
                     <div id="showAllNumber" style="height:250px;"></div>
                </div>
                
            </div>

        </div>

    </div>

</div>
</div>
			</section>
			</div>
			<!-- / jquery -->
			<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
			<!-- / jquery mobile events (for touch and slide) -->
			<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
			<!-- / jquery migrate (for compatibility with new jquery) -->
			<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
			<!-- / jquery ui -->
			<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
			<!-- / bootstrap -->
			<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
			<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
			<!-- / slider nav (address book) -->
			<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
			<script src='assets/javascripts/nav.js' type='text/javascript'></script>
			<script src='assets/javascripts/echarts.min.js' type='text/javascript'></script>
	</body>

<script type="text/javascript">
	$(document).ready(function(){
		// 饼状图方法
		function showPie(data,name){
			option = {
			    backgroundColor: '#ffffff',
			    tooltip: {
			        trigger: 'item',
			        formatter: '{a} <br/>{b} : {c} ({d}%)'
			    },
				
			    visualMap: {
			        show: false,
			        min: 80,
			        max: 600,
			        inRange: {
			            colorLightness: [0, 1]
			        }
			    },
			    series: [
			        {
			            name: name,
			            type: 'pie',
			            radius: '55%',
			            center: ['50%', '50%'],
			            data: data.sort(function (a, b) { return a.value - b.value; }),
			            roseType: 'radius',
			            label: {
			                color: 'rgba(0, 0, 0, 0.9)'
			            },
			            labelLine: {
			                lineStyle: {
			                    color: 'rgba(0, 0, 0, 0.7)'
			                },
			                smooth: 0.2,
			                length: 10,
			                length2: 20
			            },
			            itemStyle: {
			                color: new echarts.graphic.LinearGradient(
		                            0, 0, 0, 1,
		                            [
		                                {offset: 0, color: '#f34541'},
		                                {offset: 0.5, color: '#f34541'},
		                                {offset: 1, color: '#f34541'}
		                            ]
		                        ),
			                shadowBlur: 200,
			                shadowColor: 'rgba(0, 0, 0, 0.5)'
			            },

			            animationType: 'scale',
			            animationEasing: 'elasticOut',
			            animationDelay: function (idx) {
			                return Math.random() * 1000;
			            }
			        }
			    ]
			};
			return option;
		}
		// 条形图、折线图方法
		function showBarOrLine(data,name,signs,type,text){
			let show;
			if(data.length < 10 ){
				show = 0;
			}else{
				show = 60;
			}
			option = {
			        color: ['#f34541'],
			        title: {
						text: text
						},
			        tooltip : {  //提示信息
			            trigger: 'axis', //axis轴对称
			            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
			                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			            }
			        },
			        grid: { //网格
			            left: '0%',
			            right: '0%',
			            bottom: '0%',
			            containLabel: true //把y轴的值显示出来
			        },
			        xAxis : [  //X轴的数据
			            {
			                type : 'category',
			                data : name,
			                axisTick: {
			                    alignWithLabel: true
			                },
			                "axisLabel":{
			            		interval: 0,
			            		rotate:show
			            	}

			            }
			        ],
			        yAxis : [  //Y轴的网格已值为准
			            {
			                type : 'value'
			            }
			        ],
			        series : [
			            {
			                name:signs, //提示中的信息
			                type:type, //图的类型 bar条形
			                barWidth: '60%', //每个条形的宽度比例
			                data:data //Y轴数据
			            }
			        ]
			    };
			return option;

		}
		
		// 获取商家编号
		let sellerId = "${sessionScope.seller.sellerId}";
		
		// 获取销量top10
		// 饼状图
		$.ajax({
			url:"${pageContext.request.contextPath}/back/SellerSalesChart",
			data:{
				op:"getNumberTopPie",
				sellerId:sellerId
			},
			dataType:"json",
			success:function(result,status,xhr){
				//初始化echarts实例
			    var myChart = echarts.init(document.getElementById('showNumberTop10Pie'));
			    //使用制定的配置项和数据显示图表
			    myChart.setOption(showPie(result,"销量"));
			},error:function(){
				alert("ajax请求失败");
			}
		});
		
		$("#showNumberTopBar").click(function(){
			// 条形图
			$.ajax({
				url:"${pageContext.request.contextPath}/back/SellerSalesChart",
				data:{
					op:"getNumberTopBar",
					sellerId:sellerId
				},
				dataType:"json",
				success:function(result,status,xhr){
				    //初始化echarts实例
				    var myChart = echarts.init(document.getElementById('showNumberTop10Bar'));
				    //使用制定的配置项和数据显示图表
				    myChart.setOption(showBarOrLine(result.price,result.productName,"销售量","bar"));
				},error:function(){
					alert("ajax请求失败");
				}
			});
		});
		
		// 获取销售额top10
		// 条形图
		$.ajax({
			url:"${pageContext.request.contextPath}/back/SellerSalesChart",
			data:{
				op:"getSalesTopBar",
				sellerId:sellerId
			},
			dataType:"json",
			success:function(result,status,xhr){

			    //初始化echarts实例
			    var myChart = echarts.init(document.getElementById('showSalesTop10Bar'));
			    //使用制定的配置项和数据显示图表
			    myChart.setOption(showBarOrLine(result.price,result.productName,"销售额","bar"));
				
			},error:function(){
				alert("ajax请求失败");
			}
		});
		// 饼状图
		$("#showSalesTopPie").click(function(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/SellerSalesChart",
				data:{
					op:"getSalesTopPie",
					sellerId:sellerId
				},
				dataType:"json",
				success:function(result,status,xhr){
		
				    //初始化echarts实例
				    var myChart = echarts.init(document.getElementById('showSalesTop10Pie'));
				    //使用制定的配置项和数据显示图表
				    myChart.setOption(showPie(result,"销售额/￥"));
					
				},error:function(){
					alert("ajax请求失败");
				}
			});
		});
		
		// 开始时间
		let beginTime = "2020-03-01";
		// 结束时间
		let lastTime = "2020-07-01";
		
		// 销量图表
		let salesNumberChart = echarts.init(document.getElementById('showAllNumber'));
		// 根据时间获取每天销售量
		function getAllSales(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/SellerSalesChart",
				data:{
					op:"getAllNumber",
					sellerId:sellerId,
					beginTime:beginTime,
					lastTime:lastTime
				},
				dataType:"json",
				success:function(result,status,xhr){			

				    //使用制定的配置项和数据显示图表
				    salesNumberChart.setOption(showBarOrLine(result.numberList,result.dateList,"数量","line","销量"));
					
				},error:function(){
					alert("ajax请求失败");
				}
			});
		}
		getAllSales();
		let salesChart = echarts.init(document.getElementById('showAllSales')); 
		// 根据时间获取每天销售额
		function getAllSalesNumber(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/SellerSalesChart",
				data:{
					op:"getAllSales",
					sellerId:sellerId,
					beginTime:beginTime,
					lastTime:lastTime
				},
				dataType:"json",
				success:function(result,status,xhr){
					if(result.moneyList.length<=10){
						salesChart.setOption(showBarOrLine(result.moneyList,result.dateList,"销售额/￥","bar","销售额"));
					}else{
						 //使用制定的配置项和数据显示图表
					    salesChart.setOption(showBarOrLine(result.moneyList,result.dateList,"销售额/￥","line","销售额"));
					}
				},error:function(){
					alert("ajax请求失败");
				}
			});
		}
		getAllSalesNumber();
		
		
		$("#dateSubmit").click(function(){
			beginTime = $("#startTime").val();
			lastTime = $("#endTime").val();
			if(beginTime == "" || lastTime == ""){
				alert("请输入时间");
			}else{
				getAllSales();
				getAllSalesNumber();
			}
		});
	});

</script>

</html>
