<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>卖家注册</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
    
    <!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
    <link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jquery ui -->
    <link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / switch buttons -->
    <link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / xeditable -->
    <link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / wysihtml5 (wysywig) -->
    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jquery file upload -->
    <link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / full calendar -->
    <link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / select2 -->
    <link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / mention -->
    <link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / tabdrop (responsive tabs) -->
    <link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jgrowl notifications -->
    <link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / datatables -->
    <link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / dynatrees (file trees) -->
    <link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / color picker -->
    <link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / datetime picker -->
    <link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / daterange picker) -->
    <link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / flags (country flags) -->
    <link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / slider nav (address book) -->
    <link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / fuelux (wizard) -->
    <link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / flatty theme -->
    <link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
    <!-- / demo -->
    <link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  <style>    .logo{            position:absolute;	        left:350px;	        top:100px;	        }    </style>        </head>
<body class='contrast-red sign-up contrast-background'>
<div id='wrapper'>
    <div class='application'>
        <div class='application-content'>
            <a href="sign_in.html"><div class='icon-heart'></div>
                <span>电商管理系统</span>
            </a>
        </div>
    </div>
    <div class='controls'>           <div class="logo" style=" width:100px; height:100px;">                        <div class='box-content'>                            <img id="img3" alt="230x230&amp;text=photo" src="http://placehold.it/230x230&amp;text=Photo;" width="100px" height="100px";/>                        </div>                    </div>                       
        <div class='caret'></div>
        <div class='form-wrapper'>
            <h1 class='text-center'>卖家注册</h1>
            <form accept-charset="UTF-8" action="${pageContext.request.contextPath}/back/SellerServlet" enctype="multipart/form-data" method="post" ><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>                              <input type="hidden" name="op"  value="register">                         <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <input class="span12" id="sellerAccount" name="sellerAccount" placeholder="卖家账号" type="text" value=""  required="required"/>
                        <i class='icon-user muted'></i>                                                 <label id="sellerAccountTip" style="padding-left:0px"></label>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <input class="span12" id="sellerPassword1" name="sellerPassword1" placeholder="密码长度6-12位" type="password" value="" required="required"/>
                        <i class='icon-lock muted'></i>
                    </div>
                </div>                                  <div class='row-fluid'>                    <div class='span12 icon-over-input'>                        <input class="span12" id="sellerPassword" name="sellerPassword" placeholder="重输密码" type="password" value="" required="required"/>                        <i class='icon-rotate-left'></i>                    </div>                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <input class="span12" id="sellerAddress" name="sellerAddress" placeholder="卖家地址" type="text" value="" required="required" />
                        <i class='icon-home'></i>
                    </div>
                </div>                    <div class='row-fluid'>                    <div class='span12 icon-over-input'>                        <input class="span12" id="sellerPhone" name="sellerPhone" placeholder="卖家电话" type="text" value="" required="required"/>                        <i class='icon-mobile-phone'></i>                                                 <label id="sellerPhoneTip" style="padding-left:0px"></label>                    </div>                                </div>                                   <div class='row-fluid'>                    <div class='span12 icon-over-input'>                        <input class="span12" id="sellerPhoneCode" name="sellerPhoneCode" placeholder="请输入验证码" type="text" value="" required="required"/>                        <i class='icon-key'></i>                                            </div>                                </div>                                                <div class='row-fluid'>                    <div class='span12 icon-over-input'>                        <input class="span12" id="sellerShopName" name="sellerShopName" placeholder="店铺名称" type="text" value="" required="required"/>                                                <i class='icon-smile'></i>                                                 <label id="sellerShopNameTip" style="padding-left:0px"></label>                    </div>                    <label class="" >                                     店铺Logo上传                                        </label>                          <div id="prompt3">					          <!-- accept="image/jpg,image/jpeg,image/png" 限制上传文件的类型-->						      						      <input type="file" id="file"class="filepath" name="sellerLogo" onchange="setImagePriview()" accept="image/jpg,image/jpeg,image/png"> 					          <button id="phoneCode">发送验证码</button>					     					      </div>                                  </div>                                 <div class='row-fluid'>                   <label class="checkbox" for="agreement"><input id="agreement" name="agreement" type="checkbox" value="1" />                                       我接受                                                           <a href="#" class="text-contrast">《用户协议》</a>                                       </label>                                </div>
                <button class="btn btn-block" name="button" type="submit">注册</button>                            </form>
            <div class='text-center'>
                <hr class='hr-normal' />
                <a href="${pageContext.request.contextPath}/back/sellerSignIn.jsp"><i class='icon-chevron-left'></i>
                                  返回登录
                </a>
            </div>
        </div>
    </div>
<!--     <div class='login-action text-center'>
        <a href="forgot_password.html"><i class='icon-lock'></i>
               忘记密码
        </a>
    </div> -->
</div>
<!-- / jquery -->			<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>			<!-- / jquery mobile events (for touch and slide) -->			<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>			<!-- / jquery migrate (for compatibility with new jquery) -->			<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>			<!-- / jquery ui -->			<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>			<!-- / bootstrap -->			<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>			<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>			<!-- / slider nav (address book) -->			<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>			<script src='assets/javascripts/nav.js' type='text/javascript'></script>
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body><script type="text/javascript">		$(document).ready(function(){						// 对手机号输入框绑定失去焦点事件			$("#sellerPhone").blur(function(){				// 获取输入的账号				var sellerPhone = $(this).val().trim();				if(sellerPhone == ""){					$("#sellerPhoneTip").text("手机号不能为空");					$("#sellerPhoneTip").addClass("error");				}else{					// 发送ajax请求					$.ajax({						async:true,						url:"${pageContext.request.contextPath}/back/SellerServlet",						type:"GET",						data:{op:"existsOfSellerPhone",sellerPhone:sellerPhone},						dataType:"text",						success:function(result,status,xhr){							if(result=="true"){ 								$("#sellerPhoneTip").text("手机号已存在");								$("#sellerPhoneTip").addClass("error");								// 注册按钮设置不可用								$("#register").prop("disabled",true);							}else if(result=="false"){// 用户名不存在								$("#sellerPhoneTip").text("手机号可用");								$("#sellerPhoneTip").addClass("success");								// 注册按钮设置可用								$("#register").prop("disabled",false);																																																															}						},						error:function(xhr,status,error){							alert("ajax异步请求失败");						}					});				}			});					// 对账号输入框绑定失去焦点事件			$("#sellerAccount").blur(function(){   				// 获取输入的账号				var sellerAccountName = $("#sellerAccount").val().trim();			if(sellerAccountName == ""){					$("#sellerAccountTip").text("账号不能为空");					$("#sellerAccountTip").addClass("error");				}else{					// 发送ajax请求					$.ajax({						async:true,						url:"${pageContext.request.contextPath}/back/SellerServlet",						type:"GET",						data:{op:"existsOfAccountName",sellerAccount:sellerAccountName},						dataType:"text",						success:function(result,status,xhr){							if(result=="true"){ // 用户名存在								$("#sellerAccountTip").text("账号已存在");								$("#sellerAccountTip").addClass("error");								// 注册按钮设置不可用								$("#register").prop("disabled",true);							}else if(result=="false"){// 用户名不存在								$("#sellerAccountTip").text("账号可用");								$("#sellerAccountTip").addClass("success");								// 注册按钮设置可用								$("#register").prop("disabled",false);							}						},						error:function(xhr,status,error){							alert("ajax异步请求失败");						}					}); 				} 			});									// 对店铺名输入框绑定失去焦点事件			$("#sellerShopName").blur(function(){   				// 获取输入的账号				var sellerShopName = $("#sellerShopName").val().trim();			if(sellerShopName == ""){					$("#sellerShopNameTip").text("店铺名不能为空");					$("#sellerShopNameTip").addClass("error");				}else{					// 发送ajax请求					$.ajax({						async:true,						url:"${pageContext.request.contextPath}/back/SellerServlet",						type:"GET",						data:{op:"existsOfSellerShopName",sellerShopName:sellerShopName},						dataType:"text",						success:function(result,status,xhr){							if(result=="true"){ // 用户名存在								$("#sellerShopNameTip").text("店铺名已存在");								$("#sellerShopNameTip").addClass("error");								// 注册按钮设置不可用								$("#register").prop("disabled",true);							}else if(result=="false"){// 用户名不存在								$("#sellerShopNameTip").text("店铺名可用");								$("#sellerShopNameTip").addClass("success");								// 注册按钮设置可用								$("#register").prop("disabled",false);							}						},						error:function(xhr,status,error){							alert("ajax异步请求失败");						}					}); 				} 			});						// 提示文件上传不符合的操作			var fileuploaderror = "${param.fileuploaderror}";			if(fileuploaderror!=""){				alert("图片文件只能是jpg/jpeg/png格式");			}									//密码验证			 $("#sellerPassword").blur(function(){		            var re = /^\w{6,12}$/;		            if ( $(this).parent().find( "span" ).length > 12 ||  $(this).parent().find( "span" ).length < 6 ) {		                $(this).parent().find( "span" ).remove();    		            }		            if ( re.test( $(this).val() ) ) {		                $(this).after( "<span class='ok'>格式正确</span>" );		            }else {		                $(this).after( "<span class='error'>格式错误</span>" );		            }		        });						//双重密码验证			 $("#sellerPassword").blur(function(){				 				 if($("#sellerPassword").val()!=$("#sellerPassword1").val()){alert("密码不一致")}		           		        });										$("#phoneCode").removeAttr("disabled");				//发送验证码				$("#phoneCode").click(function(){						$.ajax({						url:"${pageContext.request.contextPath}/back/SellerServlet",						data:{							op:"phoneCode",sellerPhone:$("#sellerPhone").val()						},						type:"post",						async:true,						dataType:"text",						success : function(data) {							if(data=='true'){								alert("验证码发送成功");								time(this);							} else {								alert("验证码发送失败");							}						},						error : function() {							alert("error");						}					});				});												//验证验证码				$("#sellerPhoneCode").change(function(){						$.ajax({						url:"${pageContext.request.contextPath}/back/SellerServlet",						data:{							op:"sellerPhoneCodeCheck",sellerPhoneCode:$("#sellerPhoneCode").val()						},						type:"post",						async:true,						dataType:"text",						success : function(data) {							if(data=='true'){								alert("验证成功");							} else {								alert("验证失败");							}						},						error : function() {							alert("error");						}					});				});																				});									// 选择文件的改变事件,生成图片预览功能		function setImagePriview() {			// 得到文件列表数组,获取数组中的第一个文件			var file = document.getElementById("file").files[0];			// 没有选择文件			if(file==null){				document.getElementById("img3").src = "http://placehold.it/230x230&amp;text=Photo";			}else{				// 构建一个文件渲染对象				var reads = new FileReader();				// FileReader.readAsDataURL 读取指定Blob或File的内容				reads.readAsDataURL(file);				// 获取文件数据				reads.onload = function(){					// 显示图片					document.getElementById("img3").src = this.result;				}			}					}							     /* //图片放大功能	     $("#img3").bind("click",function(){            var width = $(this).width();             if(width==100){	                   $(this).width(300);                   $(this).height(300);               }else{                   $(this).width(100);                   $(this).height(100);                }              }); */		</script>
</html>
