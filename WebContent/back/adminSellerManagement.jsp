<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 管理员.店铺管理 -->
<!-- 丁玲 -->
<!DOCTYPE html>
<html>
	<head>
		<title>电商管理系统管理员后台</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
		<!--[if lt IE 9]>
    <script src='<%=request.getContextPath() %>/back/assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery ui -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / switch buttons -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / xeditable -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / wysihtml5 (wysywig) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery file upload -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / full calendar -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / select2 -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / mention -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / tabdrop (responsive tabs) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jgrowl notifications -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datatables -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / dynatrees (file trees) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / color picker -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datetime picker -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / daterange picker) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flags (country flags) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / slider nav (address book) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / fuelux (wizard) -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flatty theme -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
		<!-- / demo -->
		<link href='<%=request.getContextPath() %>/back/assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
	</head>

	<body class='contrast-red '>
	<%@include file="adminPublic.jsp" %>
	<div id='wrapper'>
		<div id='main-nav-bg'></div>
		<nav class='' id='main-nav'>
			<div class='navigation'>
				<ul class='nav nav-stacked'>
					<li class=''><a href='adminUserManagement.jsp'> <i class='icon-group'></i> <span>用户管理</span>
					</a></li>
					<li class='active'><a href='adminSellerManagement.jsp'> <i class='icon-home'></i> <span>店铺管理</span>
					</a></li>
					<li ><a href='adminType.jsp'> <i class='icon-tasks'></i> <span>分类管理</span>
					</a></li>
					<li class=''><a href='adminProduct.jsp'> <i class='icon-magic'></i>
							<span>商品管理</span>
					</a></li>
					<li class=''><a href='adminOrderManagement.jsp'> <i class='icon-inbox'></i> <span>订单管理</span>
					</a></li>
				</ul>
			</div>
		</nav>
			<section id='content'>
				<div class='container-fluid'>
					<div class='row-fluid' id='content-wrapper'>
						<div class='span12'>
							<div class='row-fluid'>
								<div class='span12'>
									<!-- <div class='page-header'>
										<h1 class='pull-left'>
										<i class='icon-table'></i> <span>Tables</span>
									</h1>
										<div class='pull-right'>
											<ul class='breadcrumb'>
												<li>
													<a href="index.html"><i class='icon-bar-chart'></i>
													</a>
												</li>
												<li class='separator'><i class='icon-angle-right'></i></li>
												<li class='active'>Tables</li>
											</ul>
										</div>
									</div> -->
								</div>
							</div>
							<div class="row-fluid">

								<div class="span12 box bordered-box orange-border" style="margin-bottom: 0;">

									<div class="box-header orange-background">

										<div class="title">店铺列表</div>

										<div class="actions">

											<a href="#" class="btn box-remove btn-mini btn-link"><i class="icon-remove"></i> </a>
											<a href="#" class="btn box-collapse btn-mini btn-link"><i></i> </a>

										</div>

									</div>

									<div class="box-content box-no-padding">

										<div class="responsive-table">

											<div class="scrollable-area">

												<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
													<div class="row-fluid">
														<div class="span6">
															
														</div>
														<!-- 搜索框 -->
														<div class="dataTables_filter" id="DataTables_Table_0_filter">
															<label>
															   账号或电话号: 
															    <input type="text" aria-controls="DataTables_Table_0" id="kw" value="">
																<button class="btn btn-success btn-mini" onclick="search()">搜索</button>
																<button class="btn btn-info btn-mini"    onclick="clearSearch()">清除搜索</button>
															</label>
															</br>
															
														</div>
													</div>
<!--==================================================== 表格 ========================================================= -->
													 <table class="data-table table table-bordered table-striped dataTable" style="margin-bottom: 0;" id="DataTables_sellerList" aria-describedby="DataTables_Table_0_info">
														<thead>
															<tr role="row">
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending"  style="width: 164px;">店铺编号</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">店铺账号</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">店铺地址</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">电话号</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">名称</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">logo</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">状态</th>
																<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  style="width: 164px;">操作</th>
															</tr>
														</thead>

														<tbody role="alert" aria-live="polite" aria-relevant="all">
															 
														</tbody>
													</table>
													<div
														class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
														
													</div>
													<div class="row-fluid">
														<div class="span6">
															<div class="dataTables_info" id="DataTables_Table_0_info"></div>
														</div>
														<div class="span6 text-right">
															<div class="dataTables_paginate paging_bootstrap pagination pagination-small">
																
															</div>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>

								</div>

							</div>
						</div>
					</div>
			</section>
			</div>
			<!-- 修改模态框（Modal） -->
			<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">
								修改店铺信息
							</h4>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" role="form" >
								<input type="hidden" name="op" value="updateBySellerId">
								<div class="modal-body">
										<div class="form-group row" >
												<label for="sellerId" class="col-sm-2 control-label">店铺编号</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="sellerId"  readonly="readonly" value=""  name="sellerId" >
												</div>
										</div>
										<div class="form-group row" >
											<label for="sellerAccount" class="col-sm-2 control-label">店铺账号</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="sellerAccount"  readonly="readonly" value=""  name="sellerAccount" >
											</div>
										</div>
										<div class="form-group row">
											<label for="sellerAddress" class="col-sm-2 control-label">店铺地址</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="sellerAddress" name="sellerAddress">
											</div>
										</div>
										<div class="form-group row">
					   						 <label for="sellerPhone" class="col-sm-2 control-label">店铺电话</label>
					   						 <div class="col-sm-10">
					   							 <input type="text" class="form-control" id="sellerPhone" name="sellerPhone">
					   						 </div>
					 					</div>
										<div class="form-group row">
											<label for="sellerShopName" class="col-sm-2 control-label">店铺名称</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="sellerShopName" >
											</div>
										</div>
										<div class="form-group row">
											<label for="sellerLogo" class="col-sm-2 control-label">店铺logo</label>
											<div class="col-sm-10">
												<img  name="img" id="img" src="#" width="150px" height="150px" >
												<!-- <input type="text" class="form-control" id="sellerLogo1" >  -->
										</div>
										<div class="form-group row">
											<label for="sellerStatus" class="col-sm-2 control-label">店铺状态</label>
											<div class="col-sm-10">
												<select id="sellerStatus">
													<option value="0">等待验证</option>
													<option value="1">验证通过</option>
												</select>
											</div>
										</div>
								</div>		
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">关闭
									</button>
									<!-- data-dismiss="modal" 关闭模态框 -->
									<button type="button" class="btn btn-primary" id="update_button" data-dismiss="modal">
										提交更改
									</button>
								</div>
							</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal -->
			</div>
		</div>
			<!-- / jquery -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script> 
			<!-- / jquery mobile events (for touch and slide) -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
			<!-- / jquery migrate (for compatibility with new jquery) -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
			<!-- / jquery ui -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
			<!-- / bootstrap -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
			<!-- / slider nav (address book) -->
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
			<script src='<%=request.getContextPath() %>/back/assets/javascripts/nav.js' type='text/javascript'></script>
			<%-- <script src='<%=request.getContextPath() %>/back/assets/javascripts/tables.js' type='text/javascript'></script> --%>
			<%-- <script src='<%=request.getContextPath() %>/back/assets/javascripts/theme.js' type='text/javascript'></script> --%>
			
			<script type="text/javascript">
				
				$(document).ready(function(){
					//一近页面显示店铺列表
					sendAjax();
					//删除
					$(document).on("click",".delete",function(){
						if(window.confirm("是否确定删除")){
							$.ajax({
								async:true,
								url:"${pageContext.request.contextPath}/AdminSellerManagementServlet",
								type:"GET",
								data:{op:"deleteBySellerId",selectKey:$("#kw").val(),sellerId:$(this).data("sellerid")},
								dataType:"json",
								success:function(result,status,xhr){
									showData(result);
								},
								error:function(xhr,status,error){
									alert("异步请求失败");
								}
							})
						}
					});
					
					//填充数据
					$(document).on("click",".open_update_modal",function(){
						//获取数据
						var sellerId = $(this).data("sellerid");
						var sellerAccount = $(this).data("selleraccount");
						var sellerAddress = $(this).data("selleraddress");
						var sellerPhone = $(this).data("sellerphone");
						var sellerShopName = $(this).data("sellershopname");
						 var sellerLogo = $(this).data("sellerlogo"); 
						var sellerStatus = $(this).data("sellerstatus");
						
						//在指定位置显示
						$("#updateModal #sellerId").val(sellerId);
						$("#updateModal #sellerAccount").val(sellerAccount);
						$("#updateModal #sellerAddress").val(sellerAddress);
						$("#updateModal #sellerPhone").val(sellerPhone);
						$("#updateModal #sellerShopName").val(sellerShopName);
						$("#updateModal img[name='img']").prop("src","${pageContext.request.contextPath}/download?fileName="+sellerLogo);
						/* $("#updateModal #sellerLogo1").val(sellerLogo);  */
						$("#updateModal #sellerStatus").val(sellerStatus);
					});
					
					 //修改
					 $(document).on("click","#update_button",function(){
						var updateSeller = {
								sellerId: $("#updateModal #sellerId").val(),
							/* 	sellerAccount: $("#updateModal #sellerAccount").val(),
								sellerPassword: $("#updateModal #sellerPassword").val(),  */
								sellerAddress: $("#updateModal #sellerAddress").val(),
								sellerPhone: $("#updateModal #sellerPhone").val(),
								sellerShopName: $("#updateModal #sellerShopName").val(),
								/* sellerLogo: $("#updateModal #sellerLogo").val(), */
								sellerStatus: $("#updateModal #sellerStatus").val()
						}
						$.ajax({
							async:true,
							url:"${pageContext.request.contextPath}/AdminSellerManagementServlet",
							type:"POST",
							data:{
								  op:"updateBySellerId",
								  selectKey:$("#kw").val(),
								  updateSeller:JSON.stringify(updateSeller)},
							dataType:"json",
							success:function(result,status,xhr){
								showData(result);
							},
							error:function(xhr,status,error){
								alert("异步请求失败");
							}
						});
						
					});  
					
				});
				//异步请求
				function sendAjax(){
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/AdminSellerManagementServlet",
						type:"GET",
						data:{op:"getSellerList",selectKey:$("#kw").val()},
						dataType:"json",
						success:function(result,status,xhr){
							showData(result);
						},
						error:function(xhr,status,error){
							alert("异步请求失败");
						}
					});
				}
				//搜索
				function search(){
					sendAjax();
				}
				//清除搜索
				function clearSearch(){
					$("#kw").val("");
				}
			
				
				//渲染数据
				function showData(result){
					$("#DataTables_sellerList tbody").empty();
					$.each(result,function(index,seller){
						$("#DataTables_sellerList tbody").append("<tr>"+
						"<td>"+seller.sellerId+"</td>"+
						"<td>"+seller.sellerAccount+"</td>"+
						"<td>"+seller.sellerAddress+"</td>"+
						"<td>"+seller.sellerPhone+"</td>"+
						"<td>"+seller.sellerShopName+"</td>"+
						"<td>"+"<img src=\"${pageContext.request.contextPath}/download?fileName="+seller.sellerLogo+"\"></td>"+
						"<td>"+(seller.sellerStatus==1?"正常":"等待验证")+"</td>"+
						"<td class=\"center\">"+
								"<a href=\"#\" data-toggle=\"modal\" data-target=\"#updateModal\" class=\"open_update_modal\" "+
								"data-sellerid=\""+seller.sellerId+"\""+
								"data-selleraccount=\""+seller.sellerAccount+"\""+
								"data-selleraddress=\""+seller.sellerAddress+"\""+
								"data-sellerphone=\""+seller.sellerPhone+"\""+
								"data-sellershopname=\""+seller.sellerShopName+"\""+
								"data-sellerlogo=\""+seller.sellerLogo+"\""+ 
								"data-sellerstatus=\""+seller.sellerStatus+"\">"+
									"<i class=\"icon-edit\"></i>修改</a> &nbsp;&nbsp;"+
								"<a href=\"javascrtipt:void(0)\" class=\"delete\" data-sellerid=\""+seller.sellerId+"\"><i class=\"icon-remove\"></i>删除</a>"+
						"</td>"+
					"</tr>");
					});
				}
				
			</script>
	</body>

</html>