<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<header>
		<div class='navbar'>
			<div class='navbar-inner'>
				<div class='container-fluid'>
					<a class='brand' href='index.html'> <i class='icon-heart-empty'></i>
						<span class='hidden-phone'>${sessionScope.admin.adminAccount }</span>
					</a> <a class='toggle-nav btn pull-left' href='#'> <i
						class='icon-reorder'></i>
					</a>
					<ul class='nav pull-right'>
						<li class='dropdown dark user-menu'><a
							class='dropdown-toggle' data-toggle='dropdown' href='#'> <img
								alt='Mila Kunis' height='23' src='assets/images/avatar.jpg'
								width='23' /> <span class='user-name hidden-phone'>${sessionScope.admin.adminAccount }</span> <b class='caret'></b>
						</a>
							<ul class='dropdown-menu'>
								<li><a href='AdminUserManagementServlet?op=doLogout'> <i class='icon-signout'></i>
										退出账号
								</a></li>
							</ul></li>
					</ul>
					
				</div>
			</div>
		</div>
	</header>
