<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>卖家登录</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
    
    <!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
    <link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jquery ui -->
    <link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / switch buttons -->
    <link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / xeditable -->
    <link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / wysihtml5 (wysywig) -->
    <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jquery file upload -->
    <link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / full calendar -->
    <link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / select2 -->
    <link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / mention -->
    <link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / tabdrop (responsive tabs) -->
    <link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / jgrowl notifications -->
    <link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / datatables -->
    <link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / dynatrees (file trees) -->
    <link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / color picker -->
    <link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / datetime picker -->
    <link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / daterange picker) -->
    <link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / flags (country flags) -->
    <link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / slider nav (address book) -->
    <link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / fuelux (wizard) -->
    <link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
    <!-- / flatty theme -->
    <link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
    <!-- / demo -->
    <link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class='contrast-red sign-in contrast-background'>
<div id='wrapper'>
    <div class='application'>
        <div class='application-content'>
            <a href="sign_in.html"><div class='icon-heart'></div>
                <span>电商管理系统</span>
            </a>
        </div>
    </div>
    <div class='controls'>
        <div class='caret'></div>
        <div class='form-wrapper'>
            <h1 class='text-center'>卖家登录</h1>
            <form accept-charset="UTF-8" action="${pageContext.request.contextPath}/back/SellerServlet" method="post" ><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>            <input type="hidden" name="op" value="login">
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <input class="span12" id="sellerAccount" name="sellerAccount" placeholder="账号" type="text" value="" />
                        <i class='icon-user muted'></i>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <input class="span12" id="password" name="sellerPassword" placeholder="密码" type="password" value="" />
                        <i class='icon-lock muted'></i>
                    </div>
                </div>
                <label class="checkbox" for="rememberPassword"><input id="rememberPassword" name="rememberPassword" type="checkbox" value="rememberPassword" />
                              记住密码
                </label>
                <button class="btn btn-block" name="button" type="submit">登录</button>
            </form>
        </div>
    </div>
    <div class='login-action text-center'>
        <a href="${pageContext.request.contextPath}/back/sellerSignUp.jsp"><i class='icon-user'></i>
           新用户？
            <strong>创建账户</strong>
        </a>
    </div>
</div> <script src="${pageContext.request.contextPath}/front/js/jquery.min.js"></script>          <script src="${pageContext.request.contextPath}/front/js/matrix.login.js"></script>         <script>        	// 如果登录失败会弹出一个提示框        	var loginErrorMessage = "${param.loginErrorMessage}"; // ${param.参数名} EL表达式获取页面参数值        	if(loginErrorMessage != ""){        		alert("用户名或密码错误,请重新登录!");        	}        </script>
<!-- / jquery --><!--    多用户的记住密码 -->     <script type="text/javascript">    		$(document).ready(function(){    			// 对买家账号输入框做失去焦点事件，当输入完后，判断cookie中是否存在该账号对应的name，如果有则返回value    			$("input[name='sellerAccount']").blur(function(){    				console.log("hello");    			});    			    			    			    			$("input[name='sellerAccount']").blur(function(){    				var sellerAccount = $(this).val(); //卖家登录账号：sellerAccount    				console.log(sellerAccount);    				// 获取cookie指定name的value值    				var sellerPassword = getCookie(sellerAccount);    				console.log(sellerPassword);    				// 获取成功        			if(sellerPassword != ""){        				// 填充数据        				$("input[name='sellerPassword']").val(sellerPassword);        				$("input[name='rememberPassword']").prop("checked",true);        			}else{        				$("input[name='sellerPassword']").val("");        				$("input[name='rememberPassword']").prop("checked",false);        			}    			});    		})    		// 自定义函数，通过cookie中指定的key,获取对应value值    		function getCookie(cname){    			    			var name = cname + "=";  // name="sellerAccount="    				    			var ca = document.cookie.split(';');// 根据分号进行字符串的切割,返回一个字符串数组  ca[0]="username=baby"  ca[1]=" password=123456"    			    			for(var i = 0; i < ca.length; i++) {// 遍历数组    				    				var c = ca[i].trim();   // ca[0]="username=baby"  ca[1]="password=123456"     			    				if (c.indexOf(name)==0) {  // c="username=baby" name="username="      					    					return c.substring(name.length,c.length);  // 字符串裁剪，得到value值    				}    			}    			return "";    		}    </script>
<!-- <script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
/ jquery mobile events (for touch and slide)
<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
/ jquery migrate (for compatibility with new jquery)
<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
/ jquery ui
<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
/ bootstrap
<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
/ sparklines
<script src='assets/javascripts/plugins/sparklines/jquery.sparkline.min.js' type='text/javascript'></script>
/ flot charts
<script src='assets/javascripts/plugins/flot/flot.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/flot/flot.resize.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/flot/flot.pie.js' type='text/javascript'></script>
/ bootstrap switch
<script src='assets/javascripts/plugins/bootstrap_switch/bootstrapSwitch.min.js' type='text/javascript'></script>
/ fullcalendar
<script src='assets/javascripts/plugins/fullcalendar/fullcalendar.min.js' type='text/javascript'></script>
/ datatables
<script src='assets/javascripts/plugins/datatables/jquery.dataTables.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/datatables/jquery.dataTables.columnFilter.js' type='text/javascript'></script>
/ wysihtml5
<script src='assets/javascripts/plugins/common/wysihtml5.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/common/bootstrap-wysihtml5.js' type='text/javascript'></script>
/ select2
<script src='assets/javascripts/plugins/select2/select2.js' type='text/javascript'></script>
/ color picker
<script src='assets/javascripts/plugins/bootstrap_colorpicker/bootstrap-colorpicker.min.js' type='text/javascript'></script>
/ mention
<script src='assets/javascripts/plugins/mention/mention.min.js' type='text/javascript'></script>
/ input mask
<script src='assets/javascripts/plugins/input_mask/bootstrap-inputmask.min.js' type='text/javascript'></script>
/ fileinput
<script src='assets/javascripts/plugins/fileinput/bootstrap-fileinput.js' type='text/javascript'></script>
/ modernizr
<script src='assets/javascripts/plugins/modernizr/modernizr.min.js' type='text/javascript'></script>
/ retina
<script src='assets/javascripts/plugins/retina/retina.js' type='text/javascript'></script>
/ fileupload
<script src='assets/javascripts/plugins/fileupload/tmpl.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/load-image.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/canvas-to-blob.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/jquery.iframe-transport.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/jquery.fileupload.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-fp.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-ui.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/fileupload/jquery.fileupload-init.js' type='text/javascript'></script>
/ timeago
<script src='assets/javascripts/plugins/timeago/jquery.timeago.js' type='text/javascript'></script>
/ slimscroll
<script src='assets/javascripts/plugins/slimscroll/jquery.slimscroll.min.js' type='text/javascript'></script>
/ autosize (for textareas)
<script src='assets/javascripts/plugins/autosize/jquery.autosize-min.js' type='text/javascript'></script>
/ charCount
<script src='assets/javascripts/plugins/charCount/charCount.js' type='text/javascript'></script>
/ validate
<script src='assets/javascripts/plugins/validate/jquery.validate.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/validate/additional-methods.js' type='text/javascript'></script>
/ naked password
<script src='assets/javascripts/plugins/naked_password/naked_password-0.2.4.min.js' type='text/javascript'></script>
/ nestable
<script src='assets/javascripts/plugins/nestable/jquery.nestable.js' type='text/javascript'></script>
/ tabdrop
<script src='assets/javascripts/plugins/tabdrop/bootstrap-tabdrop.js' type='text/javascript'></script>
/ jgrowl
<script src='assets/javascripts/plugins/jgrowl/jquery.jgrowl.min.js' type='text/javascript'></script>
/ bootbox
<script src='assets/javascripts/plugins/bootbox/bootbox.min.js' type='text/javascript'></script>
/ inplace editing
<script src='assets/javascripts/plugins/xeditable/bootstrap-editable.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/xeditable/wysihtml5.js' type='text/javascript'></script>
/ ckeditor
<script src='assets/javascripts/plugins/ckeditor/ckeditor.js' type='text/javascript'></script>
/ filetrees
<script src='assets/javascripts/plugins/dynatree/jquery.dynatree.min.js' type='text/javascript'></script>
/ datetime picker
<script src='assets/javascripts/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js' type='text/javascript'></script>
/ daterange picker
<script src='assets/javascripts/plugins/bootstrap_daterangepicker/moment.min.js' type='text/javascript'></script>
<script src='assets/javascripts/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.js' type='text/javascript'></script>
/ max length
<script src='assets/javascripts/plugins/bootstrap_maxlength/bootstrap-maxlength.min.js' type='text/javascript'></script>
/ dropdown hover
<script src='assets/javascripts/plugins/bootstrap_hover_dropdown/twitter-bootstrap-hover-dropdown.min.js' type='text/javascript'></script>
/ slider nav (address book)
<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
/ fuelux
<script src='assets/javascripts/plugins/fuelux/wizard.js' type='text/javascript'></script>
/ flatty theme
<script src='assets/javascripts/nav.js' type='text/javascript'></script>
<script src='assets/javascripts/tables.js' type='text/javascript'></script>
<script src='assets/javascripts/theme.js' type='text/javascript'></script>
/ demo
<script src='assets/javascripts/demo/jquery.mockjax.js' type='text/javascript'></script>
<script src='assets/javascripts/demo/inplace_editing.js' type='text/javascript'></script>
<script src='assets/javascripts/demo/charts.js' type='text/javascript'></script>
<script src='assets/javascripts/demo/demo.js' type='text/javascript'></script> -->
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body>
</html>
