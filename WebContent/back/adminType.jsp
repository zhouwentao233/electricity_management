<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<title>电商管理系统管理员后台</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
		<!--[if lt IE 9]>
    <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
    <![endif]-->
		<link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery ui -->
		<link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / switch buttons -->
		<link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / xeditable -->
		<link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
		<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / wysihtml5 (wysywig) -->
		<link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jquery file upload -->
		<link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / full calendar -->
		<link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / select2 -->
		<link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / mention -->
		<link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / tabdrop (responsive tabs) -->
		<link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / jgrowl notifications -->
		<link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datatables -->
		<link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / dynatrees (file trees) -->
		<link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / color picker -->
		<link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / datetime picker -->
		<link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / daterange picker) -->
		<link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flags (country flags) -->
		<link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / slider nav (address book) -->
		<link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / fuelux (wizard) -->
		<link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
		<!-- / flatty theme -->
		<link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
		<!-- / demo -->
		<link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>

	<body class='contrast-red '>
	<%@include file="adminPublic.jsp" %>
	<div id='wrapper'>
		<div id='main-nav-bg'></div>
		<nav class='' id='main-nav'>
			<div class='navigation'>
				<ul class='nav nav-stacked'>
					<li class=''><a href='adminUserManagement.jsp'> <i class='icon-group'></i> <span>用户管理</span>
					</a></li>
					<li class=''><a href='adminSellerManagement.jsp'> <i class='icon-home'></i> <span>店铺管理</span>
					</a></li>
					<li class='active'><a href='adminType.jsp'> <i class='icon-tasks'></i> <span>分类管理</span>
					</a></li>
					<li ><a href='adminProduct.jsp'> <i class='icon-magic'></i>
							<span>商品管理</span>
					</a></li>
					<li class=''><a href='adminOrderManagement.jsp'> <i class='icon-inbox'></i> <span>订单管理</span>
					</a></li>
				</ul>
			</div>
		</nav>
			<section id='content'>
				<div class='container-fluid'>
					
					<div class='row-fluid' id='content-wrapper'>
						<div class='span12'>
							<div class='row-fluid'>
								<div class='span12'>
									<div class='page-header'>
										<h1 class='pull-left'>
										<i class='icon-table'></i> <span>商品分类管理</span>
									</h1>
									</div>
								</div>
							</div>
							<div class="row-fluid">

								<div class="span12 box bordered-box orange-border" style="margin-bottom: 0;">

									<div class="box-header orange-background">

										<div class="title">管理全部分类</div>

										<div class="actions">

											<a href="#" class="btn box-remove btn-mini btn-link"><i class="icon-remove"></i> </a>
											<a href="#" class="btn box-collapse btn-mini btn-link"><i></i> </a>

										</div>

									</div>

									<div class="box-content box-no-padding">

										<div class="responsive-table">

											<div class="scrollable-area">

												<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
													<div class="row-fluid">
														<div class="span6">
															<div id="DataTables_Table_0_length" class="dataTables_length">
																<label><select id = "pageSize" size="1"
																name="DataTables_Table_0_length"
																aria-controls="DataTables_Table_0"><option
																		value="5" >5</option>
																	<option value="10" selected="selected">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option></select> 每页的记录</label>
																	<button type="button" class="btn btn-success" style="margin-left:10px;"  data-toggle="modal" href="#add">添加分类</button>
															</div>
														</div>
														<div class="span6 text-right">
															<div class="dataTables_filter" id="DataTables_Table_0_filter">
																<label>搜索: <input id="keyword" type="text"
																aria-controls="DataTables_Table_0"></label>
															</div>
														</div>
													</div>
													<table class="data-table table table-bordered table-striped dataTable" style="margin-bottom: 0;" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">

														<thead>

															<tr role="row">
																<th class="sorting orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="typeFatherName" data-ascdesc="desc">一级分类</th>
																<th class="sorting_asc orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="typeName" data-ascdesc="desc">二级分类</th>
																<th role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"  >操作</th>
															</tr>

														</thead>

														<tbody role="alert" aria-live="polite" aria-relevant="all" id = "typeList">
														</tbody>
													</table>
													<div class="row-fluid">
														<div class="span6">
															<div class="dataTables_info" id="total"></div>
														</div>
														<div class="span6 text-right">
															<div class="dataTables_paginate paging_bootstrap pagination pagination-small" id = "pageNum">
															</div>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>

								</div>

							</div>
						</div>
					</div>
			</section>
			</div>
			<div class="modal hide fade" id="update" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">

                <div class="modal-header">

                    <button class="close" data-dismiss="modal" type="button">×</button>

                    <h3>分类修改</h3>

                </div>

                <div class="modal-body">

                    <form class="form" style="margin-bottom: 0;" id="updateForm">
                    	<input type = "hidden" name = "typeId">

                        <div class="control-group">

                            <label class="control-label">分类名称</label>
                            <div class="controls">
                                <input class="span5" type="text" name = "typeName" required>
                                <p class="help-block">
                            </p></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">父类名称</label>
                            <div class="controls">
                                <select class="span5" name="typeFatherId"></select>
                            </div>

                        </div>
                </div>

	                <div class="modal-footer">
	
	                    <button class="btn" data-dismiss="modal">取消</button>
	
	                    <button class="btn btn-primary" type="submit">确认修改</button>
	
	                </div>
                </form>

            </div>
			<div class="modal hide fade" id="add" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">

                <div class="modal-header">

                    <button class="close" data-dismiss="modal" type="button">×</button>

                    <h3>分类添加</h3>

                </div>

                <div class="modal-body">

                    <form class="form" style="margin-bottom: 0;" id="addForm">
                    	<input type = "hidden" name = "typeId">

                        <div class="control-group">

                            <label class="control-label">分类名称</label>
                            <div class="controls">
                                <input class="span5" type="text" name = "typeName" required>
                                <p class="help-block">
                            </p></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">父类名称</label>
                            <div class="controls">
                                <select class="span5" name="typeFatherId"></select>
                            </div>

                        </div>
                </div>

	                <div class="modal-footer">
	
	                    <button class="btn" data-dismiss="modal">取消</button>
	
	                    <button class="btn btn-primary" type="submit">确认添加</button>
	
	                </div>
                </form>

            </div>
			<!-- / jquery -->
			<script src='assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
			<!-- / jquery mobile events (for touch and slide) -->
			<script src='assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
			<!-- / jquery migrate (for compatibility with new jquery) -->
			<script src='assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
			<!-- / jquery ui -->
			<script src='assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
			<!-- / bootstrap -->
			<script src='assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
			<script src='assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
			<!-- / slider nav (address book) -->
			<script src='assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
			<script src='assets/javascripts/nav.js' type='text/javascript'></script>
	</body>

<script type="text/javascript">
	$(document).ready(function(){
		// 页面显示数量
		let pageSize = parseInt($("#pageSize").val());
		// 搜索条件
		let keyword = $("keyword").val();
		// 页码
		let pageNum = 1;
		// 排序字段
		let orderBy = "typeName";
		// 排序顺序
		let ascDesc = "desc";
		
		// 根据条件查询所有记录
		function sendAjax(){
			$.ajax({
				url:"${pageContext.request.contextPath}/back/adminType",
				data:{
					op:"getAll",
					pageSize:pageSize,
					keyword:keyword,
					pageNum:pageNum,
					orderBy:orderBy,
					ascDesc:ascDesc
				},
				success:function(result,status,xhr){
					// 清空列表和分页
					$("#typeList").empty();
					$("#pageNum").empty();
					$("#total").empty();
					
					$.each(result.data,function(index,val){
						// 显示数据
						let html = '<tr id = "'+val.typeId+'">'
						+'<td>'+val.typeFatherName+'</td>'
						+'<td>'+val.typeName+'</td>'
						+'<td>'
						+'<button class="btn btn-warning typeUpdate"  data-toggle="modal" href="#update" role="button">修改</button>'
						+'<button class="btn btn-danger typeDelete" data-typename = "'+val.typeName+'">删除</button>'
						+'</td>'
						+'</tr>';
					$("#typeList").append(html);
					});
					// 判断是否有页数
					if (result.pages > 0) {
						pages = result.pages;
						// 组装分页栏
						html = '<ul>'
								+ '<li class="prev '
								+ (1 == result.pageNum ? 'disabled'
										: '')
								+ '">'
								+ '<a href="#">← Previous'
								+ '</a>' + '</li>'
						for (let i = 1; i <= result.pages; i++) {
							if (i == result.pageNum) {
								html += '<li class="active">'
										+ '<a href="#" class="pageNum">'
										+ i
										+ '</a>'
										+ '</li>';
							} else {
								html += '<li>'
										+ '<a href="#" class="pageNum">'
										+ i + '</a>'
										+ '</li>';
							}
						}
						html += '<li class="next '
								+ (result.pages == result.pageNum ? 'disabled'
										: '')
								+ '">'
								+ '<a href="#">Next → </a>'
								+ '</li>' + '</ul>';
						$("#pageNum").append(html);
						$("#total").append("从"+ ((result.pageNum - 1)*result.pageSize + 1)+ "到"+ result.pageNum* result.pageSize+ "  共"+ result.total+ "条数据");
			}
				},
				error:function(xhr,status,error){
					
				}
			});
		}
		
		// 查询全部
		sendAjax();
		
		// 显示数目改变时
		$("#pageSize").change(function() {
			pageNum = 1;
			pageSize = parseInt($("#pageSize").val());
			sendAjax();
		});
		// 搜索条件输入时
		$("#keyword").keyup(function() {
			pageNum = 1;
			keyword = $("#keyword").val();
			sendAjax();
		});
		// 点击排序条件时
		$(".orderBy").click(function() {
			pageNum = 1;
			orderBy = $(this).data("name");
			ascDesc = $(this).data("ascdesc");
			if (ascDesc == "asc") {
				$(this).data("ascdesc", "desc");
			} else {
				$(this).data("ascdesc", "asc");
			}
			console.log(orderBy,ascDesc);
			sendAjax();
			
			// 改变图标顺序
			$(".orderBy").removeClass("sorting_asc").removeClass("sorting_desc").addClass("sorting");
			if($(this).data("ascdesc") == "asc"){
				$(this).remove("sorting").addClass("sorting_desc");
			}else{
				$(this).remove("sorting").addClass("sorting_asc");
			}
		});

		// 点击下一页时
		$(document).on("click", ".next", function() {
			if (pageNum < pages) {
				pageNum += 1;
				sendAjax();
			}
		});
		// 点击上一页时
		$(document).on("click", ".prev", function() {
			if (pageNum > 1) {
				pageNum -= 1;
				sendAjax();
			}
		});
		// 点击页码时
		$(document).on("click", ".pageNum", function() {
			pageNum = parseInt($(this).text());
			sendAjax();
		});
		// 点击修改
		$(document).on("click",".typeUpdate",function(){
			// 获取分类id
			let typeId = $(this).parent().parent().prop("id");
			let typeFatherId = null;
			// 获取要修改的分类
			$.ajax({
					url:"${pageContext.request.contextPath}/back/adminType",
					data:{
						op:"detail",
						typeId:typeId
					},success:function(result,status,xhr){
						$("#update input[name='typeId']").val(result.typeId);
						$("#update input[name='typeName']").val(result.typeName);
						typeFatherId = result.typeFatherId;
					},
					error:function(){
						alert("ajax请求失败")
					}
				});
			// 获取全部分类
			$.ajax({
				url:"${pageContext.request.contextPath}/back/adminType",
				data:{
					op:"getFatherType"
				},success:function(result,status,xhr){
					$("#update select[name='typeFatherId']").empty();
					$.each(result,function(index,val){
						if(val.typeId == typeFatherId){
							$("#update select[name='typeFatherId']").append("<option value='"+val.typeId+"' selected>"+val.typeName+"</option>");
						}else{
							$("#update select[name='typeFatherId']").append("<option value='"+val.typeId+"'>"+val.typeName+"</option>");
						}
						
					});
				},
				error:function(){
					alert("ajax请求失败")
				}
			});
			
		});
		// 修改
		$("#update button[type='submit']").click(function(){
			// 获取提交表单数据
			let data = $("#updateForm").serialize();
			$.ajax({
				url:"${pageContext.request.contextPath}/back/adminType?op=update",
				data:data,
				success:function(){
					// 关闭模态框
					 $('#update').modal('hide');
					 sendAjax();
				},
				error:function(){
					alert("ajax请求失败");
				}
			});
			return false;
		});
		// 添加 
		// 设置父类
		$.ajax({
			url:"${pageContext.request.contextPath}/back/adminType",
			data:{
				op:"getFatherType"
			},success:function(result,status,xhr){
				$("#update select[name='typeFatherId']").empty();
				$.each(result,function(index,val){
					$("#add select[name='typeFatherId']").append("<option value='"+val.typeId+"'>"+val.typeName+"</option>");
				});
			},
			error:function(){
				alert("ajax请求失败")
			}
		});
		$("#add button[type='submit']").click(function(){
			// 获取提交表单数据
			let data = $("#addForm").serialize();
			$.ajax({
				url:"${pageContext.request.contextPath}/back/adminType?op=save",
				data:data,
				success:function(){
					// 关闭模态框
					 $('#add').modal('hide');
					 sendAjax();
				},
				error:function(){
					alert("ajax请求失败");
				}
			});
			return false;
		});
		// 删除
		$(document).on("click",".typeDelete",function(){
			// 获取分类id
			let typeId = $(this).parent().parent().prop("id");
			let typeName = $(this).data("typename");
			// 请求删除
			if(confirm("是否确认删除"+typeName+"？")){
				$.ajax({
					url:"${pageContext.request.contextPath}/back/adminType",
					data:{
						op:"delete",
						typeId:typeId
					},success:function(){
						alert("删除成功");
						sendAjax();
					},
					error:function(){
						alert("ajax请求失败")
					}
				});
			}
		});
		
	});

</script>
</html>