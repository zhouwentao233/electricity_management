<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
	  <meta charset="UTF-8">
	
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	
	  <title>Shoping cart</title>
	  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	  <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
	  <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link rel="shortcut icon" href="favicon.ico">
	  <link href="${pageContext.request.contextPath}/front/css/style.css" media="screen" rel="stylesheet" type="text/css">
	  <link href="${pageContext.request.contextPath}/front/css/grid.css" media="screen" rel="stylesheet" type="text/css">
	
	  <script src="${pageContext.request.contextPath}/front/js/jquery-1.7.2.min.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/html5.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jflow.plus.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/radio.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script>	
	</head>
	<body>
	 <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li  class="curent"><a href="order.jsp">订单</a></li>
           <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
	  <section id="main" class="entire_width">
	    <div class="container_12">
	       <div class="grid_12">
			   <h1 class="page_title">我的订单</h1>	
			   <div id="DataTables_Table_0_length" class="dataTables_length">
					<label>
						<select size="1" id="selectPageSize">
							<option value="10" selected="selected">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
							<option value="50">50</option>
						</select>-
						<select size="1" id="orderStatus">
							<option value="0" selected="selected">商品状态</option>
							<option value="1">未发货</option>
							<option value="2">已发货</option>
							<option value="3">已收货</option>
						</select>
						订单编号/商品名称: 
						<input type="text" id="keyword" value="" >
						<button class="btn btn-info" id="searchBtn">搜索</button>
						<button class="btn btn-success" id ="clearSearchBtn">清除搜索</button>
					</label>
				</div>
			   <table class="table table-bordered" id="DataTables_OrdersList">
			   		<thead>
			   			 <tr> 
			   			 	 <th >订单号</th>
			   			 	 <th >下单时间</th>
							 <th >价格</th>
							 <th >状态</th>
							 <th >收货操作</th>
							 <th >订单详情</th>
						  </tr>
			   		</thead>
			   		<tbody>
					 <!--  <tr>
						 <td class="images"><a href="product_page.html"><img src="images/product_6.png" alt="Product 6"></a></td>
						 <td class="bg name">Paddywax Fragrance Diffuser Set, Gardenia Tuberose,<br/> Jasmine, 4-Ounces</td>
						 <td class="edit"><a title="Edit" href="#">Edit</a></td>
						 <td class="bg price"></td>
						 <td class="qty"><input type="text" name="" value="" placeholder="1000" /></td>
						 <td class="bg subtotal">$750.00</td>
						 <td class="close"><a title="close" class="close" href="#"></a></td>
					  </tr> -->
			   		</tbody>
			   </table>
			   <div class="row-fluid">
				<div class="span6" id="DataTable_infoPage">分页简介</div>
				<div class="span6 text-right">
					<div class="dataTables_paginate paging_bootstrap pagination pagination-small" id="DataTables_pageInfo">
						<!-- <ul>
							<li class="prev disabled">
								<a href="#">← Previous
								</a>
							</li>
							<li class="active">
								<a href="#">1</a>
							</li>
							<li>
								<a href="#">2</a>
							</li>
							<li class="next">
								<a href="#">Next → </a>
							</li>
						</ul> -->
					</div>
				</div>
			</div>
	       </div><!-- .grid_12 -->
		</div>
	  </section>
	  <script type="text/javascript">
		$(document).ready(function() {
			// 页面进入，ajax异步亲求服务器响应数据-用户订单列表
			// 调用发送异步请求方法，参数是页码
			sendAjax(1);
			
			// 选择显示状态的下拉框事件
			$(document).on("change","#orderStatus",function(){
				// 调用发送异步请求的方法，参数数页码
				sendAjax(1);
			});
			
			// 选择显示记录的下拉框事件
			$(document).on("change","#selectPageSize",function() {
				// 调用发送异步请求方法，参数是页码
				sendAjax(1);
			});
			
			// 搜索事件
			$(document).on("click","#searchBtn",function() {
				// 调用发送异步请求方法，参数是页码
				sendAjax(1);
			});
			
			// 清除搜索事件
			$(document).on("click","#clearSearchBtn",function() {
				$("#keyword").val("");
				// 调用发送异步请求方法，参数是页码
				sendAjax(1);
			});
			
			//上一页事件
			$(document).on("click",".prev",function() {
				// 获取当前页
				var currentPageNum = $("#pageInfo").data("pagenum");
				if (currentPageNum == 1) {
					alert("已经是第一页，没有上一页！");
				} else {
					var pageNum = parseInt(currentPageNum) - 1;
					// 调用发送异步请求方法，参数是页码
					sendAjax(pageNum);
				}
			});
			
			//下一页事件
			$(document).on("click",".next",function() {
				// 获取当前页
				var currentPageNum = $("#pageInfo").data("pagenum");
				// 获取总页数
				var lastPageNum = $("#pageInfo").data("pagapages");
				if (currentPageNum == lastPageNum) {
					alert("已经是最后一页，没有下一页！");
				} else {
					var pageNum = parseInt(currentPageNum) + 1;
					// 调用发送异步请求方法，参数是页码
					sendAjax(pageNum);
				}
			});
			
			// 页码点击事件
			$(document).on("click",".current",function() {
				var pageNum = $(this).text();
				// 调用发送异步请求方法，参数是页码
				sendAjax(pageNum);
			});
			
			// 修改订单状态，用户确认收货
			$(document).on("click","#updateStatus",function() {
				if (window.confirm("是否确定已收到商品")) {
					var orderId = $(this).data("orderid");
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/OrdersServlet",
						type:"GET",
						data:{op:"updataOrderStatus",
							orderId:orderId,
							orderStatus:"3"
						},
						dataType:"json",
						success:function(result,status,xhr) {
							// 调用发送异步请求方法，参数是页码
							sendAjax(1);
						},
						error:function() {
							alert("修改状态失败");
						}
					});
				}
			});
		});	
		// 发送ajax异步请求，参数pageNum页码
		function sendAjax(pageNum) {
			$.ajax({
				async:true,
				url:"${pageContext.request.contextPath}/front/OrdersServlet",
				type:"GET",
				data:{op:"getOrdersListByuserId",
					userId:"${sessionScope.user.userId}",
					keyword:$("#keyword").val(),
					orderStatus:$("#orderStatus").val(),
					pageNum:pageNum,
					pageSize:$("#selectPageSize").val()},
				dataType:"json",
				success:function(result,status,xhr) {
					showData(result);
				},
				error:function(xhr,status,error) {
					alert("异步请求失败")
				}
			});
		}
		// 渲染表格
		function showData(result) {
			// 清空表格数据
			$("#DataTables_OrdersList tbody").empty();
			// 清空分页信息简介原始数据
			$("#DataTable_infoPage").empty();
			// 清空分页信息原始数据
			$("#DataTables_pageInfo").empty();
			// 1.填充数据表格
			$.each(result.data,function(index,orders){		
				var temp;
				if (orders.orderStatus == 1) {
					temp="未发货";
					$("#DataTables_OrdersList tbody").append("<tr class=\"even\">"+
						"<td class=\"  sorting_1\">"+orders.orderId+"</td>"+
						"<td class=\" \">"+orders.orderTime+"</td>"+
						"<td class=\" \">"+orders.orderTotalPrice+"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn\"style=\"margin-bottom:5px\">"+temp+"</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-danger\" style=\"margin-bottom:5px\">待发货</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-primary\" id=\"orderDetail\" style=\"margin-bottom:5px\" data-orderdetail=\""+orders.orderId+"\">订单详情</button>"+
						"</td>"+
					"</tr>");
				} else if (orders.orderStatus == 2) {
					temp="已发货";
					$("#DataTables_OrdersList tbody").append("<tr class=\"even\">"+
						"<td class=\"  sorting_1\">"+orders.orderId+"</td>"+
						"<td class=\" \">"+orders.orderTime+"</td>"+
						"<td class=\" \">"+orders.orderTotalPrice+"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-inverse\"style=\"margin-bottom:5px\">"+temp+"</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-info\" id=\"updateStatus\" style=\"margin-bottom:5px\" data-orderid=\""+orders.orderId+"\">待收货</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-primary\" id=\"orderDetail\" style=\"margin-bottom:5px\" data-orderdetail=\""+orders.orderId+"\">订单详情</button>"+
						"</td>"+
					"</tr>");
				} else if (orders.orderStatus == 3) {
					temp="已完成";
					$("#DataTables_OrdersList tbody").append("<tr class=\"even\">"+
						"<td class=\"  sorting_1\">"+orders.orderId+"</td>"+
						"<td class=\" \">"+orders.orderTime+"</td>"+
						"<td class=\" \">"+orders.orderTotalPrice+"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-success\"style=\"margin-bottom:5px\">"+temp+"</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-success\" style=\"margin-bottom:5px\">订单已完成</button>"+
						"</td>"+
						"<td class=\" \">"+
							"<button class=\"btn btn-primary\" id=\"orderDetail\" style=\"margin-bottom:5px\" data-orderdetail=\""+orders.orderId+"\">订单详情</button>"+
						"</td>"+
					"</tr>");
				}
					
			});
			// 填充分页信息
			// 分页信息
			$("#DataTable_infoPage").append("<span id=\"pageInfo\" data-pagenum=\""+result.pageNum+"\" data-pagesize=\""+result.pageSize+"\" data-pagetotal=\""+ result.pageSize+"\" data-pagepages=\""+result.pages+"\">"+
			"每页"+result.pageSize+"条 ,共"+result.total+"条,第"+result.pageNum+"/"+result.pages+"页</span>");
			// 上一页
			let html = "<ul><li class=\"prev\"><a href=\"#\">← Previous</a></li>";
			// 显示页码
			for (var i = 1; i <= result.pages; i++) {
				// 判断i==当前页，添加样式，disabled不可以用，激活状态
				if (i == result.pageNum) {
					html += "<li class=\"current active\"><a href=\"#\">"+i+"</a></li>";
				} else {
					html += "<li class=\"current\"><a href=\"#\">"+i+"</a></li>";
				}
			}
			// 下一页
			html += "<li class=\"next\"><a href=\"#\">Next → </a></li></ul>";
			$("#DataTables_pageInfo").append(html);
		}
	  </script>
	  <script type="text/javascript">
		$(function(){
			$(document).on("click","#orderDetail",function(){
				let orderId = $(this).data("orderdetail");
				location.href = "${pageContext.request.contextPath}/front/orderDetail.jsp?orderId=" + orderId;			
			});
		});
	</script>
	</body>
</html>