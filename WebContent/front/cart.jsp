<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
	  <meta charset="UTF-8">
	
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	
	  <title>购物车</title>
	  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	  <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
	  <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link rel="shortcut icon" href="favicon.ico">
	  <link href="${pageContext.request.contextPath}/front/css/style.css" media="screen" rel="stylesheet" type="text/css">
	  <link href="${pageContext.request.contextPath}/front/css/grid.css" media="screen" rel="stylesheet" type="text/css">
	
	  <script src="${pageContext.request.contextPath}/front/js/jquery-1.7.2.min.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/html5.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jflow.plus.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/radio.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script>	
	</head>
		<script type="text/javascript">
		// javasctipt函数事件
		function orderDetail() {
			location.href = "${pageContext.request.contextPath}/front/OrderServlet?op=insertOrderId";
		}
	</script>
	<body>
 <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li class="curent">
              <a href="cart.jsp">购物车</a>
            </li>
            <li ><a href="order.jsp">订单</a></li>
            <li><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
	  <section id="main" class="entire_width">
	    <div class="container_12">
	       <div class="grid_12">
			   <h1 class="page_title">购物车</h1>	   
			   <form action="${pageContext.request.contextPath}/front/OrderDetailServlet" method="post" >
			   <input type="hidden" name="op" value="insertOrder" />
				   	<table class="cart_product" id="DateTables_CartList">
			   		<thead>
			   			 <tr>
			   			 	 <th><input type="checkbox" value="" id="checkAll" />全选</th>
			   			 	 <th class="producID" ><input type="hidden" value="productid"></th>
			   			 	 <th class="productName">商品名称</th>
							 <th class="images">商品图片</th>
							 <th class="bg name">商品简介</th>
							 <!-- <th class="edit"> </th> -->
							 <th class="bg price">单价</th>
							 <th class="qty">数量</th>
							 <th class="bg subtotal">价格</th>
							 <th class="">操作</th>
						  </tr>
			   		</thead>
			   		<tbody>
						 <!--  <tr>
							 <td class="images"><a href="product_page.html"><img src="images/product_6.png" alt="Product 6"></a></td>
							 <td class="bg name">Paddywax Fragrance Diffuser Set, Gardenia Tuberose,<br/> Jasmine, 4-Ounces</td>
							 <td class="edit"><a title="Edit" href="#">Edit</a></td>
							 <td class="bg price"></td>
							 <td class="qty"><input type="text" name="" value="" placeholder="1000" /></td>
							 <td class="bg subtotal">$750.00</td>
							 <td class="close"><a title="close" class="close" href="#"></a></td>
						  </tr> -->
				   		</tbody>
			   		
			   		
			   		</table>
			   		<label>
			       		合计： <span id="showAllPrice">0</span>元
			       		<button class="btn btn-danger" style="margin-bottom:5px" id="submitOrders" type="submit">提交订单</button>
			   		</label>	
			   	</form>
	       </div><!-- .grid_12 -->
		</div>
	  </section>
	  <script type="text/javascript">
	  		$(document).ready(function() {
	  			let cartId = "1";
	  			let updateNumber = "1";
	  			let updatePrice = "1";
	  			// 1.进入页面，ajax异步请求服务器响应数据-购物车列表
	  			if("${sessionScope.user.userId}" != ""){
	  				sendAjax();		
	  			}
	  			  			
	  			// 2.删除事件
	  			$(document).on("click",".delete",function() {
	  				var $delete = $(this).parent().parent();
	  				if (window.confirm("是否确认删除")) {
						var cartId = $(this).data("cartid");
						$.ajax({
							async:true,
							url:"${pageContext.request.contextPath}/front/CartServlet",
							type:"GET",
							data:{op:"deleteByCartId",cartId:cartId, userId:"${sessionScope.user.userId}"},
							dataType:"json",
							success:function(result,status,xhr) {
								// 调用数据渲染的方法
								// showData(result);
								$delete.remove();
								showChat("${sessionScope.user.userId}");
							},
							error:function(xhr,status,error) {
								alert("删除失败");
							}
						});
						// 调用计算总价格的函数
		  				cartTotalPrice();
		  				// 调用通过商品数量的变化修改数据库的函数
		  				updateCartCountPrice();
						// 调用判断全选复选框状态的函数
		  				checkCheckAllStatus();
						// 当购物车中没有商品的时候，全选复选框设置为不可用状态
						var productCheckboxCount = $(":checkbox[name='productCheckbox']").length;
						if (productCheckboxCount == 0) {
							$("#checkAll").prop("disabled",true);
							$("#checkAll").prop("checked",false);
						}
					}
	  			});	  								  			
	  			
	  			// 加号按钮事件
	  			$(document).on("click",".addNumber",function() {
	  				cartId = $(this).parent().parent().data("cartid");
	  				// 获取数量框对象原来的值
	  				var $numberObject = $(this).prev();
	  				var oldNumber = $numberObject.val();
	  				// 当数量=1的时候，释放减号按钮的不可以状态 改成 可用状态
	  				if (oldNumber == 1) {
						$(this).prev().prev().prop("disabled",false);
					}
	  				// 对数量输入框重新赋值
	  				var newNumber = parseInt(oldNumber) + 1;
	  				$numberObject.val(newNumber);	  				
	  				// 对单个商品总价格重新赋值
	  				var price = $(this).parent().prev().text();
	  				$(this).parent().next().text(price * newNumber);	  				
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  				// 调用通过商品数量的变化修改数据库的函数
	  				updateNumber = $(this).prev().val();
	  				updatePrice = $(this).parent().next().text();
	  				updateCartCountPrice(cartId,updateNumber,updatePrice);
	  			});
	  			
	  			// 减号按钮事件
	  			$(document).on("click",".reduceNumber",function() {
	  				cartId = $(this).parent().parent().data("cartid");
	  				// 获取数量框对象原来的值
	  				var $numberObject = $(this).next();
	  				var oldNumber = $numberObject.val();
	  				// 当数量=1的时候，释放减号按钮的不可以状态 改成 可用状态
	  				if (oldNumber > 1) {
	  					// 对数量输入框重新赋值
		  				var newNumber = parseInt(oldNumber) -1;
		  				$numberObject.val(newNumber);
		  				// 对单个商品总价格重新赋值
		  				var price = $(this).parent().prev().text();
		  				$(this).parent().next().text(price * newNumber);
					} else {
						alert("商品数量最少为1,不能再减少!");
						// 将减号按钮设置为不可用状态
						$(this).prop("disabled",true);
	  				}
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  				// 调用通过商品数量的变化修改数据库的函数
	  				updateNumber = $(this).next().val();
	  				uppdataPrice = $(this).parent().next().text();
	  				updateCartCountPrice(cartId,updateNumber,uppdataPrice);
	  			});
	  			
	  			// 对数量框监听的改变事件 keyup键盘输入 change数值改变事件
	  			$(document).on("keyup change",".number",function() {
	  				cartId = $(this).parent().parent().data("cartid");
	  				// 获取数量框的值
	  				var proNumber = $(this).val();
	  				// 判断用户输入的值如果小于1，则设置为1
	  				if (proNumber < 1 && proNumber != "") {
						$(this).val(1);
						proNumber = 1;
					}
	  				// 对单个商品总价格重新赋值
	  				var price = $(this).parent().prev().text();
	  				$(this).parent().next().text(price * proNumber);
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  				// 调用通过商品数量的变化修改数据库的函数
	  				updateNumber = $(this).val();
	  				uppdataPrice = $(this).parent().next().text();
	  				updateCartCountPrice(cartId,updateNumber,uppdataPrice);
	  			});
	  			
	  			// 对数量框监听值的改变事件 blur失去焦点事件判断用户是否有输入
	  			$(document).on("blur",".number",function() {
	  				cartId = $(this).parent().parent().data("cartid");
	  				// 获取数量框的值
	  				var proNumber = $(this).val();
	  				// 判断用户输入的值如果小于1，则设置为1
	  				if (proNumber < 1 || proNumber == "") {
						$(this).val(1);
						proNumber = 1;
					}
	  				// 对单个商品的总价格重新赋值
	  				var price = $(this).parent().prev().text();
	  				$(this).parent().next().text(price * proNumber);
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  				// 调用通过商品数量的变化修改数据库的函数
	  				updateNumber = $(this).val();
	  				uppdataPrice = $(this).parent().next().text();
	  				updateCartCountPrice(cartId,updateNumber,uppdataPrice);
	  			});
	  			
	  			// 全选复选框绑定事件
	  			$("#checkAll").click(function() {
	  				// 将当前全复选框的checked属性值，赋值给每一个商品复选框
	  				$(":checkbox[name='productCheckbox']").prop("checked",$(this).prop("checked"));
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  				
	  			});
	  			
	  			// 每一个商品复选框做事件
	  			$(document).on("click",":checkbox[name='productCheckbox']",function() {
	  				// 调用判断全选复选框状态的函数
	  				checkChenkAllStatus();
	  				// 调用计算总价格的函数
	  				cartTotalPrice();
	  			});
	  			
	  			// 判断全选复选框状态的函数、单个商品复选框、删除、加入购物车
	  			function checkChenkAllStatus() {
	  				// 获取商品复选框的总个数
	  				var productCheckboxCount = $(":checkbox[name='productCheckbox']").length;
	  				// 获取商品复选框选中的个数
	  				var checkedCount = $(":checkbox[name='productCheckbox']:checked").length;
	  				// 比较选中的个数和总个数
	  				if (productCheckboxCount == checkedCount) {
						$("#checkAll").prop("checked",true);
					} else {
						$("#checkAll").prop("checked",false);
					}	  				
	  			}
	  			
	  			// 计算总价格的函数、1全选复选框、2单个商品复选框、3删除商品按钮、4数量的减号按钮、5加号按钮、6数量框、7加入购物车按钮商品已存在时
	  			function cartTotalPrice() {
	  				// 声明一个变量
	  				var totalPrice = 0;
	  				// 遍历 复选框被选中的商品列表
	  				$(":checkbox[name='productCheckbox']:checked").each(function() {
	  					totalPrice += parseInt($(this).parents("tr").find(".subtotal").text());
	  				});
	  				// 显示在指定的位置上
	  				$("#showAllPrice").text(totalPrice);
	  			}
	  			
	  		});
	  		
	  		/**
			* 发送异步请求
			*/
	  		function sendAjax() {
	  			$.ajax({
	  				async:true,
	  				url:"${pageContext.request.contextPath}/front/CartServlet",
	  				type:"GET",
	  				data:{op:"getCartList", userId:"${sessionScope.user.userId}"},
	  				dataType:"json",
	  				success:function(result,status,xhr) {
	  					showData(result);
	  				},
	  				error:function(xhr,status,error) {
	  					alert("连接失败");
	  				}
	  			});
	  		}	  		
	  		/**
			* 表格数据的渲染		
			*/
			function showData(result) {
	  			// 清空表格原数据
	  			$("#DateTables_CartList tbody").empty();
	  			// 渲染表格中的数据
	  			$.each(result,function(index,cart) {
	  				$("#DateTables_CartList tbody").append("<tr data-cartid=\""+cart.cartId+"\">"+
  						"<td><input type=\"checkbox\" name=\"productCheckbox\" value=\""+cart.cartId+"\" data-cartid=\""+cart.cartId+"\" ></td>"+
  						"<td class=\"producID\" ><input type=\"hidden\" value=\""+cart.product.productId+"\" data-productid=\""+cart.product.productId+"\"></td>"+
  						 "<td class=\"product\" >"+cart.product.productName+"</td>"+
						 "<td class=\"images\" >"+
						 	"<a href=\"showProductDetail.jsp?productId="+cart.product.productId+"\"><img src=\"${pageContext.request.contextPath}/download?fileName="+cart.product.productPicture+"\" alt=\"Product 6\"></a></td>"+
						 "<td class=\"bg name\" >"+cart.product.productIntroduce+"</td>"+
						 "<td class=\"bg price\">"+cart.product.productPrice+"</td>"+
						 "<td class=\"qty\">"+
						 	"<input type=\"button\" value=\"-\" class=\"reduceNumber\">"+
						 	"<input type=\"number\" value=\""+cart.cartCount+"\" class=\"number\" min=\"1\" data-cartcount=\""+cart.cartCount+"\">"+
						 	"<input type=\"button\" value=\"+\" class=\"addNumber\"></td>"+
						 "<td class=\"bg subtotal\" data-cartprice=\""+cart.cartPrice+"\">"+ cart.cartPrice+"</td>"+
						 "<td><a title=\"close\" class=\"delete\" data-cartid=\""+cart.cartId+"\" href=\"#\">删除</a></td>"+
					  "</tr>");
	  			});
	  		}
	  		
			/**
			* 修改商品数量和价格		
			*/
	  		function updateCartCountPrice(cartId,updateNumber,updatePrice) {
	  			$.ajax({
  					async:true,
  					url:"${pageContext.request.contextPath}/front/CartServlet",
  					type:"GET",
  					data:{op:"updateByCartId",cartId:cartId,proNumber:updateNumber,total:updatePrice,userId:"${sessionScope.user.userId}"},
  					dataType:"json",
  					success:function(result,status,xhr){
  						console.log(result);
  						showChat("${sessionScope.user.userId}");
  					},
  					error:function(xhr,status,error){
  						console.log("商品数量有误，请重新修改商品数量");
  					}
  				});
			}
	  		
	  </script>
	  <script type="text/javascript">
	  	var addressError = "${param.addressError}";
	  	if (addressError != "") {
			alert("请填写默认的收货地址！");
		}
	  </script>
	</body>
</html>