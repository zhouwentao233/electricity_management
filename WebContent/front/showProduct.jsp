<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE HTML>
<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>电商平台</title>

  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">
 
  <script src="js/jquery-1.7.2.min.js" ></script>
  <script src="js/html5.js" ></script>
  <script src="js/jflow.plus.js" ></script>
  <script src="js/jquery.carouFredSel-5.2.2-packed.js"></script>
  <script src="js/checkbox.js"></script>
  <script src="js/radio.js"></script>
  <script src="js/selectBox.js"></script>
  <script src="js/showPage.js"></script>

  <script>
	$(document).ready(function() {
		$("select").selectBox();
	});
  </script>

  <script>
	$(document).ready(function(){
	    $("#myController").jFlow({
			controller: ".control", // must be class, use . sign
			slideWrapper : "#jFlowSlider", // must be id, use # sign
			slides: "#slider",  // the div where all your sliding divs are nested in
			selectedWrapper: "jFlowSelected",  // just pure text, no sign
			width: "984px",  // this is the width for the content-slider
			height: "480px",  // this is the height for the content-slider
			duration: 400,  // time in miliseconds to transition one slide
			prev: ".slidprev", // must be class, use . sign
			next: ".slidnext", // must be class, use . sign
			auto: true	
		 });
	});
   </script>
   <script>
	$(function() {
	  $('#list_product').carouFredSel({
		prev: '#prev_c1',
		next: '#next_c1',
		auto: false
	  });
          $('#list_product2').carouFredSel({
		prev: '#prev_c2',
		next: '#next_c2',
		auto: false
	  });
	  $(window).resize();
	});
   </script>
   <script>
       $(document).ready(function(){
	      $("button").click(function(){
		     $(this).addClass('click')
	      });             
       })
  </script>
    
</head>
<body>
  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li ><a href="index.jsp">首页</a></li>
            <li class="curent"><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
  
  
  <section id="main">
    <div class="container_12">
       <div id="sidebar" class="grid_3">
	      <aside id="categories_nav">
		     <h3></h3>
		     
		     <nav class="left_menu">
			    
		     </nav><!-- .left_menu -->
	      </aside><!-- #categories_nav -->
	      
	      </div><!-- .sidebar -->
      
       <div id="content" class="grid_9">
	      <h1 class="page_title">商品 列表</h1>
	      
	      <div class="options">
		     <div class="grid-list">
			   <a class="grid curent" id = "showTable"><span>img</span></a>
			   <a class="list" id = "showList"><span>img</span></a>
		     </div><!-- .grid-list -->
		     
		     <div class="show">
			    显示
			    <select id = "pageSize">
				   <option value="3">3</option>
				   <option value="6">6</option>
				   <option value="9">9</option>
				   <option value="12" selected>12</option>
				   <option value="15">15</option>
				   <option value="18">18</option>
				   <option value="21">21</option>
			     </select>
			    
			    条&nbsp;&nbsp;数据
		     </div><!-- .show -->
		     
	      </div><!-- .options -->
	      
	      <div class="grid_product" id= "showProduct">
		     
		     
	    
	      <div class="clear"></div>
	      </div><!-- .grid_product -->
	      
	      <div class="clear"></div>
	      
	      <div class="pagination" id = "page">
		     
	      </div><!-- .pagination -->
       </div><!-- #content -->
       
      <div class="clear"></div>
      
    </div><!-- .container_12 -->
  </section><!-- #main -->
  
  <div class="clear"></div>
    
 
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		//当前页码
		let pageNum = 1;
		// 当前页面数量
		let pageSize = 12;
		// 最大页数
		let pages = 1;
		// 一级目录
		let oneType = null;
		// 二级目录
		let twoType = null;
		// 显示样式 0为表格 1为列表
		let style = 0;
		
		// 页面列表显示方式
		function productListShow(val){
			html = '<div class="product_li">'
				+'<div class="grid_3">'
				//+'<img class="sale" src="images/new.png">'
				+'<div class="prev">'
				+'<a href="${pageContext.request.contextPath}/front/showProductDetail.jsp?productId='+val.productId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.productPicture+'" alt="" title=""></a>'
				+'</div>'
				+'</div>'
				+'<div class="grid_4">'
				+'<div class="entry_content">'
				+'<a href="product_page.html"><h3 class="title">'+val.productName+'</h3></a>'
				+'<p>'+val.productIntroduce+'</p>'
				+'<a class="more" href="product_page.html">详情</a>'
				+'</div>'
				+'</div>'
				+'<div class="grid_2">'
				+'<div class="cart">'
				+'<div class="price">'
				+'<div class="price_new">￥'+val.productPrice+'</div>'
				+'<div class="price_old">￥'+val.productPrice+'</div>'
				+'</div>'
				+'<a href="#bay" class="bay" data-productid="'+val.productId+'" data-price="'+val.productPrice+'">加入购物车</a>'
				+'<a href="#" class="obn"></a>'
				+'<a href="#product" class="like" data-productid="'+val.productId+'"></a>'
				+'</div>'
				+'</div>'
				+'<div class="clear"></div>'
				+'</div>';
				$("#showProduct").addClass("listing_product").removeClass("grid_product");
				$("#showProduct").append(html);
		}
		// 页面三图一行显示方式
		function productTableShow(val){
			let html = '<div class="grid_3 product">'+
			'<div class="prev">'+
			'<a href="${pageContext.request.contextPath}/front/showProductDetail.jsp?productId='+val.productId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.productPicture+'" alt="" title="" /></a>'+
			'</div><!-- .prev -->'+
			'<h3 class="title">'+val.productName+'</h3>'+
			'<div class="cart">'+
			'<div class="price">'+
			'<div class="vert">'+
			'<div class="price_new">￥'+val.productPrice+'</div>'+
			'<div class="price_old">￥'+val.productPrice+'</div>'+
			'</div>'+
			'</div>'+
			'<a href="#" class="obn"></a>'+
			'<a href="#product" class="like" data-productid="'+val.productId+'"></a>'+
			'<a href="#bay" class="bay"  data-productid="'+val.productId+'" data-price="'+val.productPrice+'"></a>'+
			'</div>'+
			'</div>';
			$("#showProduct").addClass("grid_product").removeClass("listing_product");
			$("#showProduct").append(html);
		}
		
		// 封装ajax方法,显示商品
		function ajaxGetModel(){
			$.ajax({
				url:"${pageContext.request.contextPath}/front/productshow",
				data:{
					op:"allProduct",
					pageSize:pageSize,
					pageNum:pageNum,
					oneType:oneType,
					twoType:twoType
				},
				dataType:"json",
				success:function(result,status,xhr){
					// 清空商品标签和页码标签
					$("#showProduct").empty();
					$("#page").empty();
					
					// 设置最大页数
					pages = result.pages;
					
					$.each(result.data,function(index,val){
						if(style == 0){
							productTableShow(val);
						}else{
							productListShow(val);
						}
						
					});
					
					if(result.pages>0){
						let html = '<ul><li class="prev prevPage"><a href="#prev">&#8592;</a></li>';
						let array = page(result.pageNum,result.pages,13);
						for(let i = 0; i <array.length; i++){
							if(result.pageNum == array[i]){
								html += '<li class="curent"><a href="#page" class = "page">'+array[i]+'</a></li>';
							}else{
								html += '<li><a href="#page"  class = "page">'+array[i]+'</a></li>';
							}
						}
						html += '<li class="next nextPage"><a href="#next">&#8594;</a></li></ul>';
						$("#page").append(html);
					}
					    
				},
				error:function(xhr,status,error){
					alert("ajax请求失败");
				}
			});
		}
		
		// 点击上一页
		$(document).on("click",".prevPage",function(){
			if(pageNum > 1){
				pageNum -= 1;
				// 局部刷新页面
				ajaxGetModel();
			}
		});
		// 点击页码
		$(document).on("click",".page",function(){
			pageNum = $(this).text();
			// 局部刷新页面
			ajaxGetModel();
		});
		// 点击下一页
		$(document).on("click",".nextPage",function(){
			if(pageNum < pages){
				pageNum += 1;
				// 局部刷新页面
				ajaxGetModel();
			}
		});
		// 更换显示数量
		$("#pageSize").change(function(){
			pageSize = $(this).val();
			pageNum = 1;
			// 局部刷新页面
			ajaxGetModel();
		});
		
		
		// 一级分类显示
		function typeShowLevelOne(){
			$.ajax({
				url:"${pageContext.request.contextPath}/front/typeshow",
				data:{
					op:"levelOne"
				},
				dataType:"json",
				success:function(result,status,xhr){
					// 分类标题
					$("#categories_nav h3").empty();
					$("#categories_nav h3").text("全部商品");
					let html = '<ul>';
					$.each(result,function(index,val){ // val为type的js对象
						html += ' <li><a href="#" class="oneType" data-typeid="'+val.typeId+'">'+val.typeName+'</span></a></li>';
						if(index == result.length){
							html += ' <li class="last"><a href="#" class="oneType" data-typeid="'+val.typeId+'">'+val.typeName+'</span></a></li>';
						}
					});
					html += ' </ul>';
					$(".left_menu").empty();
					$(".left_menu").append(html);
				},
				error:function(xhr,status,error){
					alert("ajax请求错误");
				}
			});
		}
		// 初始化页面
		typeShowLevelOne();
		ajaxGetModel();
		
		// 点击一级分类时
		$(document).on("click",".oneType",function(){
			pageNum = 1; // 页面变回第一页
			let typeId = $(this).data("typeid");
			let typeName = $(this).text();
			oneType = typeId; // 存储全局变量一级分类
			twoType = null; // 清空另一个变量
			$.ajax({
				url:"${pageContext.request.contextPath}/front/typeshow",
				data:{
					op:"levelTwo",
					typeFatherId:typeId
				},
				dataType:"json",
				success:function(result,status,xhr){
					// 分类标题
					$("#categories_nav h3").empty();
					$("#categories_nav h3").html('<span id="typeReturn">&#8592;</span><span class="oneType" data-typeid="'+typeId+'">'+typeName+'</span>');
					let html = '<ul>';
					$.each(result,function(index,val){ // val为type的js对象
						html += ' <li><a href="#" class="twoType" data-typeid="'+val.typeId+'">'+val.typeName+'</span></a></li>';
						if(index == result.length){
							html += ' <li class="last"><a href="#" class="twoType" data-typeid="'+val.typeId+'">'+val.typeName+'</span></a></li>';
						}
					});
					html += ' </ul>';
					$(".left_menu").empty();
					$(".left_menu").append(html);
				},
				error:function(xhr,status,error){
					alert("ajax请求错误");
				}
			});
			ajaxGetModel();
		});
		
		// 返回根目录分类
		$(document).on("click","#typeReturn",function(){
			pageNum = 1; // 页面变回第一页
			typeShowLevelOne();
			oneType = null; // 存储变量，一级分类清空
			twoType = null; // 二级分类清空
			ajaxGetModel();
		});
		
		// 点击二级分类时
		$(document).on("click",".twoType",function(){
			pageNum = 1; // 页面变回第一页
			let typeId = $(this).data("typeid");
			let typeName = $(this).text();
			twoType = typeId; // 存储全局变量，二级分类
			oneType = null; // 清空另一个变量
			ajaxGetModel();
		});
		
		// 更换样式
		$("#showList").click(function(){
			style = 1;
			$(this).addClass("curent");
			$("#showTable").removeClass("curent");
			ajaxGetModel();
		});
		$("#showTable").click(function(){
			style = 0;
			$(this).addClass("curent");
			$("#showList").removeClass("curent");
			ajaxGetModel();
		});
		
		
		// 收藏商品
		$(document).on("click",".like",function(){
			// 获取用户id
			let userId = "${sessionScope.user.userId}";
			if(userId == ""){
				alert("请登录后收藏");
			}else{
				// 获取商品id
				let productId = $(this).data("productid");
				$.ajax({
					url:"${pageContext.request.contextPath}/front/collectServlet",
					data:{
						op:"addProduct",
						userId:userId,
						productId:productId
					},
					success:function(){
						alert("收藏成功");
					},
					error:function(){
						alert("ajax请求失败");
					}
				})
			}
		});
		
		// 添加购物车
		$(document).on("click",".bay",function(){
			let userId = "${sessionScope.user.userId}";
			let productId = $(this).data("productid");
			let price = $(this).data("price");
			if(userId != ""){
				$.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/front/CartServlet",
					data:{
						userId:userId,
						productId:productId,
						cartCount:1,
						cartPrice:price
					},
					success:function(){
						alert("添加成功");
						showChat(userId);
					},
					error:function(){
						alert("添加失败");
					}
				});
			}else{
				alert("请登录后添加购物车");
			}
			
		});
	});
</script>
</html>
