<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE HTML>
<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>电商平台</title>
 
  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">
 

    
</head>
<body>
  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
           <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
  
  <section id="main" class="entire_width">
    <div class="container_12">      
       <div id="content">
		<div class="grid_12">
			<h1 class="page_title">登录或注册账号</h1>
		</div><!-- .grid_12 -->
		
		<div class="grid_6 new_customers">
			<h2>新账户</h2>
			<p>通过在我们的商店创建一个账户，您将能够更快地完成结账过程，存储多个配送地址，在您的账户中查看和跟踪您的订单等等。</p>
			<div class="clear"></div>
				<div class="clear"></div>
			<button class="account" onclick="location.href='${pageContext.request.contextPath}/front/userSignUp.jsp'" >创建账号</button>
                </div><!-- .grid_6 -->
		<div class="grid_6">
			<form class="registed" action="${pageContext.request.contextPath}/front/UserServlet" method="post" >
			<!--  隐藏域 -->
            	<input type="hidden" name="op" value="login">
				<h2>用户登录</h2>
							
				<p>已有账号，请登录</p>
							
				<div class="email">
					<strong>用户账号:</strong><sup class="surely">*</sup><br/>
					<input type="text" name="userAccount" class="" value="" />
				</div><!-- .email -->
							
				<div class="password">
					<strong>用户密码:</strong><sup class="surely">*</sup><br/>
					<input type="text" name="userPassword" class="" value="" />
				</div><!-- .password -->
				
				<div class="remember">
					<input class="niceCheck" type="checkbox" name="rememberPassword" value="rememberPassword" />
					<span class="rem">记住密码</span>
				</div><!-- .remember -->
				
				<div class="submit">										
					<input type="submit" value="登录" />
					<sup class="surely">*</sup><span>必填项</span>
				</div><!-- .submit -->
			</form><!-- .registed -->
                </div><!-- .grid_6 -->
       </div><!-- #content -->
       
        <script src="${pageContext.request.contextPath}/front/js/jquery.min.js"></script>  
        <script src="${pageContext.request.contextPath}/front/js/matrix.login.js"></script> 

        <script>
        	// 如果登录失败会弹出一个提示框
        	var loginErrorMessage = "${param.loginErrorMessage}"; // ${param.参数名} EL表达式获取页面参数值
        	if(loginErrorMessage != ""){
        		alert("用户名或密码错误,请重新登录!");
        	}
        </script>
      <div class="clear"></div>
    </div><!-- .container_12 -->
  </section><!-- #main -->
  
  <div class="clear"></div>
    
  <footer>
    <div class="f_navigation">
      <div class="container_12">
        <div class="grid_3">
          <h3>Contact Us</h3>
          <ul class="f_contact">
            <li>49 Archdale, 2B Charlestone</li>
            <li>+777 (100) 1234</li>
            <li>mail@example.com</li>
          </ul><!-- .f_contact -->
        </div><!-- .grid_3 -->
      
        <div class="grid_3">
          <h3>Information</h3>
          <nav class="f_menu">
            <ul>
              <li><a href="#">About As</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Terms & Conditions</a></li>
              <li><a href="#">Secure payment</a></li>
            </ul>
          </nav><!-- .private -->
        </div><!-- .grid_3 -->
        
        <div class="grid_3">
          <h3>Costumer Servise</h3>
          <nav class="f_menu">
            <ul>
              <li><a href="contact_us.html">Contact As</a></li>
              <li><a href="#">Return</a></li>
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Site Map</a></li>
            </ul>
          </nav><!-- .private -->
        </div><!-- .grid_3 -->
        
        <div class="grid_3">
          <h3>My Account</h3>
          <nav class="f_menu">
            <ul>
              <li><a href="#">My Account</a></li>
              <li><a href="#">Order History</a></li>
              <li><a href="#">Wish List</a></li>
              <li><a href="#">Newsletter</a></li>
            </ul>
          </nav><!-- .private -->
        </div><!-- .grid_3 -->
        
        <div class="clear"></div>
      </div><!-- .container_12 -->
    </div><!-- .f_navigation -->
    
    <div class="f_info">
      <div class="container_12">
        <div class="grid_6">
          <p class="copyright">第二阶段项目：电商管理平台<a  href="#"></a></p>
        </div><!-- .grid_6 -->
        
        <div class="grid_6">
          <div class="soc">
            <a class="google" href="#"></a>
            <a class="twitter" href="#"></a>
            <a class="facebook" href="#"></a>
          </div><!-- .soc -->
        </div><!-- .grid_6 -->
        
        <div class="clear"></div>
      </div><!-- .container_12 -->
    </div><!-- .f_info -->
  </footer>
 
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
 
 
        <script src="${pageContext.request.contextPath}/front/js/jquery.min.js"></script>  
        <script src="${pageContext.request.contextPath}/front/js/matrix.login.js"></script> 

<!--    多用户的记住密码 -->
     <script type="text/javascript">
    		$(document).ready(function(){
    			
    			// 对买家账号输入框做失去焦点事件，当输入完后，判断cookie中是否存在该账号对应的name，如果有则返回value
    			$("input[name='userAccount']").blur(function(){
    				var userAccount = $(this).val(); // adminId表示用户输入的登录账号
    				// 获取cookie指定name的value值
    				var userPassword = getCookie(userAccount);// 登录账号是cookie中的name
    				// 获取成功
        			if(userPassword != ""){
        				// 填充数据
        				$("input[name='userPassword']").val(userPassword);
        				$("input[name='rememberPassword']").prop("checked",true);
        			}else{
        				$("input[name='userPassword']").val("");
        				$("input[name='rememberPassword']").prop("checked",false);
        			}
    			});
    		})
    		// 自定义函数
    		// 通过cookie中指定的key,获取对应value值
    		function getCookie(cname){
    			
    			var name = cname + "=";  // name="username="   name="password="
    				
    			var ca = document.cookie.split(';');// 根据分号进行字符串的切割,返回一个字符串数组  ca[0]="username=baby"  ca[1]=" password=123456"
    			
    			for(var i = 0; i < ca.length; i++) {// 遍历数组
    				
    				var c = ca[i].trim();   // ca[0]="username=baby"  ca[1]="password=123456" 
    			
    				if (c.indexOf(name)==0) {  // c="username=baby" name="username="  
    					
    					return c.substring(name.length,c.length);  // 字符串裁剪，得到value值
    				}
    			}
    			return "";
    		}
    

    </script>
</body>
</html>