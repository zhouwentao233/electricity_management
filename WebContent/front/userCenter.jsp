<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
	  <meta charset="UTF-8">
	
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	
	  <title>个人中心</title>
	
	  <link rel="shortcut icon" href="favicon.ico">
	  <link href="${pageContext.request.contextPath}/front/css/style.css" media="screen" rel="stylesheet" type="text/css">
	  <link href="${pageContext.request.contextPath}/front/css/grid.css" media="screen" rel="stylesheet" type="text/css">
	
	  <script src="${pageContext.request.contextPath}/front/js/jquery-1.7.2.min.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/html5.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jflow.plus.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/radio.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script>	
	</head>
	<body>
	  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li ><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class="curent"><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
	  <section id="main" class="entire_width">
	    <div class="container_12">
	       <div class="grid_12">
			   <h1 class="page_title">个人中心</h1>	
			<div class="container">
			<div class="reg">
				<h3>修改个人信息</h3>
				 <form action="${pageContext.request.contextPath}/front/FrontUserCenterServlet" method="POST" enctype="multipart/form-data">
					<ul>
						<li class="text-info">账号： </li>
						<li><input type="text" value="" name="userAccount" id="userAccount"></li>
					</ul>
					<ul>
						<li class="text-info" >用户名: </li>
						<li><input type="text" value="" name="userName"  id="userName"></li>
					 </ul>				 
					<ul>
						<li class="text-info">密码: </li>
						<li><input type="password" value="" name="userPassword" id="userPassword" /></li>
					</ul>
					<div class="userImage">
					<img src="#" id="img3" class="hide"/>
				</div>
					<ul>
						<li class="text-info">头像: </li>
						<li><input type="file" id="file" class="filepath" name="userPicture"  accept="image/jpg,image/jpeg,image/png"></li>
					</ul>
					<ul>
						<li class="text-info">电话号码:</li>
						<li><input type="text" value="" name="userPhone" id="userPhone"></li>
					</ul>						
					<input type="submit" value="确认修改" id="confirm">
					
				</form>
			</div>
		</div>
	       </div><!-- .grid_12 -->
		</div>
	  </section>
	  <script>

  </script>
	  <script type="text/javascript">
	  $(document).ready(function(){
		  var user = {userId:1,userAccount:"苏帆",userName:"苏帆",userPssword:"123",userPicture:"1.jpeg",userPhone:"111"};
		  	var userA = $("#userAccount").val(user.userAccount);
			var password=$("#userPassword").val(user.userPassword);
			// 对账号输入框绑定失去焦点事件
			$("#userAccount").blur(function(){
				// 获取输入的账号
				var userAccount = $(this).val().trim();
				if(userAccount == ""){
					alert("输入正确账号")
				}else{
					// 发送ajax请求
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/FrontUserCenterServlet",
						type:"post",
						data:{op:"existsOfAccountName",userAccount:userAccount},
						dataType:"text",
						success:function(result,status,xhr){
							if(result=="false"){// 用户名不存在
								alert("该用户不存在")
								// 修改按钮设置不可用
								$("#confirm").prop("disabled",true);
							}else if(result == "true"){
								$("#confirm").prop("disabled",false);
								
							}
						},
						error:function(xhr,status,error){
							alert("ajax异步请求失败");
						}
					});
				}
				
			});
			$("#file").change(function(){
			       var objUrl = getObjectURL(this.files[0]) ;
			       console.log("objUrl = "+objUrl) ;
			       if (objUrl) 
			       {
			         $("#img3").attr("src", objUrl);
			         $("#img3").removeClass("hide");
			       }
			     }) ;
			     //建立一個可存取到該file的url
			     function getObjectURL(file) 
			     {
			       var url = null ;
			       if (window.createObjectURL!=undefined) 
			       { // basic
			         url = window.createObjectURL(file) ;
			       }
			       else if (window.URL!=undefined) 
			       {
			         // mozilla(firefox)
			         url = window.URL.createObjectURL(file) ;
			       } 
			       else if (window.webkitURL!=undefined) {
			         // webkit or chrome
			         url = window.webkitURL.createObjectURL(file) ;
			       }
			       return url ;
			     }
			     
			    /*  $("#confirm").click(function(){
			    	 var userAccount = $("#userAccount").val();
			    	 var userName = $("#userName").val();
			    	 var userPassword = $("#userPassword").val;
			    	 var img = $("#img3").val();
			    	 var userPhone = $("#userPhone").val();
			    	 $.ajax({
							async:true,
							url:"${pageContext.request.contextPath}/front/FrontUserCenterServlet",
							type:"post",
							data:{op:"updateAccount",userAccount:userAccount,},
							dataType:"text",
							success:function(result,status,xhr){
								if(result=="false"){// 用户名不存在
									alert("该用户不存在")
									// 修改按钮设置不可用
									$("#confirm").prop("disabled",true);
								}else if(result == "true"){
									$("#confirm").prop("disabled",false);
									
								}
							},
							error:function(xhr,status,error){
								alert("ajax异步请求失败");
							}
						}); 
			     }) */
		})
	  		
	  </script>
	</body>
</html>