<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE HTML>
<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>收藏夹</title>

  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">
 
  <!-- <script src="js/jquery-1.7.2.min.js" ></script>
  <script src="js/html5.js" ></script>
  <script src="js/jflow.plus.js" ></script>
  <script src="js/jquery.carouFredSel-5.2.2-packed.js"></script>
  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
  <script src="js/radio.js"></script>
  <script src="js/selectBox.js"></script> -->
 <script src="${pageContext.request.contextPath}/front/js/jquery.min.js"></script> 
<script src="${pageContext.request.contextPath}/front/js/matrix.login.js"></script>  
<script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script>
<script src="${pageContext.request.contextPath}/front/js/jflow.plus.js" ></script>
<script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
 <script src="${pageContext.request.contextPath}/front/js/bootstrap.min.js"></script>
  <script>
	$(document).ready(function() {
		$("select").selectBox();
	});
  </script>

  <script>
	$(document).ready(function(){
	    $("#myController").jFlow({
			controller: ".control", // must be class, use . sign
			slideWrapper : "#jFlowSlider", // must be id, use # sign
			slides: "#slider",  // the div where all your sliding divs are nested in
			selectedWrapper: "jFlowSelected",  // just pure text, no sign
			width: "984px",  // this is the width for the content-slider
			height: "480px",  // this is the height for the content-slider
			duration: 400,  // time in miliseconds to transition one slide
			prev: ".slidprev", // must be class, use . sign
			next: ".slidnext", // must be class, use . sign
			auto: true	
		 });
	});
   </script>
   <script>
	$(function() {
	  $('#list_product').carouFredSel({
		prev: '#prev_c1',
		next: '#next_c1',
		auto: false
	  });
          $('#list_product2').carouFredSel({
		prev: '#prev_c2',
		next: '#next_c2',
		auto: false
	  });
	  $(window).resize();
	});
   </script>
   <script>
       $(document).ready(function(){
	      $("button").click(function(){
		     $(this).addClass('click')
	      });             
       })
  </script>
    
</head>
<body>
  <body>
  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li ><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class="curent"><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
   
  <section id="main">
    <div class="container_12">
       <div id="sidebar" class="grid_3">
	      <aside id="categories_nav">
		     <h3>收藏和地址信息</h3>
		     
		     <nav class="left_menu">
			    <ul>
				   <li><a href="#" id="collectProduct" name="collectProduct">商品收藏夹 <span></span></a></li>
				   <li><a href="#" id="collectShops" name="collectShops">店铺收藏夹 <span></span></a></li>
				   <li><a href="#" id="address" name="address">地址管理 <span></span></a></li>
				   
			    </ul>
		     </nav><!-- .left_menu -->
	      </aside><!-- #categories_nav -->
	      
	     
	      </div><!-- .sidebar -->
      
       <div id="content" class="grid_9">
	      
	     <div class="grid_product" id= "showProduct">
 		     
		     
	    
	       <div class="clear"></div> 
	       </div>
	      
	      <div class="clear"></div>
	      
	      <div class="pagination" >
		    <button class="btn btn-delete addAddress " id="addAddress"  data-userid="val.user.userId">增加地址</button>
		    <div id="addAddressdiv"></div>
	      </div><!-- .pagination -->
	      
       </div><!-- #content -->
       
      <div class="clear"></div>
      
    </div><!-- .container_12 -->
  </section><!-- #main -->
  
  <div class="clear"></div>
    
  
 <!-- 改库存 -->
 <div onclick="show1()" id="thisDiv" style="border:1px solid red;width:100px;height:100px">thisDiv</div>
 <!-- -- 模态框 --
<div class="modal fade hide" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
 
      模态框头部
      <div class="modal-header">
        <h4 class="modal-title">修改地址</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
 
      模态框主体
      <div class="modal-body">
       <div class="modal-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="addressInfo" name="addressInfo" placeholder="请在这里输入地址">
				</div>
				<div class="modal-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="receiveName" name="receiveName" placeholder="收件人姓名">
				</div>
				<div class="modal-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="receivePhone" name="receivePhone" placeholder="收件人电话">
				</div>
				<div class="modal-footer">
                    <button type="button" data-dismiss="modal">关闭</button>
                <button type="button"  id="input-button" data-dismiss="modal" data-productid="" >
                    		确认修改 成功的按钮
                </button>
      </div>
 </div>
 </div>
 </div>
 </div> -->
      <!-- 模态框底部 -->
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
      </div>
 
    </div>
  </div>
</div>
			 <div class="modal hide fade" id="myModal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="addressUpdate">
                    		   修改地址
                    </h4>
                </div>
                <div class="modal1-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="addressInfo" name="addressInfo" placeholder="请在这里输入地址">
				</div>
				<div class="modal1-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="receiveName" name="receiveName" placeholder="收件人姓名">
				</div>
				<div class="modal1-body" style="width: 550px; margin: 0px auto;">
                    <input type="text"  id="receivePhone" name="receivePhone" placeholder="收件人电话">
				</div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal">关闭</button>
                <button type="button"  id="input-button" data-dismiss="modal" data-productid="" >
                    		确认修改 成功的按钮
                </button>
            </div>
        </div>/.modal-content
    </div>/.modal-dialog
</div>/.modal -->
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		//当前页码
		let pageNum = 1;
		// 当前页面数量
		let pageSize = 12;
		
		// 用户id
		let userId = "${sessionScope.user.userId}";
		if(userId == ""){
			alert("请登录后操作");
		}else{
			$.ajax({
				url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
				data:{
					op:"collectProduct",
					pageSize:pageSize,
					pageNum:pageNum,
					userId:userId
				},
				dataType:"json",
				success:function(result,status,xhr){
					// 清空商品标签和页码标签
					$("#showProduct").empty();
					$("#page").empty();
					
					 // 设置最大页数
					/* pages = result.pages; */
					
					 $.each(result,function(index,val){
						
							productCollect(val);
						
						
						
					});
				},
				error:function(xhr,status,error){
					alert("ajax请求失败");
				}
			});
			 $("#collectProduct").click(function(){
				 $.ajax({
						url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
						data:{
							op:"collectProduct",
							pageSize:pageSize,
							pageNum:pageNum,
							userId:userId
						},
						dataType:"json",
						success:function(result,status,xhr){
							// 清空商品标签和页码标签
							$("#showProduct").empty();
							$("#page").empty();
							
							 // 设置最大页数
							/* pages = result.pages; */
							
							 $.each(result,function(index,val){
								
									productCollect(val);
								
								
								
							});
						},
						error:function(xhr,status,error){
							alert("ajax请求失败");
						}
					});
			}) 
			 $("#collectShops").click(function(){
				 $.ajax({
						url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
						data:{
							op:"collectShops",
							pageSize:pageSize,
							pageNum:pageNum,
							userId:userId
						},
						dataType:"json",
						success:function(result,status,xhr){
							// 清空商品标签和页码标签
							$("#showProduct").empty();
							$("#page").empty();
							
							 // 设置最大页数
							/* pages = result.pages; */
							
							 $.each(result,function(index,val){
								
									shopsCollect(val);
								
								
								
							});
							 
							/* if(result.pages>0){
								let html = '<ul><li class="prev"><a href="#">&#8592;</a></li>';
								for(let i = 1; i <= result.pages; i++){
									if(result.pageNum == i){
										html += '<li class="curent"><a href="#" class = "page">'+i+'</a></li>';
									}else{
										html += '<li><a href="#"  class = "page">'+i+'</a></li>';
									}
								}
								html += '<li class="next"><a href="#">&#8594;</a></li></ul>';
								$("#page").append(html);
							}  */
							
							
							
							
						},
						error:function(xhr,status,error){
							alert("ajax请求失败");
						}
					});
			}) 
			
			//地址信息展示
			function showAddress(result){
				 
				 let html = 
					 
					 '<table class=\"data-table table table-bordered table-striped dataTable\" style=\"margin-bottom: 0;\" id=\"table\" aria-describedby=\"DataTables_Table_0_info\" >'+
						'<thead>'+
							'<tr role=\"row\">'+
			'<th class=\" orderBy\" role=\"columnheader\" tabindex=\"0\" aria-controls=\"DataTables_Table_0\" rowspan=\"1\" colspan=\"1\" data-name=\"addressName\" data-ascdesc=\"desc\">'+'地址信息'+'</th>'+
						'<th class=\" orderBy\" role=\"columnheader\" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="addressStatus" data-ascdesc="desc">是否为默认地址</th>'	+	
							'<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="receiveName" data-ascdesc="desc">收货人姓名</th>'	+
								'<th class=" orderBy" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" data-name="receivePhone" data-ascdesc="desc">联系电话</th>'+
								'<th class="" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >操作</th>'+
							'</tr>'+

						'</thead>'+

						'<tbody role="alert" aria-live="polite" aria-relevant="all">';
				 $.each(result,function(index,val){
						html+=
					 	'<tr>'
						+'<td>'+val.addressName+'</td>'
						+'<td>'+(val.addressStatus==1?"默认地址":"非默认地址")+'</td>'
						+'<td>'+val.receiveName+'</td>'
						+'<td>'+val.receivePhone+'</td>'
						/* +'<td>'+(val.addressStatus==0?"非默认地址":"默认地址")+'</td>' */
						
						+'<td colspan=\"4\">'
						+(val.addressStatus==1?"":("<button class=\"btn btn-danger mainAddress\" id=\"mainAddress\" data-addressid=\""+val.addressId+"\">设为默认地址</button>"))
						+"&nbsp;"
						/* +(val.addressStatus==0?"":("<button class=\"btn  btn-success\" id=\"noMainAddress\" data-addressName=\""+""+val.addressName+""+"\">设为备选地址 </button> "))
						+"&nbsp;" */
						/* +"<button   class=\"btn btn-primary updateAddressInfo\" data-toggle=\"modal\" data-target=\"#myModal\" data-addressid=\""+val.addressId+"\">修改地址</button> "
						+"&nbsp;" */
						+"<button class=\"btn btn-delete btndelete \" id=\"btndelete\"  data-addressid=\""+val.addressId+"\">删除地址</button></td>"
						+'</tr>'
					
				});
				 
					
				html+='</tbody>'
					+'</table>'	;
					/* $("#showProduct").addClass("grid_product").removeClass("listing_product"); */
					$("#showProduct").append(html); 
				 
				 
			 }
			 //方法：
			 function newAddress(){
				 $.ajax({
						url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
						data:{
							op:"address",
							pageSize:pageSize,
							pageNum:pageNum,
							userId:userId
						},
						dataType:"json",
						success:function(result,status,xhr){
							// 清空商品标签和页码标签
							$("#showProduct").empty();
							$("#page").empty();
							
							 // 设置最大页数
							/* pages = result.pages; */
							showAddress(result);
							
							},
						error:function(xhr,status,error){
							alert("ajax请求失败");
						}
					});
			 }
			  
			
			 //
			 $("#address").click(function(){
				 newAddress();
			}) 
			//新增地址
			$(document).on("click",".addAddress",function(){
				let html=
					'<div class="modal-body hidendiv" style="width: 550px; margin: 0px auto;" style="display:block">'+
	           '<input type="text" class="inputAddress" id="addressInfo" name="addressInfo" placeholder="请在这里输入地址">' +
			    '</div>'+
			    '<div class="modal-body" style="width: 550px; margin: 0px auto;">'+
	           '<input type="text" class="inputReceiveName" id="receiveName" name="receiveName" placeholder="收件人姓名">' +
			   '</div>'+
			   '<div class="modal-body" style="width: 550px; margin: 0px auto;">'+
	            '<input type="text"  class="inputReceivePhone" id="receivePhone" name="receivePhone" placeholder="收件人电话">'+
			   '</div>'+
			   '<div class="modal-footer" style="width: 550px; margin: 0px auto;">'+
	            '<button type="button" data-dismiss="modal">取消</button>'+'</br>'+
	            '<button type="button" class="sure" id="input-button" data-dismiss="modal" data-productid="" >'+'确认修改'+'</button>'+
			  '</div>';
				$("#addAddressdiv").append(html);
				
				
			})
			$(document).on("click",".sure",function(){
				
				
				let addressName = $(".inputAddress").val(); 
				let receiveName = $(".inputReceiveName").val();
				let receivePhone = $(".inputReceivePhone").val();
				$.ajax({
					async:true,
					url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
					type:"POST",
					data:{
						op:"addAddress",
						pageSize:pageSize,
						pageNum:pageNum,
						userId:userId,
						addressName:addressName,
						receiveName:receiveName,
						receivePhone:receivePhone
						/* productId:productId,
						  op:"updateInventory",
						  productInventory:productInventory */
					},
					dataType:"json",
					success:function(result,status,xhr){
							alert("添加成功");
							
							$("#addAddressdiv").empty();
							/* pages = result.pages; */
							newAddress();
						},
					
					error:function(xhr,status,error){
						alert("异步请求失败");
					}
			
				})		
			})
			//修改地址
			
				$(document).on("click",".updateAddressInfo",function(){
					let addressId = $(this).data("addressid");
					let html=
						'<div class="modal-body hidendiv" id="hidendiv" style="width: 550px; margin: 0px auto;" style="display:none">'+
		           '<input type="text" class="inputAddress" id="addressInfo" name="addressInfo" value="" placeholder="请在这里输入地址">' +
				    '</div>'+
				    '<div class="modal-body" style="width: 550px; margin: 0px auto;">'+
		           '<input type="text" class="inputReceiveName" id="receiveName" name="receiveName" placeholder="收件人姓名">' +
				   '</div>'+
				   '<div class="modal-body" style="width: 550px; margin: 0px auto;">'+
		            '<input type="text"  class="inputReceivePhone" id="receivePhone" name="receivePhone" placeholder="收件人电话">'+
				   '</div>'+
				   '<div class="modal-footer" style="width: 550px; margin: 0px auto;">'+
		            '<button type="button" data-dismiss="modal">取消</button>'+'</br>'+
		            '<button type="button" class="suresure" id="input-button" data-dismiss="modal" data-productid="" >'+'确认修改'+'</button>'+
				  '</div>';
					$(".pagination").append(html);
					
					
				})
				$(document).on("click",".suresure",function(){
					 
					 
					let addressName = $(".inputAddress").val(); 
					let receiveName = $(".inputReceiveName").val();
					let receivePhone = $(".inputReceivePhone").val();
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
						type:"POST",
						data:{
							op:"updateAddress",
							pageSize:pageSize,
							pageNum:pageNum,
							userId:userId,
							addressId:addressId,
							addressName:addressName,
							receiveName:receiveName,
							receivePhone:receivePhone
							/* productId:productId,
							  op:"updateInventory",
							  productInventory:productInventory */
						},
						dataType:"json",
						success:function(result,status,xhr){
								alert("修改成功")
								
								/* pages = result.pages; */
								showAddress(result);
							},
						
						error:function(xhr,status,error){
							alert("异步请求失败");
						}
				
					})		
				})
				
			 
			 //设为默认地址
			  $(document).on("click",".mainAddress",function(){
			 
				 
				 let addressId = $(this).data("addressid");
				 $.ajax({
						url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
						data:{
							op:"statusAddress",
							pageSize:pageSize,
							pageNum:pageNum,
							addressId:addressId
						},
						dataType:"json",
						success:function(result,status,xhr){
							// 清空商品标签和页码标签
							$("#showProduct").empty();
							$("#page").empty();
							alert("设置成功");
							$.ajax({
								url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
								data:{
									op:"address",
									pageSize:pageSize,
									pageNum:pageNum,
									userId:userId
								},
								dataType:"json",
								success:function(result,status,xhr){
									// 清空商品标签和页码标签
									
									
									 // 设置最大页数
									/* pages = result.pages; */
									newAddress();
									},
								error:function(xhr,status,error){
									alert("ajax请求失败");
								}
							});
							},
						error:function(xhr,status,error){
							alert("ajax请求失败");
						}
					}); 
			 })
			 //删除地址
			 $(document).on("click",".btndelete",function(){
				 let addressId = $(this).data("addressid");
				 if(confirm("是否确认删除")){
					 $.ajax({
							url:"${pageContext.request.contextPath}/front/FrontShowCollectServlet",
							data:{
								op:"deleteAddress",
								pageSize:pageSize,
								pageNum:pageNum,
								addressId:addressId
							},
							dataType:"json",
							success:function(result,status,xhr){
								// 清空商品标签和页码标签
								
								
								alert("删除成功");
								
								newAddress();
								
								},
							error:function(xhr,status,error){
								alert("ajax请求失败");
							}
						}); 
				 }
				 
			 })
			function productCollect(val){
				 let html = '<div class="grid_3 product">'+
					'<div class="prev">'+
					'<a href="${pageContext.request.contextPath}/front/showProductDetail.jsp?productId='+val.productCollectId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.product.productPicture+'" alt="" title="" /></a>'+
					'</div><!-- .prev -->'+
					'<h3 class="title">'+val.product.productName+'</h3>'+
					'<div class="cart">'+
					'<div class="price">'+
					'<div class="vert">'+
					'<div class="price_new">￥'+val.product.productPrice+'</div>'+
					'</div>'+
					'</div>'+
					'<a href="#" class="obn"></a>'+
					'<a href="#" class="like"></a>'+
					'<a href="#" class="bay"></a>'+
					'</div>'+
					'</div>';
					/* $("#showProduct").addClass("grid_product").removeClass("listing_product"); */
					$("#showProduct").append(html); 
				 
				 
			 }
			 function shopsCollect(val){
				 let html = '<div class="grid_3 product">'+
					'<div class="prev">'+
					'<a href="${pageContext.request.contextPath}/front/showProductDetail.jsp?shopsCollectId='+val.shopsCollectId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.seller.sellerLogo+'" alt="" title="" /></a>'+
					'</div><!-- .prev -->'+
					'<h3 class="title">'+val.seller.sellerShopName+'</h3>'+
					'<div class="cart">'+
					'<div class="price">'+
					'<div class="vert">'+
					/* '<div class="price_new">￥'+val.product.productPrice+'</div>'+ */
					'</div>'+
					'</div>'+
					'<a href="#" class="obn"></a>'+
					'<a href="#" class="like"></a>'+
					'<a href="#" class="bay"></a>'+
					'</div>'+
					'</div>';
					/* $("#showProduct").addClass("grid_product").removeClass("listing_product"); */
					$("#showProduct").append(html); 
				 
				 
			 }
		}
	});
</script>
</html>