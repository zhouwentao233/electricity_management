<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
	  <meta charset="UTF-8">
	
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	
	  <title>电商平台</title>
	  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	  <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
	  <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link rel="shortcut icon" href="favicon.ico">
	  <link href="${pageContext.request.contextPath}/front/css/style.css" media="screen" rel="stylesheet" type="text/css">
	  <link href="${pageContext.request.contextPath}/front/css/grid.css" media="screen" rel="stylesheet" type="text/css">
	
	  <script src="${pageContext.request.contextPath}/front/js/jquery-1.7.2.min.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/html5.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jflow.plus.js" ></script>
	  <script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/radio.js"></script>
	  <script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script>	
	</head>
	<script type="text/javascript">
		/* // javasctipt函数事件
		function select() {
			var orderId = $(this).data("orderid"); 
			location.href = "${pageContext.request.contextPath}/front/OrderDetailServlet?op=getOrderDetail&orderId=" + orderId;
		} */
	</script>
	<body>
<%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li class="curent"><a href="order.jsp">订单</a></li>
            <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
	  <section id="main" class="entire_width">
	    <div class="container_12">
	       <div class="grid_12">
			   <h1 class="page_title">订单详情</h1>	
			   <table class="table table-bordered" id="DateTables_Order">
					<thead>
						<tr>
							<th>系统信息</th>
							<th>用户信息</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="DateTables_OrderProduct"></div>
	       </div><!-- .grid_12 -->
		</div>
	  </section>
	  <script type="text/javascript">
	  		$(document).ready(function() {
	  			// 1.进入页面，ajax异步请求服务器响应数据-订单详情列表
	  			sendAjax();	  	
	  			sendProductAjax();
	  			$(document).on("click","#comments",function() {
	  				let productId = $(this).data("productid");
	  				location.href = "${pageContext.request.contextPath}/front/showProductDetail.jsp?productId=" + productId;
	  				
	  			});
	  		});
	  		
	  		/**
			* 发送异步请求
			*/
	  		function sendAjax() {
	  			var orderId = "${param.orderId}";
	  			$.ajax({
	  				async:true,
	  				url:"${pageContext.request.contextPath}/front/OrderDetailServlet",
	  				type:"GET",
	  				data:{op:"getOrderDetail", 
	  					orderId:orderId},
	  				dataType:"json",
	  				success:function(result,status,xhr) {
	  					// alert(restult.orders.orderPayMethod);
	  					showData(result);
	  				},
	  				error:function(xhr,status,error) {
	  					alert("连接失败");
	  				}
	  			});
	  		}	  		
	  		/**
			* 表格数据的渲染		
			*/
			function showData(result) {
	  			// 清空表格原数据
	  			$("#DateTables_Order tbody").empty();
  				var orderId = "${param.orderId}";
  				var temp;
  				if (result.orderStatus == 1) {
					temp="未发货";
				} else if (result.orderStatus == 2) {
					temp="已发货";
				} else if (result.orderStatus == 3) {
					temp="订单已完成";
				}
  				$("#DateTables_Order tbody").append("<tr><td>收货人：</td><td>"+result.orderReceiveName+"</td></tr>"+
					  "<tr><td>收货人电话：</td><td>"+result.orderReceivePhone+"</td></tr>"+
					  "<tr><td>收货人地址：</td><td>"+result.orderReceiveAddress+"</td></tr>"+
					  "<tr><td>订单编号：</td><td>"+orderId+"</td></tr>"+
					  "<tr><td>下单时间：</td><td>"+result.orderTime+"</td></tr>"+
					  "<tr><td>支付方式：</td><td>"+result.orderPayMethod+"</td></tr>"+
					  "<tr><td>订单总额：</td><td>"+result.orderTotalPrice+"</td></tr>"+
					  "<tr><td>订单状态：</td><td>"+temp+"</td></tr>");
	  		}	  
			/**
			* 发送异步请求
			*/
	  		function sendProductAjax() {
	  			var orderId = "${param.orderId}";
	  			$.ajax({
	  				async:true,
	  				url:"${pageContext.request.contextPath}/front/OrderDetailServlet",
	  				type:"GET",
	  				data:{op:"getOrderProduct", 
	  					orderId:orderId},
	  				dataType:"json",
	  				success:function(result,status,xhr) {
	  					showProduceData(result);
	  				},
	  				error:function(xhr,status,error) {
	  					alert("连接失败");
	  				}
	  			});
	  		}	  		
	  		/**
			* 表格数据的渲染		
			*/
			function showProduceData(result) {
	  			// 清空表格原数据
	  			$("#DateTables_OrderProduct").empty();
	  			// 渲染表格中的数据
	  			$.each(result,function(index,orderproduct) {
	  				var orderId = "${param.orderId}";
	  				var temp;
	  				if (orderproduct.orderDetailStatus == 1) {
						temp="未发货";
					} else if (orderproduct.orderDetailStatus == 2) {
						temp="已发货";
					}
	  				$("#DateTables_OrderProduct").append("<table class=\"table table-bordered\">"+
	  						"<thead><tr><th>订单商品</th><th>商品信息</th></tr></thead>"+
							"<tbody>"+
							  "<tr><td>商品名称：</td><td>"+orderproduct.productName+"</td></tr>"+
							  "<tr><td>商品图片：</td><td><img width='200px' height='200px' src='${pageContext.request.contextPath}/download?fileName="+orderproduct.productPicture+"' /></td></tr>"+
							  "<tr><td>商品数量：</td><td>"+orderproduct.orderDetailCount+"</td></tr>"+
							  "<tr><td>商品价格：</td><td>"+orderproduct.orderDetailPrice+"</td></tr>"+
							  "<tr><td>商品状态：</td><td>"+temp+"</td></tr>"+
							  "<tr><td>商品评论：</td><td><button class=\"btn btn-primary\" id=\"comments\" data-productid=\""+orderproduct.productId+"\">商品评论</button></td></tr>"+
							"</tbody>"+
						"</table>");
	  				});
	  			}	  
	  </script>
	</body>
</html>