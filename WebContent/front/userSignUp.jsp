<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE HTML>
<html>
<script src="${pageContext.request.contextPath}/front/js/jquery-3.2.1.js"></script>
 <%--  
  <script src="${pageContext.request.contextPath}/front/js/jquery-1.7.2.min.js" ></script>
 
  <script src="${pageContext.request.contextPath}/front/js/html5.js" ></script>
  <script src="${pageContext.request.contextPath}/front/js/jflow.plus.js"></script>
  <script src="${pageContext.request.contextPath}/front/js/jquery.carouFredSel-5.2.2-packed.js"></script>
  <script src="${pageContext.request.contextPath}/front/js/checkbox.js"></script>
  <script src="${pageContext.request.contextPath}/front/js/radio.js"></script>
  <script src="${pageContext.request.contextPath}/front/js/selectBox.js"></script> --%>

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>买家用户注册</title>

  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">
  

 <!--  <script>
	$(document).ready(function() {
		$("select").selectBox();
	});
  </script>

 <script>
	$(document).ready(function(){
		$("#myController").jFlow({
			controller: ".control", // must be class, use . sign
			slideWrapper : "#jFlowSlider", // must be id, use # sign
			slides: "#slider",  // the div where all your sliding divs are nested in
			selectedWrapper: "jFlowSelected",  // just pure text, no sign
			width: "984px",  // this is the width for the content-slider
			height: "480px",  // this is the height for the content-slider
			duration: 400,  // time in miliseconds to transition one slide
			prev: ".slidprev", // must be class, use . sign
			next: ".slidnext", // must be class, use . sign
			auto: true	
		});
	});
  </script>
  <script>
	$(function() {
	  $('#list_product').carouFredSel({
		prev: '#prev_c1',
		next: '#next_c1',
		auto: false
	  });
          $('#list_product2').carouFredSel({
		prev: '#prev_c2',
		next: '#next_c2',
		auto: false
	  });
	  $('#list_banners').carouFredSel({
		prev: '#ban_prev',
		next: '#ban_next',
		scroll: 1,
		auto: false
	  });
	  $(window).resize();
	});
  </script>
  <script>
       $(document).ready(function(){
	      $("button").click(function(){
		     $(this).addClass('click')
	      });             
       })
  </script>
     -->
     
    <style>
    .logo{
            position:absolute;
	        left:300px;
	        top:471px;
	        }
    </style>
</head>
<body>
  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>
  
  <section id="main" class="entire_width">
    <div class="container_12">      
       <div id="content">
		<div class="grid_12">
			<h1 class="page_title">新用户</h1>
		</div><!-- .grid_12 -->
		
		<div class="grid_4 adress">
			<img id="img3" src="http://placehold.it/230x230&amp;text=Photo" width="230px" height="230px">
			<button id="phoneCode">发送验证码</button>
        </div>
        

        
		
		<div class="grid_8">
			<form class="contact" action="${pageContext.request.contextPath}/front/UserServlet" enctype="multipart/form-data" method="post" style="height:400px" >
				 <input type="hidden" name="op"  value="register"> 
				<h2>用户注册&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" id="file"class="filepath" name="userPicture" onchange="setImagePriview()" accept="image/jpg,image/jpeg,image/png"> </h2>
							
				<div class="name">
					<strong>买家账号:&nbsp;&nbsp;&nbsp;<span id="userAccountTip" style="padding-left:0px;"></span></strong><br/>
					<input type="text" name="userAccount" id="userAccount" placeholder="请输入账号" value="" />
					
				</div><!-- .name -->
				
				<div class="email">
					<strong>买家用户名:&nbsp;&nbsp;&nbsp;<span id="userNameTip" style="padding-left:0px;"></span></strong><br/>
					<input type="text" name="userName" id="userName" placeholder="请输入用户名" value="" />
					
				</div><!-- .email -->
				
				<div class="email">
					<strong>买家密码: </strong><br/>
					<input type="text" name="userPassword1"  id="userPassword1" placeholder="密码长度6-12位" value="" />
					
				</div><!-- .email -->
				
					<div class="name">
					<strong>买家电话:&nbsp;&nbsp;&nbsp;<span id="userPhoneTip" style="padding-left:0px;"></span></strong><br/>
					<input type="text" name="userPhone" id="userPhone" placeholder="请输入手机号" value="" />
				
				</div><!-- .name -->
				
				
					<div class="name">
					<strong>验证码:&nbsp;&nbsp;&nbsp;<span id="userPhoneTip" style="padding-left:0px;"></span></strong><br/>
					<input type="text" name="userPhoneCode" id="userPhoneCode" placeholder="请输入验证码" value="" />
					
				
				</div><!-- .name -->
				
				
				<div class="email">
				
					<strong>重输密码: </strong><br/>
					<input type="text" name="userPassword"  id="userPassword" placeholder="密码确认" value="" />
				
				</div><!-- .email -->
				<!-- 
				<div class="comment">
					<strong>Comment:</strong><sup class="surely">*</sup><br/>
					<textarea name="comment"></textarea>
				</div>.comment -->
				
				<div class="submit">
					<input type="submit" value="提交" />
				</div><!-- .submit -->
			</form><!-- .contact -->
			
                </div><!-- .grid_8 -->
       </div><!-- #content -->
      
      <div class="clear"></div>
    </div><!-- .container_12 -->
  </section><!-- #main -->
  
  <div class="clear"></div>
    
 
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540'charset='gb2312'></script></div>
</body>


</html>

<script type="text/javascript">

//验证码倒计时
	var wait = 60;
	function time(obj) {
		if(wait==0) {
			$("#phoneCode").removeAttr("disabled");
			$("#phoneCode").val("获取验证码");
			wait = 60;
		}else {
			$("#phoneCode").attr("disabled","true");
			$("#phoneCode").val(wait+"秒后重试");
			wait--;
			//倒计时方法
			setTimeout(function() {		
				time(obj);
				//间隔为1s
			},1000);	
		}
	}
	
	
		$(document).ready(function(){
			
			// 对手机号输入框绑定失去焦点事件
			$("#userPhone").blur(function(){
				// 获取输入的账号
				var userPhone = $(this).val().trim();
				if(userPhone == ""){
					$("#userPhoneTip").text("手机号不能为空");
					$("#userPhoneTip").addClass("error");
				}else{
					// 发送ajax请求
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/UserServlet",
						type:"GET",
						data:{op:"existsOfUserPhone",userPhone:userPhone},
						dataType:"text",
						success:function(result,status,xhr){
							if(result=="true"){ 
								$("#userPhoneTip").text("手机号已存在");
								$("#userPhoneTip").addClass("error");
								// 注册按钮设置不可用
								$("#register").prop("disabled",true);
							}else if(result=="false"){// 用户名不存在
								$("#userPhoneTip").text("手机号可用");
								$("#userPhoneTip").addClass("success");
								// 注册按钮设置可用
								$("#register").prop("disabled",false);
								
								
								
								
								
								
								
							}
						},
						error:function(xhr,status,error){
							alert("ajax异步请求失败");
						}
					});
				}
				return false;
			});
		
			// 对账号输入框绑定失去焦点事件
			$("#userAccount").blur(function(){   
				// 获取输入的账号
				var userAccount = $("#userAccount").val().trim();
			if(userAccount == ""){
					$("#userAccountTip").text("账号不能为空");
					$("#userAccountTip").addClass("error");
				}else{
					// 发送ajax请求
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/UserServlet",
						type:"GET",
						data:{op:"existsOfUserAccount",userAccount:userAccount},
						dataType:"text",
						success:function(result,status,xhr){
							if(result=="true"){ // 用户名存在
								$("#userAccountTip").text("账号已存在");
								$("#userAccountTip").addClass("error");
								// 注册按钮设置不可用
								$("#register").prop("disabled",true);
							}else if(result=="false"){// 用户名不存在
								$("#userAccountTip").text("账号可用");
								$("#userAccountTip").addClass("success");
								// 注册按钮设置可用
								$("#register").prop("disabled",false);
							}
						},
						error:function(xhr,status,error){
							alert("ajax异步请求失败");
						}
					}); 
				} 
			});
			
			
			// 对店铺名输入框绑定失去焦点事件
			$("#userName").blur(function(){   
				// 获取输入的账号
				var userName = $("#userName").val().trim();
			if(userName == ""){
					$("#userNameTip").text("买家用户名不能为空");
					$("#userNameTip").addClass("error");
				}else{
					// 发送ajax请求
					$.ajax({
						async:true,
						url:"${pageContext.request.contextPath}/front/UserServlet",
						type:"GET",
						data:{op:"existsOfUserName",userName:userName},
						dataType:"text",
						success:function(result,status,xhr){
							if(result=="true"){ // 用户名存在
								$("#userNameTip").text("买家用户名已存在");
								$("#userNameTip").addClass("error");
								// 注册按钮设置不可用
								$("#register").prop("disabled",true);
							}else if(result=="false"){// 用户名不存在
								$("#userNameTip").text("买家用户名可用");
								$("#userNameTip").addClass("success");
								// 注册按钮设置可用
								$("#register").prop("disabled",false);
							}
						},
						error:function(xhr,status,error){
							alert("ajax异步请求失败");
						}
					}); 
				} 
			});
			
			
			$("#phoneCode").removeAttr("disabled");
			//发送验证码
			$("#phoneCode").click(function(){	
				$.ajax({
					url:"${pageContext.request.contextPath}/front/UserServlet",
					data:{
						op:"phoneCode",userPhone:$("#userPhone").val()
					},
					type:"post",
					async:true,
					dataType:"text",
					success : function(data) {
						if(data=='true'){
							alert("验证码发送成功");
							time(this);
						} else {
							alert("验证码发送失败");
						}
					},
					error : function() {
						alert("error");
					}
				});
			});
			
			

			//验证验证码
			$("#userPhoneCode").change(function(){	
				$.ajax({
					url:"${pageContext.request.contextPath}/front/UserServlet",
					data:{
						op:"userPhoneCodeCheck",userPhoneCode:$("#userPhoneCode").val()
					},
					type:"post",
					async:true,
					dataType:"text",
					success : function(data) {
						if(data=='true'){
							alert("恭喜您，验证成功");
						} else {
							alert("验证失败");
						}
					},
					error : function() {
						alert("error");
					}
				});
			});
			
			
			
			//密码长度验证
			 $("#userPassword").blur(function(){
		            var re = /^\w{6,12}$/;
		            if ( $(this).parent().find( "span" ).length > 12 ||  $(this).parent().find( "span" ).length < 6 ) {
		                $(this).parent().find( "span" ).remove();    
		            }
		            if ( re.test( $(this).val() ) ) {
		               /*  $(this).top( "<span class='ok'>格式正确</span>" ); */
		                alert("密码格式正确！")
		                
		            }else {
		                /* $(this).after( "<span class='error'>格式错误</span>" ); */
		                alert("密码格式错误！")
		            }
		        });
			
			//双重密码验证
			 $("#userPassword").blur(function(){
				 
				 if($("#userPassword").val()!=$("#userPassword1").val()){alert("密码不一致")}
		           
		        });
			
			
		     //图片放大功能
		     $("#img3").bind("click",function(){
	            var width = $(this).width();
	             if(width==230){
		
	                   $(this).width(520);
	                   $(this).height(520);
	               }else{
	                   $(this).width(230);
	                   $(this).height(230); 
	               }      
	        });
			
			
		 	// 提示文件上传不符合的操作
				var fileuploaderror = "${param.fileuploaderror}";
				if(fileuploaderror!=""){
					alert("图片文件只能是jpg/jpeg/png格式");
				}
			
			
		});
		
		
		
			// 选择文件的改变事件,生成图片预览功能
		function setImagePriview() {
			// 得到文件列表数组,获取数组中的第一个文件
			var file = document.getElementById("file").files[0];
			// 没有选择文件
			if(file==null){
				document.getElementById("img3").src = "http://placehold.it/230x230&amp;text=Photo";
			}else{
				// 构建一个文件渲染对象
				var reads = new FileReader();
				// FileReader.readAsDataURL 读取指定Blob或File的内容
				reads.readAsDataURL(file);
				// 获取文件数据
				reads.onload = function(){
					// 显示图片
					document.getElementById("img3").src = this.result;
				}
			}
			
		}
		
	
		
	
	     
	   //发送验证码
/* 	   $(function(){
			$("#phoneCode").removeAttr("disabled");
			//发送验证码
			$("#phoneCode").click(function(){	
				$.ajax({
					url:"${pageContext.request.contextPath}/front/UserServlet",
					data:{
						op:"phoneCode",userPhone:$("#userPhone").val()
					},
					type:"post",
					async:true,
					dataType:"text",
					success : function(data) {
						if(data=='true'){
							alert("验证码发送成功");
							time(this);
						} else {
							alert("验证码发送失败");
						}
					},
					error : function() {
						alert("error");
					}
				});
			});
			
			

			//验证验证码
			$("#userPhoneCode").change(function(){	
				$.ajax({
					url:"${pageContext.request.contextPath}/front/UserServlet",
					data:{
						op:"userPhoneCodeCheck",userPhoneCode:$("#userPhoneCode").val()
					},
					type:"post",
					async:true,
					dataType:"text",
					success : function(data) {
						if(data=='true'){
							alert("恭喜您，验证成功");
						} else {
							alert("验证失败");
						}
					},
					error : function() {
						alert("error");
					}
				});
			}); 
		})
 */
         
     


   
 
	     
	     
	     
	     
	     

	</script>