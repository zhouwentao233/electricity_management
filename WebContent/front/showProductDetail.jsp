<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE HTML>
<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>电商平台</title>

  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/jquery.jqzoom.css" type="text/css">

  <script src="js/jquery-1.7.2.min.js" ></script>
  <script src="js/html5.js" ></script>
  <script src="js/jflow.plus.js" ></script>
  <script src="js/jquery.carouFredSel-5.2.2-packed.js"></script>
  <script src="js/checkbox.js"></script>
  <script src="js/radio.js"></script>
  <script src="js/selectBox.js"></script>
  <script src="js/jquery.jqzoom-core.js" ></script>

  <script>
	$(document).ready(function() {
		$('.jqzoom').jqzoom({
			zoomType: 'standard',
			lens:true,
			preloadImages: true,
			alwaysOn:false
		});
	});
  </script>

  <script>
	$(document).ready(function() {
		$("select").selectBox();
	});
  </script>

  <script>
	$(document).ready(function() {
		$('#wrapper_tab a').click(function() {
			if ($(this).attr('class') != $('#wrapper_tab').attr('class') ) {
				$('#wrapper_tab').attr('class',$(this).attr('class'));
			}
			return false;
		});
	});
  </script>

  <script>
	$(function() {
	  $('#list_product').carouFredSel({
		prev: '#prev_c1',
		next: '#next_c1',
		auto: false
	  });
          $('#list_banners').carouFredSel({
		prev: '#ban_prev',
		next: '#ban_next',
		scroll: 1,
		auto: false
	  });
	  $('#thumblist').carouFredSel({
		prev: '#img_prev',
		next: '#img_next',
		scroll: 1,
		auto: false,
		circular: false,
	  });
	  $(window).resize();
	});
  </script>
  <script>
       $(document).ready(function(){
	      $("button").click(function(){
		     $(this).addClass('click')
	      });
       })
  </script>

</head>
<body>
   <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li ><a href="index.jsp">首页</a></li>
            <li class="curent"><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>


  <div class="clear"></div>

  <section id="main">
    <div class="container_12">
       <div id="sidebar" class="grid_3">
	      <aside>
		     <h3 id="return">返回上一页</h3>
	      </aside><!-- #categories_nav -->

	      <aside id="specials" class="specials">
		     <h3 id = "shopName">店铺名称</h3>

		     </aside><!-- #specials -->

       </div><!-- .sidebar -->

       <div id="content" class="grid_9">
       	  <input type ="hidden" id="productId">
	      <h1 class="page_title" id = "productName" >商品名称</h1>

		<div class="product_page">
			<div class="grid_4 img_slid" id="products">
				<img class="sale" src="images/sale.png" alt="Sale"/>
				<div class="preview slides_container">
					<div class="prev_bg">
						<a href="#" class="jqzoom" rel='' title="" id="imgHref">
							<img src="#"  title="" alt="" id="img"/>
						</a>
					</div>
				</div><!-- .prev -->
			</div><!-- .grid_4 -->

			<div class="grid_5">
				<div class="entry_content">
					<p style="height:188px;" class="productIntroduce"></p>
					<div class="ava_price">
						<div class="price">
							<div class="price_new">￥<span class="productPrice productPriceShow"></span></div>
							<div class="price_old">￥<span class="productPrice"></span></div>
						</div><!-- .price -->
					</div><!-- .ava_price -->

					<div class="block_cart">
						<div class="obn_like">
							<div class="like"><a href="#like" id="like" class="like" data-productid="${param.productId }">收藏商品</a></div>
						</div>

						<div class="cart">
							<a href="#" class="bay" id = "addCart">加入购物车</a>
							<input type="text" name="productNumber" class="number" value="1" />
							<span>数量:</span>
						</div>
						<div class="clear"></div>
					</div><!-- .block_cart -->
				</div><!-- .entry_content -->

			</div><!-- .grid_5 -->

			<div class="clear"></div>

			<div class="grid_9" >
				<div id="wrapper_tab" class="tab1">
					<a href="#" class="tab1 tab_link">评论</a>
					<a href="#" class="tab2 tab_link">商品介绍</a>
					<a href="#" class="tab3 tab_link">全部评论</a>

					<div class="clear"></div>

					<div class="tab1 tab_body">
						<form class="add_comments">
							<h4>评论</h4>

							<div class="evaluation">
								<div class="quality">
									<strong>评价</strong><sup class="surely">*</sup>
									<span class=""><input  type="radio" name="commentScore" tabindex="0" value="1" checked></span><span class="eva_num">1</span>
									<span class=""><input  type="radio" name="commentScore"  tabindex="0" value="2" checked></span><span class="eva_num">2</span>
									<span class=""><input  type="radio" name="commentScore" tabindex="0" value="3" checked></span><span class="eva_num">3</span>
									<span class=""><input  type="radio" name="commentScore" tabindex="0" value="4" checked></span><span class="eva_num">4</span>
									<span class=""><input  type="radio" name="commentScore" tabindex="0" value="5" checked></span><span class="eva_num">5</span>
								</div>
								<div class="clear"></div>
							</div><!-- .evaluation -->


							<div class="text_review">
								<strong>评论内容</strong><sup class="surely" >*</sup><br>
								<textarea name="text" id="commentContent"> </textarea>
							</div><!-- .text_review -->

							<input type="button" value="确认评论" id="submitComment">
						</form>
					<div class="clear"></div>
					</div><!-- .tab1 .tab_body -->
					
					<div class="tab2 tab_body">
						<h4>商品简介</h4>
						<p class="productIntroduce"></p>
					<div class="clear"></div>
					</div><!-- .tab1 .tab_body -->

					<div class="tab3 tab_body">
						<h4>商品全部评论</h4>
						<ul class="comments" id = "comments"></ul><!-- .comments -->
					<div class="clear"></div>
					</div><!-- .tab2 .tab_body -->
					<div class="clear"></div>
				</div>​<!-- #wrapper_tab -->
				<div class="clear"></div>
			</div><!-- .grid_9 -->

			<div class="clear"></div>
		</div><!-- .product_page -->
		<div class="clear"></div>

       </div><!-- #content -->

      <div class="clear"></div>

    </div><!-- .container_12 -->
  </section><!-- #main -->

  <div class="clear"></div>

  
<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		// 获取商品id
		let productId = "${param.productId}";
		// 店铺id
		let sellerId ;
		// 获取商品详细信息
		$.ajax({
			url:"${pageContext.request.contextPath}/front/productshow",
			data:{
				op:"detail",
				productId:productId
			},
			dataType:"json",
			success:function(result,status,xhr){
				$("#productId").text(result.productId);
				$("#productName").text(result.productName);
				$(".productIntroduce").text(result.productIntroduce);
				$(".productPrice").text(result.productPrice);
				$("#shopName").text(result.seller.sellerShopName);
				$("#img").attr("src","${pageContext.request.contextPath}/download?fileName="+result.productPicture);
				$("#imgHref").attr("href","${pageContext.request.contextPath}/download?fileName="+result.productPicture)
				sellerId = result.seller.sellerId;
			},
			error:function(){
				alert("ajax请求失败");
			}
		});
		
		// 返回上一页
		$("#return").click(function(){
			window.history.go(-1);
		});
		
		// 加入购物车
		$("#addCart").click(function(){
			// 用户id
			let userId = "${sessionScope.user.userId}";	
			if(userId == ""){
				alert("请登录后操作");
				return;
			}
			// 商品数量
			let cartCount = $("input[name='productNumber']").val();
			// 商品总价(单价x数量)
			let cartPrice = parseInt($(".productPriceShow").text())*parseInt(cartCount);
			console.log($(".productPriceShow").text(),cartCount,cartPrice);
			// 添加购物车
			$.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/front/CartServlet",
				data:{
					userId:userId,
					productId:productId,
					cartCount:cartCount,
					cartPrice:cartPrice
				},
				success:function(){
					alert("添加成功");
					showChat(userId);
				},
				error:function(){
					alert("添加失败");
				}
			});
		});
		
		$("#shopName").click(function(){
			// 跳转到店铺页面
			window.location.href = '${pageContext.request.contextPath}/front/showSellerProduct.jsp?sellerId='+sellerId;
		});
		
		// 评论
		// 获取评论的ajax
		function sendAjax(){
			$.ajax({
				url:"${pageContext.request.contextPath}/front/comment",
				data:{
					productId:productId
				},
				dataType:"json",
				success:function(result,status,xhr){
					$("#comments").empty(); // 清空评论
					$.each(result,function(index,val){
						// 如果是一级评论，则输出
						if(val.commentFatherId == 0){
							// 输出一级评论
							html = '<li class="comment" data-commentid="'+val.commentId+'">'
							+'<div class="autor">'+val.user.userName+'</div>, <time datetime='+val.commentTime+'">'+val.commentTime+'</time>'
							+'<div class="evaluation">'
							+'<div class="quality">'
							+'<strong>评价</strong>';
							// 评分
							let count = val.commentScore;
							for(let i = 0; i < 5 ; i++){
								if(count>0){
									count--;
									html += '<a class="plus" href="#"></a>';
								}else{
									html += '<a href="#"></a>';
								}
							}
							html += '</div>'
							+'<span style="position: relative;left:555px;"><span class="reply">回复</span>|<span class="change" data-commentid="'+val.commentId+'">折叠</span></span>'
							+'<div class="clear"></div>'
							+'</div>'
							+'<p>'+val.commentContent+'</p>'
							+'</li>';
							
							
							let commentId = val.commentId; // 获取当前评论编号
							$.each(result,function(i,v){
								// 如果当前父评论编号为一级评论编号，则作为二级评论输出
								if(v.commentFatherId == commentId){
									// 输出二级评论
									html += '<div class="'+commentId+'" style="background:#f1f3f5;"><li style="position: relative;left:30px;border:0px">'
									+'<div class="autor">'+v.user.userName+'</div>, <time datetime='+v.commentTime+'">'+v.commentTime+'</time>'
									+'<div class="evaluation">'
									+'<div class="clear"></div>'
									+'</div>'
									+'<p>'+v.commentContent+'</p>'
									+'</li></div>';
								}
							});
							$("#comments").append(html); // 追加数据
						}
					});
				},
				error:function(xhr,status,error){
					alert("获取评论失败");
				}
			});
		}
		
		// 1. 先获取商品的全部一级评论
		sendAjax();
		
		// 展开/收缩评论
		$(document).on("click",".change",function(){
			if($(this).text()=="折叠"){
				$(this).text("展开");
				$("."+$(this).data("commentid")).css("display","none");
			}else{
				$(this).text("折叠");
				$("."+$(this).data("commentid")).css("display","block");
			}
		});
		//  回复评论（只能登录操作）形成二级评论
		$(document).on("click",".reply",function(){
			$(".replyComment,.replyCommit").remove();
			$(this).parents(".comment").append("<input type='text' style='width:600px' class='replyComment'><input type='button' style='width:100px;height:35px; background:#2AB4C4;' value='提交' class='replyCommit'>");
		});
		
		//回复完成删除
		$(document).on("click",".replyCommit",function(){
			// 评论内容
			let commentContent = $(".replyComment").val();
			if(commentContent == ""){
				alert("评论不能为空");
				return;
			}
			// 用户id
			let userId = "${sessionScope.user.userId}";
			// 父类评论
			let commentFatherId = $(this).parent().data("commentid");
			if(userId == ""){
				alert("登录后才能评论");
			}else{
				// 发送ajax
				$.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/front/comment",
					data:{
						commentContent:commentContent,
						userId:userId,
						commentFatherId:commentFatherId,
						productId:productId
					},
					success:function(){
						// 局部更新dom 提交框下面插入
						sendAjax();
					},
					error:function(){
						alert("回复失败");
					}
				});
			}
			
			
		});
		
		
		// 收藏商品
		$(document).on("click","#like",function(){
			// 获取用户id
			let userId = "${sessionScope.user.userId}";
			if(userId == ""){
				alert("请登录后收藏");
			}else{
				// 获取商品id
				let productId = $(this).data("productid");
				$.ajax({
					url:"${pageContext.request.contextPath}/front/collectServlet",
					data:{
						op:"addProduct",
						userId:userId,
						productId:productId
					},
					success:function(){
						alert("收藏成功");
					},
					error:function(){
						alert("ajax请求失败");
					}
				})
			}
		});
		
		
		// 提交评论
		$("#submitComment").click(function(){
			// 获取评价
			var commentScore = $('input[name="commentScore"]:checked').val();
			
			// 获取内容
			let commentContent = $("#commentContent").val().trim();
			
			if(commentScore == null || commentContent == ""){
				alert("不能为空");
				return;
			}
			
			// 获取商品id
			let productId = "${param.productId}";
			
			// 用户id
			let userId = "${sessionScope.user.userId}";
			
			// 保存评论
			if(userId == ""){
				alert("登录后才能评论");
			}else{
				// 发送ajax
				$.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/front/comment",
					data:{
						commentContent:commentContent,
						userId:userId,
						commentFatherId:0,
						productId:productId,
						commentScore:commentScore
					},
					success:function(){
						// 局部更新dom 提交框下面插入
						alert("评论成功");
						location.reload();
					},
					error:function(){
						alert("回复失败");
					}
				});
			}
		});
	});

</script>
</html>
