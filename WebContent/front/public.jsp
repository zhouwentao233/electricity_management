<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="container_12">
    <div id="top">
      <div class="grid_6">
        <div class="welcome">
        ${sessionScope.user==null?"欢迎游客浏览，你可以<a href=\"userSignIn.jsp\">登录</a> 或者 <a href=\"userSignUp.jsp\">创建账号</a>.":""}
         
        </div><!-- .welcome -->
      </div><!-- .grid_6 -->
        
    </div><!-- #top -->

    

    <header id="branding">
      <div class="grid_3">
        <hgroup>
          <h1 id="site_logo" ><a href="#" title=""><img src="images/logo.png" alt="Online Store Theme Logo"/></a></h1>
          <h2 id="site_description">电商平台</h2>
        </hgroup>
      </div><!-- .grid_3 -->

      <div class="grid_3">
        <h2 style="display:block">电商平台</h2>
      </div><!-- .grid_3 -->

      <div class="grid_6">
        <ul id="cart_nav">
          <li>
            <a class="cart_li" href="#">购物车</a>
            
            <ul class="cart_cont" id="showCart">
            </ul>
          </li>
        </ul>

        <nav class="private">
          <ul>
           <c:if test="${sessionScope.user==null }">
	           <li><a href="userSignIn.jsp">登录</a></li>
				<li class="separator">|</li>
	            <li><a href="userSignUp.jsp">注册</a></li>
            </c:if>
            <c:if test="${sessionScope.user!=null }">
            	<li>
            	<a href="#">${sessionScope.user.userName }</a>
            	
            	</li>
            	<li>
            	<a href="${pageContext.request.contextPath}/front/UserServlet?op=logout">退出登录</a>
            	</li>
            </c:if>
            
          </ul>
        </nav><!-- .private -->
      </div><!-- .grid_6 -->
    </header><!-- #branding -->
  </div><!-- .container_12 -->

  <div class="clear"></div>
  <script type="text/javascript">
  function showChat(userId){
  		$.ajax({
  			url:"${pageContext.request.contextPath}/front/CartServlet",
  			data:{
  				op:"getCartList",
  				userId:userId
  			},
  			dataType:"json",
  			success:function(result,status,xhr){
  				$("#showCart").empty();
  				let html = '<li class="no_border"><p>列表显示</p></li>';
  				$.each(result,function(index,val){
  					html += '<li>'
  						+'<a href="cart.jsp" class="prev_cart"><div class="cart_vert"><img src="${pageContext.request.contextPath}/download?fileName='+val.product.productPicture+'" alt="" title="" width="48px" height="47px" /></div></a>'
  						+'<div class="cont_cart">'
  						+'<h4>'+val.product.productName+'</h4>'
  						+'<div class="price">'+val.cartCount+' x ￥'+val.product.productPrice+'</div>'
  						+'</div>'
  						+'<div class="clear"></div>'
  						+'</li>' ;
  					
  				});
  				html += '<li class="no_border">'
	  	  				+'<a href="cart.jsp" class="view_cart">查看购物车</a>'
	  	  				+'</li>';
  				$("#showCart").append(html);
  			},
  			error:function(xhr,status,error){
  				alert("购物车ajax请求失败");
  			}
  		});
	}
  	/* 显示购物车*/
  	$(document).ready(function(){
  		
  		// 获取id
  		let userId = "${sessionScope.user.userId}";
  		console.log("..."+userId);
  		if(userId != ""){
  			showChat(userId);
  		}
  	});
  	
  	
  </script>
