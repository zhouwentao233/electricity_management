<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
<!DOCTYPE HTML>
<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="UTF-8">

  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>电商管理系统</title>

  <link rel="shortcut icon" href="favicon.ico">
  <link href="css/style.css" media="screen" rel="stylesheet" type="text/css">
  <link href="css/grid.css" media="screen" rel="stylesheet" type="text/css">

  <script src="js/jquery-1.7.2.min.js" ></script>
  <script src="js/html5.js" ></script>
  <script src="js/jflow.plus.js" ></script>
  <script src="js/jquery.carouFredSel-5.2.2-packed.js"></script>

  <script>
	$(document).ready(function(){
	    
  });
  </script>
  <script>
	
  </script>
  <script>
       $(document).ready(function(){
	      $("button").click(function(){
		     $(this).addClass('click')
	      });
       })
  </script>

</head>
<body>
  <%@include file="public.jsp" %>
  <div id="block_nav_primary">
    <div class="container_12">
      <div class="grid_12">
        <nav class="primary">
          <ul>
            <li class="curent"><a href="index.jsp">首页</a></li>
            <li><a href="showProduct.jsp">分类</a></li>
            <li><a href="#">店铺</a></li>
            <li>
              <a href="cart.jsp">购物车</a>
            </li>
            <li><a href="order.jsp">订单</a></li>
            <li  class=""><a href="#">个人中心</a>
            	<ul class="sub">
            		<li><a  href="userCenter.jsp">个人信息修改</a></li>
            		<li ><a href="showCollect.jsp">地址和收藏信息</a></li>
            	</ul>
            </li>
          </ul>
        </nav><!-- .primary -->
      </div><!-- .grid_12 -->
    </div><!-- .container_12 -->
  </div><!-- .block_nav_primary -->

  <div class="clear"></div>

  <div class="container_12">
    <div class="grid_12">
        <div class="slidprev"><span>Prev</span></div>
        <div class="slidnext"><span>Next</span></div>
        <div id="slider">
          <div id="slide1">
            <img src="${pageContext.request.contextPath}/download?fileName=index_01.jpg" alt="" title="" />
            <div class="slid_text">
              <h3 class="slid_title"><span>电商平台</span></h3>
              <p><span>欢迎你的入驻</span></p>
              <p><span>我们有最优质的服务</span></p>
              <p><span>和最好的团队</span></p>
            </div>
          </div>

          <div id="slide2">
            <img src="${pageContext.request.contextPath}/download?fileName=index_02.jpg" alt="" title="" />
            <div class="slid_text">
              <h3 class="slid_title"><span>电商平台</span></h3>
              <p><span>欢迎你的入驻</span></p>
              <p><span>我们有最优质的服务</span></p>
              <p><span>和最好的团队</span></p>
            </div>
          </div>

          <div id="slide3">
            <img src="${pageContext.request.contextPath}/download?fileName=index_03.jpg" alt="" title="" />
            <div class="slid_text">
              <h3 class="slid_title"><span>电商平台</span></h3>
              <p><span>欢迎你的入驻</span></p>
              <p><span>我们有最优质的服务</span></p>
              <p><span>和最好的团队</span></p>
            </div>
          </div>
        </div><!-- .slider -->
        <div id="myController">
          <div class="control"><span>1</span></div>
          <div class="control"><span>2</span></div>
          <div class="control"><span>3</span></div>
        </div>


    </div><!-- .grid_12 -->
  </div><!-- .container_12 -->
  <div class="copyrights">Collect from 网站模板</div>
  <div class="clear"></div>

  <section id="main" class="home">
    <div class="container_12">
      <div class="carousel">
        <div class="c_header">
          <div class="grid_10">
            <h2>销量排行</h2>
          </div><!-- .grid_10 -->

        </div><!-- .c_header -->

        <div class="list_carousel">

        <ul id="sale_product" class="list_product"> <!-- 销量排行前5商品显示-->
        </ul><!-- #list_product -->
        </div><!-- .list_carousel -->
      </div><!-- .carousel -->
	
      <div class="carousel">
        <div class="c_header">
          <div class="grid_10">
            <h2>新品展示</h2>
            <hr style="margin-bottom:20px;"/>
          </div><!-- .grid_10 -->

        </div><!-- .c_header -->

        <div class="list_carousel">
        <ul id="new_product" class="list_product"> <!-- 新品前5商品显示-->
        </ul><!-- #list_product2 -->
        </div><!-- .list_carousel -->
      </div><!-- .carousel -->
    </div><!-- .container_12 -->
  </section><!-- #main -->

  <div class="clear"></div>

<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		// 获取销量排行
		$.ajax({
			url:"${pageContext.request.contextPath}/front/productshow",
			data:{
				op:"salesTop"
			},
			dataType:"json",
			success:function(result,status,xhr){
				$.each(result,function(index,val){
					html = '<li class="">'
						+'<div class="grid_3 product">'
						+'<img class="sale" src="images/sale.png" alt="Sale"/>'
						+'<div class="prev">'
						+'<a href="showProductDetail.jsp?productId='+val.productId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.productPicture+'" alt="" title="" /></a>'
						+'</div>'
						+'<h3 class="title">'+val.productName+'</h3>'
						+'<div class="cart">'
						+'<div class="price">'
						+'<div class="vert">'
						+'<div class="price_new">￥'+val.productPrice+'</div>'
						+'<div class="price_old">￥'+val.productPrice+'</div>'
						+'</div>'
						+'</div>'
						+'<a href="#" class="obn"></a>'
						+'<a href="#like" class="like" data-productid="'+val.productId+'"></a>'
						+'<a href="#bay" class="bay" data-productid="'+val.productId+'" data-price="'+val.productPrice+'"></a>'
						+'</div>'
						+'</div>'
						+'</li>';
					$("#sale_product").append(html);
				});
			}
		});
		
		// 获取新品排行
		$.ajax({
			url:"${pageContext.request.contextPath}/front/productshow",
			data:{
				op:"newTop"
			},
			dataType:"json",
			success:function(result,status,xhr){
				$.each(result,function(index,val){
					html = '<li class="">'
						+'<div class="grid_3 product">'
						+'<img class="sale" src="images/new.png" alt="Sale"/>'
						+'<div class="prev">'
						+'<a href="showProductDetail.jsp?productId='+val.productId+'"><img src="${pageContext.request.contextPath}/download?fileName='+val.productPicture+'" alt="" title="" /></a>'
						+'</div>'
						+'<h3 class="title">'+val.productName+'</h3>'
						+'<div class="cart">'
						+'<div class="price">'
						+'<div class="vert">'
						+'<div class="price_new">￥'+val.productPrice+'</div>'
						+'<div class="price_old">￥'+val.productPrice+'</div>'
						+'</div>'
						+'</div>'
						+'<a href="#" class="obn"></a>'
						+'<a href="#like" class="like" data-productid="'+val.productId+'"></a>'
						+'<a href="#bay" class="bay" data-productid="'+val.productId+'" data-price="'+val.productPrice+'"></a>'
						+'</div>'
						+'</div>'
						+'</li>';
					$("#new_product").append(html);
				});
			}
		});
		
		// 收藏商品
		$(document).on("click",".like",function(){
			// 获取用户id
			let userId = "${sessionScope.user.userId}";
			if(userId == ""){
				alert("请登录后收藏");
			}else{
				// 获取商品id
				let productId = $(this).data("productid");
				$.ajax({
					url:"${pageContext.request.contextPath}/front/collectServlet",
					data:{
						op:"addProduct",
						userId:userId,
						productId:productId
					},
					success:function(){
						alert("收藏成功");
					},
					error:function(){
						alert("ajax请求失败");
					}
				})
			}
		});
		
		
		// 添加购物车
		$(document).on("click",".bay",function(){
			let userId = "${sessionScope.user.userId}";
			let productId = $(this).data("productid");
			let price = $(this).data("price");
			if(userId != ""){
				$.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/front/CartServlet",
					data:{
						userId:userId,
						productId:productId,
						cartCount:1,
						cartPrice:price
					},
					success:function(){
						alert("添加成功");
						showChat(userId);
					},
					error:function(){
						alert("添加失败");
					}
				});
			}else{
				alert("请登录后添加购物车");
			}
			
		});
	});
	
	$("#myController").jFlow({
		controller: ".control", // must be class, use . sign
		slideWrapper : "#jFlowSlider", // must be id, use # sign
		slides: "#slider",  // the div where all your sliding divs are nested in
		selectedWrapper: "jFlowSelected",  // just pure text, no sign
		width: "984px",  // this is the width for the content-slider
		height: "480px",  // this is the height for the content-slider
		duration: 400,  // time in miliseconds to transition one slide
		prev: ".slidprev", // must be class, use . sign
		next: ".slidnext", // must be class, use . sign
		auto: true
});
</script>
</html>

