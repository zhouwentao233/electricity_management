package com.zretc.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	
	/**
	 * @author 赵坤
	 * Md5加密工具，32位加密
	 * @param inputText 需加密字符串
	 * @return 加密后的字符串
	 */
	public static String getMd5(String inputText) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(inputText.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for(int offset = 0; offset<b.length; offset++) {
				i = b[offset];
				if(i<0)
					i += 256;
				if(i<16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
