﻿package com.zretc.util;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016103000778218";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDE+jzyerDxh1TmgmwXIFpcAmn1mDuTkBK3j4/O8PdFiU2FdYgNy6ichgMorbwvYUlOL5zZxjVtmcASYzPzWUiFUcjIKjUIChP51e6EuqU9Fptamg0NuD25/GBjNgKa5CLmnoMzVPVOA5Y7MC2gyhelX4at6+9wguNgxqS2LlgDsIu3unXcOB1vYN4jWLI306clMy+NuGXbwRra8SZAoHY1TK2t7WIye1Rw50VvXqAxHUL/WgTMielMSsUf2YNi3rvaVs7yS5FYxOBizrHvACn5oSxRqVYQnMErFMauVKow375uedxDmFYb5Kww2ey03bU6GT70t/0Fo8Pu0V/1kldjAgMBAAECggEAcWanY+niqIqDBFodpIrJuqLhIbVMxz1wqSELZ9YuZWgQqz7o3xhasBIvC2JKLvTRYSPkJzQHWlvjyjI2nGjQaYCWGTbDZSd4LKNPODbEHpE/Sz/ZOAbT8pkrXLKheU7SEPc9DXZh61IBUX4BJspyJpECVmq4ZIvWH8b82K2Bmk0r84qOY+n70/KmN+3i4MAzTQaA3ezgK2CDlZ1NPxEFOh4CZZjLL+SPIsziq5bm7XrY6CjcIYxLH6KtiYU0fTEBCyrI80w6jjkyH47VxE/IiqSjeqkemvs/1p2Y8FOAbqkUQGehf7FiDgmBAs9BnxMBuYYEKm5ruvsD0W9+BxOBMQKBgQD1KsDWJjkYUOPE1Wq2GPaL9MstpHbFlcKZShKPLpoax1UEIarzPP7ZhhkxZbAV4vBH5YNBVwvsmKJ3VT1Ea/gk1ccYrXivJ0G71xSXg9ymFGbwXoN0jet1k4OVZX99TgHBXdMEtkAv3XE4K6KH9+UrvHlJFPDlNMke+mRjJe6dhQKBgQDNrmGZNSi5AT3qUh9Anj0rKpYGQCyzSI3lFpHYaG/8Hnci6HJbk3+Nt1VcusnCJM1PG0rhg5M0yZoFKbRfoABRNkiMsIITGNR3TgRncr203fWfdtCLItkQTiEsoWbb26ayn3pVLNdiZ3Z8XSYaLojna1x6emexsPkMrn13HVThxwKBgQCs3O1E0YSWWYqPoa7xIF7ORdbworLAFB02uhyl6kTsNroCBL8lbE+BDmU7CCkE9ZNYDkLi0j5tHx4GqApf8QTw6+7Kvp2GWlal3Dv/rRDp5R7DwtBLYTmxGEbCEuO2dUcVkVMVUm+lkCyhX1rlJevnT6fw2JwV8IcSmZgotHkvhQKBgQCfsblNfxyzJ+euuzxQq4vKrDH3gHQ0byzcpf1vDQeFtRHDJL9FouvyWFoKuWxVNMXDmek11zbAUZcoUw0dAdm9NGx378POmAS+7PA2cbwhZgSDX3jjzpbCNidDw71dDtoK7g/Jehtw5+vy+0/OHDtnktAmuEsyh7mWwxPfHAL/mQKBgEf6jyiIfqCbAzs4XxI9u2IJ6/APF85pNsL0p6q0GyslnS1joONPfZN9DnPX5mHpgXgT6kHToGF0vgK1PllHpmfQwkKQMlfh338FBXKtGRc1BAcb4d4Gb8YlnIW8AzPi1ktMfNeh+bpw4fP31MaR18Qu66PPlObxa792ttiJZrpK";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2u70E1OLr3r0+aJMtxWg1WfFyVXsx3CFiDfiLQLBxrXYvQUnuBbuGVRCr9H8E+/16wJAbIhDwKMH/Squn4UFUjac1hF+jzZsODAJ4KUbnB0ifHftgUb5sqruwcHel+uRSwtJmPoLTMD6hRt9CIUyGMeteDxioQMiXSmFhkFScm07FF2EG/NM2/RBFSfciKpx1BD4XOudbPqJ9wNnoPNVvgtT5DK7RzIlAryYhn/++gns5gNGzR0qsPSTDFveJZ9PUGRypG+oWqE+HKp+b0Y2zCegAccPcnC51OQuiXqGYzlilKLgyfTkNc3Y0lcKihjTdbraocKecYVm9dLVKMGOmQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://loaclhost:8080/electricity_management/front/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/electricity_management/front/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
            
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

