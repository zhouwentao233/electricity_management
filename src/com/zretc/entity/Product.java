package com.zretc.entity;

/**
 * @author Product 商品信息表
 * 陈波
 */
public class Product {
	private Integer productId; // 商品编号  主键
	private String productName; // 商品名称
	private Seller seller; // 商家信息类  连接商家信息表
	private Integer productInventory; //  商品库存
	private Float productPrice; // 商品价格
	private String productPicture; // 商品图片
	private Type type; // 分类信息类   连接分类信息表
	private String productTime; // 上架时间
	private Integer productStatus; // 商品状态   0下架   1上架
	private String productIntroduce; // 商品介绍
	
	public Product() {
		super();
	}

	public Product(Integer productId, String productName, Seller seller, Integer productInventory, Float productPrice,
			String productPicture, Type type, String productTime, Integer productStatus, String productIntroduce) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.seller = seller;
		this.productInventory = productInventory;
		this.productPrice = productPrice;
		this.productPicture = productPicture;
		this.type = type;
		this.productTime = productTime;
		this.productStatus = productStatus;
		this.productIntroduce = productIntroduce;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Integer getProductInventory() {
		return productInventory;
	}

	public void setProductInventory(Integer productInventory) {
		this.productInventory = productInventory;
	}

	public Float getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Float productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductPicture() {
		return productPicture;
	}

	public void setProductPicture(String productPicture) {
		this.productPicture = productPicture;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getProductTime() {
		return productTime;
	}

	public void setProductTime(String productTime) {
		this.productTime = productTime;
	}

	public Integer getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}

	public String getProductIntroduce() {
		return productIntroduce;
	}

	public void setProductIntroduce(String productIntroduce) {
		this.productIntroduce = productIntroduce;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", seller=" + seller
				+ ", productInventory=" + productInventory + ", productPrice=" + productPrice + ", productPicture="
				+ productPicture + ", type=" + type + ", productTime=" + productTime + ", productStatus="
				+ productStatus + ", productIntroduce=" + productIntroduce + "]";
	}
	
}
