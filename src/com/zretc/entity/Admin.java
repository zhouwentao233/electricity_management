package com.zretc.entity;

/**
 * @author 周文涛
 * 管理员用户表
 */
public class Admin {

	private Integer adminId; // 管理员编号
	private String adminAccount; // 管理员账号
	private String adminPassword; // 管理员密码
	
	public Admin() {
		super();
	}
	public Admin(Integer adminId, String adminAccount, String adminPassword) {
		super();
		this.adminId = adminId;
		this.adminAccount = adminAccount;
		this.adminPassword = adminPassword;
	}
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getAdminAccount() {
		return adminAccount;
	}
	public void setAdminAccount(String adminAccount) {
		this.adminAccount = adminAccount;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", adminAccount=" + adminAccount + ", adminPassword=" + adminPassword
				+ "]";
	}
	
}
