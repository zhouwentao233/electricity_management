package com.zretc.entity;
/**
 * @author Cart 购物车信息表
 * 陈波
 */
public class Cart {
	private Integer cartId; // 购物车编号 主键
	private User user; //	用户信息类  连接用户信息表
	private Product product; // 商品信息类   连接商品信息表
	private Integer cartCount; // 数量
	private Float cartPrice; // 价格
	
	public Cart() {
		super();
	}

	public Cart(Integer cartId, User user, Product product, Integer cartCount, Float cartPrice) {
		super();
		this.cartId = cartId;
		this.user = user;
		this.product = product;
		this.cartCount = cartCount;
		this.cartPrice = cartPrice;
	}

	public Integer getCartId() {
		return cartId;
	}

	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getCartCount() {
		return cartCount;
	}

	public void setCartCount(Integer cartCount) {
		this.cartCount = cartCount;
	}

	public Float getCartPrice() {
		return cartPrice;
	}

	public void setCartPrice(Float cartPrice) {
		this.cartPrice = cartPrice;
	}

	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", user=" + user + ", product=" + product + ", cartCount=" + cartCount
				+ ", cartPrice=" + cartPrice + "]";
	}
	
}
