package com.zretc.entity;
/**
 * @author User 用户表
 * 陈波
 */
public class User {
	private Integer userId; // 用户编号  主键
	private String userAccount; // 用户账号
	private String userName; // 用户名称
	private String userPassword; // 用户密码
	private String userPicture; // 用户头像
	private String userPhone; //  手机号码
	
	public User() {
		super();
	}
	public User(Integer userId, String userAccount, String userName, String userPassword, String userPicture,
			String userPhone) {
		super();
		this.userId = userId;
		this.userAccount = userAccount;
		this.userName = userName;
		this.userPassword = userPassword;
		this.userPicture = userPicture;
		this.userPhone = userPhone;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserPicture() {
		return userPicture;
	}
	public void setUserPicture(String userPicture) {
		this.userPicture = userPicture;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userAccount=" + userAccount + ", userName=" + userName + ", userPassword="
				+ userPassword + ", userPicture=" + userPicture + ", userPhone=" + userPhone + "]";
	}
	
}
