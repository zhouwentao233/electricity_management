package com.zretc.entity;

/**
 * @author Comment 评论信息表
 * 陈波
 */
public class Comment {
	private Integer commentId; // 评论编号  主键
	private User user; //  用户信息类  连接用户信息表
	private Product product; // 商品信息类  连接商品信息表
	private String commentTime; // 评论时间
	private String commentContent; // 评论内容
	private Integer commentFatherId; // 父类评论编号
	private Integer commentScore; // 商品评分
	
	public Comment() {
		super();
	}
	
	public Comment(Integer commentId, User user, Product product, String commentTime, String commentContent,
			Integer commentFatherId, Integer commentScore) {
		super();
		this.commentId = commentId;
		this.user = user;
		this.product = product;
		this.commentTime = commentTime;
		this.commentContent = commentContent;
		this.commentFatherId = commentFatherId;
		this.commentScore = commentScore;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getCommentTime() {
		return commentTime;
	}

	public void setCommentTime(String commentTime) {
		this.commentTime = commentTime;
	}

	public String getCommentContent() {
		return commentContent;
	}

	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public Integer getCommentFatherId() {
		return commentFatherId;
	}

	public void setCommentFatherId(Integer commentFatherId) {
		this.commentFatherId = commentFatherId;
	}

	public Integer getCommentScore() {
		return commentScore;
	}

	public void setCommentScore(Integer commentScore) {
		this.commentScore = commentScore;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", user=" + user + ", product=" + product + ", commentTime="
				+ commentTime + ", commentContent=" + commentContent + ", commentFatherId=" + commentFatherId
				+ ", commentScore=" + commentScore + "]";
	}
	
}
