package com.zretc.entity;
/**
 * @author Order 订单信息表
 * 陈波
 */
public class Orders {
	private String orderId; // 订单编号  主键  自动生成
	private Float orderTotalPrice; // 订单总价格
	private String orderTime; // 下单时间
	private Integer orderStatus; // 订单状态   0已支付  1已发货  2已收货 
	private User user; // 用户信息类  连接用户信息表
	private String orderPayMethod; // 付款方式
	private String orderReceiveAddress; // 收货地址
	private String orderReceiveName; // 收货人
	private String orderReceivePhone; // 收货人电话
	
	public Orders() {
		super();
	}
	
	public Orders(String orderId, Float orderTotalPrice, String orderTime, Integer orderStatus, User user,
			String orderPayMethod, String orderReceiveAddress, String orderReceiveName, String orderReceivePhone) {
		super();
		this.orderId = orderId;
		this.orderTotalPrice = orderTotalPrice;
		this.orderTime = orderTime;
		this.orderStatus = orderStatus;
		this.user = user;
		this.orderPayMethod = orderPayMethod;
		this.orderReceiveAddress = orderReceiveAddress;
		this.orderReceiveName = orderReceiveName;
		this.orderReceivePhone = orderReceivePhone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Float getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public void setOrderTotalPrice(Float orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOrderPayMethod() {
		return orderPayMethod;
	}

	public void setOrderPayMethod(String orderPayMethod) {
		this.orderPayMethod = orderPayMethod;
	}

	public String getOrderReceiveAddress() {
		return orderReceiveAddress;
	}

	public void setOrderReceiveAddress(String orderReceiveAddress) {
		this.orderReceiveAddress = orderReceiveAddress;
	}

	public String getOrderReceiveName() {
		return orderReceiveName;
	}

	public void setOrderReceiveName(String orderReceiveName) {
		this.orderReceiveName = orderReceiveName;
	}

	public String getOrderReceivePhone() {
		return orderReceivePhone;
	}

	public void setOrderReceivePhone(String orderReceivePhone) {
		this.orderReceivePhone = orderReceivePhone;
	}

	@Override
	public String toString() {
		return "Orders [orderId=" + orderId + ", orderTotalPrice=" + orderTotalPrice + ", orderTime=" + orderTime
				+ ", orderStatus=" + orderStatus + ", user=" + user + ", orderPayMethod=" + orderPayMethod
				+ ", orderReceiveAddress=" + orderReceiveAddress + ", orderReceiveName=" + orderReceiveName
				+ ", orderReceivePhone=" + orderReceivePhone + "]";
	}

	
	
}
