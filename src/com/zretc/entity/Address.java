package com.zretc.entity;

/**
 * @author Address 收货地址信息表
 * 陈波
 */
public class Address {
	private Integer addressId; // 收货地址编号  主键
	private User user; // 用户信息类   连接用户信息表
	private String addressName; // 收货地址
	private Integer addressStatus; // 是否默认收货地址    0不默认    1默认
	private String receiveName; // 收货人姓名
	private String receivePhone; // 收货人电话
	
	public Address() {
		super();
	}
	public Address(Integer addressId, User user, String addressName, Integer addressStatus, String receiveName,
			String receivePhone) {
		super();
		this.addressId = addressId;
		this.user = user;
		this.addressName = addressName;
		this.addressStatus = addressStatus;
		this.receiveName = receiveName;
		this.receivePhone = receivePhone;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public Integer getAddressStatus() {
		return addressStatus;
	}
	public void setAddressStatus(Integer addressStatus) {
		this.addressStatus = addressStatus;
	}
	public String getReceiveName() {
		return receiveName;
	}
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}
	public String getReceivePhone() {
		return receivePhone;
	}
	public void setReceivePhone(String receivePhone) {
		this.receivePhone = receivePhone;
	}
	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", user=" + user + ", addressName=" + addressName
				+ ", addressStatus=" + addressStatus + ", receiveName=" + receiveName + ", receivePhone=" + receivePhone
				+ "]";
	}
	
}
