package com.zretc.entity;
/**
 * @author Seller 商家信息表
 * 陈波
 */
public class Seller {
	private Integer sellerId; // 商家编号   主键
	private String sellerAccount; // 商家账号
	private String sellerPassword; // 商家密码
	private String sellerAddress; // 商家地址
	private String sellerPhone; // 商家联系电话
	private String sellerShopName; // 商家名称
	private String sellerLogo; // 商家Logo图标
	private Integer sellerStatus; // 商家状态码   0申请中  1申请成功   2申请封禁
	
	public Seller() {
		super();
	}
	public Seller(Integer sellerId, String sellerAccount, String sellerPassword, String sellerAddress,
			String sellerPhone, String sellerShopName, String sellerLogo, Integer sellerStatus) {
		super();
		this.sellerId = sellerId;
		this.sellerAccount = sellerAccount;
		this.sellerPassword = sellerPassword;
		this.sellerAddress = sellerAddress;
		this.sellerPhone = sellerPhone;
		this.sellerShopName = sellerShopName;
		this.sellerLogo = sellerLogo;
		this.sellerStatus = sellerStatus;
	}
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	public String getSellerAccount() {
		return sellerAccount;
	}
	public void setSellerAccount(String sellerAccount) {
		this.sellerAccount = sellerAccount;
	}
	public String getSellerPassword() {
		return sellerPassword;
	}
	public void setSellerPassword(String sellerPassword) {
		this.sellerPassword = sellerPassword;
	}
	public String getSellerAddress() {
		return sellerAddress;
	}
	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress;
	}
	public String getSellerPhone() {
		return sellerPhone;
	}
	public void setSellerPhone(String sellerPhone) {
		this.sellerPhone = sellerPhone;
	}
	public String getSellerShopName() {
		return sellerShopName;
	}
	public void setSellerShopName(String sellerShopName) {
		this.sellerShopName = sellerShopName;
	}
	public String getSellerLogo() {
		return sellerLogo;
	}
	public void setSellerLogo(String sellerLogo) {
		this.sellerLogo = sellerLogo;
	}
	public Integer getSellerStatus() {
		return sellerStatus;
	}
	public void setSellerStatus(Integer sellerStatus) {
		this.sellerStatus = sellerStatus;
	}
	@Override
	public String toString() {
		return "Seller [sellerId=" + sellerId + ", sellerAccount=" + sellerAccount + ", sellerPassword="
				+ sellerPassword + ", sellerAddress=" + sellerAddress + ", sellerPhone=" + sellerPhone
				+ ", sellerShopName=" + sellerShopName + ", sellerLogo=" + sellerLogo + ", sellerStatus=" + sellerStatus
				+ "]";
	}
	
}
