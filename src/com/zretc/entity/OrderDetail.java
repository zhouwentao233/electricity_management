package com.zretc.entity;
/**
 * @author OrderDetail 订单详情信息表
 * 陈波
 */
public class OrderDetail {
	private Integer orderDetailId; // 订单详情编号 主键
	private Orders orders; // 订单信息类  连接订单信息表
	private Product product; // 商品信息类  连接商品信息表
	private Integer orderDetailCount; // 商品数量
 	private Float orderDetailPrice; // 价格
 	private Integer orderDetailStatus; // 订单状态
	public OrderDetail() {
		super();
	}
	
	public OrderDetail(Integer orderDetailId, Orders orders, Product product, Integer orderDetailCount,
			Float orderDetailPrice, Integer orderDetailStatus) {
		super();
		this.orderDetailId = orderDetailId;
		this.orders = orders;
		this.product = product;
		this.orderDetailCount = orderDetailCount;
		this.orderDetailPrice = orderDetailPrice;
		this.orderDetailStatus = orderDetailStatus;
	}

	public Integer getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(Integer orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getOrderDetailCount() {
		return orderDetailCount;
	}
	public void setOrderDetailCount(Integer orderDetailCount) {
		this.orderDetailCount = orderDetailCount;
	}
	public Float getOrderDetailPrice() {
		return orderDetailPrice;
	}
	public void setOrderDetailPrice(Float orderDetailPrice) {
		this.orderDetailPrice = orderDetailPrice;
	}
	public Integer getOrderDetailStatus() {
		return orderDetailStatus;
	}
	public void setOrderDetailStatus(Integer orderDetailStatus) {
		this.orderDetailStatus = orderDetailStatus;
	}
	@Override
	public String toString() {
		return "OrderDetail [orderDetailId=" + orderDetailId + ", orders=" + orders + ", product=" + product
				+ ", orderDetailCount=" + orderDetailCount + ", orderDetailPrice=" + orderDetailPrice
				+ ", orderDetailStatus=" + orderDetailStatus + "]";
	}
	
}
