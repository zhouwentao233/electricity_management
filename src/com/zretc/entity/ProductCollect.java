package com.zretc.entity;

/**
 * @author ProductCollect 商品收藏信息表
 * 陈波
 */
public class ProductCollect {
	private Integer productCollectId; // 收藏编号  主键
	private User user; // 用户信息类  连接用户信息表
	private  Product product; // 商品信息类  连接商品信息表
	
	public ProductCollect() {
		super();
	}

	public ProductCollect(Integer productCollectId, User user, Product product) {
		super();
		this.productCollectId = productCollectId;
		this.user = user;
		this.product = product;
	}

	public Integer getProductCollectId() {
		return productCollectId;
	}

	public void setProductCollectId(Integer productCollectId) {
		this.productCollectId = productCollectId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductCollect [productCollectId=" + productCollectId + ", user=" + user + ", product=" + product + "]";
	}
	
}
