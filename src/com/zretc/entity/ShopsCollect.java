package com.zretc.entity;

/**
 * @author ShopsCollect  店铺收藏信息表
 * 陈波
 */
public class ShopsCollect {
	private Integer shopsCollectId; // 店铺收藏编号   主键
	private Seller seller; // 商家信息类  连接商家信息表
	private User user; // 用户信息类  连接用户信息表 
	
	public ShopsCollect() {
		super();
	}
	
	public ShopsCollect(Integer shopsCollectId, Seller seller, User user) {
		super();
		this.shopsCollectId = shopsCollectId;
		this.seller = seller;
		this.user = user;
	}

	public Integer getShopsCollectId() {
		return shopsCollectId;
	}

	public void setShopsCollectId(Integer shopsCollectId) {
		this.shopsCollectId = shopsCollectId;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ShopsCollect [shopsCollectId=" + shopsCollectId + ", seller=" + seller + ", user=" + user + "]";
	}
	
}
