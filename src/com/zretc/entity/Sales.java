package com.zretc.entity;

/**
 * @author Sales 销售统计信息表
 * 陈波
 */
public class Sales {
	private Integer salesId; // 销售编号  主键
	private Product product; // 商品信息类   连接商品信息表
	private Integer salesCount; // 数量
	private Orders orders; // 订单信息类     连接订单信息表
	private Float salesPrice; // 销售价格
	private String salesDate;//销售时间
	
	
	
	public Sales() {
		super();
	}
	public Sales(Integer salesId, Product product, Integer salesCount, Orders orders, Float salesPrice,
			String salesDate) {
		super();
		this.salesId = salesId;
		this.product = product;
		this.salesCount = salesCount;
		this.orders = orders;
		this.salesPrice = salesPrice;
		this.salesDate = salesDate;
	}
	public Integer getSalesId() {
		return salesId;
	}
	public void setSalesId(Integer salesId) {
		this.salesId = salesId;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(Integer salesCount) {
		this.salesCount = salesCount;
	}
	public Orders getOrder() {
		return orders;
	}
	public void setOrder(Orders orders) {
		this.orders = orders;
	}
	public Float getSalesPrice() {
		return salesPrice;
	}
	public void setSalesPrice(Float salesPrice) {
		this.salesPrice = salesPrice;
	}
	public String getSalesDate() {
		return salesDate;
	}
	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}
	@Override
	public String toString() {
		return "Sales [salesId=" + salesId + ", product=" + product + ", salesCount=" + salesCount + ", orders="
				+ orders + ", salesPrice=" + salesPrice + ", salesDate=" + salesDate + "]";
	}
	
}
