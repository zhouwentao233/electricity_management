package com.zretc.entity;
/**
 * @author Type 商品分类信息表
 * 陈波
 */
public class Type {
	private Integer typeId; // 分类编号  主键
	private Integer typeFatherId; // 父类编号
	private String typeName; // 分类名称
	
	public Type() {
		super();
	}
	public Type(Integer typeId, Integer typeFatherId, String typeName) {
		super();
		this.typeId = typeId;
		this.typeFatherId = typeFatherId;
		this.typeName = typeName;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Integer getTypeFatherId() {
		return typeFatherId;
	}
	public void setTypeFatherId(Integer typeFatherId) {
		this.typeFatherId = typeFatherId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	@Override
	public String toString() {
		return "Type [typeId=" + typeId + ", typeFatherId=" + typeFatherId + ", typeName=" + typeName + "]";
	}
	
}
