package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zretc.dao.CartDao;
import com.zretc.entity.Cart;
import com.zretc.entity.Product;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;

/**
 * 购物车的dao层实现类
 * @author chenbo
 *
 */
public class CartDaoImpl implements CartDao{

	@Override
	public List<Cart> findCartAll(Integer userId) {
		List<Cart> list = new ArrayList<>();
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select")
		.append(" A.cart_id,")
		.append(" B.user_id,")
		.append(" C.product_id,")
		.append(" C.product_name,")
		.append(" C.product_picture,")
		.append(" C.product_introduce,")
		.append(" C.product_price,")
		.append(" A.cart_count,")
		.append(" A.cart_price")
		.append(" from cart A")
		.append(" inner join user B")
		.append(" on A.user_id = B.user_id")
		.append(" inner join product C")
		.append(" on A.product_id = C.product_id")
		.append(" where B.user_id = ?");
		rs = DBUtil.doQuery(sb.toString(),userId);
		try {
			while (rs.next()) {
				Integer cartId = rs.getInt("cart_id");
				Integer productId = rs.getInt("product_id");
				String productName = rs.getString("product_name");
				String productPicture= rs.getString("product_picture");
				String productIntroduce = rs.getString("product_introduce");
				Float productPrice = rs.getFloat("product_price");
				Integer cartCount = rs.getInt("cart_count");
				Float cartPrice = rs.getFloat("cart_price");
				Cart cart = new Cart();
				cart.setCartId(cartId);
				User user = new User();
				user.setUserId(userId);
				cart.setUser(user);
				Product product = new Product();
				product.setProductId(productId);
				product.setProductName(productName);
				product.setProductPicture(productPicture);
				product.setProductIntroduce(productIntroduce);
				product.setProductPrice(productPrice);
				cart.setProduct(product);				
				cart.setCartCount(cartCount);
				cart.setCartPrice(cartPrice);
				list.add(cart);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public int deleteCartByCartId(Integer cartId) {
		String sql = "delete from cart where cart_id = ?";
		return DBUtil.doUpdate(sql, cartId);
	}

	@Override
	public int updateCart(Cart cart) {
		String sql = "update cart set cart_count = ?, cart_price = ? where cart_id = ?";
		return DBUtil.doUpdate(sql, cart.getCartCount(), cart.getCartPrice(), cart.getCartId());
	}

	@Override
	public int insertCart(Cart cart) {
		String sql = "insert into cart(user_id, product_id, cart_count, cart_price) values(?,?,?,?)";		
		return DBUtil.doUpdate(sql, cart.getUser().getUserId(), cart.getProduct().getProductId(), cart.getCartCount(), cart.getCartPrice());
	}

	@Override
	public Cart findCartAllBycartId(Integer cartId) {
		Cart cart = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select")
		.append(" cart_id,")
		.append(" user_id,")
		.append(" product_id,")
		.append(" cart_count,")
		.append(" cart_price")
		.append(" from cart")
		.append(" where cart_id = ?");
		rs = DBUtil.doQuery(sb.toString(), cartId);
		try {
			while (rs.next()) {
				Integer userId = rs.getInt("user_id");
				Integer productId = rs.getInt("product_id");
				Integer cartCount = rs.getInt("cart_count");
				Float cartPrice = rs.getFloat("cart_price");
				cart = new Cart();				
				User user = new User();
				user.setUserId(userId);
				cart.setUser(user);				
				Product product = new Product();
				product.setProductId(productId);
				cart.setProduct(product);
				cart.setCartCount(cartCount);
				cart.setCartPrice(cartPrice);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cart;
	}

}
