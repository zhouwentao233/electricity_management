package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zretc.dao.ShopsCollectDao;

import com.zretc.entity.Seller;
import com.zretc.entity.ShopsCollect;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;
import com.zretc.util.PageInfo;

public class ShopsCollectDaoImpl implements ShopsCollectDao{

	@Override
	public int insertShopsCollect(ShopsCollect shopsCollect) {
		String sql = "insert into shops_collect (seller_id, user_id) values(?,?)";		
		return DBUtil.doUpdate(sql,shopsCollect.getSeller().getSellerId(), shopsCollect.getUser().getUserId());
	
	}

	@Override
	public int deleteShopsCollectByCollectId(int shopsCollectId) {
		String sql = "delete from shops_collect where shops_collect_id = ?";
		return DBUtil.doUpdate(sql, shopsCollectId);
	}

	@Override
	public PageInfo<ShopsCollect> findShopsCollectByPage(Map<String, String> params) {
		Integer pageNum = Integer.valueOf(params.get("pageNum"));
		// 页面数量
		Integer pageSize = Integer.valueOf(params.get("pageSize"));
		
		StringBuilder sql = new StringBuilder("select ")
				.append(" c.shops_collect_id")
				.append(",c.seller_id")
				.append(",c.user_id")
				.append(",s.seller_shop_name")
				.append(",s.seller_logo")
				.append(" from shops_collect c")
				.append(" inner join seller s")
				.append(" on c.seller_id = s.seller_id")
				.append(" inner join user u")
				.append(" on c.user_id = u.user_id")
				.append(" where c.user_id = ?")
				.append(" limit ?,?");
		List<ShopsCollect> data = new ArrayList<ShopsCollect>();
		ResultSet rs = DBUtil.doQuery(sql.toString(),params.get("userId"),(pageNum-1)*pageSize,pageSize);
		try {
			while(rs.next()) {
				Integer shopsCollectId = rs.getInt("shops_collect_id"); 
				 
				Seller seller = new Seller();
				seller.setSellerId(rs.getInt("seller_id"));
				User user =new User();
				user.setUserId(rs.getInt("user_id"));
				seller.setSellerShopName(rs.getString("seller_shop_name"));
				seller.setSellerLogo(rs.getString("seller_logo"));
				ShopsCollect shopsCollect=new ShopsCollect(shopsCollectId, seller, user);
				
				data.add(shopsCollect);
			}
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		// 查询总数量
		Integer total = 0;
		String s = "select count(*) from shops_collect where shops_collect_id = ? limit ?,?";
		rs = DBUtil.doQuery(s, params.get("shopsCollectId"),pageNum,pageSize);
		PageInfo<ShopsCollect> pageInfo = new PageInfo<>(data, pageNum, pageSize, total);
		return pageInfo;
	}

	@Override
	public ShopsCollect getShopsCollect(Integer userId, Integer sellerId) {
		String sql = "select shops_collect_id,seller_id,user_id from shops_collect where user_id = ? and seller_id = ?";
		ResultSet rs = DBUtil.doQuery(sql, userId,sellerId);
		ShopsCollect shopsCollect = null;
		try {
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("user_id"));
				Seller seller = new Seller();
				seller.setSellerId(rs.getInt("seller_id"));
				Integer shopsCollectId = rs.getInt("shops_collect_id");
				shopsCollect = new ShopsCollect(shopsCollectId, seller, user);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return shopsCollect;
	}

}
