package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zretc.dao.TypeDao;
import com.zretc.entity.Type;
import com.zretc.util.DBUtil;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 分类dao层的实现类
 */
public class TypeDaoImpl implements TypeDao{

	@Override
	public List<Type> typeListByFatherId(Integer typeFatherId) {
		String sql = " select type_id,type_father_id,type_name from type where type_father_id = ?";
		ResultSet rs = DBUtil.doQuery(sql, typeFatherId);
		List<Type> list = new ArrayList<>();
		try {
			while(rs.next()) {
				Integer typeId = rs.getInt("type_id");
				typeFatherId = rs.getInt("type_father_id");
				String typeName = rs.getString("type_name");
				Type type = new Type(typeId, typeFatherId, typeName);
				list.add(type);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public PageInfo<Map<String, Object>> typeListByLikeName(Map<String, String> map) {
		// 获取页数
		Integer pageNum = Integer.valueOf(map.get("pageNum"));
		// 获取单页显示数量
		Integer pageSize = Integer.valueOf(map.get("pageSize"));
		
		StringBuilder sql = new StringBuilder(" select t.type_id ")
							.append(",t.type_name")
							.append(",t.type_father_id")
							.append(",f.type_name as type_father_name")
							.append(" from type t")
							.append(" inner join type f")
							.append(" on t.type_father_id = f.type_id")
							.append(" where t.type_name like ? ")
							.append(" or f.type_name like ? ");
		// 判断排序
		String orderBy = map.get("orderBy");
		String ascDesc = map.get("ascDesc");
		if("typeName".equals(orderBy)) {
			if("asc".equals(ascDesc)) {
				sql.append(" order by type_name asc");
			}else {
				sql.append(" order by type_name desc");
			}
		}else if("typeFatherName".equals(orderBy)) {
			if("asc".equals(ascDesc)) {
				sql.append(" order by type_father_name asc");
			}else {
				sql.append(" order by type_father_name desc");
			}
		}
		sql.append(" limit ?,?");
		String keyword = map.get("keyword");
		ResultSet rs = DBUtil.doQuery(sql.toString(), keyword,keyword,(pageNum-1)*pageSize,pageSize);
		List<Map<String, Object>> data = new ArrayList<>();
		try {
			while(rs.next()) {
				Integer typeId = rs.getInt("type_id");
				Integer typeFatherId = rs.getInt("type_father_id");
				String typeFatherName = rs.getString("type_father_name");
				String typeName = rs.getString("type_name");
				Map<String,Object> type = new HashMap<String, Object>();
				type.put("typeId", typeId);
				type.put("typeFatherId", typeFatherId);
				type.put("typeFatherName", typeFatherName);
				type.put("typeName", typeName);
				data.add(type);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// 获取数量
		sql = new StringBuilder(" select count(*) ")
				.append(" from type t")
				.append(" inner join type f")
				.append(" on t.type_father_id = f.type_id")
				.append(" where t.type_name like ? ")
				.append(" or f.type_name like ? ");
		rs = DBUtil.doQuery(sql.toString(), keyword,keyword);
		Integer total = 0;
		try {
			while (rs.next()) {
				total = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(data, pageNum, pageSize, total);
		return pageInfo;
	}

	@Override
	public int saveType(Type type) {
		String sql = " insert into type(type_father_id,type_name) values(?,?)";
		return DBUtil.doUpdate(sql, type.getTypeFatherId(),type.getTypeName());
	}

	@Override
	public int deleteType(Integer typeId) {
		String sql = " delete from type where type_id = ?";
		return DBUtil.doUpdate(sql, typeId);
	}

	@Override
	public int updateType(Type type) {
		String sql = "update type set type_father_id=?,type_name=? where type_id=?";
		return DBUtil.doUpdate(sql, type.getTypeFatherId(),type.getTypeName(),type.getTypeId());
	}

	@Override
	public Type getTypeDetail(Integer typeId) {
		String sql = "select type_id,type_father_id,type_name from type where type_id = ?";
		ResultSet rs = DBUtil.doQuery(sql, typeId);
		Type type = null;
		try {
			while(rs.next()) {
				typeId = rs.getInt("type_id");
				Integer typeFatherId = rs.getInt("type_father_id");
				String typeName = rs.getString("type_name");
				type = new Type(typeId, typeFatherId, typeName);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return type;
	}


}
