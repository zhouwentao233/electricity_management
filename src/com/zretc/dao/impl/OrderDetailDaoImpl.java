package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zretc.dao.OrderDetailDao;
import com.zretc.entity.OrderDetail;
import com.zretc.entity.Orders;
import com.zretc.entity.Product;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;
import com.zretc.util.PageInfo;

/**
 * @author chenbo
 * 订单详情dao层的实现类
 */
public class OrderDetailDaoImpl implements OrderDetailDao{

	@Override
	public Orders findOrdersByOrderId(String orderId) {
		Orders orders = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append(" select")
		.append(" order_id,")
		.append(" order_time,")
		.append(" order_total_price,")
		.append(" order_status,")
		.append(" order_pay_method,")
		.append(" order_receive_address,")
		.append(" order_receive_name,")
		.append(" order_receive_phone")
		.append(" from orders")
		.append(" where order_id = ?");
		rs = DBUtil.doQuery(sb.toString(), orderId);
		try {
			while (rs.next()) {
				String orderTime = rs.getString("order_time");
				Float orderTotalPrice = rs.getFloat("order_total_price");
				Integer orderStatus = rs.getInt("order_status");
				String orderPayMethod = rs.getString("order_pay_method");
				String orderReceiveAddress = rs.getString("order_receive_address");
				String orderReceiveName = rs.getString("order_receive_name");
				String orderReceivePhone = rs.getString("order_receive_phone");
				orders = new Orders();
				orders.setOrderTime(orderTime);
				orders.setOrderTotalPrice(orderTotalPrice);
				orders.setOrderStatus(orderStatus);
				orders.setOrderPayMethod(orderPayMethod);
				orders.setOrderReceiveAddress(orderReceiveAddress);
				orders.setOrderReceiveName(orderReceiveName);
				orders.setOrderReceivePhone(orderReceivePhone);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;
	}
	
	@Override
	public List<Map<String, Object>> findOrdersProductByOrderId(String orderId) {
		List<Map<String, Object>> list = new ArrayList<>();
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append(" select")
		.append(" A.order_id,")
		.append(" B.product_id,")
		.append(" B.product_name,")
		.append(" B.product_picture,")
		.append(" A.order_detail_count,")
		.append(" A.order_detail_price,")
		.append(" A.order_detail_status")
		.append(" from order_detail A")
		.append(" inner join product B")
		.append(" on A.product_id = B.product_id")
		.append(" where A.order_id = ?");
		rs = DBUtil.doQuery(sb.toString(), orderId);
		try {
			while (rs.next()) {
				Integer productId = rs.getInt("product_id");
				String productName = rs.getString("product_name");
				String productPicture = rs.getString("product_picture");
				Integer orderDetailCount = rs.getInt("order_detail_count");
				Float orderDetailPrice = rs.getFloat("order_detail_price");
				Integer orderDetailStatus = rs.getInt("order_detail_status");
				Map<String, Object> map = new HashMap<>();
				map.put("productId", productId);
				map.put("productName", productName);
				map.put("productPicture", productPicture);
				map.put("orderDetailCount", orderDetailCount);
				map.put("orderDetailPrice", orderDetailPrice);
				map.put("orderDetailStatus", orderDetailStatus);
				list.add(map);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public int insertOrdersDetail(OrderDetail orderDetail) {
		String sql = "insert into order_detail(order_id, product_id, order_detail_count, order_detail_price, order_detail_status) values(?,?,?,?,?)";
		return DBUtil.doUpdate(sql, orderDetail.getOrders().getOrderId(), orderDetail.getProduct().getProductId(), orderDetail.getOrderDetailCount(), orderDetail.getOrderDetailPrice(), orderDetail.getOrderDetailStatus());
	}

	/**
	 * denglingqi
	 * 后端 商城管理员查看订单详情信息
	 * 根据订单ID，查询订单详情信息
	 * @return List<OrderDetail>
	 */
	@Override
	public List<OrderDetail> findOrderDetailByOrderId(String orderId) {
		List<OrderDetail> list = new ArrayList<>();
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append(" select")
		.append(" A.product_picture,")
		.append(" A.product_name,")
		.append(" A.product_introduce,")
		.append(" B.order_detail_count,")
		.append(" B.order_detail_price")
		.append(" from product A")
		.append(" inner join order_detail B")
		.append(" on A.product_id=B.product_id")
		.append(" inner join orders C")
		.append(" on B.order_id=C.order_id")
		.append(" where B.order_id = ?");
		rs = DBUtil.doQuery(sb.toString(), orderId);
		try {
			while (rs.next()) {				
				String productPicture = rs.getString("product_picture");
				String productName = rs.getString("product_name");
				String productIntroduce = rs.getString("product_introduce");			
				Integer orderDetailCount = rs.getInt("order_detail_count");
				Float orderDetailPrice = rs.getFloat("order_detail_price");
				OrderDetail orderDetail = new OrderDetail();
				Product product = new Product();
				product.setProductPicture(productPicture);
				product.setProductName(productName);
				product.setProductIntroduce(productIntroduce);
				orderDetail.setProduct(product);
				orderDetail.setOrderDetailCount(orderDetailCount);
				orderDetail.setOrderDetailPrice(orderDetailPrice);
				list.add(orderDetail);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
