package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zretc.dao.CommentDao;
import com.zretc.entity.Comment;
import com.zretc.entity.Product;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 *	评论dao的实现
 */
public class CommentDaoImpl implements CommentDao{

	@Override
	public List<Comment> commentListByFatherId(Integer commentFatherId) {
		StringBuilder sql = new StringBuilder(" select ")
										.append("c.comment_id")
										.append(",c.user_id")
										.append(",c.product_id")
										.append(",u.user_name")
										.append(",u.user_picture")
										.append(",c.comment_time")
										.append(",c.comment_content")
										.append(",c.comment_father_id")
										.append(",c.comment_score")
										.append(" from comment c")
										.append(" inner join user u")
										.append(" on u.user_id = c.user_id")
										.append(" where comment_father_id = ?");
		List<Comment> list = new ArrayList<Comment>();
		ResultSet rs = DBUtil.doQuery(sql.toString(), commentFatherId);
		try {
			while(rs.next()) {
				Integer commentId = rs.getInt("comment_id");
				User user = new User();
				user.setUserId(rs.getInt("user_id"));
				user.setUserName(rs.getString("user_name"));
				user.setUserPicture(rs.getString("user_picture"));
				Product product = new Product();
				product.setProductId(rs.getInt("product_id"));
				String commentTime = rs.getString("comment_time");
				String commentContent = rs.getString("comment_content"); 
				commentFatherId = rs.getInt("comment_father_id");
				Integer commentScore = rs.getInt("comment_score"); 
				Comment comment = new Comment(commentId, user, product, commentTime, commentContent, commentFatherId, commentScore);
				list.add(comment);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public List<Comment> commentListByProductId(Integer productId) {
		StringBuilder sql = new StringBuilder(" select ")
				.append("c.comment_id")
				.append(",c.user_id")
				.append(",c.product_id")
				.append(",u.user_name")
				.append(",c.comment_time")
				.append(",c.comment_content")
				.append(",c.comment_father_id")
				.append(",c.comment_score")
				.append(" from comment c")
				.append(" inner join user u")
				.append(" on u.user_id = c.user_id")
				.append(" where product_id = ?")
				.append(" order by c.comment_time desc");
		List<Comment> list = new ArrayList<Comment>();
		ResultSet rs = DBUtil.doQuery(sql.toString(), productId);
		try {
		while(rs.next()) {
			Integer commentId = rs.getInt("comment_id");
			User user = new User();
			user.setUserId(rs.getInt("user_id"));
			user.setUserName(rs.getString("user_name"));
			Product product = new Product();
			product.setProductId(rs.getInt("product_id"));
			String commentTime = rs.getString("comment_time");
			String commentContent = rs.getString("comment_content"); 
			Integer commentFatherId = rs.getInt("comment_father_id");
			Integer commentScore = rs.getInt("comment_score"); 
			Comment comment = new Comment(commentId, user, product, commentTime, commentContent, commentFatherId, commentScore);
			list.add(comment);
		}
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		return list;
	}

	
	
	@Override
	public Comment selectCommentDetails(Integer commentId) {
		StringBuilder sql = new StringBuilder(" select ")
				.append("c.comment_id")
				.append(",c.user_id")
				.append(",c.product_id")
				.append(",u.user_name")
				.append(",c.comment_time")
				.append(",c.comment_content")
				.append(",c.comment_father_id")
				.append(",c.comment_score")
				.append(" from comment c")
				.append(" inner join user u")
				.append(" on u.user_id = c.user_id")
				.append(" where comment_id = ?");
		ResultSet rs = DBUtil.doQuery(sql.toString(), commentId);
		Comment comment = null;
		try {
		while(rs.next()) {
			commentId = rs.getInt("comment_id");
			User user = new User();
			user.setUserId(rs.getInt("user_id"));
			user.setUserName(rs.getString("user_name"));
			Product product = new Product();
			product.setProductId(rs.getInt("product_id"));
			String commentTime = rs.getString("comment_time");
			String commentContent = rs.getString("comment_content"); 
			Integer commentFatherId = rs.getInt("comment_father_id");
			Integer commentScore = rs.getInt("comment_score"); 
			comment = new Comment(commentId, user, product, commentTime, commentContent, commentFatherId, commentScore);
		}
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		return comment;
	}

	@Override
	public int deleteComment(Integer commentId) {
		String sql = " delete from comment where comment_id = ?";
		return DBUtil.doUpdate(sql, commentId);
	}

	@Override
	public int updateComment(Comment comment) {
		return 0;
	}

	@Override
	public int insertComment(Comment comment) {
		String sql;
		if(comment.getCommentScore() == null) {
			sql = " insert into comment(user_id,product_id,comment_time,comment_content,comment_father_id,comment_score) values(?,?,?,?,?,null)";
			return DBUtil.doUpdate(sql, comment.getUser().getUserId(),
					comment.getProduct().getProductId(),
					comment.getCommentTime(),
					comment.getCommentContent(),
					comment.getCommentFatherId());
		}else {
			sql = " insert into comment(user_id,product_id,comment_time,comment_content,comment_father_id,comment_score) values(?,?,?,?,?,?)";
			return DBUtil.doUpdate(sql, comment.getUser().getUserId(),
					comment.getProduct().getProductId(),
					comment.getCommentTime(),
					comment.getCommentContent(),
					comment.getCommentFatherId(),
					comment.getCommentScore());
		}
		
	}

	@Override
	public List<Comment> commentListBySeller(Map<String, String> map) {
		// 获取商家编号
		String sellerIdTemp = map.get("sellerId");
		// 获取商品编号
		String productIdTemp = map.get("productId");
		// 获取显示数量
		Integer pageSize = Integer.valueOf(map.get("pageSize"));
		Integer pageNum = Integer.valueOf(map.get("pageNum"));
		
		ResultSet rs = null;
		if(sellerIdTemp != null) {
			// 根据商家编号查
			String sqlBySellerId = "select u.user_name,c.comment_time,c.comment_score,c.comment_content,c.comment_id,p.product_id,p.product_name from comment c inner join user u on u.user_id = c.user_id  inner join product p on p.product_id = c.product_id where p.seller_id = ? and c.comment_father_id = 0 order by c.comment_time desc limit ?,?";
			Integer sellerId = Integer.valueOf(sellerIdTemp);
			rs = DBUtil.doQuery(sqlBySellerId, sellerId,(pageNum-1)*pageSize,pageSize);
		}else if(productIdTemp != null) {
			// 根据商品编号查
			String sqlByProductId = "select u.user_name,c.comment_time,c.comment_score,c.comment_content,c.comment_id,p.product_id,p.product_name from comment c inner join user u on u.user_id = c.user_id  inner join product p on p.product_id = c.product_id  where c.product_id = ? and c.comment_father_id = 0 order by c.comment_time desc limit ?,?";
			Integer productId = Integer.valueOf(productIdTemp);
			rs = DBUtil.doQuery(sqlByProductId, productId,(pageNum-1)*pageSize,pageSize);
		}
		ArrayList<Comment> list = new ArrayList<Comment>();
		try {
			while(rs.next()) {
				Integer commentId = rs.getInt("comment_id");
				Integer commentScore = rs.getInt("comment_score");
				String commentContent = rs.getString("comment_content");
				String userName = rs.getString("user_name");
				String commentTime = rs.getString("comment_time");
				String productName = rs.getString("product_name");
				Integer productId = rs.getInt("product_id");
				User user = new User();
				user.setUserName(userName);
				Product product = new Product();
				product.setProductId(productId);
				product.setProductName(productName);
				Comment comment = new Comment(commentId, user, product, commentTime, commentContent, null, commentScore);
				list.add(comment);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		

	}

	

}
