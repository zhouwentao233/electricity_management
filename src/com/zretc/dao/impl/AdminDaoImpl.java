package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.zretc.dao.AdminDao;
import com.zretc.entity.Admin;
import com.zretc.util.DBUtil;



public class AdminDaoImpl implements AdminDao{


	@Override
	public Admin findAdminInfoByAdminAccountAndAdminPassword(String adminAccount, String adminPassword) {
		Admin admin = null;
		ResultSet doQuery = DBUtil.doQuery("select admin_id,admin_account,admin_password from admin where admin_account = ? and admin_password = ? ",adminAccount,adminPassword);
		try {
			while (doQuery.next()) {
				Integer adminId = doQuery.getInt("admin_id");
				String adminAct = doQuery.getString("admin_account");
				String admingPwd = doQuery.getString("admin_password");
				admin = new Admin(adminId,adminAct,admingPwd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return admin;
	}
	}

