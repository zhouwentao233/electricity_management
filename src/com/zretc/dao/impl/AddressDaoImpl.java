package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zretc.dao.AddressDao;
import com.zretc.entity.Address;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;
/**
 * 
 * @author sufan
 *
 */
public class AddressDaoImpl implements AddressDao{

	@Override
	public int insertAddress(Address address) {
		String sql = "insert into address(address_id, user_id, address_name, address_status, receive_name, receive_phone) values(?,?,?,?,?,?)";		
		return DBUtil.doUpdate(sql, address.getAddressId(), address.getUser().getUserId(), address.getAddressName(), address.getAddressStatus(),address.getReceiveName(), address.getReceivePhone());
	}

	@Override
	public List<Address> findAllAddressByUserId(User user) {
		List<Address> list = new ArrayList<>();
		Address address = null;
		ResultSet doQuery = DBUtil.doQuery("select address_id, address_name, address_status, receive_name, receive_phone from address where user_id = ?",user.getUserId());
		try {
			while (doQuery.next()) {
				Integer addressId = doQuery.getInt("address_id");
				String addressName = doQuery.getString("address_name");
				Integer addressStatus = doQuery.getInt("address_status");
				String receiveName = doQuery.getString("receive_name");
				String receivePhone = doQuery.getString("receive_phone");
				address = new Address(addressId, user, addressName, addressStatus, receiveName, receivePhone);
				list.add(address);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int updateAddressByAddressId(Address address) {
		String sql = "update address set  address_name = ?, address_status = ?, receive_name = ?, receive_phone = ? where address_id = ?";
		return DBUtil.doUpdate(sql,address.getAddressName(), address.getAddressStatus(), address.getReceiveName(), address.getReceivePhone(), address.getAddressId());
	}

	@Override
	public int deleteAddressByAddressId(int addressId) {
		String sql = "delete from address where address_id = ?";
		return DBUtil.doUpdate(sql, addressId);
	}

	@Override
	public Address fingAddressByUserId(Integer userId) {
		Address address = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select")
		.append(" address_name,")
		.append(" receive_name,")
		.append(" receive_phone,")
		.append(" address_status,")
		.append(" address_id")
		.append(" from address")
		.append(" where address_status = 1")
		.append(" and user_id = ?");
		rs = DBUtil.doQuery(sb.toString(), userId);
		try {
			while (rs.next()) {
				String addressName = rs.getString("address_name");
				String receiveName = rs.getString("receive_name");
				String receivePhone = rs.getString("receive_phone");
				Integer addressStatus = rs.getInt("address_status");
				
				address = new Address();
				address.setAddressId(rs.getInt("address_id"));
				address.setAddressName(addressName);
				address.setReceiveName(receiveName);
				address.setReceivePhone(receivePhone);
				address.setAddressStatus(addressStatus);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return address;
	}

	@Override
	public Address findAddressByAddressId(int addressId) {
		Address address = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select")
		.append(" address_name,")
		.append(" user_id,")
		.append(" receive_name,")
		.append(" receive_phone,")
		.append(" address_status")
		.append(" from address")
		.append(" where  address_id = ?");
		rs = DBUtil.doQuery(sb.toString(), addressId);
		try {
			while (rs.next()) {
				String addressName = rs.getString("address_name");
				String receiveName = rs.getString("receive_name");
				String receivePhone = rs.getString("receive_phone");
				Integer addressStatus = rs.getInt("address_status");
				Integer userId =rs.getInt("user_id");
				User user = new User();
				user.setUserId(userId);
				address = new Address();
				address.setAddressId(addressId);
				address.setUser(user);
				address.setAddressName(addressName);
				address.setReceiveName(receiveName);
				address.setReceivePhone(receivePhone);
				address.setAddressStatus(addressStatus);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return address;
	}

}
