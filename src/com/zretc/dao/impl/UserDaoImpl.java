package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zretc.dao.UserDao;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;

public class UserDaoImpl implements UserDao {

	@Override
	public int insertUser(User user) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate(
				"insert into user(user_account,user_name,user_password,user_picture,user_phone) values(?,?,?,?,?)",
				user.getUserAccount(), user.getUserName(), user.getUserPassword(),
				user.getUserPicture(), user.getUserPhone());
	}

	@Override
	public User findByUserAccountAndPassword(String userAccount, String userPassword) {
		User user = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select user_id,user_account,user_name,user_password,user_picture,user_phone from user where user_account = ? and user_password = ?",
				userAccount, userPassword);
		try {
			while (doQuery.next()) {
				Integer userId = doQuery.getInt("user_id");
				String userName = doQuery.getString("user_name");
				String userPicture = doQuery.getString("user_picture");
				String userPhone = doQuery.getString("user_phone");
				user = new User(userId, userName, userAccount, userPassword, userPicture, userPhone);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}

	@Override
	public List<User> findByUserCondition(Map<String, String> params) {
		List<User> list = new ArrayList<>();
		ResultSet doQuery = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select user_id").append(",user_account").append(",user_name").append(",user_password")
				.append(",user_picture").append(",user_phone").append(" from user")
				.append(" where user_phone  like ? or user_account like ?");
		String nameKey = params.get("nameKey");
		doQuery = DBUtil.doQuery(sb.toString(), nameKey, nameKey);
		try {
			while (doQuery.next()) {
				Integer userId = doQuery.getInt("user_id");
				String userAccount = doQuery.getString("user_account");
				String userName = doQuery.getString("user_name");
				String userPassword = doQuery.getString("user_password");
				String userPicture = doQuery.getString("user_picture");
				String userPhone = doQuery.getString("user_phone");

				User user = new User();
				user.setUserId(userId);
				user.setUserAccount(userAccount);
				user.setUserName(userName);
				user.setUserPassword(userPassword);
				user.setUserPicture(userPicture);
				user.setUserPhone(userPhone);
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int deleteByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate("delete from user where user_id = ?", userId);
	}

	@Override
	public int updateByUserId(User user) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate(
				"update user set user_name = ?,user_password = ?, user_picture = ?, user_phone = ? where user_id = ?",
				user.getUserName(), user.getUserPassword(), user.getUserPicture(), user.getUserPhone(),
				user.getUserId());
	}

	@Override
	public User findByUserAccount(String userAccount) {
		User user = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select user_id,user_name,user_password,user_picture,user_phone from user where user_account = ?",
				userAccount);
		try {
			while (doQuery.next()) {
				Integer userId = doQuery.getInt("user_id");
				String userName = doQuery.getString("user_name");
				String userPassword = doQuery.getString("user_password");
				String userPicture = doQuery.getString("user_picture");
				String userPhone = doQuery.getString("user_phone");
				user = new User(userId, userAccount, userName, userPassword, userPicture, userPhone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User findByUserPhone(String userPhone) {
		User user = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select user_id,user_account,user_name,user_password,user_picture from user where user_phone = ?",
				userPhone);
		try {
			while (doQuery.next()) {
				Integer userId = doQuery.getInt("user_id");
				String userAccount = doQuery.getString("user_account");
				String userName = doQuery.getString("user_name");
				String userPassword = doQuery.getString("user_password");
				String userPicture = doQuery.getString("user_picture");

				user = new User(userId, userAccount, userName, userPassword, userPicture, userPhone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User findByUserName(String userName) {
		User user = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select user_id,user_account,user_name,user_password,user_picture,user_phone from user where user_name = ?",
				userName);
		try {
			while (doQuery.next()) {
				Integer userId = doQuery.getInt("user_id");
				String userAccount = doQuery.getString("user_account");
				String userPassword = doQuery.getString("user_password");
				String userPicture = doQuery.getString("user_picture");
				String userPhone = doQuery.getString("user_phone");

				user = new User(userId, userAccount, userName, userPassword, userPicture, userPhone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;

	}
}
