package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.regexp.internal.recompile;
import com.zretc.dao.ProductCollectDao;
import com.zretc.entity.Product;
import com.zretc.entity.ProductCollect;
import com.zretc.entity.Seller;
import com.zretc.entity.Type;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;
import com.zretc.util.PageInfo;

import sun.text.resources.cldr.om.FormatData_om;
/**
 * 
 * @author sufan
 *
 */
public class ProductCollectDaoImpl implements ProductCollectDao{

	@Override
	public int insertProductCollect(ProductCollect productCollect) {
		String sql = "insert into product_collect (user_id, product_id) values(?,?)";		
		return DBUtil.doUpdate(sql,productCollect.getUser().getUserId(), productCollect.getProduct().getProductId());
	}


	@Override
	public int deleteProductCollectByCollectId(int productCollectId) {
		String sql = "delete from product_collect where product_collect_id = ?";
		return DBUtil.doUpdate(sql, productCollectId);
	}

	@Override
	public PageInfo<ProductCollect> findProductCollectByPage(Map<String, String> params) {
				// 当前页码
				Integer pageNum = Integer.valueOf(params.get("pageNum"));
				// 页面数量
				Integer pageSize = Integer.valueOf(params.get("pageSize"));
				StringBuilder sql = new StringBuilder("select ")
						.append(" c.product_collect_id")
						.append(",c.user_id")
						.append(",c.product_id")
						.append(",p.product_name")
						.append(",p.product_price")
						.append(",p.product_picture")
						.append(" from product p")
						.append(" inner join product_collect c")
						.append(" on p.product_id = c.product_id")
						.append(" inner join user u")
						.append(" on c.user_id = u.user_id")
						.append(" where c.user_id = ?")
						.append(" limit ?,?");
				List<ProductCollect> data = new ArrayList<ProductCollect>();
				ResultSet rs = DBUtil.doQuery(sql.toString(),params.get("userId"),(pageNum-1)*pageSize,pageSize);
				try {
					while(rs.next()) {
						Integer productCollectId = rs.getInt("product_collect_id"); 
						User user =new User();
						user.setUserId(rs.getInt("user_id")); 
						Product product = new Product();
						product.setProductId(rs.getInt("product_id"));
     					String productName = rs.getString("product_name"); 
						Float productPrice = rs.getFloat("product_price"); 
						String productPicture = rs.getString("product_picture"); 
						product.setProductName(productName);
						product.setProductPrice(productPrice);
						product.setProductPicture(productPicture);
						ProductCollect productCollect=new ProductCollect(productCollectId, user, product);
						
						data.add(productCollect);
					}
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
				
				// 查询总数量
				Integer total = 0;
				String s = "select count(*) from product_collect where product_collect_id = ? limit ?,?";
				rs = DBUtil.doQuery(s, params.get("productCollectId"),pageNum,pageSize);
				PageInfo<ProductCollect> pageInfo = new PageInfo<>(data, pageNum, pageSize, total);
				return pageInfo;
			}


	@Override
	public ProductCollect getProductCollect(Integer productId, Integer userId) {
		String sql = "select product_collect_id,user_id,product_id from product_collect where product_id=? and user_id = ?";
		ResultSet rs = DBUtil.doQuery(sql, productId,userId);
		ProductCollect productCollect = null;
		if(rs != null) {
			try {
				while(rs.next()) {
					User user = new User();
					user.setUserId(rs.getInt("user_id"));
					Product product = new Product();
					product.setProductId(rs.getInt("product_id"));
					Integer productCollectId = rs.getInt("product_collect_id");
					productCollect = new ProductCollect(productCollectId, user, product);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return productCollect;
	}
}
