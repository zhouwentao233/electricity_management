package com.zretc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zretc.dao.SellerDao;
import com.zretc.entity.Seller;
import com.zretc.entity.User;
import com.zretc.util.DBUtil;

public class SellerDaoImpl implements SellerDao{

	@Override
	public int insertShop(Seller seller) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate("insert into seller(seller_account,seller_password,seller_address,seller_phone,seller_shop_name,seller_logo,seller_status) values(?,?,?,?,?,?,?)",seller.getSellerAccount(),seller.getSellerPassword(),seller.getSellerAddress(),seller.getSellerPhone(),seller.getSellerShopName(),seller.getSellerLogo(),seller.getSellerStatus());
	}

	@Override
	public Seller findBySellerAccountAndSellerPassword(String sellerAccount,String sellerPassword) {
		Seller seller = null;
		ResultSet doQuery = DBUtil.doQuery("select seller_id,seller_account,seller_password,seller_address,seller_phone,seller_shop_name,seller_logo,seller_status from seller where seller_account = ? and seller_password = ?", sellerAccount,sellerPassword);
		try {
			while (doQuery.next()) {
			Integer sellerId = doQuery.getInt("seller_id");
			String sellerAddress = doQuery.getString("seller_address");
			String sellerPhone = doQuery.getString("seller_phone");
			String sellerShopName  = doQuery.getString("seller_shop_name");
			String sellerLogo  = doQuery.getString("seller_logo");
			Integer sellerStatus  = doQuery.getInt("seller_status");
			seller = new Seller(sellerId,sellerAccount,sellerPassword,sellerAddress,sellerPhone,sellerShopName,sellerLogo,sellerStatus);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return seller;
	}
	
	/**
	 * 根据不同条件查询卖家信息
	 * @param params 条件参数
	 * @return 卖家信息表
	 */
	@Override
	public List<Seller> findBySellerCondition(Map<String, String> params) {
		List<Seller> list = new ArrayList<>();
		ResultSet doQuery = null;
		StringBuilder sb = new StringBuilder();
		sb.append("select seller_id")
		  .append(",seller_account")
		  .append(",seller_password")
		  .append(",seller_address")
		  .append(",seller_phone")
		  .append(",seller_shop_name")
		  .append(",seller_logo")
		  .append(",seller_status")
		  .append(" from seller")
		  .append(" where seller_phone like ? or seller_account like ?");
		String selectKey = params.get("selectKey");
		doQuery = DBUtil.doQuery(sb.toString(), selectKey,selectKey);
		try {
			while (doQuery.next()) {
				Integer sellerId = doQuery.getInt("seller_id");
				String sellerAccount = doQuery.getString("seller_account");
				String sellerPassword = doQuery.getString("seller_password");
				String sellerAddress = doQuery.getString("seller_address");
				String sellerPhone  = doQuery.getString("seller_phone");
				String sellerShopName = doQuery.getString("seller_shop_name");
				String sellerLogo = doQuery.getString("seller_logo");
				Integer sellerStatus = doQuery.getInt("seller_status");
				
				Seller seller = new Seller();
				seller.setSellerId(sellerId);
				seller.setSellerAccount(sellerAccount);
				seller.setSellerPassword(sellerPassword);
				seller.setSellerAddress(sellerAddress);
				seller.setSellerPhone(sellerPhone);
				seller.setSellerShopName(sellerShopName);
				seller.setSellerLogo(sellerLogo);
				seller.setSellerStatus(sellerStatus);
				list.add(seller);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 根据卖家编号删除卖家信息
	 * @param sellerId
	 * @return 影响行数
	 */
	@Override
	public int deleteBySellerId(Integer sellerId) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate("delete from seller where seller_id = ?", sellerId);
	}
	
	/**
	 * 根据卖家编号修改卖家信息
	 * @return 影响行数
	 */
	@Override
	public int updateBySellerId(Seller seller) {
		// TODO Auto-generated method stub
		return DBUtil.doUpdate("update seller set seller_address = ?, seller_phone = ?, seller_shop_name = ?,seller_status = ? where seller_id = ?",
				seller.getSellerAddress(),seller.getSellerPhone(),seller.getSellerShopName(),seller.getSellerStatus(),seller.getSellerId());
	}
	
	/**
	 * 根据卖家账号查询卖家信息
	 * @return 影响行数
	 */

	@Override
	public Seller findBySellerAccount(String sellerAccount) {
		Seller seller = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select seller_id,seller_account,seller_password,seller_address,seller_phone,seller_shop_name,seller_logo,seller_status from seller where seller_account = ?",
				sellerAccount);
		try {
			while (doQuery.next()) {
				Integer sellerId = doQuery.getInt("seller_id");
				String sellerPassword = doQuery.getString("seller_password");
				String sellerAddress = doQuery.getString("seller_address");
				String sellerPhone = doQuery.getString("seller_phone");
				String sellerShopName = doQuery.getString("seller_shop_name");
				String sellerLogo = doQuery.getString("seller_logo");
				Integer sellerStatus = doQuery.getInt("seller_status");
				
				seller = new Seller(sellerId,sellerAccount,sellerPassword, sellerAddress, sellerPhone, sellerShopName, sellerLogo,sellerStatus);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seller;
	}
	
	/**
	 * 根据卖家电话查询卖家信息
	 * @return 影响行数
	 */

	@Override
	public Seller findBySellerPhone(String sellerPhone) {
		Seller seller = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select seller_id,seller_account,seller_password,seller_address,seller_shop_name,seller_logo,seller_status from seller where seller_phone = ?",
				sellerPhone);
		try {
			while (doQuery.next()) {
				Integer sellerId = doQuery.getInt("seller_id");
				String sellerAccount = doQuery.getString("seller_account");
				String sellerPassword = doQuery.getString("seller_password");
				String sellerAddress = doQuery.getString("seller_address");
				String sellerShopName = doQuery.getString("seller_shop_name");
				String sellerLogo = doQuery.getString("seller_logo");
				Integer sellerStatus = doQuery.getInt("seller_status");
				
				seller = new Seller(sellerId,sellerAccount,sellerPassword,sellerAddress,sellerPhone,sellerShopName,sellerLogo,sellerStatus);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seller;
	}

	@Override
	public int updatePassword(Integer sellerId, String password) {
		String sql = "update seller set seller_password = ? where seller_id = ?";
		return DBUtil.doUpdate(sql, password,sellerId);
	}

	@Override
	public Seller getSellerDetail(Integer sellerId) {
		Seller seller = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select seller_id,seller_account,seller_password,seller_address,seller_phone,seller_shop_name,seller_logo,seller_status from seller where seller_id = ?",
				sellerId);
		try {
			while (doQuery.next()) {
				sellerId = doQuery.getInt("seller_id");
				String sellerAccount = doQuery.getString("seller_account");
				String sellerPassword = doQuery.getString("seller_password");
				String sellerAddress = doQuery.getString("seller_address");
				String sellerPhone = doQuery.getString("seller_phone");
				String sellerShopName = doQuery.getString("seller_shop_name");
				String sellerLogo = doQuery.getString("seller_logo");
				Integer sellerStatus = doQuery.getInt("seller_status");
				
				seller = new Seller(sellerId,sellerAccount,sellerPassword, sellerAddress, sellerPhone, sellerShopName, sellerLogo,sellerStatus);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seller;
	}

	/**
	 * 根据店铺名查询卖家信息
	 * @return 影响行数
	 */

	@Override
	public Seller findBySellerShopName(String sellerShopName) {
		Seller seller = null;
		ResultSet doQuery = DBUtil.doQuery(
				"select seller_id,seller_account,seller_password,seller_address,seller_phone,seller_logo,seller_status from seller where seller_shop_name = ?",
				sellerShopName);
		try {
			while (doQuery.next()) {
				Integer sellerId = doQuery.getInt("seller_id");
				String sellerAccount = doQuery.getString("seller_account");
				String sellerPassword = doQuery.getString("seller_password");
				String sellerAddress = doQuery.getString("seller_address");
				String sellerPhone = doQuery.getString("seller_phone");
				String sellerLogo = doQuery.getString("seller_logo");
				Integer sellerStatus = doQuery.getInt("seller_status");
				
				seller = new Seller(sellerId,sellerAccount,sellerPassword,sellerAddress,sellerPhone,sellerShopName,sellerLogo,sellerStatus);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seller;
	}
	/**
	 * @author 
	 * 根据编号修改状态
	 * @param sellerId 编号
	 * @param password 状态
	 * @return
	 */

	@Override
	public int updateStatus(Seller seller) {
		return DBUtil.doUpdate("update seller set seller_status = ? where seller_id = ?",seller.getSellerStatus(),seller.getSellerId());
	}
	}

	

