package com.zretc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zretc.entity.Sales;
import com.zretc.util.DBUtil;

/**
 * @author Administrator
 *销售表
 */
public interface SalesDao {
	
	
	
	/**
	 * 查询销售表
	 * @return
	 */
	
	List<Sales> find();
	

	
	
	/**
	 * 添加销售记录
	 * @return
	 */
	int insertSales(Sales sales);
	
	
	/**
	 * @author wentao
	 * // 销售额排行前十商品（饼状图）
	 * @param sellerId
	 * @return
	 */
	List<Map<String, Object>> salesTopTenPie(Integer sellerId);
	

	/**
	 * // 销售额排行前十商品（条形图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesNumberTopTenBar(Integer sellerId);
	
	

	/**
	 * // 销售量排行前十商品（饼状图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String, Object> salesTopTenBar(Integer sellerId);
	

	/**
	 * // 销售量排行前十商品（条形图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	List<Map<String,Object>> salesNumberTopTenPie(Integer sellerId);
	

	/**
	 * // 根据时间查询销量
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesListByDate(Integer sellerId,String beginTime ,String lastTime);
	

	/**
	 * // 根据时间查询销售额
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesNumberListByDate(Integer sellerId,String beginTime ,String lastTime);
}
