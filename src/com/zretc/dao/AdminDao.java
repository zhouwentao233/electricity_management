package com.zretc.dao;


import com.zretc.entity.Admin;

/**
 * @author 赵坤
 * @return
 */

public interface AdminDao {
	

	/**
	 * 根据管理员账号查询管理员信息
	 * @param adminAccount 管理员账号
	 * @param adminPassword 管理员密码
	 * @return 管理员信息表
	 */
	Admin findAdminInfoByAdminAccountAndAdminPassword(String adminAccount,String adminPassword); 
}
