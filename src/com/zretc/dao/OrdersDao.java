package com.zretc.dao;

import java.util.List;
import java.util.Map;

import com.zretc.entity.OrderDetail;
import com.zretc.entity.Orders;
import com.zretc.entity.User;
import com.zretc.util.PageInfo;

/**
 * @author 陈波
 * 订单信息dao层
 */
public interface OrdersDao {
	
	/**
	 * 前端 订单管理 根据用户的编号
	 * 查询购物车的商品信息
	 * @param uesrId 用户编号
	 * @return List<Order>
	 */
	PageInfo<Orders> findOrderAllByUserId(Map<String, String> params);
	
	/**
	 * 前端修改订单详情信息
	 * @param orders 订单信息类
	 * @return 是否有影响行数 0/1
	 */
	int updateOrderByOrderStatus(Orders orders);
	/**
	 * 后端 订单管理 查询订单信息
	 * @param params 条件参数 分页查询
	 * 根据订单号和订单状态查询
	 * 分页当前页码 pageNum
	 * 分页获取条数 pageSize
	 * @return Order
	 */
	PageInfo<Map<String, Object>> findPageOrderAll(Map<String, String> params);	
	
	/**
	 * 后端 订单管理 查询基本订单信息
	 * @param params 
	 * @return Order
	 */
	List<Map<String, Object>> findOrderByorderId(String orderId);
	
	/**
	 * 后端 订单管理 查询订单商品信息
	 * @param params 
	 * @return Order
	 */
	List<OrderDetail> findOrderDetailByorderId(String orderId, Integer sellerId);
	
	/**
	 * 根据订单编号删除订单信息
	 * @param orderId 订单编号
	 * @return 是否有影响行数 0/1
	 */
	int deleteOrder(String orderId);
	
	/**
	 * 添加订单信息
	 * @param orders 订单信息类
	 * @return 是否有影响行数 0/1
	 */
	int insertOrder(Orders orders);
	
	/**
	 * 后端修改订单详情信息
	 * @param orders 订单信息类
	 * @return 是否有影响行数 0/1
	 */
	int updateOrderByOrderStatus(OrderDetail orderDetail);
	
	/**
	 * denglingqi
	 * 商城管理
	 * 后端 订单管理 查询订单信息
	 * @param params 条件参数 分页查询
	 * 根据订单号和店铺名
	 * 订单状态，交易时间
	 * 分页当前页码 pageNum
	 * 分页获取条数 pageSize
	 * @return Order
	 */
	PageInfo<Orders> findPageOrder(Map<String, String> params);
}
