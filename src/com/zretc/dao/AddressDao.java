package com.zretc.dao;

import java.util.List;


import com.zretc.entity.Address;
import com.zretc.entity.User;
/**
 * 
 * @author sufan
 *
 */
public interface AddressDao {
	/**
	 * 增加地址
	 * @param address地址
	 * @return
	 * @author sufan
	 */
public int insertAddress(Address address);
	/**
	 * 通过用户id查询该用户所有地址
	 * @param userId用户id
	 * @return
	 * @author sufan
	 */
public List<Address> findAllAddressByUserId(User user);
    /**
     * 根据地址id更新用户地址
     * @return
     * @author sufan
     */
public int updateAddressByAddressId(Address address);
    /**
     * 根据地址id删除地址
     * @param addressId地址id
     * @return
     * @author sufan
     */
public int deleteAddressByAddressId(int addressId);

/**
 * 根据用户Id,查询默认地址
 * @param userId,
 * @param addressStatus
 * @return
 * * @author chenbo
 */
Address fingAddressByUserId(Integer userId);
/**
 * 根据地址id查地址
 * @param addressId地址id
 * @return
 * @author sufan
 */
public Address findAddressByAddressId(int addressId);

}
