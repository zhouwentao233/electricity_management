package com.zretc.dao;

import java.util.List;
import java.util.Map;

import com.zretc.entity.User;





/**
 * @author 赵坤
 * @param user 用户信息
 * @param userName 用户名
 * @return
 */


public interface UserDao {
	
	/**
	 * 新增用户信息，用于注册新用户
	 * @return
	 */
	
	int insertUser(User user); 
	
	/**
	 * 根据买家账号密码查询买家信息
	 * @param userAccount 买家账号
	 * @param userPassword 买家密码
	 * @return 用户信息表
	 */
	
	User findByUserAccountAndPassword(String userAccount,String userPassword); 
	
	/**
	 * 根据不同条件查询买家信息
	 * @param params 条件参数
	 * @return 买家信息表
	 */
	List<User> findByUserCondition(Map<String,String> params);
	
	
	/**
	 * 根据买家编号删除买家信息
	 * @param userId
	 * @return 影响行数
	 */
	int deleteByUserId(Integer userId);
	
	/**
	 * 根据买家编号修改买家信息
	 * @return 影响行数
	 */
	int updateByUserId(User user);
	
	/**
	 * 根据买家账号查询买家信息
	 * @return 买家信息
	 */
	User findByUserAccount(String userAccount);
	
	/**
	 * 根据买家手机号查询买家信息
	 * @return 买家信息
	 */
	User findByUserPhone(String userAccount);
	
	/**
	 * 根据买家用户名查询买家信息
	 * @return 买家信息
	 */

	User findByUserName(String userName);
	
}
