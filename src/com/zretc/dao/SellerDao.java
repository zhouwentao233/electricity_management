package com.zretc.dao;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Seller;
import com.zretc.entity.User;

/**
 * @author赵坤
 * 注册店铺及卖家用户信息
 * @param seller卖家信息
 * @param sellerName 卖家用户名
 * @return
 */

public interface SellerDao {
	
      /**
      * 添加店铺及卖家账户
      * @return
      */
	  int insertShop(Seller seller);
	
	  /**
	  * 通过卖家账号名查询卖家信息
	  * @param sellerAccount 卖家账号
	  * @param sellerPassword 卖家密码
	  * @return
	  */
	  Seller findBySellerAccountAndSellerPassword(String sellerAccount,String sellerPassword);
	  
	  /**
		 * 根据不同条件查询卖家信息
		 * @param params 条件参数
		 * @return 卖家信息表
		 */
		List<Seller> findBySellerCondition(Map<String,String> params);
		
		
		/**
		 * 根据卖家编号删除卖家信息
		 * @param sellerId
		 * @return 影响行数
		 */
		int deleteBySellerId(Integer sellerId);
		
		/**
		 * 根据卖家编号修改卖家信息
		 * @return 影响行数
		 * 
		 * @author wentao 注：我调用了这个类，改的时候提醒下
		 */
		int updateBySellerId(Seller seller);
		
		/**
		 * 根据卖家账号查询卖家信息
		 * @return 卖家对象
		 */
		Seller findBySellerAccount(String sellerAccount);
		
		/**
		 * 根据卖家手机号查询卖家信息
		 * @return 卖家对象
		 */

		Seller findBySellerPhone(String sellerPhone);
		
		/**
		 * @author wentao
		 * 根据编号修改密码
		 * @param sellerId 编号
		 * @param password 密码
		 * @return
		 */
		int updatePassword(Integer sellerId,String password);
		
		/**
		 * @author wentao
		 * 根据编号获取信息
		 * @param sellerId 编号
		 * @return
		 */
		Seller getSellerDetail(Integer sellerId);
		
		/**
		 * 根据店铺名查询卖家信息
		 * @return 卖家对象
		 */

		Seller findBySellerShopName(String sellerShopName);
		
		/**
		 * @author 
		 * 根据编号修改状态
		 * @param sellerId 编号
		 * @param password 状态
		 * @return
		 */
		int updateStatus(Seller seller);


}
