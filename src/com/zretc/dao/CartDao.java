package com.zretc.dao;

import java.util.List;

import com.zretc.entity.Cart;
import com.zretc.entity.User;

/**
 * @author 陈波
 * 购物车信息dao层
 */
public interface CartDao {
		
	/**
	 * 前端 用户购物车
	 * 根据用户的ID，查询购物车的商品信息
	 * @param user 用户信息类
	 * @return List
	 */
	List<Cart> findCartAll(Integer userId);
	
	/**
	 * 前端 用户购物车
	 * 根据用户的ID，查询购物车的商品信息
	 * @param cartId 购物车Id
	 * @return List
	 */
	Cart findCartAllBycartId(Integer cartId);
	
	/**
	 * 根据购物车编号删除商品信息
	 * @param cartId 购物车编号
	 * @return 是否有影响行数 0/1
	 */
	int deleteCartByCartId(Integer cartId);
	
	/**
	 * 修改购物车商品信息
	 * @param cart 购物车信息类
	 * @return 是否有影响行数 0/1
	 */
	int updateCart(Cart cart);
	
	/**
	 * 添加购物车商品信息
	 * @param cart 购物车信息类
	 * @return 是否有影响行数 0/1
	 */
	int insertCart(Cart cart);
}
