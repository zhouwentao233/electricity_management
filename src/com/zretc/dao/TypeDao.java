package com.zretc.dao;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Type;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 分类表dao
 */
public interface TypeDao {
	
	/**
	 * 根据父类id查询类别
	 * @param typeFatherId 父类编号
	 * @return 分类集合
	 */
	List<Type> typeListByFatherId(Integer typeFatherId);
	
	/**
	 * 名称模糊查询(父类名称或者分类名称)
	 * @param map 查询条件
	 * @return 分类集合
	 */
	PageInfo<Map<String, Object>> typeListByLikeName(Map<String,String> map);
	
	/**
	 * 添加
	 * @param type 分类对象
	 * @return 影响行数
	 */
	int saveType(Type type);
	
	/**
	 * 删除
	 * @param typeId 分类id
	 * @return 影响行数
	 */
	int deleteType(Integer typeId);

	/**
	 * 修改
	 * @param type 分类对象
	 * @return 影响行数
	 */
	int updateType(Type type);
	
	/**
	 * @author wentao
	 * 添加于2020年6月19日14:45:42
	 * @param typeId 分类编号
	 * @return 分类对象
	 */
	Type getTypeDetail(Integer typeId);

}
