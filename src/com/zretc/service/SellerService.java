package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Seller;
import com.zretc.entity.User;



/**
 * @author wentao
 *	卖家信息表
 */
public interface SellerService {

	/**
	 * @author 赵坤
	 * 新增店铺信息，用于申请新店铺
	 * @param seller 用户对象
	 * @return 影响行数
	 */
	int insertShop(Seller seller);

	/**
	 * @author 赵坤
	 * 根据卖家用户名查询店铺信息，用于卖家用户成功登录后的卖家用户信息反馈
	 * @param sellerName 用户名称
	 * @return 商家用户对象
	 */
	Seller sellerLogin(String sellerAccount,String sellerPassword);
	
	/**
	 * @author 赵坤
	 *根据不同情况获得卖家信息
	 *@param 查询条件
	 *@return卖家信息
	*/
	List<Seller> getSellerListCondition(Map<String, String> params);
	
	/**
	 * @author 赵坤
	 * 根据卖家编号删除卖家信息
	 * @param sellerId 卖家编号
	 * @return 影响行数
	 */
	int deleteById(Integer sellerId);
	
	/**
	 * @author 赵坤
	 * 根据卖家编号修改卖家信息
	 * @return 影响行数
	 */
	int modifyBySellerId(Seller seller);
	
	/**
	 * @author 赵坤
	 * 判断卖家账号是否存在
	 * @param userAccount 卖家账号
	 * @return 账号是否存在（true/false）
	 */
	boolean existsOfSellerName(String sellerAccount); 
	
	/**
	 * @author 赵坤
	 * 判断卖家手机号是否存在
	 * @param sellerPhone 卖家手机号
	 * @return 手机号是否存在（true/false）
	 */
	boolean existsOfSellerPhone(String sellerPhone); 
	
	/**
	 * @author wentao
	 * 根据账号修改密码
	 * @param sellerId
	 * @param password
	 * @return
	 */
	int updatePassword(Integer sellerId,String password);
	
	/**
	 * @author wentao
	 * 根据编号获取信息
	 * @param sellerId 编号
	 * @return
	 */
	Seller getSellerDetail(Integer sellerId);
	
	/**
	 * @author 赵坤
	 * 判断店铺名是否存在
	 * @param sellerShopName 店铺名
	 * @return 手机号是否存在（true/false）
	 */

	boolean existsOfSellerShopName(String sellerShopName);
	
	/**
	 * @author 
	 * 根据编号修改状态
	 * @param sellerId 编号
	 * @param password 状态
	 * @return
	 */
	int updateStatus(Seller seller);
}
