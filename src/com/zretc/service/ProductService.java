package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Product;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 商品service类
 */
public interface ProductService {
	
	/**
	 * 增加产品
	 * @param product 产品对象
	 * @return 影响行数
	 */
	int insertProduct(Product product);

	
	/**
	 * 根据不同条件分页查询商品
	 * @param params 条件参数 根据商品名称ProductName 分页当前页码 pageNum 分页获取条数 pageSize
	 * @return 商品分页对象
	 */
	PageInfo<Product> findByPage(Map<String, String> params);

	/**
	 * 修改产品
	 * @param product 产品对象
	 * @return 影响行数
	 */
	int updateProduct(Product product);

	/**
	 * 删除产品
	 * @param productId 产品id
	 * @return 影响行数
	 */
	int deleteProduct(int productId);

	/**
	 * 根据分类查询全部商品（分页）
	 * @param map typeId分类编号  pageNum 页码 pageSize 显示数量
	 * @return 商品分页对象
	 */
	PageInfo<Product> productListByType(Map<String, String> map);
	
	/**
	 * 根据店铺查询全部商品（分页）
	 * @param map sellerId卖家编号 pageNum 页码 pageSize 显示数量
	 * @return
	 */
	PageInfo<Product> productListBySeller(Map<String, String> map);
	
	/**
	 * 商品详情
	 * @param productId 商品id
	 * @return 商品对象
	 */
	Product getProductByProductId(Integer productId);

	/**
	 * 查询全部上架商品
	 * @param map 分页条件
	 * @return 全部上架商品分页
	 */
	PageInfo<Product> productListByStatus(Map<String, String> map);

	/**
	 * 根据父分类查询全部商品（分页）
	 * @param map typeFatherId分类编号  pageNum 页码 pageSize 显示数量
	 * @return 商品分页对象
	 */
	PageInfo<Product> productListByFatherType(Map<String, String> map);

	/**
	 * @author wentao
	 * @param map 管理员模块 多条件查询并排序（商品名称、一级分类、二级分类）商品名称排序、价格排序、分类排序、上架时间排序
	 * @return
	 */
	PageInfo<Map<String, Object>> productListByMoreCondition(Map<String, String> map);
	/**
	 * @author 苏帆
	 * @param map 卖家商品管理模块 多条件查询
	 * @return
	 */
	PageInfo<Product> productListBySellerIdandKeyword(Map<String, String> map);
	/**
	 * @author 苏帆
	 * @param map 卖家商品管理模块 多条件查询
	 * @return
	 */
	PageInfo<Product> productListBySellerIdandKeywordandStatus(Map<String, String> map);
	

	/**
	 * @author wentao
	 * 商品top5 排行榜
	 * @return 列表
	 */
	List<Map<String, Object>> productListSalesTop();
	
	/**
	 * @author wentao
	 * 商品新品排行榜top5
	 * @return 列表
	 */
	List<Product> productListNewTop();
	
	/**
	 * 查询库存
	 * @author chenbo
	 * @param productId
	 * @return
	 */
	Product getProductInventoryByProductId(Integer productId);
	
	/**
	 * 修改库存
	 * @author chenbo
	 * @param product
	 * @return	
	 */
	int updateProductInventoryByProductId(Product product);
}
