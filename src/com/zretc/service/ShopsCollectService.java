package com.zretc.service;

import java.util.Map;

import com.zretc.entity.ProductCollect;
import com.zretc.entity.ShopsCollect;
import com.zretc.util.PageInfo;
/**
 * 
 * @author wentao
 * 店铺收藏service层
 */
public interface ShopsCollectService {
	/**
	 * 添加店铺收藏
	 * @return
	 * @param shopsCollect店铺收藏记录
	 * @author sufan
	 */
public int insertShopsCollect(ShopsCollect shopsCollect);
/**
 * 删除店铺收藏
 * @return
 * @param shopsCollectId 店铺收藏id
 * @author sufan
 */
public int deleteShopsCollectByCollectId(int shopsCollectId);
/**
 * 根据不同条件分页查询收藏店铺
 * @author sufan
 * @param params 条件参数		     
 *        分页当前页码 pageNum
 *        分页获取条数 pageSize
 * @return 店铺列表
 */
PageInfo<ShopsCollect> findShopsCollectByPage(Map<String,String> params);

/**
 * 查询是否收藏店铺
 * @author wentao
 * @param userId
 * @param sellerId
 * @return
 */
ShopsCollect getShopsCollect(Integer userId,Integer sellerId);
}
