package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Orders;
import com.zretc.entity.OrderDetail;
import com.zretc.entity.User;
import com.zretc.util.PageInfo;

/**
 * @author chenbo
 * 订单详情信息Service层
 */
public interface OrderDetailService {
	
	/**
	 * 前端 用户订单详情信息
	 * 根据用户的ID，查询订单详情用户信息
	 * @param user 用户信息类
	 * @return Orders
	 */
	Orders getOrdersByOrderId(String orderId);
		
	/**
	 * 前端 用户订单详情信息
	 * 根据用户的ID，查询订单详情商品信息
	 * @param user 用户信息类
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> getOrdersProductByOrderId(String orderId);

	/**
	 * 添加订单详情信息
	 * @param orderDetail 订单详情信息类
	 * @return 是否有影响行数 0/1
	 */
	int insertOrderDetail(OrderDetail orderDetail);
	

	/**
	 * denglingqi
	 * 后端 商城管理员查看详情信息
	 * 根据订单ID，查询订单详情信息
	 * @return List<OrderDetail>
	 */
	List<OrderDetail> findOrderDetailByOrderId(String orderId);
}
