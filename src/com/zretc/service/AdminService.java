package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Admin;
import com.zretc.entity.Seller;
import com.zretc.entity.User;

/**
 * @author 赵坤
 * @return
 */
public interface AdminService {
	
	/**
	 * 管理员登录
	 * @param adminAccount 管理员账号
	 * @param adminPassword 管理员密码
	 * @return 管理员信息
	 */
	Admin adminLogin (String adminAccount,String adminPassword); 
	
	/**
	 * 删除根据买家Id删除买家信息
	 * @param userId 买家ID
	 * @return 影响行数
	 */
	int deleteByUserId(Integer userId);
	
	/**
	 * 根据卖家编号删除卖家信息
	 * @param sellerId
	 * @return 影响行数
	 */
	int deleteBySellerId(Integer sellerId);
	
	/**
	 * 根据买家编号修改买家信息
	 * @return 影响行数
	 */
	int updateByUserId(User user);
	
	/**
	 * 根据卖家编号修改卖家信息
	 * @return 影响行数
	 */
	int updateBySellerId(Seller seller);
	
	/**
	 * 根据不同条件查询买家信息
	 * @param params 条件参数，根据selectKey进行判断
	 * @return 买家信息表
	 */
	List<User> findByUserCondition(Map<String,String> params);
	
	/**
	 * 根据不同条件查询卖家信息
	 * @param params 条件参数，根据selectKey进行判断
	 * @return 卖家信息表
	 */
	List<Seller> findBySellerCondition(Map<String,String> params);

}
