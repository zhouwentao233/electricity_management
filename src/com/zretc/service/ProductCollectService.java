package com.zretc.service;

import java.util.Map;

import com.zretc.entity.ProductCollect;
import com.zretc.util.PageInfo;
/**
 * 
 * @author wentao
 *	商品收藏service
 */
public interface ProductCollectService {
	
/**
 * 添加商品收藏
 * @return
 * @param productCollect商品收藏记录
 */
public int insertProductCollect(ProductCollect productCollect);
/**
 * 删除商品收藏
 * @return
 * @param productCollectId 商品收藏id
 */
public int deleteProductCollectByCollectId(int productCollectId);
/**
 * 根据不同条件分页查询收藏商品
 * @param params 条件参数	     
 *        分页当前页码 pageNum
 *        分页获取条数 pageSize
 * @return 商品列表
 */
PageInfo<ProductCollect> findProductCollectByPage(Map<String,String> params);

/**
 * @author wentao
 * 判断是否已经存在收藏记录
 * @param productId
 * @param userId
 * @return
 */
ProductCollect getProductCollect(Integer productId ,Integer userId);

}
