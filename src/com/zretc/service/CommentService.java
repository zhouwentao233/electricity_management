package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Comment;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 评论dao层
 */
public interface CommentService {
	
	/**
	 * 根据父评论id分页显示评论
	 * @param commentFatherId 父评论id
	 * @return 二级评论
	 */
	List<Comment> commentListByFatherId(Integer commentFatherId);
	

	/**
	 * 根据商品编号获取评论
	 * @param productId 商品编号
	 * @return 同一个商品的评论
	 */
	List<Comment> commentListByProductId(Integer productId);
	
	
	/**
	 * 根据id查看评论详细信息
	 * @param commentId 评论id
	 * @return 评论对象
	 */
	Comment selectCommentDetails(Integer commentId);
	
	/**
	 * 用户删除评论
	 * @param commentId 评论id
	 * @return 影响行数
	 */
	int deleteComment(Integer commentId);
	
	/**
	 * 用户修改评论
	 * @param comment 评论对象
	 * @return 影响行数
	 */
	int updateComment(Comment comment);
	
	/**
	 * 增加评论
	 * @param comment 评论对象
	 * @return 影响行数
	 */
	int insertComment(Comment comment);
	
	/**
	 * 商家查询评论
	 * @param map 商家id 和 商品id 时间降序排列
	 * @return 一级评论
	 */
	List<Comment> commentListBySeller(Map<String, String> map);
}
