package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.User;

/**
 * 买家用户注册
 * 根据买家用户名查询用户信息
 * @author wentao
 * @return
 */
public interface UserService {
	
	
	
	/**
	 * @author 赵坤
	 * 新增用户信息，用于注册新用户
	 * @param user 用户对象
	 * @return 影响行数
	 */
	int insertUser(User user); 
	
	/**
	 * @author 赵坤
	 * 买家登录
	 * @param userAccount 买家账号
	 * @param userPassword 买家密码
	 * @return 买家信息对象
	 */
	User userlogin(String userAccount,String userPassword);
	
	/**
	 * @author 赵坤
	 * 判断买家账号是否存在
	 * @param userAccount 买家账号
	 * @return 账号是否存在（true/false）
	 */
	boolean existsOfUserAccount(String userAccount); 
	/**
	 * @author 苏帆
	 * 根据id修改用户信息
	 * @param user用户信息
	 * @return 影响行数
	 */
	int updateUserByUserId(User user); 
	
	/**
	 * @author 赵坤
	 * 根据不同条件查询买家信息
	 * @param params 条件参数
	 * @return 买家信息集合
	 */
	List<User> findByUserCondition(Map<String,String> params);
	
	/**
	 * @author 赵坤
	 * 判断买家手机号是否存在
	 * @param userPhone 买家手机号
	 * @return 手机号是否存在（true/false）
	 */
	boolean existsOfUserPhone(String userPhone); 
	
	/**
	 * @author 赵坤
	 * 根据id删除买家信息
	 * @param userId 买家编号
	 * @return 影响行数
	 */
	int deleteByUserId(Integer userId);
	
	/**
	 * @author 赵坤
	 * 判断买家用户名是否存在
	 * @param userName 买家用户名
	 * @return 买家用户名是否存在（true/false）
	 */

	boolean existsOfUserName(String userName); 
	

}
