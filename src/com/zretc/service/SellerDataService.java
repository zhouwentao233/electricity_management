package com.zretc.service;

import java.util.List;
import java.util.Map;

/**
 * @author wentao
 * 商家主页service
 * 2020年6月20日19:20:28
 *
 */
public interface SellerDataService {
	/**
	 * 新订单 条件：商家编号,30天内，订单编号唯一
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int newOrderNumber(Integer sellerId);
	
	/**
	 * 已发货 条件：商家编号，订单状态1
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int shippedOrderNumber(Integer sellerId);
	
	/**
	 *已完成 条件：商家编号，订单状态2
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int finishOrderNumber(Integer sellerId);
	
	/**
	 * //	全部订单 条件：商家编号
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int allOrderNumber(Integer sellerId);
	
	/**
	 * //	等待发货 条件：商家编号，订单状态0
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int waitOrderNumber(Integer sellerId);
	
	/**
	 * //	收藏店铺 条件：店铺编号、用户唯一
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int collectShopUserNumber(Integer sellerId);

	/**
	 * 	//	购买商品用户数 条件：商家的商品，用户编号唯一
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int shoppingProductUserNumber(Integer sellerId);

	/**
	 * 	//	收藏商品用户数 条件：商家编号，用户编号唯一
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int collectProductUserNumber(Integer sellerId);

	/**
	 * 	//	最近下单时间 条件：商家订单：时间排序第一的
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	String lastOrderDate(Integer sellerId);

	/**
	 * 	//	全部评论数量 商家为唯一编号，查询商品，再根据商品查询评论数量
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int allCommentNumber(Integer sellerId);

	/**
	 * 	//	已回复 商家商品的用户id为0的数量
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int replyCommentNumber(Integer sellerId);

	/**
	 * 	//	等待回复 子评论中没有商家id为0的用户评论
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	int waitReplyCommentNumber(Integer sellerId);

	/**
	 * 	//	商家月销售额（一个月的）
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	Map<String, Float> monthSales(Integer sellerId);

	/**
	 * 	//	商家商品下单用户数（用户号唯一）（一个月的）
	 * @author wentao
	 * @param sellerId 店铺id
	 * @return
	 */
	Map<String, Integer> placeTheOrderUserNumber(Integer sellerId);
	
	 
	 /**
	  * @author wentao
	  * 获取全部上架商品库存
	  * 2020年6月22日11:21:30
	 * @param sellerId 店铺编号
	 * @return 数据模型
	 */
	Map<String, Object> getProductInventory(Integer sellerId);
}
