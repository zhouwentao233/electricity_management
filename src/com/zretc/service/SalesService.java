package com.zretc.service;

import java.util.List;
import java.util.Map;

import com.zretc.entity.Sales;


/**
 * @author wentao
 * 销售service层
 */
public interface SalesService {
	
	/**
	 * 获取销售信息
	 * @return
	 */
	
	List<Sales> getSalesList();
	
	/**
	 * 添加销售对象
	 * @param sales 销售对象
	 * @return 影响行数
	 */
	int insertSales(Sales sales);
	
	
	
	/**
	 * @author wentao
	 * // 销售额排行前十商品（饼状图）
	 * @param sellerId
	 * @return
	 */
	List<Map<String, Object>> salesTopTenPie(Integer sellerId);
	

	/**
	 * // 销售额排行前十商品（条形图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesNumberTopTenBar(Integer sellerId);
	
	

	/**
	 * // 销售量排行前十商品（饼状图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String, Object> salesTopTenBar(Integer sellerId);
	

	/**
	 * // 销售量排行前十商品（条形图）
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	List<Map<String,Object>> salesNumberTopTenPie(Integer sellerId);
	

	/**
	 * // 根据时间查询销量
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesListByDate(Integer sellerId,String beginTime ,String lastTime);
	

	/**
	 * // 根据时间查询销售额
	 * @author wentao
	 * @param sellerId
	 * @return
	 */
	Map<String,Object> salesNumberListByDate(Integer sellerId,String beginTime ,String lastTime);

}
