package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.UserDao;
import com.zretc.dao.impl.UserDaoImpl;
import com.zretc.entity.User;
import com.zretc.service.UserService;
import com.zretc.util.MD5Util;



/**
 * User 业务逻辑层的实现类
 * @author 赵坤
 *
 */

public class UserServiceImpl implements UserService{
	
private UserDao userDao = new UserDaoImpl();

	@Override
	public int insertUser(User user) {
		//加密用户密码
		String userPassword = MD5Util.getMd5(user.getUserPassword());
		User user1 = new User(user.getUserId(),user.getUserAccount(),user.getUserName(),userPassword,user.getUserPicture(),user.getUserPhone());
		return userDao.insertUser(user1);
	}

	@Override
	public User userlogin(String userAccount, String userPassword) {
		//加密用户密码
	    String userPasswordMd5 = MD5Util.getMd5(userPassword);
		return userDao.findByUserAccountAndPassword(userAccount, userPasswordMd5);
	}

	@Override
	public boolean existsOfUserAccount(String userAccount) {
		User findByUserAccount = userDao.findByUserAccount(userAccount);
		
		return findByUserAccount != null ? true : false;
	}

	@Override
	public int updateUserByUserId(User user) {
		// TODO Auto-generated method stub
		return userDao.updateByUserId(user);
	}

	@Override
	public List<User> findByUserCondition(Map<String, String> params) {
		
		String nameKey = params.get("nameKey");
		params.put("nameKey", "%"+nameKey+"%");
		return userDao.findByUserCondition(params);
	}

	@Override
	public boolean existsOfUserPhone(String userPhone) {
        User findByUserPhone = userDao.findByUserPhone(userPhone);
		
		return findByUserPhone != null ? true : false;
	}

	@Override
	public int deleteByUserId(Integer userId) {
		
		
		return userDao.deleteByUserId(userId);
	}

	@Override
	public boolean existsOfUserName(String userName) {
		 User findByUserName = userDao.findByUserName(userName);
			
			return findByUserName != null ? true : false;
	}

	

}
