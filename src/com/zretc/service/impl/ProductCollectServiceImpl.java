package com.zretc.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.zretc.dao.ProductCollectDao;
import com.zretc.dao.impl.ProductCollectDaoImpl;
import com.zretc.entity.ProductCollect;
import com.zretc.service.ProductCollectService;
import com.zretc.util.PageInfo;

public class ProductCollectServiceImpl implements ProductCollectService{
	ProductCollectDao productCollectDao = new ProductCollectDaoImpl();
	@Override
	public int insertProductCollect(ProductCollect productCollect) {
		// TODO Auto-generated method stub
		return productCollectDao.insertProductCollect(productCollect);
	}

	@Override
	public int deleteProductCollectByCollectId(int productCollectId) {
		// TODO Auto-generated method stub
		return productCollectDao.deleteProductCollectByCollectId(productCollectId);
	}

	@Override
	public PageInfo<ProductCollect> findProductCollectByPage(Map<String, String> params) {

		// 判断是否为空
		if(params == null) {
			params = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = params.get("pageNum");
		if(pageNum == null) {
			params.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = params.get("pageSize");
		if(pageSize == null) {
			params.put("pageSize", "10");
		}
		// 判断条件是否为空
		/*String productName = params.get("productName");
		if(productName == null) {
			params.put("productName", "%");
		}else {
			params.put("productName", "%"+params.get("productName")+"%");
		}*/
		return productCollectDao.findProductCollectByPage(params);
	}

	@Override
	public ProductCollect getProductCollect(Integer productId, Integer userId) {
		// TODO Auto-generated method stub
		return productCollectDao.getProductCollect(productId, userId);
	}

}
