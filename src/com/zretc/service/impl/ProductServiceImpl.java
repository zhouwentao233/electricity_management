package com.zretc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zretc.dao.ProductDao;
import com.zretc.dao.impl.ProductDaoImpl;
import com.zretc.entity.Product;
import com.zretc.service.ProductService;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 *	商品service 的实现类
 */
public class ProductServiceImpl implements ProductService{
	
	/**
	 * 新建productDao对象
	 */
	private ProductDao productDao = new ProductDaoImpl();

	@Override
	public int insertProduct(Product product) {
		return productDao.insertProduct(product);
	}

	@Override
	public PageInfo<Product> findByPage(Map<String, String> params) {
		
		// 判断是否为空
		if(params == null) {
			params = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = params.get("pageNum");
		if(pageNum == null) {
			params.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = params.get("pageSize");
		if(pageSize == null) {
			params.put("pageSize", "10");
		}
		// 判断条件是否为空
		String productName = params.get("productName");
		if(productName == null) {
			params.put("productName", "%");
		}else {
			params.put("productName", "%"+params.get("productName")+"%");
		}
		return findByPage(params);
	}

	@Override
	public int updateProduct(Product product) {
		return productDao.updateProduct(product);
	}

	@Override
	public int deleteProduct(int productId) {
		return productDao.deleteProduct(productId);
	}

	@Override
	public PageInfo<Product> productListByType(Map<String, String> map) {
		
		// 判断是否为空
		if(map == null) {
			map = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		// 查询是否有typeId，没有则变为根目录
		String typeId = map.get("typeId");
		if(typeId == null) {
			map.put("typeId", "0");
		}
		return productDao.productListByType(map);
	}

	@Override
	public PageInfo<Product> productListBySeller(Map<String, String> map) {
		
		// 判断是否为空
		if(map == null) {
			map = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		
		return productDao.productListBySeller(map);
	}

	@Override
	public Product getProductByProductId(Integer productId) {
		return productDao.getProductByProductId(productId);
	}

	@Override
	public PageInfo<Product> productListByStatus(Map<String, String> map) {
		// 判断是否为空
		if(map == null) {
			map = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		return productDao.productListByStatus(map);
	}

	@Override
	public PageInfo<Product> productListByFatherType(Map<String, String> map) {
		// 判断是否为空
		if(map == null) {
			map = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		return productDao.productListByFatherType(map);
	}

	@Override
	public PageInfo<Map<String, Object>> productListByMoreCondition(Map<String, String> map) {
		// 管理员模块 多条件查询并排序（商品名称、一级分类、二级分类）商品名称排序、价格排序、一级分类排序、二级分类排序、上架时间排序
		if(map == null) {
			map = new HashMap<>();
		}
		// 页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		// 关键字
		String keyword = map.get("keyword");
		if(keyword == null) {
			keyword = "%";
		}else {
			keyword = "%"+keyword+"%";
		}
		map.put("keyword", keyword);
		// 判断是否有排序条件
		String orderBy = map.get("orderBy");
		if(orderBy == null || !("productName".equals(orderBy)||"productPrice".equals(orderBy)||"productTime".equals(orderBy)||"typeId".equals(orderBy)||"typeFatherId".equals(orderBy)||"sellerShopName".equals(orderBy))) {
			map.put("orderBy", "productTime");
		}
		// 判断是否有排序顺序
		String ascDesc = map.get("ascDesc");
		if(ascDesc == null || !("asc".equals(ascDesc)||"desc".equals(ascDesc))) {
			map.put("ascDesc", "desc");
		}
		return productDao.productListByMoreCondition(map);
	}

	@Override
	public PageInfo<Product> productListBySellerIdandKeyword(Map<String, String> map) {
		// 判断是否为空
				if(map == null) {
					map = new HashMap<String, String>(5);
				}
				
				// 判断是否有页码
				String pageNum = map.get("pageNum");
				if(pageNum == null) {
					map.put("pageNum", "1");
				}
				// 判断是否有页面显示数量
				String pageSize = map.get("pageSize");
				if(pageSize == null) {
					map.put("pageSize", "10");
				}
				// 关键字
				String keyword = map.get("keyword");
				if(keyword == null) {
					keyword = "%";
				}else {
					keyword = "%"+keyword+"%";
				}
				map.put("keyword", keyword);
				return productDao.productListBySellerIdandKeyword(map);
	}

	@Override
	public PageInfo<Product> productListBySellerIdandKeywordandStatus(Map<String, String> map) {
		// 判断是否为空
		if(map == null) {
			map = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = map.get("pageNum");
		if(pageNum == null) {
			map.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = map.get("pageSize");
		if(pageSize == null) {
			map.put("pageSize", "10");
		}
		// 关键字
		String keyword = map.get("keyword");
		if(keyword == null) {
			keyword = "%";
		}else {
			keyword = "%"+keyword+"%";
		}
		map.put("keyword", keyword);
		return productDao.productListBySellerIdandKeywordandStatus(map);
	}

	@Override
	public List<Map<String, Object>> productListSalesTop() {
		return productDao.productListSalesTop();
	}

	@Override
	public List<Product> productListNewTop() {
		return productDao.productListNewTop();
	}

	@Override
	public Product getProductInventoryByProductId(Integer productId) {
		
		return productDao.findProductInventoryByProductId(productId);
	}

	@Override
	public int updateProductInventoryByProductId(Product product) {
		
		return productDao.updateProductInventoryByProductId(product);
	}

}
