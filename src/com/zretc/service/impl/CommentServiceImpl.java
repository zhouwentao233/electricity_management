package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.CommentDao;
import com.zretc.dao.impl.CommentDaoImpl;
import com.zretc.entity.Comment;
import com.zretc.service.CommentService;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 *	评论的service层
 */
public class CommentServiceImpl implements CommentService{
	
	private CommentDao commentDao = new CommentDaoImpl();

	@Override
	public List<Comment> commentListByFatherId(Integer commentFatherId) {
		return commentDao.commentListByFatherId(commentFatherId);
	}

	@Override
	public List<Comment> commentListByProductId(Integer productId) {
		return commentDao.commentListByProductId(productId);
	}

	@Override
	public Comment selectCommentDetails(Integer commentId) {
		// TODO Auto-generated method stub
		return commentDao.selectCommentDetails(commentId);
	}

	@Override
	public int deleteComment(Integer commentId) {
		// TODO Auto-generated method stub
		return commentDao.deleteComment(commentId);
	}

	@Override
	public int updateComment(Comment comment) {
		// TODO Auto-generated method stub
		return commentDao.updateComment(comment);
	}

	@Override
	public int insertComment(Comment comment) {
		// TODO Auto-generated method stub
		return commentDao.insertComment(comment);
	}

	@Override
	public List<Comment> commentListBySeller(Map<String, String> map) {
		// TODO Auto-generated method stub
		return commentDao.commentListBySeller(map);
	}

	

}
