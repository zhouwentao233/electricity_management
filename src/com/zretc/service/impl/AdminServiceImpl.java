package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.AdminDao;
import com.zretc.dao.SellerDao;
import com.zretc.dao.UserDao;
import com.zretc.dao.impl.AdminDaoImpl;
import com.zretc.dao.impl.SellerDaoImpl;
import com.zretc.dao.impl.UserDaoImpl;
import com.zretc.entity.Admin;
import com.zretc.entity.Seller;
import com.zretc.entity.User;
import com.zretc.service.AdminService;

public class AdminServiceImpl implements AdminService{
	
	AdminDao adminDao = new AdminDaoImpl();
	UserDao userDao = new UserDaoImpl();
	SellerDao sellerDao = new SellerDaoImpl();

	@Override
	public Admin adminLogin(String adminAccount, String adminPassword) {
		// TODO Auto-generated method stub
		return adminDao.findAdminInfoByAdminAccountAndAdminPassword(adminAccount, adminPassword); 
	}

	@Override
	public int deleteByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return userDao.deleteByUserId(userId);
	}

	@Override
	public int deleteBySellerId(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDao.deleteBySellerId(sellerId);
	}

	@Override
	public int updateByUserId(User user) {
		// TODO Auto-generated method stub
		return userDao.updateByUserId(user);
	}

	@Override
	public int updateBySellerId(Seller seller) {
		// TODO Auto-generated method stub
		return sellerDao.updateBySellerId(seller);
	}

	@Override
	public List<User> findByUserCondition(Map<String, String> params) {
		// TODO Auto-generated method stub
		return userDao.findByUserCondition(params);
	}

	@Override
	public List<Seller> findBySellerCondition(Map<String, String> params) {
		// TODO Auto-generated method stub
		return sellerDao.findBySellerCondition(params);
	}

}
