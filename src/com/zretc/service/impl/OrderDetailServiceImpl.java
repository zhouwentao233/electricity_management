package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.OrderDetailDao;
import com.zretc.dao.impl.OrderDetailDaoImpl;
import com.zretc.entity.OrderDetail;
import com.zretc.entity.Orders;
import com.zretc.entity.User;
import com.zretc.service.OrderDetailService;

/**
 * @author chenbo
 * 订单详情表service业务层的实现类
 */
public class OrderDetailServiceImpl implements OrderDetailService{
	
	private OrderDetailDao orderDetailDao = new OrderDetailDaoImpl();
	
	@Override
	public Orders getOrdersByOrderId(String orderId) {
		
		return orderDetailDao.findOrdersByOrderId(orderId);
	}

	@Override
	public List<Map<String, Object>> getOrdersProductByOrderId(String orderId) {

		return orderDetailDao.findOrdersProductByOrderId(orderId);
	}

	@Override
	public int insertOrderDetail(OrderDetail orderDetail) {
		
		return orderDetailDao.insertOrdersDetail(orderDetail);
	}

	/**
	 * denglingqi
	 * 后端 商城管理员查看详情信息
	 * 根据订单ID，查询订单详情信息
	 * @return List<OrderDetail>
	 */
	@Override
	public List<OrderDetail> findOrderDetailByOrderId(String orderId) {
		
		return orderDetailDao.findOrderDetailByOrderId(orderId);
	}

}
