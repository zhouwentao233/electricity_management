package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.TypeDao;
import com.zretc.dao.impl.TypeDaoImpl;
import com.zretc.entity.Type;
import com.zretc.service.TypeService;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 *	分类service的实现类
 */
public class TypeServiceImpl implements TypeService{
	/**
	 * 新建分类dao的对象（向上造型）接口->实现类
	 */
	private TypeDao typeDao = new TypeDaoImpl();

	@Override
	public List<Type> typeListByFatherId(Integer typeFatherId) {
		return typeDao.typeListByFatherId(typeFatherId);
	}

	@Override
	public PageInfo<Map<String, Object>> typeListByLikeName(Map<String, String> map) {
		if(map.get("pageNum")==null) {
			map.put("pageNum", "1");
		}
		if(map.get("pageSize")==null) {
			map.put("pageSize", "10");
		}
		if(map.get("keyword")==null) {
			map.put("keyword", "%");
		}else {
			map.put("keyword","%"+map.get("keyword")+"%");
		}
		// 排序
		String orderBy = map.get("orderBy");
		if(orderBy ==null||!("typeName".equals(orderBy)||"typeFatherName".equals(orderBy))) {
			map.put("orderBy", "typeName");
		}
		// 顺序
		String ascDesc = map.get("ascDesc");
		if(ascDesc == null||!("asc".equals(ascDesc)||"desc".equals(ascDesc))) {
			map.put("ascDesc", "desc");
		}
		return typeDao.typeListByLikeName(map);
	}

	@Override
	public int saveType(Type type) {
		return typeDao.saveType(type);
	}

	@Override
	public int deleteType(Integer typeId) {
		return typeDao.deleteType(typeId);
	}

	@Override
	public int updateType(Type type) {
		return typeDao.updateType(type);
	}

	@Override
	public Type getTypeDetail(Integer typeId) {
		return typeDao.getTypeDetail(typeId);
	}

}
