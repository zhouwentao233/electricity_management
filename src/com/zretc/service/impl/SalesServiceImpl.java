package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.SalesDao;
import com.zretc.dao.impl.SalesDaoImpl;
import com.zretc.entity.Sales;
import com.zretc.service.SalesService;

/**
 * @author Administrator
 *销售的service层实现类
 */
public class SalesServiceImpl implements SalesService{
	
	private SalesDao salesDao = new SalesDaoImpl();
	
	/**
	 * @return
	 * 获取销售列表
	 */
	
	@Override
	public List<Sales> getSalesList() {
		return salesDao.find();
	}
	
	
	@Override
	public int insertSales(Sales sales) {
		// TODO Auto-generated method stub
		return salesDao.insertSales(sales);
	}


	@Override
	public List<Map<String, Object>> salesTopTenPie(Integer sellerId) {
		// TODO Auto-generated method stub
		return salesDao.salesTopTenPie(sellerId);
	}


	@Override
	public Map<String, Object> salesNumberTopTenBar(Integer sellerId) {
		// TODO Auto-generated method stub
		return salesDao.salesNumberTopTenBar(sellerId);
	}


	@Override
	public Map<String, Object> salesTopTenBar(Integer sellerId) {
		// TODO Auto-generated method stub
		return salesDao.salesTopTenBar(sellerId);
	}


	@Override
	public List<Map<String, Object>> salesNumberTopTenPie(Integer sellerId) {
		// TODO Auto-generated method stub
		return salesDao.salesNumberTopTenPie(sellerId);
	}


	@Override
	public Map<String, Object> salesListByDate(Integer sellerId, String beginTime, String lastTime) {
		// TODO Auto-generated method stub
		return salesDao.salesListByDate(sellerId, beginTime, lastTime);
	}


	@Override
	public Map<String, Object> salesNumberListByDate(Integer sellerId, String beginTime, String lastTime) {
		// TODO Auto-generated method stub
		return salesDao.salesNumberListByDate(sellerId, beginTime, lastTime);
	}

	

}
