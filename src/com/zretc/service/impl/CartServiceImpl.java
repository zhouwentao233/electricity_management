package com.zretc.service.impl;

import java.util.List;

import com.zretc.dao.CartDao;
import com.zretc.dao.impl.CartDaoImpl;
import com.zretc.entity.Cart;
import com.zretc.entity.User;
import com.zretc.service.CartService;

/**
 * Cart 业务逻辑层的实现类
 * @author chenbo
 *
 */
public class CartServiceImpl implements CartService{
	
	private CartDao cartDao = new CartDaoImpl();

	@Override
	public List<Cart> getCartAll(Integer userId) {
		
		return cartDao.findCartAll(userId);
	}

	@Override
	public int deleteCartByCartId(Integer cartId) {
		
		return cartDao.deleteCartByCartId(cartId);
	}

	@Override
	public int updateCart(Cart cart) {

		return cartDao.updateCart(cart);
	}

	@Override
	public int insertCart(Cart cart) {

		return cartDao.insertCart(cart);
	}

	@Override
	public Cart getCartAllBycartId(Integer cartId) {
		
		return cartDao.findCartAllBycartId(cartId);
	}

}
