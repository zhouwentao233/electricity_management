package com.zretc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zretc.dao.CartDao;
import com.zretc.dao.OrderDetailDao;
import com.zretc.dao.OrdersDao;
import com.zretc.dao.impl.CartDaoImpl;
import com.zretc.dao.impl.OrderDetailDaoImpl;
import com.zretc.dao.impl.OrdersDaoImpl;
import com.zretc.entity.Cart;
import com.zretc.entity.OrderDetail;
import com.zretc.entity.Orders;
import com.zretc.service.OrdersService;
import com.zretc.util.PageInfo;

/**
 * @author chenbo
 * 订单表service业务层的实现类
 */
public class OrdersServiceImpl implements OrdersService{
	
	private OrdersDao ordersDao = new OrdersDaoImpl();
	private CartDao cartDao = new CartDaoImpl();
	private OrderDetailDao orderDetailDao = new OrderDetailDaoImpl();

	@Override
	public PageInfo<Orders> getOrderAllByUserId(Map<String, String> params) {
		if (params == null) {
			params = new HashMap<>();
		}
		// 页码
		String pageNum = params.get("pageNum");
		if (pageNum == null) {
			params.put("pageNum", "1");
		}
		// 页面显示数量
		String pageSize = params.get("pageSize");
		if (pageSize == null) {
			params.put("pageSize", "10");
		}
		// 关键字
		String keyword = params.get("keyword");
		if (keyword == null) {
			keyword = "%%";
		} else {
			keyword = "%" + keyword + "%";
		}		
		params.put("keyword", keyword);
		// 状态
		String orderStatus = params.get("orderStatus");
		if (orderStatus == null) {
			params.put("orderStatus", "0");
		}
		// 商家账号
		String userIdKey = params.get("userIdKey");
		if (userIdKey == null) {
			params.put("userIdKey", "1");
		}		
		return ordersDao.findOrderAllByUserId(params);
	}

	@Override
	public PageInfo<Map<String, Object>> getPageOrderAll(Map<String, String> params) {		
		if (params == null) {
			params = new HashMap<>();
		}
		// 页码
		String pageNum = params.get("pageNum");
		if (pageNum == null) {
			params.put("pageNum", "1");
		}
		// 页面显示数量
		String pageSize = params.get("pageSize");
		if (pageSize == null) {
			params.put("pageSize", "6");
		}
		// 关键字
		String keyword = params.get("keyword");
		if (keyword == null) {
			keyword = "%%";
		} else {
			keyword = "%" + keyword + "%";
		}		
		params.put("keyword", keyword);
		// 状态
		String orderStatus = params.get("orderStatusKey");
		if (orderStatus == null) {
			params.put("orderStatus", "0");
		}
		// 商家账号
		String sellerId = params.get("sellerId");
		if (sellerId == null) {
			params.put("sellerId", "2");
		}		
		return ordersDao.findPageOrderAll(params);
	}
	
	@Override
	public List<Map<String, Object>> getOrderByorderId(String orderId) {
		
		return ordersDao.findOrderByorderId(orderId);
	}

	@Override
	public List<OrderDetail> getOrderDetailByorderId(String orderId, Integer sellerId) {
		
		return ordersDao.findOrderDetailByorderId(orderId, sellerId);
	}
	
	@Override
	public int deleteOrder(String orderId) {

		return ordersDao.deleteOrder(orderId);
	}

	@Override
	public int insertOrder(Orders orders) {
		return ordersDao.insertOrder(orders);
	}
	

	@Override
	public int updateOrderByOrderStatus(Orders orders) {
		
		return ordersDao.updateOrderByOrderStatus(orders);
	}
	
	@Override
	public int updateOrderByOrderStatus(OrderDetail orderDetail) {

		return ordersDao.updateOrderByOrderStatus(orderDetail);
	}

	/**
	 * denglingqi
	 * 商城管理
	 * 后端 订单管理 查询订单信息
	 * @param params 条件参数 分页查询
	 * 根据订单号和店铺名
	 * 订单状态，交易时间
	 * 分页当前页码 pageNum
	 * 分页获取条数 pageSize
	 * @return Order
	 */
	@Override
	public PageInfo<Orders> findPageOrder(Map<String, String> params) {
		params.put("orderId",params.get("orderId"));
		params.put("sellerId",params.get("sellerId"));
		params.put("orderTime",params.get("orderTime")+"%");
		params.put("orderTimes",params.get("orderTimes")+"%");
		params.put("orderStatus",params.get("orderStatus"));	
		return ordersDao.findPageOrder(params);
	}

}
