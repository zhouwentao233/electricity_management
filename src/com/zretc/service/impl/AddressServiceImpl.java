package com.zretc.service.impl;

import java.util.List;

import com.zretc.dao.AddressDao;
import com.zretc.dao.UserDao;
import com.zretc.dao.impl.AddressDaoImpl;
import com.zretc.dao.impl.UserDaoImpl;
import com.zretc.entity.Address;

import com.zretc.entity.User;
import com.zretc.service.AddressService;

public class AddressServiceImpl implements AddressService{
    AddressDao addressDao = new AddressDaoImpl();
    UserDao userDao =new UserDaoImpl();
    
	@Override
	public int insertAddress(Address address) {
		/**
		 * @author sufan
		 */
		return addressDao.insertAddress(address);
	}
	
	@Override
	public int deleteAddressByAddressId(int addressId) {
		
		return addressDao.deleteAddressByAddressId(addressId);
	}

	@Override
	public List<Address> findAllAddressByUserId(User user) {
		// TODO Auto-generated method stub
		return addressDao.findAllAddressByUserId(user);
	}

	@Override
	public int updateAddressByAddressId(Address address) {
		// TODO Auto-generated method stub
		return addressDao.updateAddressByAddressId(address);
	}

	@Override
	public Address getAddressByUserId(Integer userId) {
		
		return addressDao.fingAddressByUserId(userId);
	}

	@Override
	public Address findAddressByAddressId(int addressId) {
		// TODO Auto-generated method stub
		return addressDao.findAddressByAddressId(addressId);
	}

}
