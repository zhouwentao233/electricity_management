package com.zretc.service.impl;

import java.util.List;
import java.util.Map;

import com.zretc.dao.SellerDao;
import com.zretc.dao.impl.SellerDaoImpl;
import com.zretc.entity.Seller;
import com.zretc.entity.User;
import com.zretc.service.SellerService;
import com.zretc.util.MD5Util;

/**
 * Seller 业务逻辑层的实现类
 * @author 赵坤
 *
 */

public class SellerServiceImpl implements SellerService {
	
	private SellerDao sellerDao = new SellerDaoImpl();

	@Override
	public int insertShop(Seller seller) {
		//加密用户密码
				String sellerPassword = MD5Util.getMd5(seller.getSellerPassword());
				Seller seller1 = new Seller(seller.getSellerId(),seller.getSellerAccount(),sellerPassword,seller.getSellerAddress(),seller.getSellerPhone(),seller.getSellerShopName(),seller.getSellerLogo(),seller.getSellerStatus());
		return sellerDao.insertShop(seller1);
	}

	@Override
	public Seller sellerLogin(String sellerAccount, String sellerPassword) {
		//加密用户密码
	    String sellerPasswordMd5 = MD5Util.getMd5(sellerPassword);
		return sellerDao.findBySellerAccountAndSellerPassword(sellerAccount, sellerPasswordMd5);
	}

//	店铺查询
	@Override
	public List<Seller> getSellerListCondition(Map<String, String> params) {
		String selectKey =params.get("selectKey");
		params.put("selectKey","%"+ selectKey+"%");
		return sellerDao.findBySellerCondition(params);
	}

//	店铺删除
	@Override
	public int deleteById(Integer sellerId) {
		return sellerDao.deleteBySellerId(sellerId);
	}
//	店铺信息修改
	@Override
	public int modifyBySellerId(Seller seller) {
		return sellerDao.updateBySellerId(seller);
	}
//判断卖家账号是否存在
	@Override
	public boolean existsOfSellerName(String sellerAccount) {
    Seller findBySellerAccount = sellerDao.findBySellerAccount(sellerAccount);
		
		return findBySellerAccount != null ? true : false;
	}
	//判断卖家手机号是否存在
	@Override
	public boolean existsOfSellerPhone(String sellerPhone) {
		Seller findBySellerPhone = sellerDao.findBySellerPhone(sellerPhone);
		return findBySellerPhone != null ? true : false;
	}

	@Override
	public int updatePassword(Integer sellerId, String password) {
		return sellerDao.updatePassword(sellerId, password);
	}

	@Override
	public Seller getSellerDetail(Integer sellerId) {
		return sellerDao.getSellerDetail(sellerId);
	}

	@Override
	public boolean existsOfSellerShopName(String sellerShopName) {
		Seller findBySellerShopName = sellerDao.findBySellerShopName(sellerShopName);
		return findBySellerShopName != null ? true : false;
	}
	/**
	 * @author 
	 * 根据账号修改状态
	 * @param sellerId
	 * @param sellerStatus
	 * @return
	 */
	@Override
	public int updateStatus(Seller seller) {
		return sellerDao.updateStatus(seller);
	}
	
	
	
	

	

}
