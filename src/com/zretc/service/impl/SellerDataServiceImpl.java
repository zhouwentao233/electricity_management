package com.zretc.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zretc.dao.SellerDataDao;
import com.zretc.dao.impl.SellerDataDaoImpl;
import com.zretc.service.SellerDataService;


/**
 * @author wentao
 * 2020年6月20日19:22:42
 *
 */
public class SellerDataServiceImpl implements SellerDataService{

	private SellerDataDao sellerDataDao = new SellerDataDaoImpl();
	
	@Override
	public int newOrderNumber(Integer sellerId) {
		return sellerDataDao.newOrderNumber(sellerId);
	}

	@Override
	public int shippedOrderNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.shippedOrderNumber(sellerId);
	}

	@Override
	public int finishOrderNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.finishOrderNumber(sellerId);
	}

	@Override
	public int allOrderNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.allOrderNumber(sellerId);
	}

	@Override
	public int waitOrderNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.waitOrderNumber(sellerId);
	}

	@Override
	public int collectShopUserNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.collectShopUserNumber(sellerId);
	}

	@Override
	public int shoppingProductUserNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.shoppingProductUserNumber(sellerId);
	}

	@Override
	public int collectProductUserNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.collectProductUserNumber(sellerId);
	}

	@Override
	public String lastOrderDate(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.lastOrderDate(sellerId);
	}

	@Override
	public int allCommentNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.allCommentNumber(sellerId);
	}

	@Override
	public int replyCommentNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.replyCommentNumber(sellerId);
	}

	@Override
	public int waitReplyCommentNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.waitReplyCommentNumber(sellerId);
	}

	@Override
	public Map<String, Float> monthSales(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.monthSales(sellerId);
	}

	@Override
	public Map<String, Integer> placeTheOrderUserNumber(Integer sellerId) {
		// TODO Auto-generated method stub
		return sellerDataDao.placeTheOrderUserNumber(sellerId);
	}

	@Override
	public Map<String, Object> getProductInventory(Integer sellerId) {
		Map<String, Integer> map = sellerDataDao.getProductInventory(sellerId);
		List<String> productList = new ArrayList<String>();
		List<Integer> inventoryList = new ArrayList<>();
		Set<String> keySet = map.keySet();
		for(String key : keySet) {
			productList.add(key);
			inventoryList.add(map.get(key));
		}
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("productList", productList);
		m.put("inventoryList", inventoryList);
		return m;
	}

}
