package com.zretc.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.zretc.dao.ShopsCollectDao;
import com.zretc.dao.impl.ShopsCollectDaoImpl;
import com.zretc.entity.ProductCollect;
import com.zretc.entity.ShopsCollect;
import com.zretc.service.ShopsCollectService;
import com.zretc.util.PageInfo;

public class ShopsCollectServiceImpl implements ShopsCollectService{
	ShopsCollectDao shopsCollectDao = new ShopsCollectDaoImpl();
	@Override
	public int insertShopsCollect(ShopsCollect shopsCollect) {
		// TODO Auto-generated method stub
		return shopsCollectDao.insertShopsCollect(shopsCollect);
	}

	@Override
	public int deleteShopsCollectByCollectId(int shopsCollectId) {
		// TODO Auto-generated method stub
		return shopsCollectDao.deleteShopsCollectByCollectId(shopsCollectId);
	}

	@Override
	public PageInfo<ShopsCollect> findShopsCollectByPage(Map<String, String> params) {

		// 判断是否为空
		if(params == null) {
			params = new HashMap<String, String>(5);
		}
		
		// 判断是否有页码
		String pageNum = params.get("pageNum");
		if(pageNum == null) {
			params.put("pageNum", "1");
		}
		// 判断是否有页面显示数量
		String pageSize = params.get("pageSize");
		if(pageSize == null) {
			params.put("pageSize", "10");
		}
		/*// 判断条件是否为空
		String productName = params.get("productName");
		if(productName == null) {
			params.put("productName", "%");
		}else {
			params.put("productName", "%"+params.get("productName")+"%");
		}*/
		return shopsCollectDao.findShopsCollectByPage(params);
	}

	@Override
	public ShopsCollect getShopsCollect(Integer userId, Integer sellerId) {
		// TODO Auto-generated method stub
		return shopsCollectDao.getShopsCollect(userId, sellerId);
	}

}
