package com.zretc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Cart;
import com.zretc.entity.Product;
import com.zretc.entity.User;
import com.zretc.service.CartService;
import com.zretc.service.ProductService;
import com.zretc.service.impl.CartServiceImpl;
import com.zretc.service.impl.ProductServiceImpl;

/**
 * Servlet implementation class CartServlet
 */
/**
 * @author chenbo
 * 购物车表的控制层
 */
@WebServlet("/front/CartServlet")
public class FrontCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CartService cartService = new CartServiceImpl(); 
	private ProductService productService = new ProductServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 接收请求中的参数op，请求操作标识
		String op = request.getParameter("op");
		
		if (op == null || "getCartList".equals(op)) {
			// 获取购物车列表
			doGetCartList(request,response);
		} else if ("deleteByCartId".equals(op)) {
			// 删除商品
			doDeleteByCartId(request,response);
		} else if ("updateByCartId".equals(op)) {
			// 修改商品信息
			doUpdateByCartId(request,response);
		} 
	}

	private void doUpdateByCartId(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Integer cartId = Integer.valueOf(request.getParameter("cartId"));
		Integer cartCount = Integer.valueOf(request.getParameter("proNumber"));
		Float cartPrice = Float.valueOf(request.getParameter("total"));		
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		Cart cart = new Cart();
		cart.setCartId(cartId);
		cart.setCartCount(cartCount);
		cart.setCartPrice(cartPrice);
		
		Cart cartAllBycartId = cartService.getCartAllBycartId(cartId);
		Product product = cartAllBycartId.getProduct();
		Integer productId = product.getProductId();
		product.setProductId(productId);
		Product productInventoryByProductId = productService.getProductInventoryByProductId(productId);
		Integer productInventory = productInventoryByProductId.getProductInventory();
		
		if (productInventory >= cartCount) {
			cartService.updateCart(cart);
			// 再一次调用查询方法
			doGetCartList(request,response);
		} else {
			
		}
	}

	private void doDeleteByCartId(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Integer cartId = Integer.valueOf(request.getParameter("cartId"));
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		int deleteCartByCartId = cartService.deleteCartByCartId(cartId);	
		// 再一次调用查询方法
		doGetCartList(request,response);
	}

	private void doGetCartList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 调用模型层，获取数据
		// String user = request.getParameter("userId");
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		List<Cart> cartList = cartService.getCartAll(userId);
		
		// 利用第三方类库，将数据转换成JSON字符串
		Gson gson = new Gson();
		String jsonObject = gson.toJson(cartList);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(jsonObject);		
	}

	/**
	 * @author wentao
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		saveCart(request, response);
	}
	
	/**
	 *  添加商品进购物车
	 * @author wentao
	 * @param request 请求
	 * @param response 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void saveCart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		// 获取用户编号
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		// 获取商品编号
		Integer productId =  Integer.valueOf(request.getParameter("productId"));
		// 获取商品数量
		Integer cartCount =  Integer.valueOf(request.getParameter("cartCount"));
		// 获取价格
		Float cartPrice = Float.valueOf(request.getParameter("cartPrice"));
		User user = new User();
		user.setUserId(userId);
		Product product = new Product();
		product.setProductId(productId);
		Cart cart = new Cart(null, user, product, cartCount, cartPrice);
		
		// 获取商品库存
		Product productInventoryByProductId = productService.getProductInventoryByProductId(productId);
		Integer productInventory = productInventoryByProductId.getProductInventory();
		
		// 查找是否存在id相同的购物车商品
		List<Cart> cartAll = cartService.getCartAll(userId);
		boolean flag = true ;
		for(Cart c:cartAll) {
			if(c.getProduct().getProductId()== productId) {
				// 判断库存是否充足
				if(productInventory >= c.getCartCount()+cartCount ) {
					c.setCartCount(c.getCartCount()+cartCount);
					cartService.updateCart(c);
					flag = false;
				}else {
					return;
				}
				
			}
		}
		if(flag) {
			// 判断库存是否充足
			if(productInventory >= cartCount ) {
				cartService.insertCart(cart);
			}else {
				return;
			}
			
		}
		
	}

}
