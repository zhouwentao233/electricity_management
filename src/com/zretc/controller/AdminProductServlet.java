package com.zretc.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Product;
import com.zretc.service.ProductService;
import com.zretc.service.impl.ProductServiceImpl;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 管理员商品管理
 * Servlet implementation class AdminProductServlet
 */
@WebServlet("/back/adminProduct")
public class AdminProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// 获取商品业务层对象
	private ProductService productService = new ProductServiceImpl();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取操作类型
		String op = request.getParameter("op");
		if(op == null) {
			op = "";
		}
		
		// 判断操作类型
		switch (op) {
		case "getAll":
			// 根据条件获取全部商品并排序
			getProductList(request, response);
			break;
		case "soldOut":
			// 下架商品
			soldOutProduct(request, response);
			break;
		case "detail":
			// 下架商品
			getProductDetail(request, response);
			break;

		default:
			break;
		}
	}
	
	/**
	 * wentao
	 * @param request 请求
	 * @param response 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getProductList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取显示数量
		String pageNum = request.getParameter("pageNum");
		if(pageNum == null) {
			pageNum = "10";
		}
		// 获取页码
		String pageSize = request.getParameter("pageSize");
		if(pageSize == null) {
			pageSize = "1";
		}
		// 获取搜索条件
		String keyword = request.getParameter("keyword");
		if(keyword == null) {
			keyword = "";
		}
		// 获取排序条件
		String orderBy = request.getParameter("orderBy");
		// 获取排序顺序
		String ascDesc = request.getParameter("ascDesc");
		// 构建map条件
		Map<String, String> condition = new HashMap<>();
		condition.put("pageNum", pageNum);
		condition.put("pageSize", pageSize);
		condition.put("keyword", keyword);
		condition.put("orderBy", orderBy);
		condition.put("ascDesc", ascDesc);
		
		// 分页查询数据
		PageInfo<Map<String, Object>> pageInfo = productService.productListByMoreCondition(condition);
		// 响应json
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(pageInfo));
	}
	
	/**
	 * @author wentao
	 * 下架商品
	 */
	public void soldOutProduct(HttpServletRequest request, HttpServletResponse response) {
		// 获取商品id
		Integer productId = Integer.valueOf(request.getParameter("productId"));
		// 根据商品id获取商品
		Product product = productService.getProductByProductId(productId);
		// 设置为下架状态
		product.setProductStatus(0);
		// 更新商品
		productService.updateProduct(product);
	}
	
	public void getProductDetail(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 获取商品id
		Integer productId = Integer.valueOf(request.getParameter("productId"));
		// 根据商品id获取商品
		Product product = productService.getProductByProductId(productId);
		// 获取图片列表
		List<String> images = Arrays.asList(product.getProductPicture().split(","));
		Map<String, Object> map = new HashMap<>();
		map.put("product", product);
		map.put("images", images);
		// 响应json
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(map));
	}

}
