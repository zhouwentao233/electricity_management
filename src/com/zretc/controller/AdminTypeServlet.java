package com.zretc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Type;
import com.zretc.service.TypeService;
import com.zretc.service.impl.TypeServiceImpl;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * 管理员分类模块
 * Servlet implementation class AdminTypeServlet
 */
@WebServlet("/back/adminType")
public class AdminTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private TypeService typeService = new TypeServiceImpl();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取操作类型
		String op = request.getParameter("op");
		if(op == null) {
			op = "";
		}
		switch (op) {
		case "getAll":
			getAllType(request, response);
			break;
		case "getFatherType":
			getFatherType(request, response);
			break;
		case "detail":
			getTypeDetail(request, response);
			break;
		case "update":
			updateType(request, response);
			break;
		case "save":
			saveType(request, response);
			break;
		case "delete":
			deleteType(request, response);
			break;

		default:
			break;
		}
	}
	
	/**
	 * @author wentao
	 * 获取全部分类
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void getAllType(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取分页页码
		String pageNum = request.getParameter("pageNum");
		// 获取显示数量
		String pageSize = request.getParameter("pageSize");
		// 获取查询条件
		String keyword = request.getParameter("keyword");
		// 获取排序
		String orderBy = request.getParameter("orderBy");
		// 获取顺序
		String ascDesc = request.getParameter("ascDesc");
		// 转换成map条件
		Map<String,String> condition = new HashMap<String, String>();
		condition.put("pageNum", pageNum);
		condition.put("pageSize", pageSize);
		condition.put("keyword", keyword);
		condition.put("orderBy", orderBy);
		condition.put("ascDesc", ascDesc);
		// 查询
		PageInfo<Map<String, Object>> page = typeService.typeListByLikeName(condition);
		// 转换成json
		Gson gson = new Gson();
		String json = gson.toJson(page);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
		
	} 
	/**
	 * @author wentao
	 * 获取分类详细信息
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void getFatherType(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取全部父分类
		List<Type> list = typeService.typeListByFatherId(0);
		// 转换成json
		Gson gson = new Gson();
		String json = gson.toJson(list);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
		
	} 
	/**
	 * @author wentao
	 * 获取分类详细信息
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void getTypeDetail(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取分类id
		Integer tpeyId = Integer.valueOf(request.getParameter("typeId"));
		// 获取分类信息
		Type type = typeService.getTypeDetail(tpeyId);
		// 转换成json
		Gson gson = new Gson();
		String json = gson.toJson(type);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
		
	} 
	/**
	 * @author wentao
	 * 更新
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void updateType(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取分类id
		Integer typeId = Integer.valueOf(request.getParameter("typeId"));
		// 获取父分类id
		Integer typeFatherId = Integer.valueOf(request.getParameter("typeFatherId"));
		// 获取分类名称
		String typeName = request.getParameter("typeName");
		// 更新数据
		Type type = new Type(typeId, typeFatherId, typeName);
		typeService.updateType(type);
		
	} 
	/**
	 * @author wentao
	 * 保存
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void saveType(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取父分类id
		Integer typeFatherId = Integer.valueOf(request.getParameter("typeFatherId"));
		// 获取分类名称
		String typeName = request.getParameter("typeName");
		// 更新数据
		Type type = new Type(null, typeFatherId, typeName);
		typeService.saveType(type);
	} 
	/**
	 * @author wentao
	 * 删除
	 * @param request 请求
	 * @param rServletResponse 响应
	 * @throws ServletException 异常
	 * @throws IOException 异常
	 */
	public void deleteType(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		// 获取分类id
		Integer typeId = Integer.valueOf(request.getParameter("typeId"));
		// 删除
		typeService.deleteType(typeId);
	} 

}
