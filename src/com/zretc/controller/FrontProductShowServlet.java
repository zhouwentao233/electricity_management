package com.zretc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.dao.ProductDao;
import com.zretc.entity.Product;
import com.zretc.service.ProductService;
import com.zretc.service.impl.ProductServiceImpl;
import com.zretc.util.PageInfo;

/**
 * @author wentao
 * Servlet implementation class FrontProductShowServlet
 * 前端页面商品信息显示
 */
@WebServlet("/front/productshow")
public class FrontProductShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 创建商品业务处对象
	 */
	private ProductService productService = new ProductServiceImpl();
       
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	// 获取操作类型
    	String op = request.getParameter("op");
    	// 判断是否为空
    	if(op == null) {
    		op = "";
    	}
    	switch (op) {
		case "allProduct":
			// 全部商品,ajax获取
			showAllProduct(request, response);
			break;
		case "seller":
			// 店铺商品
			showSellerProduct(request, response);
			break;
		case "detail":
			// 商品详情
			showProductDetail(request, response);
			break;
		case "salesTop":
			// 商品详情
			salesTop(request, response);
			break;
		case "newTop":
			// 商品详情
			newTop(request, response);
			break;
		
		default:
			break;
		}
    }
    
    /**
     * 显示全部商品
     * @throws IOException 响应错误
     */
    public void showAllProduct(HttpServletRequest request,HttpServletResponse response) throws IOException {
    	// 获取分类一级分类和二级分类
    	String oneType = request.getParameter("oneType");
    	String twoType = request.getParameter("twoType");
    	if(!"null".equals(oneType)) {
    		showLevelOneTypeProduct(request, response);
    	}else if(!"null".equals(twoType)) {
    		showLevelTwoTypeProduct(request, response);
    	}else {
    		// 获取页面页码
        	String pageNum = request.getParameter("pageNum");
        	if(pageNum == null) {
        		pageNum = "1";
        	}
        	// 获取要显示的数量
        	String pageSize = request.getParameter("pageSize");
        	if(pageSize == null) {
        		pageSize = "10";
        	}
        	
        	Map<String, String> condition = new HashMap<String, String>(); 
        	condition.put("pageNum", pageNum);
        	condition.put("pageSize", pageSize);
        	// 获取分页对象(包括全部上架商品数据)
        	PageInfo<Product> page = productService.productListByStatus(condition);
        	
        	// 将分页对象转换成json
        	Gson gson = new Gson();
        	String json = gson.toJson(page);
        	
        	// 响应格式
        	response.setContentType("application/json");
        	response.getWriter().write(json);
    	}
    	
    }
    
    /**
     * 显示一级分类全部商品
     * @throws IOException 响应错误
     */
    public void showLevelOneTypeProduct(HttpServletRequest request,HttpServletResponse response) throws IOException {
    	// 获取一级分类编号
    	String typeFatherId = request.getParameter("oneType");
    	// 获取页面页码
    	String pageNum = request.getParameter("pageNum");
    	if(pageNum == null) {
    		pageNum = "1";
    	}
    	// 获取要显示的数量
    	String pageSize = request.getParameter("pageSize");
    	if(pageSize == null) {
    		pageSize = "10";
    	}
    	
    	Map<String, String> condition = new HashMap<String, String>(); 
    	condition.put("pageNum", pageNum);
    	condition.put("pageSize", pageSize);
    	condition.put("typeFatherId", typeFatherId);
    	
    	// 获取数据模型
    	PageInfo<Product> page = productService.productListByFatherType(condition);
    	
    	// 响应
    	Gson gson = new Gson();
    	String json = gson.toJson(page);
    	
    	// 响应格式
    	response.setContentType("application/json");
    	response.getWriter().write(json);
    }
    
    /**
     * 显示二级分类全部商品
     * @throws IOException 响应错误
     */
    public void showLevelTwoTypeProduct(HttpServletRequest request,HttpServletResponse response) throws IOException {
    	// 获取二级分类编号
    	String typeFatherId = request.getParameter("twoType");
    	// 获取页面页码
    	String pageNum = request.getParameter("pageNum");
    	if(pageNum == null) {
    		pageNum = "1";
    	}
    	// 获取要显示的数量
    	String pageSize = request.getParameter("pageSize");
    	if(pageSize == null) {
    		pageSize = "10";
    	}
    	
    	Map<String, String> condition = new HashMap<String, String>(); 
    	condition.put("pageNum", pageNum);
    	condition.put("pageSize", pageSize);
    	condition.put("typeId", typeFatherId);
    	
    	// 获取数据模型
    	PageInfo<Product> page = productService.productListByType(condition);
    	
    	// 响应
    	Gson gson = new Gson();
    	String json = gson.toJson(page);
    	
    	// 响应格式
    	response.setContentType("application/json");
    	
    	response.getWriter().write(json);
    }
    
    /**
     * 显示店铺商品
     * @throws IOException 响应错误
     */
    public void showSellerProduct(HttpServletRequest request,HttpServletResponse response) throws IOException {
    	// 获取店铺编号
    	String sellerId = request.getParameter("sellerId");
    	// 获取页面页码
    	String pageNum = request.getParameter("pageNum");
    	if(pageNum == null) {
    		pageNum = "1";
    	}
    	// 获取要显示的数量
    	String pageSize = request.getParameter("pageSize");
    	if(pageSize == null) {
    		pageSize = "10";
    	}
    	
    	Map<String, String> condition = new HashMap<String, String>(); 
    	condition.put("pageNum", pageNum);
    	condition.put("pageSize", pageSize);
    	condition.put("sellerId", sellerId);
    	
    	// 获取数据模型
    	PageInfo<Product> page = productService.productListBySeller(condition);
    	
    	// 响应
    	Gson gson = new Gson();
    	String json = gson.toJson(page);
    	response.setContentType("application/json");
    	response.getWriter().write(json);
    }
    
    /**
     * 显示商品详细信息
     * @throws IOException 异常
     * @throws ServletException 异常
     */
    public void showProductDetail(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
    	// 获取商品编号
    	Integer productId = Integer.valueOf(request.getParameter("productId"));
    	// 根据编号获取商品数据
    	Product product = productService.getProductByProductId(productId);
    	
    	// 对象转换成json格式字符串
    	Gson gson = new Gson();
    	String json = gson.toJson(product);
    	
    	// 响应
    	response.setContentType("application/json");
    	response.getWriter().write(json);
    }
    public void salesTop(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
    	// 获取销量排名前5
    	List<Map<String, Object>> list = productService.productListSalesTop();
    	
    	// 对象转换成json格式字符串
    	Gson gson = new Gson();
    	String json = gson.toJson(list);
    	
    	// 响应
    	response.setContentType("application/json");
    	response.getWriter().write(json);
    }
    public void newTop(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
    	// 获取新品排名前5
    	List<Product> list = productService.productListNewTop();
    	
    	// 对象转换成json格式字符串
    	Gson gson = new Gson();
    	String json = gson.toJson(list);
    	
    	// 响应
    	response.setContentType("application/json");
    	response.getWriter().write(json);
    }

}
