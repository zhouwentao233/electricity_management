package com.zretc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.zretc.entity.Comment;
import com.zretc.entity.Product;
import com.zretc.entity.User;
import com.zretc.service.CommentService;
import com.zretc.service.ProductService;
import com.zretc.service.impl.CommentServiceImpl;
import com.zretc.service.impl.ProductServiceImpl;
import com.zretc.util.PageInfo;

/**
 * @author wentao 商品评论模块
 */
@WebServlet("/back/sellerComment")
public class SellerCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CommentService commentService = new CommentServiceImpl();
	private ProductService productService = new ProductServiceImpl();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取操作类型 
		String op = request.getParameter("op");
		if(op == null) {
			op = "";
		}
		switch (op) {
		case "getAll":
			// 获取一级评论
			getAllComment(request, response);
			break;
		case "two":
			// 获取二级评论
			getTwoComment(request, response);
			break;
		case "save":
			// 商家回复（一级或者二级）
			saveComment(request, response);
			break;
		case "getProduct":
			// 获取全部商品（用做下拉框显示）
			getAllProduct(request, response);
			break;

		default:
			break;
		}
	}
	
	/**
	 * @author wentao 
	 * 获取符号条件的全部商品评论
	 * @throws IOException 
	 */
	public void getAllComment(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 商品编号
		String productId = request.getParameter("productId");
		// 商家编号
		String sellerId = request.getParameter("sellerId");
		// 显示数量
		String pageSize = request.getParameter("pageSize");
		String pageNum = request.getParameter("pageNum");
		Map<String, String> condition = new HashMap<>();
		condition.put("productId", productId);
		condition.put("sellerId", sellerId);
		condition.put("pageSize",pageSize);
		condition.put("pageNum",pageNum);
		// 查询
		List<Comment> list = commentService.commentListBySeller(condition);
		
		// 转换成json
		String json = new Gson().toJson(list);
		
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * 获取二级评论
	 * @throws IOException 
	 */
	public void getTwoComment(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 获取评论编号
		Integer commentId = Integer.valueOf(request.getParameter("commentId"));
		// 获取二级评论
		List<Comment> list = commentService.commentListByFatherId(commentId) ;
		// 转换成json
		String json = new Gson().toJson(list);
		
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * 商家回复（一级或者二级）
	 */
	public void saveComment(HttpServletRequest request, HttpServletResponse response) {
		// 获取评论编号
		Integer commentFatherId = Integer.valueOf(request.getParameter("commentId"));
		// 获取评论内容
		String commentContent = request.getParameter("commentContent");
		// 获取评论时间
		String commentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
		// 获取用户编号(商家统一用用户编号为0的账号)
		User user = new User();
		user.setUserId(0);
		// 获取商品编号
		Integer productId = Integer.valueOf(request.getParameter("productId"));
		Product product = new Product();
		product.setProductId(productId);
		
		Comment comment = new Comment(null, user, product, commentTime, commentContent, commentFatherId, null);

		// 保存数据
		commentService.insertComment(comment);
	}
	/**
	 * @author wentao 
	 * 获取全部商品（用做下拉框显示）
	 * @throws IOException 
	 */
	public void getAllProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 获取店铺编号
		String sellerId = request.getParameter("sellerId");
		// 获取关键字
		String keyword = request.getParameter("keyword");
		// 页数，页码，条件组装
		String pageSize = "1000";
		String pageNum = "1";
		Map<String, String> condition = new HashMap<>();
		condition.put("pageSize", pageSize);
		condition.put("pageNum", pageNum);
		condition.put("sellerId", sellerId);
		condition.put("keyword", keyword);
		// 根据店铺查询商品
		PageInfo<Product> list = productService.productListBySeller(condition);
		
		// 转换成json
		String json = new Gson().toJson(list);
		
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
}
