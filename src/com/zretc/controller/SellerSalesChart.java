package com.zretc.controller;

import java.awt.Font;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.VerticalAlignment;

import com.google.gson.Gson;
import com.zretc.entity.Product;
import com.zretc.entity.Sales;
import com.zretc.service.SalesService;
import com.zretc.service.impl.SalesServiceImpl;

/**
 * 销量柱状图
 * Servlet implementation class SalesChart1
 */
@WebServlet("/back/SellerSalesChart")
public class SellerSalesChart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SalesService salesService = new SalesServiceImpl();
	
	
	public static JFreeChart createChart() {
		
		
//		从数据库获取数据
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		SalesService salesService = new SalesServiceImpl();
		List<Sales> salesList = salesService.getSalesList();
		for (Sales sales : salesList) {
			String saleDate = sales.getSalesDate();
			Integer salesCount = sales.getSalesCount();
			String productName = sales.getProduct().getProductName();
			dataSet.addValue(salesCount, saleDate, productName);
		}
		
		
//		创建柱形图
		JFreeChart chart = ChartFactory.createBarChart3D(
				"销量统计表", "月份", "销量（件）", dataSet,PlotOrientation.VERTICAL,true,false,false);
		chart.getTitle().setFont(new Font("隶书", Font.PLAIN, 25));//设置标题字体
		chart.getLegend().setItemFont(new Font("宋体", Font.PLAIN, 12));//图例类别字体
		chart.setBorderVisible(true);//设置边框是否显示
//		设置副标题
		TextTitle subTitle = new TextTitle("店铺销量统计");
		subTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);//设置居中
		chart.addSubtitle(subTitle);//添加字标题
//		设置透明度及背景
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setForegroundAlpha(0.8f);
		plot.setBackgroundAlpha(0.5f);
//		plot.setBackgroundImage(image);
//		获得坐标轴
		CategoryAxis categoryAxis = plot.getDomainAxis();
//		设计坐标轴字体
		plot.getRangeAxis().setLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setTickLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		ValueAxis valueAxis = plot.getRangeAxis();
		BarRenderer3D renderer = new BarRenderer3D();
		renderer.setItemMargin(0.32);
		plot.setRenderer(renderer);
		
		
		return chart;
		
	}
		
	
	/**
	 * @author wentao
	 * 2020年6月22日22:02:01
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 判断操作
		String op = request.getParameter("op");
		if (op == null) {
			op = "";
		}
		switch (op) {
		case "getSalesTopPie":
			getSalesTopPie(request, response);
			break;
		case "getSalesTopBar":
			getSalesTopBar(request, response);
			break;
		case "getNumberTopPie":
			getNumberTopPie(request, response);
			break;
		case "getNumberTopBar":
			getNumberTopBar(request, response);
			break;
		case "getAllSales":
			getAllSales(request, response);
			break;
		case "getAllNumber":
			getAllNumber(request, response);
			break;

		default:
			break;
		}
	
	}
	
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getSalesTopPie(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		List<Map<String, Object>> data = salesService.salesTopTenPie(sellerId);
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getSalesTopBar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		Map<String, Object> data = salesService.salesTopTenBar(sellerId);
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getNumberTopPie(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		List<Map<String, Object>> data = salesService.salesNumberTopTenPie(sellerId);
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getNumberTopBar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		Map<String, Object> data = salesService.salesNumberTopTenBar(sellerId);
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getAllSales(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		// 获取开始时间 yyyy-MM-dd
		String beginTime = request.getParameter("beginTime");
		String lastTime = request.getParameter("lastTime");
		
		Map<String, Object> data = salesService.salesListByDate(sellerId, beginTime, lastTime);
		
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	/**
	 * @author wentao 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getAllNumber(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取商家编号
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		// 获取开始时间 yyyy-MM-dd
		String beginTime = request.getParameter("beginTime");
		String lastTime = request.getParameter("lastTime");
		
		Map<String, Object> data = salesService.salesNumberListByDate(sellerId, beginTime, lastTime);
		// 转换格式并响应
		Gson gson = new Gson();
		String json = gson.toJson(data);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
}

