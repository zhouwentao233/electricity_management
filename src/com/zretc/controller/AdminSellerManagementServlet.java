package com.zretc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Seller;
import com.zretc.service.SellerService;
import com.zretc.service.impl.SellerServiceImpl;

/**
 * 店铺管理
 * Servlet implementation class AdminSellerManagementServlet
 */
@WebServlet("/AdminSellerManagementServlet")
public class AdminSellerManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SellerService sellerService = new SellerServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminSellerManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op = request.getParameter("op");
		if (op == null || "getSellerList".equals(op)) {
			//获取店铺列表
			doGetSellerList(request,response);
		}else if ("deleteBySellerId".equals(op)) {
			//删除店铺
			doDeleteBySellerId(request,response);
		}else if ("updateBySellerId".equals(op)) {
			//修改店铺
			doUpdateBySellerId(request,response);
		}else if("updateStatus".equals(op)) {
			//修改店铺状态
			updateStatus(request,response);
		}
	}
	
	/**
	 * 获得店铺列表
	 */
	private void doGetSellerList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String selectKey = request.getParameter("selectKey");
		Map<String, String> params = new HashMap<String,String>();
		params.put("selectKey", selectKey);
		List<Seller> sellerList = sellerService.getSellerListCondition(params);
		
		Gson gson = new Gson();
		String jsonObject = gson.toJson(sellerList);
//		响应
		response.setContentType("appliction/json");
		response.getWriter().write(jsonObject);
	
	}
	
	/**
	 * 删除店铺列表
	 */
	private void doDeleteBySellerId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		sellerService.deleteById(sellerId);
		doGetSellerList(request, response);
	
	}
	
	/**
	 * 修改店铺列表
	 */
	private void doUpdateBySellerId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//解析json字符串
		String jsonString = request.getParameter("updateSeller");
		Gson gson = new Gson();
		Seller seller = gson.fromJson(jsonString, Seller.class);
		sellerService.modifyBySellerId(seller);
		
		doGetSellerList(request, response);
	
	}
	
	/**
	 * 修改店铺状态
	 */
	public void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String json = request.getParameter("updateSeller");
		Gson gson = new Gson();
		Seller seller = gson.fromJson(json, Seller.class);
		sellerService.updateStatus(seller);
		
		doGetSellerList(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
