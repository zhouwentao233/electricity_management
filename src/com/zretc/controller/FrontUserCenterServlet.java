package com.zretc.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


import com.zretc.entity.User;
import com.zretc.service.UserService;
import com.zretc.service.impl.UserServiceImpl;

/**
 * Servlet implementation class UserCenterServlet
 */
@WebServlet("/front/FrontUserCenterServlet")
@MultipartConfig
public class FrontUserCenterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private UserService userService=new UserServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontUserCenterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 设置请求参数的编码格式
				request.setCharacterEncoding("UTF-8");
				// 接收请求中的参数op,请求操作标识
				String op = request.getParameter("op");
				if ("existsOfAccountName".equals(op)) {
					// 判断用户名是否存在
					doExistsOfAccountName(request,response);
				
				} 
				int id = 1;
				String  Account =request.getParameter("userAccount");
				String  userName =request.getParameter("userName");
				String  userPassword =request.getParameter("userPassword");
				
				Part part =request.getPart("userPicture");
				String  userPhone =request.getParameter("userPhone");
				if (part.getSize()!=0) {
					
					String header=part.getHeader("content-disposition");
					String facePicture=header.substring(header.indexOf("filename=")+10, header.length()-1);
					String fileSuffix=facePicture.substring(facePicture.lastIndexOf("."));
					LocalDateTime now=LocalDateTime.now();
					DateTimeFormatter ofPattern=DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS");
					String formatDate=ofPattern.format(now);
					int random=(int)(Math.random()*10000-1);
					StringBuffer fileName = new StringBuffer();
					fileName.append(formatDate).append(random);
					facePicture=fileName+fileSuffix;
					User user=new User(id, Account, userName, userPassword, facePicture, userPhone);
					int result = userService.updateUserByUserId(user);
					 if(result>0)
					    {
					    response.sendRedirect("index.html");
					    }
				}
				
				
	}

	private void doRegister(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
	}


	private void doExistsOfAccountName(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String userAccount = request.getParameter("userAccount");
		boolean existsOfUserAccount = userService.existsOfUserName(userAccount);
		// 响应数据
		response.getWriter().write(existsOfUserAccount+"");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
