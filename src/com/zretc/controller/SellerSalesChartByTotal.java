package com.zretc.controller;

import java.awt.Font;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.VerticalAlignment;

import com.zretc.entity.Sales;
import com.zretc.service.SalesService;
import com.zretc.service.impl.SalesServiceImpl;

/**
 * 销售额柱状图
 * Servlet implementation class SalesChartBySal
 */
@WebServlet("/back/SellerSalesChartByTotal")
public class SellerSalesChartByTotal extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private SalesService salesService = new SalesServiceImpl();
	
	public static JFreeChart createChart() {
		
		
//		从数据库获取数据
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		SalesService salesService = new SalesServiceImpl();
		List<Sales> salesList = salesService.getSalesList();
//		遍历
		for (Sales sales : salesList) {
			String saleDate = sales.getSalesDate();
			Integer salesCount = sales.getSalesCount();
			Float salesVolume = sales.getSalesPrice()*salesCount;
			String productName = sales.getProduct().getProductName();
			dataSet.addValue(salesVolume, saleDate, productName);
		}
		
		
//		创建柱形图
		JFreeChart chart = ChartFactory.createBarChart3D(
				"销售额统计表", "月份", "销售量（元）", dataSet,PlotOrientation.VERTICAL,true,false,false);
		chart.getTitle().setFont(new Font("隶书", Font.PLAIN, 25));//设置标题字体
		chart.getLegend().setItemFont(new Font("宋体", Font.PLAIN, 12));//图例类别字体
		chart.setBorderVisible(true);//设置边框是否显示
//		设置副标题
		TextTitle subTitle = new TextTitle("店铺销售额统计");
		subTitle.setVerticalAlignment(VerticalAlignment.BOTTOM);
		chart.addSubtitle(subTitle);
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setForegroundAlpha(0.8f);
		plot.setBackgroundAlpha(0.5f);
//		plot.setBackgroundImage(image);
		CategoryAxis categoryAxis = plot.getDomainAxis();
//		设置字体
		plot.getRangeAxis().setLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setTickLabelFont(new Font("宋体", Font.PLAIN, 12));
		categoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		ValueAxis valueAxis = plot.getRangeAxis();
		BarRenderer3D renderer = new BarRenderer3D();
		renderer.setItemMargin(0.32);
		plot.setRenderer(renderer);
		return chart;
		
	}

}
