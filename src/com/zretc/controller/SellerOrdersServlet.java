package com.zretc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.OrderDetail;
import com.zretc.entity.Orders;
import com.zretc.service.OrderDetailService;
import com.zretc.service.OrdersService;
import com.zretc.service.impl.OrderDetailServiceImpl;
import com.zretc.service.impl.OrdersServiceImpl;
import com.zretc.util.PageInfo;

/**
 * Servlet implementation class SellerOrdersServlet
 */
@WebServlet("/back/SellerOrdersServlet")
public class SellerOrdersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private OrdersService ordersService = new OrdersServiceImpl();
	private OrderDetailService detailService = new OrderDetailServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SellerOrdersServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 接收请求中的参数op，请求操作标识
		String op = request.getParameter("op");
		if ("getOrdersListByPage".equals(op)) {
			// 获取订单列表
			doGetOrdersListByPage(request,response);
		} else if ("updateOrderStatus".equals(op)) {
			// 修改订单状态
			doUpdataOrderStatus(request,response);
		} else if ("selectOrderDetail".equals(op)) {
			// 查看订单详情
			doSelectOrderDetail(request,response);
		} else if ("ordersProduct".equals(op)) {
			// 查看订单的商品详情
			doOrdersProduct(request,response);
		}
	}

	private void doOrdersProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String orderId = request.getParameter("orderId");
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		List<OrderDetail> orderDetailByorderId = ordersService.getOrderDetailByorderId(orderId, sellerId);
		// 利用第三方类库,将数据转换成JSON字符串
		Gson gson = new Gson();
		String jsonObject = gson.toJson(orderDetailByorderId);
		// 2.响应
		response.setContentType("application/json");
		response.getWriter().write(jsonObject);
		
	}

	private void doSelectOrderDetail(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String orderId = request.getParameter("orderId");
		List<Map<String, Object>> orderByorderId = ordersService.getOrderByorderId(orderId);
		// 利用第三方类库,将数据转换成JSON字符串
		Gson gson = new Gson();
		String jsonObject = gson.toJson(orderByorderId);
		// 2.响应
		response.setContentType("application/json");
		response.getWriter().write(jsonObject);

	}

	private void doUpdataOrderStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String orderId = request.getParameter("orderId");
		String jsonId = request.getParameter("orderDetailId");
		String replace = jsonId.replace("[","");
		String replace2 = replace.replace("]", "");
		
		String[] array = replace2.split(",");
		Orders orders = null;
		for (String string : array) {
			OrderDetail orderDetail = new OrderDetail();
			orders = new Orders();
			orders.setOrderId(orderId);
			orderDetail.setOrders(orders);
			Integer in = Integer.valueOf(string);
			orderDetail.setOrderDetailId(in);
			orderDetail.setOrderDetailStatus(2);
			ordersService.updateOrderByOrderStatus(orderDetail);
		}		
		
		List<Map<String, Object>> ordersProductByOrderId = detailService.getOrdersProductByOrderId(orderId);
		int count = ordersProductByOrderId.size();
		for (Map<String, Object> map : ordersProductByOrderId) {
			Integer orderDetailStatus = (Integer) map.get("orderDetailStatus");
			if (orderDetailStatus == 2) {
				count--;
			}			
		}
		if (count == 0) {
			orders.setOrderStatus(2);
			ordersService.updateOrderByOrderStatus(orders);
		}
		
	}

	private void doGetOrdersListByPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 1.调用模型层，获取数据
		String sellerId = request.getParameter("sellerId");
		String keyword = request.getParameter("keyword");
		String orderStatusKey = request.getParameter("orderStatus");
		String pageNum = request.getParameter("pageNum");
		String pageSize = request.getParameter("pageSize");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("sellerIdKey", sellerId);
		params.put("keyword", keyword);
		params.put("orderStatusKey", orderStatusKey);
		params.put("pageNum", pageNum);
		params.put("pageSize", pageSize);
		PageInfo<Map<String, Object>> pageInfo = ordersService.getPageOrderAll(params);
		// 利用第三方类库,将数据转换成JSON字符串
		Gson gson = new Gson();
		String jsonObject = gson.toJson(pageInfo);
		// 2.响应
		response.setContentType("application/json");
		response.getWriter().write(jsonObject);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
