package com.zretc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zretc.entity.Product;
import com.zretc.entity.ProductCollect;
import com.zretc.entity.Seller;
import com.zretc.entity.ShopsCollect;
import com.zretc.entity.User;
import com.zretc.service.ProductCollectService;
import com.zretc.service.ShopsCollectService;
import com.zretc.service.impl.ProductCollectServiceImpl;
import com.zretc.service.impl.ShopsCollectServiceImpl;

/**
 * @author wentao
 * 添加收藏
 * Servlet implementation class FrontCollectServlet
 */
@WebServlet("/front/collectServlet")
public class FrontCollectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ProductCollectService productCollectService = new ProductCollectServiceImpl();
	private ShopsCollectService shopsCollectService = new ShopsCollectServiceImpl();
    
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 判断操作类型
		String op = request.getParameter("op");
		if(op == null) {
			op = "";
		}
		switch (op) {
		case "addProduct":
			// 收藏商品
			saveProductCollect(request, response);
			break;
		case "addShop":
			// 收藏商品
			saveShopCollect(request, response);
			break;

		default:
			break;
		}
	}
	
	/**
	 * 收藏商品
	 * @param request
	 * @param response
	 */
	public void saveProductCollect(HttpServletRequest request,HttpServletResponse response) {
		// 用户id
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		// 商品id
		Integer productId = Integer.valueOf(request.getParameter("productId"));
		
		// 判断是否存在
		ProductCollect flag = productCollectService.getProductCollect(productId, userId);
		if(flag == null) {
			// 创建对象
			User user = new User();
			user.setUserId(userId);
			Product product = new Product();
			product.setProductId(productId);
			ProductCollect productCollect = new ProductCollect(null, user, product);
			
			// 保存
			productCollectService.insertProductCollect(productCollect);
		}
		
		
		
	}
	/**
	 * 收藏店铺
	 * @param request
	 * @param response
	 */
	public void saveShopCollect(HttpServletRequest request,HttpServletResponse response) {
		// 用户id
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		// 店铺id
		Integer sellerId = Integer.valueOf(request.getParameter("sellerId"));
		
		// 先判断存不存在
		ShopsCollect flag = shopsCollectService.getShopsCollect(userId, sellerId);
		if(flag == null) {
			// 创建对象
			User user = new User();
			user.setUserId(userId);
			Seller seller = new Seller();
			seller.setSellerId(sellerId);
			ShopsCollect shopsCollect = new ShopsCollect(null, seller, user);
			
			// 保存
			shopsCollectService.insertShopsCollect(shopsCollect);
		}
		
	}
}
