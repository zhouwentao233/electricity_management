package com.zretc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Orders;
import com.zretc.entity.Product;
import com.zretc.entity.Sales;
import com.zretc.service.OrderDetailService;
import com.zretc.service.OrdersService;
import com.zretc.service.SalesService;
import com.zretc.service.impl.OrderDetailServiceImpl;
import com.zretc.service.impl.OrdersServiceImpl;
import com.zretc.service.impl.SalesServiceImpl;
import com.zretc.util.PageInfo;

/**
 * Servlet implementation class FrontOrdersServlet
 */
@WebServlet("/front/OrdersServlet")
public class FrontOrdersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private OrdersService ordersService = new OrdersServiceImpl();
	private OrderDetailService orderDetailService = new OrderDetailServiceImpl();
	private SalesService salesService = new SalesServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontOrdersServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op = request.getParameter("op");
		if ("getOrdersListByuserId".equals(op)) {
			// 获取订单列表
			dogetOrdersListByuserId(request, response);
		} else if ("updataOrderStatus".equals(op)) {
			// 修改订单状态/确定收货
			doUpdateOrderStatus(request,response);
		}
	}
	private void doUpdateOrderStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 1.调用模型层，获取数据
		String userId = request.getParameter("userId");
		String orderId = request.getParameter("orderId");
		Integer orderStatus = Integer.valueOf(request.getParameter("orderStatus"));
		Orders orders = new Orders();
		orders.setOrderId(orderId);
		orders.setOrderStatus(orderStatus);
		Product product = null;
		// 获取订单列表
		ordersService.updateOrderByOrderStatus(orders);
		List<Map<String, Object>> ordersProductByOrderId = orderDetailService.getOrdersProductByOrderId(orderId);
		for (Map<String, Object> map : ordersProductByOrderId) {
			Integer productId = (Integer) map.get("productId");
			Integer salesCount = (Integer) map.get("orderDetailCount");
			Float salesPrice = (Float) map.get("orderDetailPrice");			
			// 日期
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
			String salesDate = format.format(date);		
			
			Sales sales = new Sales();
			sales.setOrder(orders);
			product = new Product();
			product.setProductId(productId);
			sales.setProduct(product);
			sales.setSalesCount(salesCount);
			sales.setSalesPrice(salesPrice);
			sales.setSalesDate(salesDate);
			salesService.insertSales(sales);
			
		}
		
		dogetOrdersListByuserId(request, response);
	}

	private void dogetOrdersListByuserId(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 1.调用模型层，获取数据
		String userId = request.getParameter("userId");
		String keyword = request.getParameter("keyword");
		String orderStatus = request.getParameter("orderStatus");
		String pageNum = request.getParameter("pageNum");
		String pageSize = request.getParameter("pageSize");
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", userId);
		params.put("keyword", keyword);
		params.put("orderStatus", orderStatus);
		params.put("pageNum", pageNum);
		params.put("pageSize", pageSize);
		PageInfo<Orders> pageInfo = ordersService.getOrderAllByUserId(params);	
		// 利用第三方类库,将数据转换成JSON字符串
		Gson gson = new Gson();
		String jsonObject = gson.toJson(pageInfo);
		// 2.响应
		response.setContentType("application/json");
		response.getWriter().write(jsonObject);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
