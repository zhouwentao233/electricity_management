package com.zretc.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.zretc.entity.Type;
import com.zretc.service.TypeService;
import com.zretc.service.impl.TypeServiceImpl;

import sun.net.www.content.text.plain;

/**
 * @author wentao
 * 前端分类显示
 * Servlet implementation class FrontShowTypeServlet
 */
@WebServlet("/front/typeshow")
public class FrontTypeShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private TypeService typeService = new TypeServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取操作类型
		String op = request.getParameter("op");
		if(op == null) {
			op = "";
		}
		switch (op) {
		case "levelOne":
			// 一级分类显示
			showLevelOneType(request, response);
			break;
		case "levelTwo":
			// 二级分类显示
			showLevelTwoType(request, response);
			break;

		default:
			break;
		}
	}
	
	public void showLevelOneType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 显示父类分类编号为0的分类
		List<Type> types = typeService.typeListByFatherId(0);
		// 转换为json
		Gson gson = new Gson();
		String json = gson.toJson(types);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	
	public void showLevelTwoType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 获取点击时的分类编号当做父类编号查询
		Integer typeFatherId = Integer.valueOf(request.getParameter("typeFatherId"));
		
		List<Type> types = typeService.typeListByFatherId(typeFatherId);
		// 转换为json
		Gson gson = new Gson();
		String json = gson.toJson(types);
		// 响应
		response.setContentType("application/json");
		response.getWriter().write(json);
	}
	


}
