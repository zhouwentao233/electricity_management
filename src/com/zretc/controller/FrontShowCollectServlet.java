package com.zretc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sun.javafx.collections.MappingChange.Map;
import com.zretc.dao.ProductCollectDao;
import com.zretc.dao.UserDao;
import com.zretc.dao.impl.ProductCollectDaoImpl;
import com.zretc.dao.impl.UserDaoImpl;
import com.zretc.entity.Address;
import com.zretc.entity.ProductCollect;
import com.zretc.entity.ShopsCollect;
import com.zretc.entity.User;
import com.zretc.service.AddressService;
import com.zretc.service.ProductCollectService;
import com.zretc.service.ShopsCollectService;
import com.zretc.service.impl.AddressServiceImpl;
import com.zretc.service.impl.ProductCollectServiceImpl;
import com.zretc.service.impl.ShopsCollectServiceImpl;
import com.zretc.util.PageInfo;

/**
 * Servlet implementation class FrontShowCollectServlet
 */
@WebServlet("/front/FrontShowCollectServlet")
public class FrontShowCollectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private ProductCollectService productCollectService=new ProductCollectServiceImpl();
       private ShopsCollectService shopsCollectService =new ShopsCollectServiceImpl();
       private AddressService addressService=new AddressServiceImpl();
       /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontShowCollectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op = request.getParameter("op");
		
		// 判断操作类型
		switch (op) {
		case "collectProduct":
			// 根据条件获取在售商品并排序
			getcollectProduct(request, response);
			break;
		case "collectShops":
			// 根据条件获取在售商品并排序
			getcollectShops(request, response);
			break;
			
		case "address":
			// 根据条件获取在售商品并排序
			getAddress(request, response);
			break;
			
		case "updateAddress":
			// 根据条件获取在售商品并排序
			updateAddress(request, response);
			break;
		case "deleteAddress":
			// 根据条件获取在售商品并排序
			deleteAddress(request, response);
			break;
		case "statusAddress":
			// 根据条件获取在售商品并排序
			statusAddress(request, response);
			break;	
		case "addAddress":
			// 根据条件获取在售商品并排序
			addAddress(request, response);
			break;		
		default:
			break;
	}

}

	private void addAddress(HttpServletRequest request, HttpServletResponse response) {
		Address address=new Address();
		address.setAddressStatus(0);
		
		Integer userId=Integer.valueOf(request.getParameter("userId"));
		User user=new User();
		user.setUserId(userId);
		address.setUser(user);
		String addressName=request.getParameter("addressName");
		String receiveName=request.getParameter("receiveName");
		String receivePhone=request.getParameter("receivePhone");
		address.setAddressName(addressName);
		address.setReceiveName(receiveName);
		address.setReceivePhone(receivePhone);
		addressService.insertAddress(address);
	}

	private void statusAddress(HttpServletRequest request, HttpServletResponse response) {
		Integer addressId=Integer.valueOf(request.getParameter("addressId"));
		Address address=addressService.findAddressByAddressId(addressId);
		address.setAddressStatus(1);
		 
		User user = address.getUser();
		Integer userId = user.getUserId();
		Address addressByUserId = addressService.getAddressByUserId(userId);
		if(addressByUserId!=null) {
			addressByUserId.setAddressStatus(0);
			addressService.updateAddressByAddressId(addressByUserId);
		}
		addressService.updateAddressByAddressId(address); 
	}

	private void deleteAddress(HttpServletRequest request, HttpServletResponse response) {
		Integer addressId=Integer.valueOf(request.getParameter("addressId"));
		addressService.deleteAddressByAddressId(addressId);
	}

	private void updateAddress(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Integer addressId =Integer.valueOf(request.getParameter("addressId"));
		String addressName=request.getParameter("addressName");
		String receiveName =request.getParameter("receiveName");
		String receivePhone=request.getParameter("receivePhone");
		Address address=addressService.findAddressByAddressId(addressId);//根据地址id更新
		address.setAddressId(addressId);
		address.setAddressName(addressName);
		address.setAddressStatus(0);
		address.setReceiveName(receiveName);
		address.setReceivePhone(receivePhone);
		int updateAddressByAddressId = addressService.updateAddressByAddressId(address);
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(updateAddressByAddressId));
	}

	private void getAddress(HttpServletRequest request, HttpServletResponse response) throws IOException {
		/*String pageNum = request.getParameter("pageNum");
		if("".equals(pageNum)) {
			pageNum = "10";
		}
		// 获取页码
		String pageSize = request.getParameter("pageSize");
		if("".equals(pageSize)) {
			pageSize = "1";
		};*/
		// 获取用户id
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		
		User user=new User();
		user.setUserId(userId);
		
		List<Address> findAllAddressByUserId = addressService.findAllAddressByUserId(user);
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(findAllAddressByUserId));
	}

	private void getcollectShops(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String pageNum = request.getParameter("pageNum");
		if("".equals(pageNum)) {
			pageNum = "10";
		}
		// 获取页码
		String pageSize = request.getParameter("pageSize");
		if("".equals(pageSize)) {
			pageSize = "1";
		}
		// 获取userId
		String userId = request.getParameter("userId");
		HashMap<String, String> condition = new HashMap<String, String>();
		condition.put("pageNum", pageNum);
		condition.put("pageSize", pageSize);
		condition.put("userId", userId);
		PageInfo<ShopsCollect> findShopsCollectByPage = shopsCollectService.findShopsCollectByPage(condition); 
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(findShopsCollectByPage.getData()));
	}

	private void getcollectProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String pageNum = request.getParameter("pageNum");
		if("".equals(pageNum)) {
			pageNum = "10";
		}
		// 获取页码
		String pageSize = request.getParameter("pageSize");
		if("".equals(pageSize)) {
			pageSize = "1";
		}
		
		// 获取userId
		String userId = request.getParameter("userId");
		
		HashMap<String, String> condition = new HashMap<String, String>();
		condition.put("pageNum", pageNum);
		condition.put("pageSize", pageSize);
		condition.put("userId", userId);
		PageInfo<ProductCollect> findProductCollectByPage = productCollectService.findProductCollectByPage(condition);
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(findProductCollectByPage.getData()));
	}
}